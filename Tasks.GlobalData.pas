unit Tasks.GlobalData;

interface

uses
  System.Classes,
  System.Generics.Collections,
  Xml.XMLIntf,
  pbInput,
  pbOutput,
  Common.AbstractClasses;

type
  TGlobalData = class(TAbstractData)
  strict private
    class var FInstance: TGlobalData;

    class function GetInstance: TGlobalData; static;
  strict private
    FConnectionString: string;

    procedure LoadSettings;

    constructor Create; reintroduce;

  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer)
      : Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;

    procedure intLoad(Node: IXMLNode); //override;
    procedure intSave(Node: IXMLNode); //override;
  public
    destructor Destroy; override;
    class destructor Destroy;

    class procedure Init;
    class function Instance: TGlobalData;

    property ConnectionString: string read FConnectionString;
  end;

implementation

uses
  System.SysUtils,
  Xml.XMLDoc,
  uXMLFunctions,
  Common.Interfaces,
  Utils.GlobalServices,
  //Utils.AirportInfo,
  //Utils.DataPresentation,
  Tasks.Settings;

{ TGlobalData }

constructor TGlobalData.Create;
begin
  inherited Create(nil);
  TDllPresentation.Init;

  TSpecificSettingsContainer.Instance;
  LoadSettings;
end;

destructor TGlobalData.Destroy;
var
  gs: IGlobalService;
begin
  if Supports(GlobalServices, IGlobalService, gs) then
    gs.ReleaseLocalInstance;
  inherited;
end;

class destructor TGlobalData.Destroy;
begin
  FInstance.Free;
  FInstance := nil;
end;

class function TGlobalData.GetInstance: TGlobalData;
begin
  if not Assigned(FInstance) then
    FInstance := TGlobalData.Create;
  Result := FInstance;
end;

class procedure TGlobalData.Init;
begin
  GetInstance;
end;

class function TGlobalData.Instance: TGlobalData;
begin
  Result:=GetInstance;
end;

procedure TGlobalData.intLoad(Node: IXMLNode);
begin
  inherited;
  FConnectionString := (GetNodeValueByName(Node.ChildNodes, 'ConnectionStr'));
  TSpecificSettingsContainer.Instance.Load(Node.ChildNodes.FindNode('SpecificSettings'));
end;

procedure TGlobalData.intSave(Node: IXMLNode);
begin
  inherited;
  Node.AddChild('ConnectionStr').Text := FConnectionString;
  TSpecificSettingsContainer.Instance.Save(Node.AddChild('SpecificSettings'));
end;

procedure TGlobalData.LoadSettings;
var
  Xml: IXMLDocument;
begin
  Xml := SafeLoadXMLDocument(ChangeFileExt(ParamStr(0), '.xml'));
  if Assigned(Xml) then
    Load(Xml.DocumentElement);
  Xml := NewXMLDocument;
  XML.NodeIndentStr:=#9;
  XML.Options:=XML.Options+[doNodeAutoIndent];
  Save(Xml.AddChild('root'));
  Xml.SaveToFile(ChangeFileExt(ParamStr(0), '.xml'));
end;

function TGlobalData.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer): Boolean;
begin
  Result := inherited;
end;

procedure TGlobalData.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;

end;

end.
