unit uMouseTransparentControls;

interface
uses
  System.Classes,
  Winapi.Windows,
  Winapi.Messages,
  Vcl.ExtCtrls;

type
  TMouseTransparentPanel = class(TPanel)
  private
    FMouseTransparent: Boolean;
    procedure SetMouseTransparent(const Value: Boolean);

    procedure WMNCHHitTest(var Message: TWMNCHitMessage); message WM_NCHITTEST;
  public
    property MouseTransparent: Boolean read FMouseTransparent write SetMouseTransparent;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('RIVC', [TMouseTransparentPanel]);
end;

{ TMouseTransparentPanel }

procedure TMouseTransparentPanel.SetMouseTransparent(const Value: Boolean);
begin
  FMouseTransparent := Value;
end;

procedure TMouseTransparentPanel.WMNCHHitTest(var Message: TWMNCHitMessage);
begin
  if FMouseTransparent then
    message.Result := HTTRANSPARENT
  else
    inherited;
end;

end.
