unit Tasks.Orchestra;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Generics.Collections,
  Vcl.ExtCtrls,
  pbInput,
  pbOutput,
  Common.Interfaces,
  Common.AbstractClasses,
  Tasks.Dictionaries,
  Tasks.SourceRequests,
  Tasks.Manager;

type
  // ������:
  // �������������� ���������� ��������� �������� ���������� ��
  // ���������� ���������� ������ �� ������� "��� ����� ���������� ������"
  // ���� ���-�� ������� - ���������� ������ � ����, �������� �� ������
  // ����������� �� ���������� ��������.
  TExchangeOrchestra = class(TAbstractData)
  strict private
    FTimer: TTimer;
    FLastDictionariesReaded: TDateTime;
    FLastLogClear: TDateTime;

    FSourceRequestsOperator: TSourceRequestsOperator;
    FTaskManager: TTaskManager;
    FTaskDictionaries: TTaskDictionaryContainer;

    procedure OnTimer(Sender: TObject);
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer)
      : Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    constructor Create(AOwner: TObject); override;
    destructor Destroy; override;
  end;

implementation

uses
  System.DateUtils,
  WinAPI.ActiveX,
  uThreadSafeLogger,
  Monitoring.Informer,
  Tasks.Settings;

const
  cTimerInterval = 1000;

  { TExchangeOrchestra }

constructor TExchangeOrchestra.Create(AOwner: TObject);
begin
  inherited;
  CoInitialize(nil);

  FTaskDictionaries := TTaskDictionaryContainer.Create;
  FSourceRequestsOperator := TSourceRequestsOperator.Create(Self);

  FTaskManager := TTaskManager.Create(Self);

  FTimer := TTimer.Create(nil);
  FTimer.Interval := cTimerInterval;
  FTimer.OnTimer := OnTimer;
  FTimer.Enabled := True;

  FLastDictionariesReaded := Now;
end;

destructor TExchangeOrchestra.Destroy;
begin
  FreeAndNil(FTimer);
  FreeAndNil(FTaskManager);
  FreeAndNil(FSourceRequestsOperator);
  FreeAndNil(FTaskDictionaries);
  CoUninitialize;
  inherited;
end;

function TExchangeOrchestra.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer): Boolean;
begin
  Result := inherited;
end;

procedure TExchangeOrchestra.OnTimer(Sender: TObject);
var
  i: integer;
  ReqList: TSourceRequestList;
  Req: TAbstractSourceRequest;
  iSecondsBetween: Int64;
begin
  try
    iSecondsBetween := SecondsBetween(FLastDictionariesReaded, Now);
    if iSecondsBetween > 10 then
      TMonitoringInformer.ImEnterInWork(20000);

    if (TSpecificSettingsContainer.Instance.ReloadDictionariesIntervalMin > 0) and
      (iSecondsBetween >= (TSpecificSettingsContainer.Instance.ReloadDictionariesIntervalMin * 60)) then
    begin
      if FTaskManager.TotalRequestCount <> 0 then
      begin
        LogFileX.Log('Wait for all task finished to reload settings');

        if (FLastDictionariesReaded <> 0) and
          (iSecondsBetween > (TSpecificSettingsContainer.Instance.ReloadDictionariesIntervalMin * 2 * 60)) then
        begin
          TMonitoringInformer.ImInException
            ('������� ������� �������� ���������� �������. ��������� ����������!!!', False);
          LogFileX.Log('������ �������� ������� - ������ ������� ����� �������!!! ��������� ����������!!!');
        end;

        FTaskManager.StartPendedTasks;
        Exit;
      end;
      LogFileX.Log('Before FTaskDictionaries.Reload');
      FTaskDictionaries.Reload;
      LogFileX.Log('After FTaskDictionaries.Reload');
      FLastDictionariesReaded := Now;
      Exit;
    end;

    ReqList := TSourceRequestList.Create(Self);
    try
      FSourceRequestsOperator.GetRequests(ReqList);
      i := 0;
      while ReqList.Count <> 0 do
      begin
        Req := ReqList.Extract(ReqList[i]);
        try
          FTaskManager.AddToWork(Req);
        except
          on e: EProgrammerNotFound do
          begin
            LogFileX.LogException(e);
            TMonitoringInformer.ImInException(e, True);
            Req.Free;
          end;
        end;
      end;
      FTaskManager.StartPendedTasks;
    finally
      ReqList.Free;
    end;
  except
    on e: Exception do
    begin
      TMonitoringInformer.ImInException(e, True);
      LogFileX.LogException(e);
    end;
  end;

  if MinutesBetween(FLastLogClear, Now) > 30 then
  begin
    LogFileX.DeleteOldFromArchive(IncDay(Now, -TSpecificSettingsContainer.Instance.LogDepthDays));
    FLastLogClear := Now;
  end;
end;

procedure TExchangeOrchestra.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;

end;

end.
