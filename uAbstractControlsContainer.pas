unit uAbstractControlsContainer;
/// generic container for visual components
/// usable as "replacement" for FMX.TTabControl
/// I think, you dont need it, because it is very specific

/// author: kami (c) 2017
/// you can use this code without any limitations.
/// The code provided "as is", wtihout warranty of any kind.

{ .$DEFINE SingletonContainer }
{$DEFINE FMX}

interface

uses
  System.Classes,
  {$IFDEF FMX}
  FMX.Types,
  FMX.Controls,
  FMX.Layouts,
  {$ELSE}
  Vcl.Controls,
  Vcl.ExtCtrls,
  {$ENDIF}
  System.Generics.Collections,
  uEnumerations;

type
  TControlClass = class of TControl;

  TUIInnerItemsContainer<T: record > = class({$IFDEF FMX}TLayout{$ELSE}TPanel{$ENDIF})
  private
    class var FRegisteredClasses: TDictionary<T, TControlClass>;
    {$IFDEF SingletonContainer}
    class var FInstance: TUIInnerItemsContainer<T>;
    {$ENDIF}
    function GetItemByType(UIType: T): TControl;
    function GetOrCreateItem(UIType: T): TControl;

    function IsControlClassRegistered(AControl: TControl; out UIType: T): Boolean; overload;
    function IsControlClassRegistered(AControl: TControl): Boolean; overload;

    function GetControlsCount: Integer;
    function GetVisibleItemType: T;
    procedure SetVisibleItemType(const Value: T);
  public
    class procedure RegisterControlClass(UIType: T; AClass: TControlClass);
    class procedure Fin;

    class function NewInstance: TObject; override;

  type
    TEnumerator = class(TEnumerator<TControl>)
    private
      FOwner: TUIInnerItemsContainer<T>;
      FCurrent: TControl;
      function GetCurrent: TControl;

      function FindFirst: Boolean;
      function FindNext: Boolean;
    protected
      function DoGetCurrent: TControl; override;
      function DoMoveNext: Boolean; override;
    public
      constructor Create(const AOwner: TUIInnerItemsContainer<T>);
      property Current: TControl read GetCurrent;
      function MoveNext: Boolean;
    end;

  function GetEnumerator: TEnumerator; reintroduce;

  property VisibleItemType: T read GetVisibleItemType write SetVisibleItemType;
  end;

implementation

uses
  System.SysUtils;

{ TUIInnerItemsContainer }

class procedure TUIInnerItemsContainer<T>.Fin;
begin
  FreeAndNil(FRegisteredClasses);
end;

function TUIInnerItemsContainer<T>.GetControlsCount: Integer;
begin
  {$IFDEF FMX}
  Result := ControlsCount;
  {$ELSE}
  Result := ControlCount;
  {$ENDIF}
end;

function TUIInnerItemsContainer<T>.GetEnumerator: TEnumerator;
begin
  Result := TEnumerator.Create(Self);
end;

function TUIInnerItemsContainer<T>.GetItemByType(UIType: T): TControl;
var
  tmpClass: TControlClass;
  i: Integer;
begin
  Result := nil;
  if not Assigned(FRegisteredClasses) then
    Exit;

  if not FRegisteredClasses.ContainsKey(UIType) then
    Exit;

  tmpClass := FRegisteredClasses[UIType];
  for i := 0 to GetControlsCount - 1 do
    begin
      if Controls[i] is tmpClass then
        begin
          Result := Controls[i];
          Break;
        end;
    end;
end;

function TUIInnerItemsContainer<T>.GetOrCreateItem(UIType: T): TControl;
var
  tmpClass: TControlClass;
begin
  Result := GetItemByType(UIType);
  if Assigned(Result) then
    Exit;

  if not FRegisteredClasses.ContainsKey(UIType) then
    Exit;

  tmpClass := FRegisteredClasses[UIType];
  Result := tmpClass.Create(Self);
  Result.Parent := Self;
  Result.Align := {$IFDEF FMX}TAlignLayout.Client{$ELSE}alClient{$ENDIF};
end;

function TUIInnerItemsContainer<T>.GetVisibleItemType: T;
var
  i: Integer;
  Item: TControl;
  tmp: T;
begin
  Result := TEnumeration<T>.FromOrdinal(TEnumeration<T>.MinValue);

  for i := 0 to GetControlsCount - 1 do
    begin
      Item := Controls[i];
      if Item.Visible then
        if IsControlClassRegistered(Item, tmp) then
          begin
            Result := tmp;
            Break;
          end;
    end;
  {
    for i := TEnumeration<T>.MinValue to TEnumeration<T>.MaxValue do
    begin
    Item := GetItemByType(TEnumeration<T>.FromOrdinal(i));
    if Assigned(Item) then
    if Item.Visible then
    begin
    Result := TEnumeration<T>.FromOrdinal(i);
    Break;
    end;
    end; }
end;

function TUIInnerItemsContainer<T>.IsControlClassRegistered(AControl: TControl): Boolean;
var
  tmp: T;
begin
  Result := IsControlClassRegistered(AControl, tmp);
end;

function TUIInnerItemsContainer<T>.IsControlClassRegistered(AControl: TControl; out UIType: T): Boolean;
var
  i: T;
begin
  Result := False;
  UIType := TEnumeration<T>.FromOrdinal(TEnumeration<T>.MinValue);
  if not Assigned(FRegisteredClasses) then
    Exit;

  for i in FRegisteredClasses.Keys do
    begin
      if AControl is FRegisteredClasses[i] then
        begin
          Result := True;
          UIType := i;
          Break;
        end;
    end;
end;

class function TUIInnerItemsContainer<T>.NewInstance: TObject;
begin
  {$IFDEF SingletonContainer}
  if FInstance = nil then
    FInstance := TUIInnerItemsContainer<T>(inherited NewInstance);
  Result := FInstance;
  {$ELSE}
  if not TEnumeration<T>.IsEnumeration then
    raise ENotSupportedException.Create('You cannot create TUIInnerItemsContainer with non-enum type');
  Result := inherited NewInstance;
  {$ENDIF}
end;

class procedure TUIInnerItemsContainer<T>.RegisterControlClass(UIType: T; AClass: TControlClass);
begin
  if not Assigned(FRegisteredClasses) then
    FRegisteredClasses := TDictionary<T, TControlClass>.Create;
  FRegisteredClasses.AddOrSetValue(UIType, AClass)
end;

procedure TUIInnerItemsContainer<T>.SetVisibleItemType(const Value: T);
var
  i: Integer;
  Item: TControl;
  tmp: T;
begin
  for i := 0 to GetControlsCount - 1 do
    begin
      Item := Controls[i];
      if IsControlClassRegistered(Item, tmp) then
        if TEnumeration<T>.ToOrdinal(tmp) <> TEnumeration<T>.ToOrdinal(Value) then
          Item.Visible := False;
    end;

  { for i := TEnumeration<T>.MinValue to TEnumeration<T>.MaxValue do
    if i <> TEnumeration<T>.ToOrdinal(Value) then
    begin
    Item := GetItemByType(TEnumeration<T>.FromOrdinal(i));
    if Assigned(Item) then
    Item.Visible := False;
    end; }
  Item := GetOrCreateItem(Value);
  if Assigned(Item) then
    begin
      Item.Visible := True;
      Item.BringToFront;
    end;
end;

{ TUIInnerItemsContainer<T>.TEnumerator }

constructor TUIInnerItemsContainer<T>.TEnumerator.Create(const AOwner: TUIInnerItemsContainer<T>);
begin
  inherited Create;
  FOwner := AOwner;
  FCurrent := nil;
end;

function TUIInnerItemsContainer<T>.TEnumerator.DoGetCurrent: TControl;
begin
  Result := GetCurrent;
end;

function TUIInnerItemsContainer<T>.TEnumerator.DoMoveNext: Boolean;
begin
  Result := MoveNext;
end;

function TUIInnerItemsContainer<T>.TEnumerator.FindFirst: Boolean;
var
  i: Integer;
begin
  Result := False;

  if not Assigned(FOwner.FRegisteredClasses) then
    Exit;

  for i := 0 to FOwner.GetControlsCount - 1 do
    if FOwner.IsControlClassRegistered(FOwner.Controls[i]) then
      begin
        FCurrent := FOwner.Controls[i];
        Result := True;
        Break;
      end;
end;

function TUIInnerItemsContainer<T>.TEnumerator.FindNext: Boolean;
var
  i: Integer;
  SearchFrom: Integer;
begin
  Result := False;
  SearchFrom := -1;
  if not Assigned(FOwner.FRegisteredClasses) then
    Exit;
  for i := 0 to FOwner.GetControlsCount - 1 do
    if FOwner.Controls[i] = FCurrent then
      begin
        SearchFrom := i;
        Break;
      end;

  for i := SearchFrom + 1 to FOwner.GetControlsCount - 1 do
    if FOwner.IsControlClassRegistered(FOwner.Controls[i]) then
      begin
        FCurrent := FOwner.Controls[i];
        Result := True;
        Break;
      end;
end;

function TUIInnerItemsContainer<T>.TEnumerator.GetCurrent: TControl;
begin
  Result := FCurrent;
end;

function TUIInnerItemsContainer<T>.TEnumerator.MoveNext: Boolean;
begin
  Result := False;
  if not Assigned(FCurrent) then
    Result := FindFirst
  else
    Result := FindNext;
end;

end.
