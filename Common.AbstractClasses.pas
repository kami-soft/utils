unit Common.AbstractClasses;

interface

uses
  System.Classes,
  Data.DB,
  Data.Win.ADODB,
  System.Generics.Defaults,
  System.Generics.Collections,
  Xml.XMLIntf,
  pbPublic,
  pbInput,
  pbOutput,
  Common.Interfaces,
  uAbstractProtoBufClasses;

type
  TCurrentActionEvent = procedure(Sender: TObject; const Action: string) of object;
  TSaveRequestEvent<T> = procedure(Sender: TObject; ItemToSave: T; var AllowSave: Boolean) of object;

  TAbstractData = class(TAbstractProtoBuf, IUnknown, IOwneredIntf, IChangeNotificator, IProtoBufStorage, IActionCallback)
  protected
    { IUnknown }
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
  strict private
    FChangeNotificator: IChangeNotificator;

    FOnCurrentActionCallback: TCurrentActionEvent;

    { IOwneredIntf }
    FOwner: TObject;
    function GetOwner: TObject;
    procedure SetOwner(const Value: TObject);
  strict private
    FTagString: string;
    FTagObject: TObject;
    FTag: Integer;
    FTagDouble: Double;
  protected
    function GetWriteSPName: string; virtual;

    procedure FillWriteParams(Parameters: TParameters); virtual;
    procedure PostProcessWrite(SP: TADOStoredProc); virtual;
  strict protected
    procedure BeforeLoad; override;
    procedure AfterLoad; override;

    procedure DoReadFromDS(DS: TDataSet); virtual;
    procedure DoReadInternals(Conn: TADOConnection); virtual;

    procedure intOnCurrentActionCallback(Sender: TObject; const Action: string);
    procedure DoActionCallback(const Action: string);
  public
    constructor Create(AOwner: TObject); reintroduce; virtual;
    destructor Destroy; override;

    procedure BeforeDestruction; override;

    class procedure RegisterSelf; virtual;

    function IsEqual(AData: TAbstractData): Boolean;

    { IDataBaseItemReadWrite }
    procedure ReadFromDS(DS: TDataSet; ConnForReadInternals: TADOConnection = nil);
    procedure WriteToDB(SP: TADOStoredProc);
    procedure TerminateReading; virtual;

    procedure LoadFromBufAsField(ProtoBuf: TProtoBufInput; FieldNumber: Integer);
    procedure SaveToBufAsField(ProtoBuf: TProtoBufOutput; FieldNumber: Integer);

    property Owner: TObject read GetOwner write SetOwner;
    property Tag: Integer read FTag write FTag;
    property TagObject: TObject read FTagObject write FTagObject;
    property TagString: string read FTagString write FTagString;
    property TagDouble: Double read FTagDouble write FTagDouble;

    property OnCurrentActionCallback: TCurrentActionEvent read FOnCurrentActionCallback write FOnCurrentActionCallback;

    property ChangeNotificator: IChangeNotificator read FChangeNotificator implements IChangeNotificator;
  end;

  TAbstractDataClass = class of TAbstractData;

  TAbstractList<T> = class(TList<T>)
  public
    procedure Assign(Source: TAbstractList<T>); virtual;

    procedure LoadFromStream(Stream: TStream);
    procedure SaveToStream(Stream: TStream);

    procedure LoadFromBuf(ProtoBuf: TProtoBufInput); virtual; abstract;
    procedure SaveToBuf(ProtoBuf: TProtoBufOutput); virtual; abstract;
  end;

  TAbstractDataList<T: TAbstractData, constructor> = class(TProtoBufList<T>, IUnknown, IOwneredIntf, IChangeNotificator, IActionCallback)
  protected
    { IUnknown }
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
  strict private
  type
    TAbstractDataItemContainer = class(TAbstractProtoBuf)
    strict private
      FItem: TAbstractData;
      FItemClassName: string;
    private
      FOwner: TAbstractDataList<T>;
    strict protected
      function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: Integer; WireType: Integer): Boolean; override;
      procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
    public
      property ItemClassName: string read FItemClassName write FItemClassName;
      property Item: TAbstractData read FItem write FItem;
    end;
  strict private
    FChangeNotificator: IChangeNotificator;

    FOnCurrentActionCallback: TCurrentActionEvent;
    FOnSaveRequest: TSaveRequestEvent<T>;

    FReadingTerminated: Boolean;

    procedure ChangeItemNotification(Sender: TObject; Notification: TChangeNotificationStructure);
  strict private
    { IOwneredIntf }
    FOwner: TObject;
    function GetOwner: TObject;
    procedure SetOwner(const Value: TObject);
  protected
    procedure BeforeAppendFromDS; virtual;
    procedure AfterAppendFromDS; virtual;

    function CreateItemByClassName(const AClassName: string): T; virtual;

    procedure intOnCurrentActionCallback(Sender: TObject; const Action: string);
    procedure DoActionCallback(const Action: string);
  protected
    procedure Notify(const Value: T; Action: TCollectionNotification); override;
    function DoSaveRequest(Item: T): Boolean; virtual;
  public
    constructor Create(AOwner: TObject; AOwnsObjects: Boolean = True); reintroduce; virtual;
    destructor Destroy; override;

    procedure BeforeDestruction; override;

    function IsEqual(AData: TAbstractDataList<T>): Boolean;

    procedure Assign(Source: TAbstractDataList<T>); virtual;

    function AddFromBuf(ProtoBuf: TProtoBufInput; FieldNum: Integer): Boolean; override;
    procedure SaveToBuf(ProtoBuf: TProtoBufOutput; FieldNum: Integer); override;

    procedure AppendFromDS(DS: TDataSet; ConnForReadInternals: TADOConnection = nil; DataClass: TAbstractDataClass = nil); virtual;
    procedure ReadFromDS(DS: TDataSet; ConnForReadInternals: TADOConnection = nil; DataClass: TAbstractDataClass = nil);
    procedure WriteToDB(SP: TADOStoredProc);
    procedure TerminateReading;

    property ChangeNotificator: IChangeNotificator read FChangeNotificator implements IChangeNotificator;

    property Owner: TObject read GetOwner write SetOwner;
    property Terminated: Boolean read FReadingTerminated;

    property OnCurrentActionCallback: TCurrentActionEvent read FOnCurrentActionCallback write FOnCurrentActionCallback;
    property OnSaveRequest: TSaveRequestEvent<T> read FOnSaveRequest write FOnSaveRequest;
  end;

  TAbstractDataClassFactory = class(TList<TAbstractDataClass>)
  strict private
    class var FCurrent: TAbstractDataClassFactory;
  strict private
    FDict: TDictionary<string, TAbstractDataClass>;
    constructor Create;
  private
    class procedure Fin;
  strict protected
    procedure Notify(const Item: TAbstractDataClass; Action: TCollectionNotification); override;
  public
    destructor Destroy; override;

    class procedure RegisterAbstractDataClass(AData: TAbstractDataClass);
    class function Current: TAbstractDataClassFactory;

    function GetClassByName(const AClassName: string): TAbstractDataClass;
  end;

  TOwnerFinder<T: class> = class(TObject)
  public
    class function FindOwnerOf(ChildObj: TObject): T;
  end;

const
  ItemClassNameAttr = 'DataType';

type
  TAbstractRefreshThread = class(TThread)
  strict private
    FOnCurrentActionCallback: TCurrentActionEvent;
  strict protected
    procedure DoCallback(const Action: string); virtual;
  strict protected
    FExitDescription: string;

    procedure ThreadSafeDoCallback(const Action: string);
    procedure TerminatedSet; override;
  public
    constructor Create; virtual;
    destructor Destroy; override;

    property ReturnValue;  //-2 = common error; -1 = Interrupted by user
    property ExitDescription: string read FExitDescription;
    property OnCurrentActionCallback: TCurrentActionEvent read FOnCurrentActionCallback write FOnCurrentActionCallback;
  end;

  TRefreshThreadClass = class of TAbstractRefreshThread;

implementation

uses
  System.SysUtils,
  uXMLFunctions,
  Common.Notificator;

{ TAbstractData }

procedure TAbstractData.AfterLoad;
begin
  inherited;
  ChangeNotificator.UnLock;
end;

procedure TAbstractData.BeforeDestruction;
begin
  FChangeNotificator.DoNotify(TChangeNotificationStructure.Create(Self, caDelete, ''));
  inherited;
end;

procedure TAbstractData.BeforeLoad;
begin
  ChangeNotificator.Lock;
  inherited;
end;

constructor TAbstractData.Create(AOwner: TObject);
begin
  inherited Create;
  FOwner := AOwner;
  FChangeNotificator := TChangeNotificator.Create(Self);
end;

destructor TAbstractData.Destroy;
begin
  inherited;
end;

procedure TAbstractData.FillWriteParams(Parameters: TParameters);
begin

end;

function TAbstractData.GetOwner: TObject;
begin
  Result := FOwner;
end;

function TAbstractData.GetWriteSPName: string;
begin
  Result := '';
end;

procedure TAbstractData.intOnCurrentActionCallback(Sender: TObject; const Action: string);
var
  intf: IActionCallback;
begin
  if Assigned(FOnCurrentActionCallback) then
    FOnCurrentActionCallback(Sender, Action);

  if Assigned(FOwner) then
    if Supports(FOwner, IActionCallback, intf) then
      intf.intOnCurrentActionCallback(Sender, Action);
end;

function TAbstractData.IsEqual(AData: TAbstractData): Boolean;
var
  Stream1, Stream2: TMemoryStream;
begin
  Result := False;

  if Self.ClassType <> AData.ClassType then
    Exit;

  Stream1 := TMemoryStream.Create;
  try
    SaveToStream(Stream1);

    Stream2 := TMemoryStream.Create;
    try
      AData.SaveToStream(Stream2);

      if Stream1.Size <> Stream2.Size then
        Exit;
      Result := CompareMem(Stream1.Memory, Stream2.Memory, Stream1.Size);
    finally
      Stream2.Free;
    end;
  finally
    Stream1.Free;
  end;
end;

procedure TAbstractData.LoadFromBufAsField(ProtoBuf: TProtoBufInput; FieldNumber: Integer);
var
  tmp: TProtoBufInput;
begin
  tmp := ProtoBuf.ReadSubProtoBufInput;
  try
    LoadFromBuf(tmp);
  finally
    tmp.Free;
  end;
end;

procedure TAbstractData.PostProcessWrite(SP: TADOStoredProc);
begin

end;

function TAbstractData.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := S_OK
  else
    Result := E_NOINTERFACE;
end;

procedure TAbstractData.ReadFromDS(DS: TDataSet; ConnForReadInternals: TADOConnection);
begin
  BeforeLoad;
  try
    DoReadFromDS(DS);
    if Assigned(ConnForReadInternals) then
      DoReadInternals(ConnForReadInternals);
  finally
    AfterLoad;
  end;
end;

class procedure TAbstractData.RegisterSelf;
begin
  TAbstractDataClassFactory.RegisterAbstractDataClass(Self);
end;

procedure TAbstractData.DoActionCallback(const Action: string);
begin
  intOnCurrentActionCallback(Self, Action);
end;

procedure TAbstractData.DoReadFromDS(DS: TDataSet);
begin

end;

procedure TAbstractData.DoReadInternals(Conn: TADOConnection);
begin

end;

procedure TAbstractData.SaveToBufAsField(ProtoBuf: TProtoBufOutput; FieldNumber: Integer);
var
  tmp: TProtoBufOutput;
begin
  tmp := TProtoBufOutput.Create;
  try
    SaveToBuf(tmp);
    ProtoBuf.writeMessage(FieldNumber, tmp);
  finally
    tmp.Free;
  end;
end;

procedure TAbstractData.SetOwner(const Value: TObject);
begin
  FOwner := Value;
end;

procedure TAbstractData.TerminateReading;
begin

end;

procedure TAbstractData.WriteToDB(SP: TADOStoredProc);
var
  s: string;
begin
  s := GetWriteSPName;
  if s = '' then
    raise ENotImplemented.Create('Stored proc name not declared');
  if SP.ProcedureName <> s then
  begin
    SP.ProcedureName := s;
    SP.Parameters.Refresh;
  end;
  FillWriteParams(SP.Parameters);
  SP.Open;
  try
    PostProcessWrite(SP);
  finally
    SP.Close;
  end;
end;

function TAbstractData._AddRef: Integer;
begin
  Result := -1;
end;

function TAbstractData._Release: Integer;
begin
  Result := -1;
end;

{ TAbstractDataList<T> }

function TAbstractDataList<T>.AddFromBuf(ProtoBuf: TProtoBufInput; FieldNum: Integer): Boolean;
var
  tmpBuf: TProtoBufInput;
  tmpContainer: TAbstractDataItemContainer;
begin
  if ProtoBuf.LastTag <> makeTag(FieldNum, WIRETYPE_LENGTH_DELIMITED) then
  begin
    Result := False;
    Exit;
  end;
  tmpBuf := ProtoBuf.ReadSubProtoBufInput;
  try
    tmpContainer := TAbstractDataItemContainer.Create;
    try
      tmpContainer.FOwner := Self;
      tmpContainer.LoadFromBuf(tmpBuf);
      if not Assigned(tmpContainer.Item) then
        raise EClassNotFound.Create('Class named ' + tmpContainer.ItemClassName + ' not registered!')
      else
      begin
        Add(T(tmpContainer.Item));
        tmpContainer.Item := nil;
      end;
      tmpContainer.Item.Free;
    finally
      tmpContainer.Free;
    end;
  finally
    tmpBuf.Free;
  end;
  Result := True;
end;

procedure TAbstractDataList<T>.AfterAppendFromDS;
begin
  FReadingTerminated := False;
end;

procedure TAbstractDataList<T>.AppendFromDS(DS: TDataSet; ConnForReadInternals: TADOConnection; DataClass: TAbstractDataClass);
var
  Item: T;
begin
  if Assigned(DataClass) then
    if not DataClass.InheritsFrom(T) and (DataClass.ClassName <> T.ClassName) then
      raise EConvertError.Create('Cant cast data class');
  if not Assigned(DataClass) then
    DataClass := T;

  BeforeAppendFromDS;
  try
    while not DS.Eof do
    begin
      Item := T(DataClass.Create(Self));
      try
        Item.ReadFromDS(DS, ConnForReadInternals);
        Add(Item);
        Item := nil;
      finally
        Item.Free;
      end;
      DS.Next;
      if FReadingTerminated then
        Break;
    end;
  finally
    AfterAppendFromDS;
  end;
end;

procedure TAbstractDataList<T>.Assign(Source: TAbstractDataList<T>);
var
  pbInput: TProtoBufInput;
  pbOutput: TProtoBufOutput;
  Stream: TStream;
  Tag: Integer;
  FieldNumber: Integer;
begin
  Clear;
  Stream := TMemoryStream.Create;
  try
    pbOutput := TProtoBufOutput.Create;
    try
      Source.SaveToBuf(pbOutput, 1);
      pbOutput.SaveToStream(Stream);
    finally
      pbOutput.Free;
    end;

    Stream.Seek(0, soBeginning);

    pbInput := TProtoBufInput.Create;
    try
      pbInput.LoadFromStream(Stream);
      Stream.Free;
      Stream := nil;

      Tag := pbInput.readTag;
      while Tag <> 0 do
      begin
        FieldNumber := getTagFieldNumber(Tag);
        if not Self.AddFromBuf(pbInput, FieldNumber) then
          pbInput.skipField(Tag);
        Tag := pbInput.readTag;
      end;
    finally
      pbInput.Free;
    end;
  finally
    Stream.Free;
  end;
end;

procedure TAbstractDataList<T>.BeforeAppendFromDS;
begin
  FReadingTerminated := False;
end;

procedure TAbstractDataList<T>.BeforeDestruction;
begin
  FChangeNotificator.DoNotify(TChangeNotificationStructure.Create(Self, caDelete, ''));
  inherited;
end;

procedure TAbstractDataList<T>.ChangeItemNotification(Sender: TObject; Notification: TChangeNotificationStructure);
begin
  if Notification.NotifyObject is T then
  begin
    FChangeNotificator.DoNotify(Notification);
    case Notification.Action of
      caAdd:
        ;
      caChange:
        ;
      caDelete:
        begin
          Extract(T(Notification.NotifyObject));
        end;
    end;
  end;
end;

constructor TAbstractDataList<T>.Create(AOwner: TObject; AOwnsObjects: Boolean);
begin
  inherited Create(AOwnsObjects);
  FOwner := AOwner;
  FChangeNotificator := TChangeNotificator.Create(Self);
end;

destructor TAbstractDataList<T>.Destroy;
begin
  inherited;
end;

procedure TAbstractDataList<T>.DoActionCallback(const Action: string);
begin
  intOnCurrentActionCallback(Self, Action);
end;

function TAbstractDataList<T>.DoSaveRequest(Item: T): Boolean;
begin
  Result := Assigned(TAbstractDataClassFactory.Current.GetClassByName(Item.ClassName));
  if Assigned(FOnSaveRequest) then
    FOnSaveRequest(Self, Item, Result);
end;

function TAbstractDataList<T>.GetOwner: TObject;
begin
  Result := FOwner;
end;

procedure TAbstractDataList<T>.intOnCurrentActionCallback(Sender: TObject; const Action: string);
var
  intf: IActionCallback;
begin
  if Assigned(FOnCurrentActionCallback) then
    FOnCurrentActionCallback(Sender, Action);

  if Assigned(FOwner) then
    if Supports(FOwner, IActionCallback, intf) then
      intf.intOnCurrentActionCallback(Sender, Action);
end;

function TAbstractDataList<T>.IsEqual(AData: TAbstractDataList<T>): Boolean;
var
  Stream1, Stream2: TMemoryStream;
  pb: TProtoBufOutput;
begin
  Result := False;

  if Self.ClassType <> AData.ClassType then
    Exit;

  pb := TProtoBufOutput.Create;
  try
    Stream1 := TMemoryStream.Create;
    try
      SaveToBuf(pb, 1);
      pb.SaveToStream(Stream1);
      pb.Clear;

      Stream2 := TMemoryStream.Create;
      try
        AData.SaveToBuf(pb, 1);
        pb.SaveToStream(Stream2);
        pb.Clear;

        if Stream1.Size <> Stream2.Size then
          Exit;
        Result := CompareMem(Stream1.Memory, Stream2.Memory, Stream1.Size);
      finally
        Stream2.Free;
      end;
    finally
      Stream1.Free;
    end;
  finally
    pb.Free;
  end;
end;

function TAbstractDataList<T>.CreateItemByClassName(const AClassName: string): T;
var
  AClass: TAbstractDataClass;
begin
  Result := nil;
  AClass := TAbstractDataClassFactory.Current.GetClassByName(AClassName);
  if Assigned(AClass) then
    if AClass.InheritsFrom(T) or (AClass.ClassName = T.ClassName) then
      Result := T(AClass.Create(Self));
  if not Assigned(Result) then
    raise EClassNotFound.Create('Class named ' + AClassName + ' not registered!');
end;

procedure TAbstractDataList<T>.Notify(const Value: T; Action: TCollectionNotification);
var
  intfOwnered: IOwneredIntf;
  intfChangeNotificator: IChangeNotificator;
begin
  case Action of
    cnAdded:
      begin
        if OwnsObjects then
        begin
          if Supports(Value, IOwneredIntf, intfOwnered) then
            intfOwnered.Owner := Self;
        end;
        if Supports(Value, IChangeNotificator, intfChangeNotificator) then
          intfChangeNotificator.RegisterNotification(ChangeItemNotification);

        FChangeNotificator.DoNotify(TChangeNotificationStructure.Create(Value, caAdd, ''));
      end;
    cnRemoved:
      begin
        if Supports(Value, IChangeNotificator, intfChangeNotificator) then
          intfChangeNotificator.UnregisterNotification(ChangeItemNotification);
        FChangeNotificator.DoNotify(TChangeNotificationStructure.Create(Value, caDelete, ''));
      end;
    cnExtracted:
      begin
        if Supports(Value, IChangeNotificator, intfChangeNotificator) then
          intfChangeNotificator.UnregisterNotification(ChangeItemNotification);
        FChangeNotificator.DoNotify(TChangeNotificationStructure.Create(Value, caDelete, ''));
      end;
  end;
  inherited;
end;

function TAbstractDataList<T>.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := 0
  else
    Result := E_NOINTERFACE;
end;

procedure TAbstractDataList<T>.ReadFromDS(DS: TDataSet; ConnForReadInternals: TADOConnection; DataClass: TAbstractDataClass);
begin
  Clear;
  DS.DisableControls;
  try
    AppendFromDS(DS, ConnForReadInternals, DataClass);
  finally
    DS.EnableControls;
  end;
end;

procedure TAbstractDataList<T>.SaveToBuf(ProtoBuf: TProtoBufOutput; FieldNum: Integer);
var
  tmpBuf: TProtoBufOutput;
  tmpContainer: TAbstractDataItemContainer;
  i: Integer;
  AllowSave: Boolean;
begin
  tmpContainer := TAbstractDataItemContainer.Create;
  try
    tmpBuf := TProtoBufOutput.Create;
    try
      for i := 0 to Count - 1 do
      begin
        tmpBuf.Clear;
        try
          AllowSave := DoSaveRequest(Items[i]);
          if AllowSave then
          begin
            tmpContainer.Item := Items[i];
            tmpContainer.SaveToBuf(tmpBuf);
            ProtoBuf.writeMessage(FieldNum, tmpBuf);
          end;
        finally
          tmpContainer.Item := nil;
        end;
      end;
    finally
      tmpBuf.Free;
    end;
  finally
    tmpContainer.Free;
  end;
end;

procedure TAbstractDataList<T>.SetOwner(const Value: TObject);
begin
  FOwner := Value;
end;

procedure TAbstractDataList<T>.TerminateReading;
var
  i: Integer;
begin
  FReadingTerminated := True;
  for i := 0 to Count - 1 do
    Items[i].TerminateReading;
end;

procedure TAbstractDataList<T>.WriteToDB(SP: TADOStoredProc);
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    Items[i].WriteToDB(SP);
end;

function TAbstractDataList<T>._AddRef: Integer;
begin
  Result := -1; // AtomicIncrement(FRefCount);
end;

function TAbstractDataList<T>._Release: Integer;
begin
  Result := -1; // AtomicDecrement(FRefCount);
end;

{ TAbstractList<T> }

procedure TAbstractList<T>.Assign(Source: TAbstractList<T>);
var
  { Xml: IXMLDocument;
    Node: IXMLNode; }
  Stream: TStream;
begin
  { Xml := NewXMLDocument;
    Node := Xml.AddChild('root');
    Source.Save(Node);
    Load(Node); }

  Stream := TMemoryStream.Create;
  try
    Source.SaveToStream(Stream);
    Stream.Seek(0, soBeginning);
    LoadFromStream(Stream);
  finally
    Stream.Free;
  end;
end;

procedure TAbstractList<T>.LoadFromStream(Stream: TStream);
var
  pb: TProtoBufInput;
  tmpStream: TStream;
begin
  pb := TProtoBufInput.Create;
  try
    tmpStream := TMemoryStream.Create;
    try
      tmpStream.CopyFrom(Stream, Stream.Size - Stream.Position);
      tmpStream.Seek(0, soBeginning);
      pb.LoadFromStream(tmpStream);
    finally
      tmpStream.Free;
    end;
    LoadFromBuf(pb);
  finally
    pb.Free;
  end;
end;

procedure TAbstractList<T>.SaveToStream(Stream: TStream);
var
  pb: TProtoBufOutput;
begin
  pb := TProtoBufOutput.Create;
  try
    SaveToBuf(pb);
    pb.SaveToStream(Stream);
  finally
    pb.Free;
  end;
end;

{ TAbstractDataClassFactory }

constructor TAbstractDataClassFactory.Create;
begin
  inherited Create;
  FDict := TDictionary<string, TAbstractDataClass>.Create;
end;

class function TAbstractDataClassFactory.Current: TAbstractDataClassFactory;
begin
  if not Assigned(FCurrent) then
    FCurrent := TAbstractDataClassFactory.Create;
  Result := FCurrent;
end;

destructor TAbstractDataClassFactory.Destroy;
begin
  FreeAndNil(FDict);
  inherited;
end;

class procedure TAbstractDataClassFactory.Fin;
begin
  FreeAndNil(FCurrent);
end;

function TAbstractDataClassFactory.GetClassByName(const AClassName: string): TAbstractDataClass;
begin
  Result := FDict[AClassName];
end;

procedure TAbstractDataClassFactory.Notify(const Item: TAbstractDataClass; Action: TCollectionNotification);
begin
  if Assigned(FDict) then
    case Action of
      cnAdded:
        FDict.Add(Item.ClassName, Item);
      cnRemoved, cnExtracted:
        { FDict.ExtractPair(Item.ClassName) };
    end;
  inherited;
end;

class procedure TAbstractDataClassFactory.RegisterAbstractDataClass(AData: TAbstractDataClass);
var
  s: string;
begin
  s := AData.ClassName;
  if Current.FDict.ContainsKey(s) then
    raise EInvalidInsert.Create('Class named ' + s + ' already in list');

  Current.Add(AData);
end;

{ TAbstractDataList<T>.TAbstractDataItemContainer }

function TAbstractDataList<T>.TAbstractDataItemContainer.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: Integer): Boolean;
var
  tmp: TProtoBufInput;
  AClass: TAbstractDataClass;
begin
  Result := inherited;
  if Result then
    Exit;
  case FieldNumber of
    1:
      begin
        FItemClassName := ProtoBuf.readString;
        FItem := FOwner.CreateItemByClassName(FItemClassName);
      end;
    2:
      if Assigned(FItem) then
      begin
        tmp := ProtoBuf.ReadSubProtoBufInput;
        try
          FItem.LoadFromBuf(tmp);
        finally
          tmp.Free;
        end;
      end;
  end;
  Result := (FieldNumber = 1) or (FieldNumber = 2);
end;

procedure TAbstractDataList<T>.TAbstractDataItemContainer.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
var
  tmp: TProtoBufOutput;
begin
  ProtoBuf.writeString(1, Item.ClassName);
  tmp := TProtoBufOutput.Create;
  try
    Item.SaveToBuf(tmp);
    ProtoBuf.writeMessage(2, tmp);
  finally
    tmp.Free;
  end;
end;

{ TOwnerFinder<T> }

class function TOwnerFinder<T>.FindOwnerOf(ChildObj: TObject): T;
var
  intf: IOwneredIntf;
  tmpOwner: TObject;
begin
  Result := nil;

  if not Supports(ChildObj, IOwneredIntf, intf) then
    Exit;

  tmpOwner := ChildObj;
  while Assigned(tmpOwner) do
  begin
    if tmpOwner is T then
    begin
      Result := T(tmpOwner);
      Break;
    end;
    if Supports(tmpOwner, IOwneredIntf, intf) then
      tmpOwner := intf.Owner
    else
      tmpOwner := nil;
  end;
end;

{ TAbstractRefreshThread }

constructor TAbstractRefreshThread.Create;
begin
  inherited Create(True);
  //FreeOnTerminate := True;
end;

destructor TAbstractRefreshThread.Destroy;
begin
  TThread.RemoveQueuedEvents(Self);
  inherited;
end;

procedure TAbstractRefreshThread.DoCallback(const Action: string);
begin
  if Assigned(FOnCurrentActionCallback) then
    FOnCurrentActionCallback(Self, Action);
end;

{ function TAbstractRefreshThread.GetNeedExplicitDestroy: Boolean;
  begin
  Result := False;
  end; }

procedure TAbstractRefreshThread.TerminatedSet;
begin
  inherited;
  Self.ReturnValue := -1;
end;

procedure TAbstractRefreshThread.ThreadSafeDoCallback(const Action: string);
begin
  Queue(
      procedure
    begin
      DoCallback(Action);
    end);
end;

initialization

finalization

TAbstractDataClassFactory.Fin;

end.
