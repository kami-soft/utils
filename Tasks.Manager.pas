unit Tasks.Manager;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Generics.Collections,
  pbInput,
  pbOutput,
  Common.Interfaces,
  Common.AbstractClasses,
  MonitorWaitStackFix,
  Tasks.SourceRequests,
  OtlTaskControl,
  OtlParallel;

type
  TAbstractTask = class(TAbstractData)
  strict private
    FSourceRequest: TAbstractSourceRequest;
    FOnFinished: TNotifyEvent;

    function TaskConfig: IOmniTaskConfig;

    procedure OnTerminated(const task: IOmniTaskControl);

    procedure Work;
    procedure DoTaskFinished;
  protected
    procedure SourceRequestNotificator(Sender: TObject; Notification: TChangeNotificationStructure); virtual;
    procedure SetRequest(ARequest: TAbstractSourceRequest);
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer)
      : Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;

    procedure DoWork; virtual; abstract;

    procedure Init; virtual;
    procedure Fin; virtual;
  public
    class function GetMaxRetryCount: integer; virtual; abstract;
    class function GetTryIntervalMs: integer; virtual; abstract;
  public
    destructor Destroy; override;
    procedure DirectSetRequest(ASourceRequest: TAbstractSourceRequest);

    class function CanProcessRequest(SourceRequest: TAbstractSourceRequest): Boolean; virtual; abstract;

    class function ProcessRequest(AOwner: TObject; SourceRequest: TAbstractSourceRequest): TAbstractTask;

    function SourceRequest<T: TAbstractSourceRequest>: T;

    property OnFinished: TNotifyEvent read FOnFinished write FOnFinished;
  end;

  TTaskClass = class of TAbstractTask;

  TTaskManager = class(TAbstractData)
  strict private
    FAllRequests: TSourceRequestList;

    procedure OnTaskFinished(Sender: TObject);
    function CanStartTask(SourceRequest: TAbstractSourceRequest): Boolean;
    procedure CreateAndStartTask(SourceRequest: TAbstractSourceRequest);

    class function GetTaskClassForRequest(SourceRequest: TAbstractSourceRequest): TTaskClass;
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer)
      : Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    class function CanProcessRequest(SourceRequest: TAbstractSourceRequest): Boolean;

    constructor Create(AOwner: TObject); override;
    destructor Destroy; override;

    procedure AddToWork(SourceRequest: TAbstractSourceRequest);
    procedure StartPendedTasks;

    function TotalRequestCount: integer;
  end;

implementation

uses
  System.DateUtils,
  Winapi.Windows,
  System.SyncObjs,
  Winapi.ActiveX,
  Vcl.Forms,
  uThreadSafeLogger,
  Monitoring.Informer,
  Common.Functions,
  Utils.Conversation;

{ TAbstractTask }

destructor TAbstractTask.Destroy;
begin
  if Assigned(FSourceRequest) then
    FSourceRequest.ChangeNotificator.UnregisterNotification(SourceRequestNotificator);
  inherited;
end;

procedure TAbstractTask.DirectSetRequest(ASourceRequest: TAbstractSourceRequest);
begin
  SetRequest(ASourceRequest);
end;

procedure TAbstractTask.DoTaskFinished;
begin
  if Assigned(FOnFinished) then
    FOnFinished(Self);
end;

procedure TAbstractTask.Fin;
begin
  // empty
end;

procedure TAbstractTask.Init;
begin
  // empty
end;

function TAbstractTask.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer): Boolean;
begin
  Result := inherited;
end;

procedure TAbstractTask.OnTerminated(const task: IOmniTaskControl);
var
  Ex: Exception;
begin
  Ex := task.DetachException;
  try
    if not Assigned(Ex) then
    begin
      if FSourceRequest.State <= srsInProcess then
        FSourceRequest.State := srsFinished
    end
    else
    begin
      if FSourceRequest.TryCounter > GetMaxRetryCount then
      begin
        if FSourceRequest.State <> srsFinalError then
          FSourceRequest.SetFailed('Max retry count exceeded for request ' + FSourceRequest.ToString);
      end
      else
        FSourceRequest.State := srsReaded;

      LogFileX.Log('TAbstractTask.OnTerminated - captured exception on task perform');
      LogFileX.LogException(Ex);
      TMonitoringInformer.ImInException(Ex, True);
    end;
  finally
    Ex.Free;
  end;

  try
    DoTaskFinished;
  except
    on e: Exception do
    begin
      LogFileX.Log('TAbstractTask.OnTerminated - captured exception on DoTaskFinished');
      LogFileX.LogException(e);
      TMonitoringInformer.ImInException(e, True);
    end;
  end;
end;

class function TAbstractTask.ProcessRequest(AOwner: TObject; SourceRequest: TAbstractSourceRequest): TAbstractTask;
var
  task: TAbstractTask;
begin
  Result := nil;
  if not CanProcessRequest(SourceRequest) then
    Exit;

  task := Self.Create(AOwner);
  task.SetRequest(SourceRequest);
  SourceRequest.State := srsInProcess;

  Parallel.Async(task.Work, task.TaskConfig);
  Result := task;
end;

procedure TAbstractTask.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
end;

procedure TAbstractTask.SetRequest(ARequest: TAbstractSourceRequest);
begin
  if Assigned(FSourceRequest) then
    FSourceRequest.ChangeNotificator.UnregisterNotification(SourceRequestNotificator);
  FSourceRequest := ARequest;
  if Assigned(FSourceRequest) then
    FSourceRequest.ChangeNotificator.RegisterNotification(SourceRequestNotificator);
end;

function TAbstractTask.SourceRequest<T>: T;
begin
  Result := FSourceRequest as T;
end;

procedure TAbstractTask.SourceRequestNotificator(Sender: TObject; Notification: TChangeNotificationStructure);
begin
  if Notification.Action = caDelete then
    if Notification.NotifyObject = FSourceRequest then
      raise EProgrammerNotFound.Create('Something wrong: request freeded before task finished');
end;

function TAbstractTask.TaskConfig: IOmniTaskConfig;
begin
  Result := Parallel.TaskConfig.OnTerminated(OnTerminated);
end;

procedure TAbstractTask.Work;
begin
  CoInitialize(nil);
  try
    Init;
    try
      DoWork;
    finally
      Fin;
    end;
  finally
    CoUninitialize;
  end;
end;

{ TaskManager }

procedure TTaskManager.AddToWork(SourceRequest: TAbstractSourceRequest);
begin
  if not CanProcessRequest(SourceRequest) then
    raise EProgrammerNotFound.Create('Request cant be added to work ' + SourceRequest.ClassName);

  if FAllRequests.HasSame(SourceRequest) then
    SourceRequest.Free
  else
  begin
    FAllRequests.Add(SourceRequest);
    if CanStartTask(SourceRequest) then
      CreateAndStartTask(SourceRequest);
  end;
end;

class function TTaskManager.CanProcessRequest(SourceRequest: TAbstractSourceRequest): Boolean;
var
  TaskClass: TTaskClass;
begin
  TaskClass := GetTaskClassForRequest(SourceRequest);
  Result := Assigned(TaskClass);
end;

function TTaskManager.CanStartTask(SourceRequest: TAbstractSourceRequest): Boolean;
var
  dt: TDateTime;
  TaskClass: TTaskClass;
begin
  // ��������� - ����� �� ����������
  // � ������ �� � ������� ���������� �������� ������� ������, ��� ������������ �������
  dt := Now;
  Result := (SourceRequest.State = srsReaded) and (SourceRequest.LastTry < dt);
  if not Result then
    Exit;

  TaskClass := GetTaskClassForRequest(SourceRequest);
  Result := Assigned(TaskClass);
  if not Result then
    Exit;

  Result := (MilliSecondsBetween(SourceRequest.LastTry, dt) >= TaskClass.GetTryIntervalMs);
end;

constructor TTaskManager.Create(AOwner: TObject);
begin
  inherited;
  FAllRequests := TSourceRequestList.Create(Self);
end;

procedure TTaskManager.CreateAndStartTask(SourceRequest: TAbstractSourceRequest);
var
  TaskClass: TTaskClass;
begin
  TaskClass := GetTaskClassForRequest(SourceRequest);
  TaskClass.ProcessRequest(Self, SourceRequest).OnFinished := OnTaskFinished;
end;

destructor TTaskManager.Destroy;
begin
  while FAllRequests.RequestCount(srsInProcess) <> 0 do
    Application.ProcessMessages;
  FreeAndNil(FAllRequests);
  inherited;
end;

class function TTaskManager.GetTaskClassForRequest(SourceRequest: TAbstractSourceRequest): TTaskClass;
var
  i: integer;
begin
  Result := nil;
  for i := 0 to TAbstractDataClassFactory.Current.Count - 1 do
    if TAbstractDataClassFactory.Current[i].InheritsFrom(TAbstractTask) then
      if TTaskClass(TAbstractDataClassFactory.Current[i]).CanProcessRequest(SourceRequest) then
      begin
        Result := TTaskClass(TAbstractDataClassFactory.Current[i]);
        Break;
      end;
end;

function TTaskManager.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer): Boolean;
begin
  Result := inherited;
end;

procedure TTaskManager.OnTaskFinished(Sender: TObject);
var
  task: TAbstractTask;
  Request: TAbstractSourceRequest;
begin
  if not(Sender is TAbstractTask) then
    Exit;
  task := TAbstractTask(Sender);
  Request := FAllRequests.Extract(task.SourceRequest<TAbstractSourceRequest>);

  ForceQueue(
      procedure
    begin
      try
        task.Free;
      except
        on e: Exception do
        begin
          LogFileX.LogException(e);
          TMonitoringInformer.ImInException(e, True);
        end;
      end;
    end); // �� ������ �� �����.

  task.SetRequest(nil);
  try
    case Request.State of
      srsReaded:
        AddToWork(Request);
      srsFinished, srsFinalError:
        Request.Free;
    else
      Request.Free;
      raise EProgrammerNotFound.Create('Cant finish task in status ' + TEnumeration<TSourceRequestState>.ToString
        (Request.State));
    end;
  except
    on e: Exception do
    begin
      LogFileX.LogException(e);
      TMonitoringInformer.ImInException(e, True);
    end;
  end;
end;

procedure TTaskManager.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
end;

procedure TTaskManager.StartPendedTasks;
var
  i: integer;
  Req: TAbstractSourceRequest;
begin
  i := 0;
  while i < FAllRequests.Count do
  begin
    Req := FAllRequests[i];
    if CanStartTask(Req) then
    begin
      FAllRequests.Extract(Req);
      try
        AddToWork(Req);
        Req := nil;
      finally
        Req.Free;
      end;
    end
    else
      inc(i);
  end;
end;

function TTaskManager.TotalRequestCount: integer;
begin
  Result := FAllRequests.Count;
end;

end.
