unit iFD.CommonClasses;

interface

uses
  System.SysUtils,
  System.DateUtils,
  System.Generics.Defaults,
  System.Generics.Collections,
  Xml.XMLIntf,
  Xml.XMLDoc;

type
  TIntContainer = class(TStack<Integer>)
  private
    function GetLastValue: Integer;
    procedure SetLastValue(const Value: Integer);
  public
    procedure Load(Node: IXMLNode);
    procedure Save(Node: IXMLNode);
    procedure CleanForExport;
    property LastValue: Integer read GetLastValue write SetLastValue;
  end;

  TStringContainer = class(TStack<string>)
  private
    FNodeName: string;
    function GetLastValue: string;
    procedure SetLastValue(const Value: string);
  public
    constructor Create(const NodeName: string);
    procedure Load(Node: IXMLNode);
    procedure Save(Node: IXMLNode);
    procedure CleanForExport;
    property LastValue: string read GetLastValue write SetLastValue;
  end;

  TAPDataX = class(TObject)
  public
    APName: string;
    APDate: TDateTime;
    Latitude: Double;
    Longitude: Double;

    procedure Assign(Source: TAPDataX);

    procedure Load(Node: IXMLNode);
    procedure Save(Node: IXMLNode);

    procedure Fill(const AAPName: string; const AAPDate: TDateTime; const ALatGrad, ALatMin, ALonGrad, ALonMin: string); overload;
    procedure Fill(const AAPName: string; const AAPDate: TDateTime; const ALat, ALon: Double); overload;
  end;

  THOLDTimesRec = record
    HoldStart: TDateTime;
    HoldEnd: TDateTime;

    function GetTotalTime: TDateTime;
    function GetNightTime(StartAPData, EndAPData: TAPDataX): TDateTime;

    class function Construct(HoldStart, HoldEnd, HoldCreated: TDateTime): THOLDTimesRec; static;
  end;

  THoldTimesList = class(TList<THOLDTimesRec>)
  public
    procedure AddNew(HoldStart, HoldEnd, HoldCreated: TDateTime);
    function GetTotalTime: TDateTime;
    function GetTotalNightTime(StartAPData, EndAPData: TAPDataX): TDateTime;
  end;

  TTaxiFuelCalc = class(TObject)
  private type
    TTypeVS = (tvsA319, tvsA320, tvsB737, tvsB747, tvs�777);
  strict private
    /// <summary>
    /// TypeVSName ���������� ����� �� ���� type_mod � XML
    /// </summary>
    class function GetTypeVSByName(const TypeVSName: string): TTypeVS;
  public
    /// <summary>
    /// ���������� ������� �� taxi � �� ������
    /// TypeVSName ���������� ����� �� ���� type_mod � XML
    /// </summary>
    class function GetPlannedTaxiFuelOut(const TypeVSName: string): Integer;
    /// <summary>
    /// ���������� ������� �� taxi � �� �������
    /// TypeVSName ���������� ����� �� ���� type_mod � XML
    /// </summary>
    class function GetPlannedTaxiFuelIn(const TypeVSName: string): Integer;
    /// <summary>
    /// ���������� ������� �� taxi � ��������� �����������. �������������� ������ ��
    ///  ������� �������� ������� ������� ��� ����������� ���� ��.
    /// </summary>
    class function GetActualTaxiFuelOut(const TypeVSName: string; MinutesCount: Integer): Integer;
    /// <summary>
    /// ���������� ������� �� taxi � ��������� ��������. �������������� ������ ��
    ///  ������� �������� ������� ������� ��� ����������� ���� ��.
    /// </summary>
    class function GetActualTaxiFuelIn(const TypeVSName: string; MinutesCount: Integer): Integer;
  end;

implementation

uses
  System.StrUtils,
  uXMLFunctions,
  iFD.CommonFunctions,
  iFD.LightTimesClasses;

{ TIntContainer }

procedure TIntContainer.CleanForExport;
var
  aLastValue: Integer;
begin
  aLastValue := LastValue;
  Clear;
  Push(aLastValue);
end;

function TIntContainer.GetLastValue: Integer;
begin
  Result := Peek;
end;

procedure TIntContainer.Load(Node: IXMLNode);
var
  Nodes: IXMLNodeList;
  i: Integer;
  Value: Integer;
begin
  Clear;
  if Assigned(Node) then
    begin
      Nodes := Node.ChildNodes;
      if Assigned(Nodes) then
        for i := 0 to Nodes.Count - 1 do
          if Nodes[i].NodeName = 'DistHistoryValue' then
            if TryStrToInt(GetNodeValue(Nodes[i]), Value) then
              Push(Value);
    end;
end;

procedure TIntContainer.Save(Node: IXMLNode);
var
  Values: TArray<Integer>;
  i: Integer;
begin
  Values := ToArray;
  for i := low(Values) to high(Values) do
    Node.AddChild('DistHistoryValue').Text := IntToStr(Values[i]);
end;

procedure TIntContainer.SetLastValue(const Value: Integer);
var
  i: Integer;
begin
  i := Pop;
  Push(Value);
  if i <> Value then
    Notify(Value, TCollectionNotification.cnAdded);
end;

{ TStringContainer }

procedure TStringContainer.CleanForExport;
var
  aLastValue: string;
begin
  aLastValue := LastValue;
  Clear;
  Push(aLastValue);
end;

constructor TStringContainer.Create(const NodeName: string);
begin
  inherited Create;
  FNodeName := NodeName;
end;

function TStringContainer.GetLastValue: string;
begin
  Result := Peek;
end;

procedure TStringContainer.Load(Node: IXMLNode);
var
  Nodes: IXMLNodeList;
  i: Integer;
begin
  Clear;
  if Assigned(Node) then
    begin
      Nodes := Node.ChildNodes;
      if Assigned(Nodes) then
        for i := 0 to Nodes.Count - 1 do
          if Nodes[i].NodeName = FNodeName then
            Push(GetNodeValue(Nodes[i]));
    end;
end;

procedure TStringContainer.Save(Node: IXMLNode);
var
  Values: TArray<string>;
  i: Integer;
begin
  Values := ToArray;
  for i := low(Values) to high(Values) do
    Node.AddChild(FNodeName).Text := Values[i];
end;

procedure TStringContainer.SetLastValue(const Value: string);
var
  s: string;
begin
  s := Pop;
  Push(Value);
  if s <> Value then
    Notify(Value, TCollectionNotification.cnAdded);
end;

{ TAPDataX }

procedure TAPDataX.Fill(const AAPName: string; const AAPDate: TDateTime; const ALatGrad, ALatMin, ALonGrad, ALonMin: string);
begin
  Fill( //
    AAPName, //
    DateOf(AAPDate), //
    GradMinToGrad(StrToIntDef(ALatGrad, 0), StrToFloatDef(ALatMin, 0, NormalizedFormatSettings)), //
    GradMinToGrad(StrToIntDef(ALonGrad, 0), StrToFloatDef(ALonMin, 0, NormalizedFormatSettings)) //
    );
end;

procedure TAPDataX.Assign(Source: TAPDataX);
var
  Xml: IXMLDocument;
  Node: IXMLNode;
begin
  Xml := NewXMLDocument;
  Node := Xml.AddChild('root');
  Source.Save(Node);
  Load(Node);
end;

procedure TAPDataX.Fill(const AAPName: string; const AAPDate: TDateTime; const ALat, ALon: Double);
begin
  APName := AAPName;
  APDate := DateOf(AAPDate);
  Latitude := ALat;
  Longitude := ALon;
end;

procedure TAPDataX.Load(Node: IXMLNode);
var
  ChildNodes: IXMLNodeList;
begin
  { APName: string;
    APDate: TDateTime;
    Latitude: Double;
    Longitude: Double; }
  if not Assigned(Node) then
    exit;
  ChildNodes := Node.ChildNodes;
  if not Assigned(ChildNodes) then
    exit;

  APName := GetNodeValueByName(ChildNodes, 'APName');
  APDate := StrToDateDef(GetNodeValueByName(ChildNodes, 'APDate'), 0, NormalizedFormatSettings);
  Latitude := StrToFloatDef(GetNodeValueByName(ChildNodes, 'Latitude'), 0, NormalizedFormatSettings);
  Longitude := StrToFloatDef(GetNodeValueByName(ChildNodes, 'Longitude'), 0, NormalizedFormatSettings);
end;

procedure TAPDataX.Save(Node: IXMLNode);
begin
  Node.AddChild('APName').Text := APName;
  Node.AddChild('APDate').Text := DateToStr(APDate, NormalizedFormatSettings);
  Node.AddChild('Latitude').Text := FloatToStr(Latitude, NormalizedFormatSettings);
  Node.AddChild('Longitude').Text := FloatToStr(Longitude, NormalizedFormatSettings);
end;

{ THOLDTimesRec }

class function THOLDTimesRec.Construct(HoldStart, HoldEnd, HoldCreated: TDateTime): THOLDTimesRec;
begin
  if SameDateTime(HoldStart, 0) and not SameDateTime(HoldEnd, 0) then
    HoldStart := IncMinute(HoldEnd, -4);
  if SameDateTime(HoldStart, 0) and SameDateTime(HoldEnd, 0) then
    HoldStart := HoldCreated;
  if not SameDateTime(HoldStart, 0) and SameDateTime(HoldEnd, 0) then
    HoldEnd := IncMinute(HoldStart, 4);
  if CompareDateTime(HoldStart, HoldEnd) > 0 then
    HoldEnd := IncMinute(HoldStart, 4);
  Result.HoldStart := HoldStart;
  Result.HoldEnd := HoldEnd;
end;

function THOLDTimesRec.GetNightTime(StartAPData, EndAPData: TAPDataX): TDateTime;
begin
  Result := 0;
  if not SameDateTime(HoldStart, 0) and not SameDateTime(HoldEnd, 0) then
    Result := NightTime(HoldStart, HoldEnd, StartAPData, EndAPData);
end;

function THOLDTimesRec.GetTotalTime: TDateTime;
begin
  Result := EncodeTime(0, 4, 0, 0);
  if not SameDateTime(HoldStart, 0) and not SameDateTime(HoldEnd, 0) then
    if CompareDateTime(HoldStart, HoldEnd) < 0 then
      Result := HoldEnd - HoldStart;
end;

{ THoldTimesList }

procedure THoldTimesList.AddNew(HoldStart, HoldEnd, HoldCreated: TDateTime);
begin
  Add(THOLDTimesRec.Construct(HoldStart, HoldEnd, HoldCreated));
end;

function THoldTimesList.GetTotalNightTime(StartAPData, EndAPData: TAPDataX): TDateTime;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to Count - 1 do
    Result := Result + Items[i].GetNightTime(StartAPData, EndAPData);
end;

function THoldTimesList.GetTotalTime: TDateTime;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to Count - 1 do
    Result := Result + Items[i].GetTotalTime;
end;

{ TAPTaxiFuelCalc }

class function TTaxiFuelCalc.GetActualTaxiFuelIn(const TypeVSName: string;
  MinutesCount: Integer): Integer;
var
  tvs: TTypeVS;
begin
  tvs := GetTypeVSByName(TypeVSName);
  case tvs of
    tvsA319:
      Result := 12 * MinutesCount;
    tvsA320:
      Result := 12 * MinutesCount;
    tvsB737:
      Result := 12 * MinutesCount;
    tvsB747:
      Result := 45 * MinutesCount;
    tvs�777:
      Result := 26 * MinutesCount;
  else
    Result:= 12 * MinutesCount;
  end;
end;

class function TTaxiFuelCalc.GetActualTaxiFuelOut(const TypeVSName: string; MinutesCount: Integer): Integer;
var
  tvs: TTypeVS;
begin
  tvs := GetTypeVSByName(TypeVSName);
  case tvs of
    tvsA319:
      Result := 12 * MinutesCount;
    tvsA320:
      Result := 12 * MinutesCount;
    tvsB737:
      Result := 12 * MinutesCount;
    tvsB747:
      Result := 45 * MinutesCount;
    tvs�777:
      Result := 26 * MinutesCount;
  else
    Result:= 12 * MinutesCount;
  end;
end;

class function TTaxiFuelCalc.GetPlannedTaxiFuelIn(const TypeVSName: string): Integer;
var
  tvs: TTypeVS;
begin
  tvs := GetTypeVSByName(TypeVSName);
  case tvs of
    tvsA319:
      Result := 100;
    tvsA320:
      Result := 100;
    tvsB737:
      Result := 90;
    tvsB747:
      Result := 310;
    tvs�777:
      Result := 200;
  else
    Result := 100;
  end;
end;

class function TTaxiFuelCalc.GetPlannedTaxiFuelOut(const TypeVSName: string): Integer;
var
  tvs: TTypeVS;
begin
  tvs := GetTypeVSByName(TypeVSName);
  case tvs of
    tvsA319:
      Result := 200;
    tvsA320:
      Result := 200;
    tvsB737:
      Result := 200;
    tvsB747:
      Result := 700;
    tvs�777:
      Result := 400;
  else
    Result := 200;
  end;
end;

class function TTaxiFuelCalc.GetTypeVSByName(const TypeVSName: string): TTypeVS;
begin
  Result := tvsA319;
  if AnsiContainsText(TypeVSName, '31') then
    Result := tvsA319;
  if AnsiContainsText(TypeVSName, '32') then
    Result := tvsA320;
  if AnsiContainsText(TypeVSName, '73') then
    Result := tvsB737;
  if AnsiContainsText(TypeVSName, '74') then
    Result := tvsB747;
  if AnsiContainsText(TypeVSName, '77') then
    Result := tvs�777;
end;

end.
