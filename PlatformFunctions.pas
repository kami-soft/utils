unit PlatformFunctions;
{$I DelphiVersions.inc}

interface

function GetDocumentPath(const ProgramName: string = 'iFlightDoc'): string;
// URLEncode is performed on the URL
// so you need to format it   protocol://path
function OpenURL(const URL: string; const DisplayError: Boolean = False): Boolean;
function GetProgramVersion: string;
function GetDeviceName: string;

implementation

uses
  SysUtils,
  Classes,
  System.IOUtils
  {$IFDEF XE7Up}
  //  ,
  //uStandartDialogsHelper
  {$ENDIF}
  {$IFDEF MSWINDOWS}
    ,
  Winapi.Windows,
  Winapi.ShellAPI
  {$ENDIF}
  {$IFDEF ANDROID}
    ,
  IdURI,
  FMX.Helpers.Android,
  AndroidAPI.Helpers,
  AndroidAPI.JNI.App,
  AndroidAPI.JNI.GraphicsContentViewText,
  AndroidAPI.JNI.Net,
  AndroidAPI.JNI.JavaTypes
  {$ENDIF}
  {$IFDEF IOS}
    ,
  iOSapi.UIKit,
  iOSapi.Foundation,
  FMX.Helpers.iOS,
  Macapi.CoreFoundation,
  Macapi.Helpers
  {$ENDIF};

var
  FDocumentPath: string;

function GetDocumentPath(const ProgramName: string): string;
begin
  if FDocumentPath <> '' then
    begin
      Result := FDocumentPath;
      exit;
    end;
  {$IFDEF XE7Up}
  Result := IncludeTrailingPathDelimiter(IncludeTrailingPathDelimiter(System.IOUtils.TPath.GetDocumentsPath) +
    ProgramName);
  {$ELSE}
  Result := IncludeTrailingPathDelimiter(IncludeTrailingPathDelimiter(System.IOUtils.TPath.GetHomePath) + ProgramName);
  {$ENDIF}
  ForceDirectories(Result);
  FDocumentPath := Result;
end;

function OpenURL(const URL: string; const DisplayError: Boolean = False): Boolean;
var
  {$IFDEF ANDROID}
  Intent: JIntent;
  {$ENDIF}
  {$IFDEF IOS}
  NSU: NSUrl;
  {$ENDIF}
  {$IFDEF MSWINDOWS}
  Res: HINST;
  {$ENDIF}
begin
  {$IFDEF ANDROID}
  // There may be an issue with the geo: prefix and URLEncode.
  // will need to research
  Intent := TJIntent.JavaClass.init(TJIntent.JavaClass.ACTION_VIEW,
    TJnet_Uri.JavaClass.parse(StringToJString(TIdURI.URLEncode(URL))));
  try
    TAndroidHelper.Activity.startActivity(Intent);
    exit(true);
  except
    on e: Exception do
      begin
        if DisplayError then
          TDialog.ShowMessage('Error: ' + e.Message);
        exit(False);
      end;
  end;
  {$ENDIF}
  {$IFDEF IOS}
  // iOS doesn't like spaces, so URL encode is important.
  NSU := StrToNSUrl(URL);
  if SharedApplication.canOpenURL(NSU) then
    exit(SharedApplication.OpenURL(NSU))
  else
    begin
      if DisplayError then
        TDialog.ShowMessage('Error: Opening "' + URL + '" not supported.');
      exit(False);
    end;
  {$ENDIF}
  {$IFDEF MSWINDOWS}
  Res := ShellExecute(0, 'open', PChar(URL), nil, nil, SW_SHOW);
  Result := Res > 32;
  {$ENDIF}
end;

var
  FProgramVersion: string;

function GetProgramVersion: string;
var
  {$IFDEF ANDROID}
  PackageManager: JPackageManager;
  PackageInfo: JPackageInfo;
  {$ENDIF}
  {$IFDEF IOS}
  s: MarshaledAString;
  {$ENDIF}
  {$IFDEF MSWINDOWS}
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
  {$ENDIF}
begin
  if FProgramVersion <> '' then
    begin
      Result := FProgramVersion;
      exit;
    end;
  {$IFDEF ANDROID}
  PackageManager := TAndroidHelper.Context.getPackageManager;
  PackageInfo := PackageManager.getPackageInfo(TAndroidHelper.Context.getPackageName, 0);
  Result := JStringToString(PackageInfo.versionName);
  {$ENDIF}
  {$IFDEF IOS}
  s := TNSString.Wrap(CFBundleGetValueForInfoDictionaryKey(CFBundleGetMainBundle, kCFBundleVersionKey)).UTF8String;
  Result := string(s);
  {$ENDIF}
  {$IFDEF MSWINDOWS}
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(PChar(Exe), Handle);
  if Size = 0 then
    begin
      Result := 'Unknown';
      exit;
    end;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(PChar(Exe), Handle, Size, Buffer) then
    begin
      Result := 'Unknown';
      exit;
    end;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    begin
      Result := 'Unknown';
      exit;
    end;
  Result := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi, // major
    LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
    LongRec(FixedPtr.dwFileVersionLS).Hi, // release
    LongRec(FixedPtr.dwFileVersionLS).Lo]); // build
  {$ENDIF}
  FProgramVersion := Result;
end;

var
  FDeviceName: string;

function GetDeviceName: string;
{$IFDEF IOS}
var
  Device: UIDevice;
  {$ENDIF}
  {$IFDEF MSWINDOWS}
var
  PCompName: PChar;
  nSize: Cardinal;
  {$ENDIF}
begin
  if FDeviceName <> '' then
    begin
      Result := FDeviceName;
      exit;
    end;
  Result := 'Unknown';
  {$IFDEF IOS}
  Device := TUIDevice.Wrap(TUIDevice.OCClass.currentDevice);
  if Assigned(Device) then
    Result := UTF8ToString(Device.name.UTF8String);
  {$ENDIF}
  {$IFDEF MSWINDOWS}
  nSize := 0;
  GetComputerName(nil, nSize);
  PCompName := AllocMem(nSize * SizeOf(Char));
  try
    if GetComputerName(PCompName, nSize) then
      Result := PCompName;
  finally
    FreeMem(PCompName);
  end;
  {$ENDIF}
  FDeviceName := Result;
end;

initialization

FDeviceName := '';
FProgramVersion := '';
FDocumentPath := '';

end.
