unit uThreadSafeLogger;

{ .$DEFINE USE_UTC_LOG_TIME } // ���� Define ����� ���������� ��������������� � ConditionalDefines �������.
{$DEFINE USE_MODULE_NAME_FOR_LOG}

// ���������������� ����� ������� ����� - ��������� TFileStream
interface

uses
  SysUtils,
  Classes,
  SyncObjs;

type
  TProxyLog = class(TFileStream)
  private
    FCriticalSection: TCriticalSection;
    FEncoding: TEncoding;
    FUseUTCLogTime: Boolean;
    FIgnoreLogging: Boolean;
    FInLockCounter: integer;

    function MoveFileToArchive: Boolean;
  public
    constructor Create(const FileName: string; Mode: Word; Encoding: TEncoding; UseUTCLogTime: Boolean);
    destructor Destroy; override;

    procedure Lock; inline;
    procedure Unlock; inline;

    procedure Clear;

    procedure Flush;

    function Read(var Buffer; Count: Longint): Longint; override;
    function Write(const Buffer; Count: Longint): Longint; override;
    function Seek(const Offset: Int64; Origin: TSeekOrigin): Int64; override;

    function WriteString(const s: string): Longint;
    function WriteStringCRLF(const s: string): Longint;
    function WriteChronoStringCRLF(const s: string): Longint;
    function LogIf(Expr: Boolean; const s: string): Longint;
    function Log(const s: string): Longint;
    function LogException(e: Exception): Longint;

    function GetArchiveFolderName: string;
    procedure DeleteOldFromArchive(const BeforeDate: TDateTime);
    procedure GetArchiveFileNames(SL: TStrings);

    property IgnoreLogging: Boolean read FIgnoreLogging write FIgnoreLogging;
  end;

var
  LogFileX: TProxyLog;

function GetLogFileName(const AddParam: string = ''): string;
procedure RecreateLog(LogEncoding: TEncoding);

implementation

uses
  System.IOUtils,
  {$IFDEF MSWINDOWS}
  Winapi.Windows,
  Winapi.SHFolder,
  {$ENDIF}
  System.DateUtils;

const
  sArchiveFolder = 'LogArchive';
  MaxLogSize = 1024 * 1024 * 30;

var
  FDocumentPath: string;

function GetFullModuleFileName: string;
begin
  {$IFDEF USE_MODULE_NAME_FOR_LOG}
  Result := GetModuleName(HInstance);
  {$ELSE}
  Result := ParamStr(0);
  {$ENDIF}
end;

function TranslateLogFileName(const AddParam: string = ''): string;
var
  s: string;
begin
  s := ExtractFileName(GetFullModuleFileName);
  Result := ChangeFileExt(AddParam + s, '.log')
end;

function GetLogFileName(const AddParam: string = ''): string;
  function GetDocumentPath(const ProgramName: string): string;
  const
    CSIDL_PERSONAL = 5;
  var
    LStr: array [0 .. MAX_PATH] of Char;
  begin
    if FDocumentPath <> '' then
    begin
      Result := FDocumentPath;
      exit;
    end;
    {$IFNDEF MSWINDOWS}
    Result := IncludeTrailingPathDelimiter(IncludeTrailingPathDelimiter(System.IOUtils.TPath.GetDocumentsPath) +
        ProgramName);
    ForceDirectories(Result);
    {$ELSE}
    SetLastError(ERROR_SUCCESS);

    if SHGetFolderPath(0, CSIDL_APPDATA, 0, 0, @LStr) = S_OK then
      Result := LStr
    else
      Result := ExtractFilePath(GetFullModuleFileName);
    {$ENDIF}
    FDocumentPath := Result;
  end;

  function GetDocumentedFileName: string;
  begin
    Result := IncludeTrailingPathDelimiter(GetDocumentPath(ExtractFileName(GetFullModuleFileName)));
    Result := Result + TranslateLogFileName(AddParam);
  end;

  function GetLocalOrDocumentedFileName: string;
  var
    stmpFileName: string;
    tmpFile: TFileStream;
  begin
    Result := IncludeTrailingPathDelimiter(ExtractFilePath(GetFullModuleFileName)) + //
      TranslateLogFileName(AddParam);

    stmpFileName := IncludeTrailingPathDelimiter(ExtractFilePath(Result)) + TPath.GetGUIDFileName;
    try
      tmpFile := TFileStream.Create(stmpFileName, fmCreate);
      tmpFile.Free;
      DeleteFile(PChar(stmpFileName));
    except
      on e: Exception do
      begin
        Result := GetDocumentedFileName;
      end;
    end;
  end;

begin
  {$IFDEF MSWINDOWS}
  {$DEFINE UseLocalPath}
  {$ELSE}
  {$UNDEF UseLocalPath}
  {$ENDIF}
  {$IFNDEF UseLocalPath}
  Result := GetDocumentedFileName;
  {$ELSE}
  Result := GetLocalOrDocumentedFileName;
  {$ENDIF}
end;

function SimpleTryCreateLogFile(const FileName: string; LogEncoding: TEncoding; var LogFile: TProxyLog): Boolean;
begin
  try
    ForceDirectories(ExtractFileDir(FileName));
    if FileExists(FileName) then
      LogFile := TProxyLog.Create(FileName, fmOpenReadWrite or fmShareDenyWrite, LogEncoding,
        {$IFDEF USE_UTC_LOG_TIME}True{$ELSE}False{$ENDIF})
    else
      LogFile := TProxyLog.Create(FileName, fmCreate or fmShareDenyWrite, LogEncoding,
        {$IFDEF USE_UTC_LOG_TIME}True{$ELSE}False{$ENDIF});
    Result := True;
  except
    LogFile := nil;
    Result := False;
  end;
end;

function TryCreateLogFile(MaxRecurceCount: integer; LogEncoding: TEncoding; var LogFile: TProxyLog): Boolean;
var
  s: string;
  i: integer;
begin
  Result := False;
  for i := 0 to MaxRecurceCount do
  begin
    if i = 0 then
      s := GetLogFileName
    else
      s := GetLogFileName('(' + IntToStr(i) + ')');
    Result := SimpleTryCreateLogFile(s, LogEncoding, LogFile);
    if Result then
      Break;
  end;
end;

procedure RecreateLog(LogEncoding: TEncoding);
begin
  FreeAndNil(LogFileX);

  TryCreateLogFile(100, LogEncoding, LogFileX);
  LogFileX.Seek(0, soEnd);
end;

{ TProxyLog }

procedure TProxyLog.Clear;
var
  bb: TBytes;
begin
  if not Assigned(Self) then
    exit;
  Size := 0;
  bb := FEncoding.GetPreamble;
  if Length(bb) <> 0 then
    write(bb[0], Length(bb));
end;

constructor TProxyLog.Create(const FileName: string; Mode: Word; Encoding: TEncoding; UseUTCLogTime: Boolean);
begin
  inherited Create(FileName, Mode);

  FCriticalSection := TCriticalSection.Create;

  FEncoding := Encoding;
  if not Assigned(FEncoding) then
    FEncoding := TEncoding.UTF8;

  FUseUTCLogTime := UseUTCLogTime;

  if Size = 0 then
    Clear;
end;

procedure TProxyLog.DeleteOldFromArchive(const BeforeDate: TDateTime);
var
  Res: integer;
  SR: TSearchRec;
  slNamesToDelete: TStringList;
  i: integer;
begin
  Res := FindFirst(IncludeTrailingPathDelimiter(GetArchiveFolderName) + '*.log', faAnyFile, SR);
  try
    if Res = 0 then
    begin
      slNamesToDelete := TStringList.Create;
      try
        repeat
          if SR.TimeStamp < BeforeDate then
            slNamesToDelete.Add(SR.Name);
        until FindNext(SR) <> 0;

        for i := 0 to slNamesToDelete.Count - 1 do
          TFile.Delete(IncludeTrailingPathDelimiter(GetArchiveFolderName) + slNamesToDelete[i]);
      finally
        slNamesToDelete.Free;
      end;
    end;
  finally
    SysUtils.FindClose(SR);
  end;
end;

destructor TProxyLog.Destroy;
begin
  FCriticalSection.Free;
  inherited;
end;

procedure TProxyLog.Flush;
begin
  {$IFDEF MSWINDOWS}
  //Winapi.Windows.FlushFileBuffers(Handle);
  {$ENDIF}
end;

procedure TProxyLog.GetArchiveFileNames(SL: TStrings);
var
  Res: integer;
  SR: TSearchRec;
begin
  SL.Clear;
  Res := FindFirst(IncludeTrailingPathDelimiter(GetArchiveFolderName) + '*.log', faAnyFile, SR);
  try
    if Res = 0 then
      repeat
        SL.Add(SR.Name);
      until FindNext(SR) <> 0;
  finally
    SysUtils.FindClose(SR);
  end;
end;

function TProxyLog.GetArchiveFolderName: string;
begin
  Result := ExtractFilePath(FileName) + IncludeTrailingPathDelimiter(sArchiveFolder);
end;

procedure TProxyLog.Lock;
begin
  if not Assigned(Self) then
    exit;
  FCriticalSection.Enter;
  InterlockedIncrement(FInLockCounter);
end;

function TProxyLog.Log(const s: string): Longint;
begin
  Result := WriteChronoStringCRLF(s);
end;

function TProxyLog.LogException(e: Exception): Longint;
var
  tmpIgnoreLogging: Boolean;
begin
  Lock;
  try
    tmpIgnoreLogging := FIgnoreLogging;
    try
      FIgnoreLogging := False;

      Log('Exception type: ' + e.ClassName);
      Log(e.ToString);
      Result := Log(e.StackTrace);
    finally
      FIgnoreLogging := tmpIgnoreLogging;
    end;
  finally
    Unlock;
  end;
end;

function TProxyLog.LogIf(Expr: Boolean; const s: string): Longint;
begin
  if Expr then
    Result := WriteChronoStringCRLF(s)
  else
    Result := 0;
end;

function TProxyLog.MoveFileToArchive: Boolean;
var
  sArchiveFileName: string;
  sCurrentFileName: string;
  dt: TDateTime;
begin
  Result := False;
  if not Assigned(Self) then
    exit;

  sCurrentFileName := FileName;

  dt := Now;
  if FUseUTCLogTime then
    if not TTimeZone.Local.IsInvalidTime(dt) then
      dt := TTimeZone.Local.ToUniversalTime(dt);

  sArchiveFileName := IncludeTrailingPathDelimiter(GetArchiveFolderName) +
    TranslateLogFileName(FormatDateTime('yyyy.mm.dd_hh_nn_ss', dt));

  try
    if ForceDirectories(ExtractFileDir(sArchiveFileName)) then
    begin
      System.IOUtils.TFile.Copy(sCurrentFileName, sArchiveFileName);
      Result := True;
    end;
  except
    on e: Exception do
      Result := False;
  end;
end;

function TProxyLog.Read(var Buffer; Count: integer): Longint;
begin
  if not Assigned(Self) then
  begin
    Result := 0;
  end
  else
  begin
    Lock;
    try
      Result := inherited read(Buffer, Count);
    finally
      Unlock;
    end;
  end;
end;

function TProxyLog.Seek(const Offset: Int64; Origin: TSeekOrigin): Int64;
begin
  if not Assigned(Self) then
  begin
    Result := 0;
  end
  else
  begin
    Lock;
    try
      Result := inherited Seek(Offset, Origin);
    finally
      Unlock;
    end;
  end;
end;

procedure TProxyLog.Unlock;
begin
  if not Assigned(Self) then
    exit;
  if InterlockedDecrement(FInLockCounter) = 0 then
    Flush;
  FCriticalSection.Leave;
end;

function TProxyLog.Write(const Buffer; Count: integer): Longint;
begin
  if IgnoreLogging then
    exit(0);
  if not Assigned(Self) then
  begin
    Result := 0;
  end
  else
  begin
    Lock;
    try
      Result := inherited write(Buffer, Count);

      if Size > MaxLogSize then
        if MoveFileToArchive then
          Clear;
    finally
      Unlock;
    end;
  end;
end;

function TProxyLog.WriteChronoStringCRLF(const s: string): Longint;
var
  dt: TDateTime;
begin
  if not Assigned(Self) then
  begin
    Result := 0;
  end
  else
  begin
    dt := Now;
    if FUseUTCLogTime then
      if not TTimeZone.Local.IsInvalidTime(dt) then
        dt := TTimeZone.Local.ToUniversalTime(dt);

    if not TTimeZone.Local.IsInvalidTime(dt) then
      Result := WriteStringCRLF(FormatDateTime('dd.mm.yyyy hh:nn:ss:zzz', dt) + '  ' + s)
    else
      Result := WriteStringCRLF('--' + FormatDateTime('dd.mm.yyyy hh:nn:ss:zzz', dt) + '  ' + s);
  end
end;

function TProxyLog.WriteString(const s: string): Longint;
var
  i: integer;
  Bytes: TBytes;
begin
  if not Assigned(Self) then
  begin
    Result := 0;
    exit;
  end;
  i := Length(s);
  if i > 0 then
  begin
    Bytes := FEncoding.GetBytes(s);
    Result := write(Bytes[0], Length(Bytes));
  end
  else
    Result := 0;
end;

function TProxyLog.WriteStringCRLF(const s: string): Longint;
begin
  Result := WriteString(s + #13#10);
end;

initialization

if not TryCreateLogFile(100, TEncoding.UTF8, LogFileX) then
begin
  // ShowMessage(0, '���������� ������� ���-����', '������!', MB_OK or MB_ICONERROR or MB_TASKMODAL);
  Halt;
end;
LogFileX.Seek(0, soEnd);
LogFileX.Log('==============================������ ����������======================');

finalization

LogFileX.Free;
LogFileX := nil;

end.
