unit Utils.ConnectionPool;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Generics.Defaults,
  System.Generics.Collections,
  System.SyncObjs,
  System.Rtti,
  Data.DB,
  Data.Win.ADODB,
  Winapi.Windows,
  Common.Interfaces;

type
  TThreadConnectionInfo = record
    ConnectionType: TConnectionStringType;
    ThreadID: Cardinal;
  end;

  TConnectionPool = class(TObjectDictionary<TThreadConnectionInfo, TADOConnection>)
  strict private
    class var FInstance: TConnectionPool;
  strict private
    FMREW: TMultiReadExclusiveWriteSynchronizer;
    FLastDeprecatedDeleted: TDateTime;

    function KeysComparator(const Left, Right: TThreadConnectionInfo): Boolean;
    function KeyHash(const Value: TThreadConnectionInfo): Integer;

    constructor Create;

    procedure DeleteDeprecatedConnections;
  private
    procedure DeleteThreadConnections(ThreadID: Cardinal);
  public
    destructor Destroy; override;

    function GetThreadConnection(ConnStrType: TConnectionStringType): TADOConnection;
    procedure DangerDeleteAll;

    class procedure Fin;
    class function Instance: TConnectionPool;
    class function ThreadConnection(ConnStrType: TConnectionStringType): TADOConnection;
    class procedure DeleteCurrentThreadConnections;
  end;

function OpenThread(dwDesiredAccess: DWORD; bInheritHandle: BOOL; dwThreadId: DWORD): THandle; stdcall;
  external 'Kernel32.dll';

const
  THREAD_QUERY_INFORMATION = $40;

implementation

uses
  System.DateUtils,
  System.IOUtils,
  Utils.GlobalServices;

var
  FOldSystemThreadProc: TSystemThreadEndProc;

procedure InterceptedSystemThreadEndProc(ExitCode: Integer);
begin
  TConnectionPool.DeleteCurrentThreadConnections;
  if Assigned(FOldSystemThreadProc) then
    FOldSystemThreadProc(ExitCode);
end;

{ TConnectionPool }

constructor TConnectionPool.Create;
begin
  inherited Create([doOwnsValues], TEqualityComparer<TThreadConnectionInfo>.Construct(KeysComparator, KeyHash));
  FMREW := TMultiReadExclusiveWriteSynchronizer.Create;

  FOldSystemThreadProc := SystemThreadEndProc;
  SystemThreadEndProc := InterceptedSystemThreadEndProc;
end;

procedure TConnectionPool.DangerDeleteAll;
begin
  FMREW.BeginWrite;
  try
    Clear;
  finally
    FMREW.EndWrite;
  end;
end;

class procedure TConnectionPool.DeleteCurrentThreadConnections;
begin
  Instance.DeleteThreadConnections(GetCurrentThreadId);
end;

procedure TConnectionPool.DeleteDeprecatedConnections;
  function IsThreadAlive(ThreadID: Cardinal): Boolean;
  var
    ThreadHandle: Integer;
    ExitCode: Cardinal;
  begin
    Result := True;
    ThreadHandle := OpenThread(THREAD_QUERY_INFORMATION, False, ThreadID);
    try
      if ThreadHandle = 0 then
        Result := False
      else
        if GetExitCodeThread(ThreadHandle, ExitCode) then
        begin
          if ExitCode <> STILL_ACTIVE then
            Result := False;
        end;
    finally
      CloseHandle(ThreadHandle);
    end;
  end;

var
  ConnInfo: TThreadConnectionInfo;
  DeprecatedList: TDictionary<Cardinal, Boolean>;
  i: Cardinal;
  dt: TDateTime;
begin
  dt := Now;
  if (MinutesBetween(FLastDeprecatedDeleted, dt) < 5) and (Count < 100) then
    Exit;

  FLastDeprecatedDeleted := dt;

  DeprecatedList := TDictionary<Cardinal, Boolean>.Create;
  try
    FMREW.BeginRead;
    try
      for ConnInfo in Keys do
      begin
        if not DeprecatedList.ContainsKey(ConnInfo.ThreadID) then
          if not IsThreadAlive(ConnInfo.ThreadID) then
            DeprecatedList.Add(ConnInfo.ThreadID, False)
      end;

      FMREW.BeginWrite;
      try
        for i in DeprecatedList.Keys do
          DeleteThreadConnections(i);
      finally
        FMREW.EndWrite;
      end;
    finally
      FMREW.EndRead;
    end;
  finally
    DeprecatedList.Free;
  end;
end;

procedure TConnectionPool.DeleteThreadConnections(ThreadID: Cardinal);
var
  ConnInfo: TThreadConnectionInfo;
  i: TConnectionStringType;
begin
  ConnInfo.ThreadID := ThreadID;
  FMREW.BeginWrite;
  try
    for i := low(TConnectionStringType) to high(TConnectionStringType) do
    begin
      ConnInfo.ConnectionType := i;
      Remove(ConnInfo);
    end;
  finally
    FMREW.EndWrite;
  end;
end;

destructor TConnectionPool.Destroy;
begin
  SystemThreadEndProc := FOldSystemThreadProc;

  FMREW.BeginWrite;
  try
    Clear;
  finally
    FMREW.EndWrite;
  end;
  FMREW.Free;
  FMREW := nil;
  inherited;
end;

class procedure TConnectionPool.Fin;
begin
  FInstance.Free;
  FInstance := nil;
end;

function TConnectionPool.GetThreadConnection(ConnStrType: TConnectionStringType): TADOConnection;
var
  ConnInfo: TThreadConnectionInfo;
  sModuleName: string;
begin
  DeleteDeprecatedConnections;

  ConnInfo.ConnectionType := ConnStrType;
  ConnInfo.ThreadID := GetCurrentThreadId;
  FMREW.BeginRead;
  try
    if not TryGetValue(ConnInfo, Result) then
    begin
      Result := TADOConnection.Create(nil);
      FMREW.BeginWrite;
      try
        Add(ConnInfo, Result);
        sModuleName := ChangeFileExt(ExtractFileName(GetModuleName(HInstance)), '');
        Result.ConnectionString := GlobalConnection.UpdateLetterConnectionX
          (GlobalConnection.ConnectionString[ConnStrType], sModuleName);
        Result.LoginPrompt := False;
      finally
        FMREW.EndWrite;
      end;
    end;
  finally
    FMREW.EndRead;
  end;
end;

class function TConnectionPool.Instance: TConnectionPool;
begin
  if not Assigned(FInstance) then
    FInstance := Self.Create;
  Result := FInstance;
end;

function TConnectionPool.KeyHash(const Value: TThreadConnectionInfo): Integer;
begin
  Result := BobJenkinsHash(Value.ConnectionType, SizeOf(TConnectionStringType), 65535);
  Result := BobJenkinsHash(Value.ThreadID, SizeOf(Cardinal), Result);
end;

function TConnectionPool.KeysComparator(const Left, Right: TThreadConnectionInfo): Boolean;
begin
  Result := (Left.ConnectionType = Right.ConnectionType) and (Left.ThreadID = Right.ThreadID);
end;

class function TConnectionPool.ThreadConnection(ConnStrType: TConnectionStringType): TADOConnection;
begin
  Result := Instance.GetThreadConnection(ConnStrType);
end;

initialization

finalization

TConnectionPool.Fin;

end.
