unit Utils.GlobalServices;

interface

uses
  Common.Interfaces;
/// <summary>
/// GlobalServices always contains:
///  IGlobalService, IGlobalConnection
///
///  and (if used in RibbonProject)
///  at start TGlobalData always add:
///  IUserInfo, IProgramSpecificData, IAirCoInfo, IApplicationInfo
///
///  All other services can be not presented!
/// </summary>
function GlobalServices: IGlobalServices;
procedure FinalizeGlobalServices;

function GlobalConnection: IGlobalConnection;

implementation

uses
  System.SysUtils,
  System.Classes,
  System.Generics.Collections,
  System.SyncObjs,
  PlatformFunctions;

type
  TGlobalServices = class sealed(TObject, IInterface, IGlobalService, IGlobalServices, IGlobalConnection)
  strict private
    class var FInstance: IGlobalServices;
  strict private
    FRefCount: Integer;
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
  strict private
    FServices: TDictionary<TGUID, IInterface>;
    FConnectionStrings: array [TConnectionStringType] of WideString;

    { IGlobalService }
    procedure ReleaseLocalInstance; safecall;
    { IGlobalServices }
    procedure RegisterService(const IID: TGUID; Service: IInterface); safecall;
    procedure SetExternal(AExternalServices: IGlobalServices); safecall;

    { IGlobalConnection }
    function GetMainConnectionString: WideString; safecall;
    procedure SetMainConnectionString(const Value: WideString); safecall;
    function GetConnectionString(ConnectionStringType: TConnectionStringType): WideString; safecall;
    procedure SetConnectionString(ConnectionStringType: TConnectionStringType; const Value: WideString); safecall;

    function UpdateLetterConnection(const ConnectionString: WideString; ConnType: TTypeADOConnection)
      : WideString; safecall;
    function UpdateLetterConnectionX(const ConnectionString: WideString; const ConnType: WideString)
      : WideString; safecall;
  public
    constructor Create;
    destructor Destroy; override;

    class procedure Fin;
    class function CurrentX: IGlobalServices;
  end;

function GlobalServices: IGlobalServices;
begin
  Result := TGlobalServices.CurrentX;
end;

procedure FinalizeGlobalServices;
begin
  TGlobalServices.Fin;
end;

function GlobalConnection: IGlobalConnection;
begin
  if not Supports(GlobalServices, IGlobalConnection, Result) then
    Result := nil;
end;

const
  LETTER_TYPE_CONNECTION: array [TTypeADOConnection] of string = ('General', 'PaxLoad', 'Brif', 'Tablet', 'Map',
    'DelayActs', 'Delay1', 'Delay2', 'Access', 'Alerts', 'Fleet', 'Airport', 'Restrict', 'Represent', 'Designer',
    'Report', 'Transfer', 'SeatsUsing', 'FlightInfo', 'OOSMain', 'Crew', 'RepCancelled', 'Licence', 'DelayRep',
    'AircoInfo', 'SSIM', 'CargoBox', 'OtherReports', 'Contracts', 'CargoUn', 'BaggageNorm', 'DelayDuty', 'NalCh',
    'DrivingTimes', 'FPL', 'PPL', 'Fuel', 'AutoOFP', 'AFTN', 'SOFI', 'ZitoChain', 'RouteAlts');

  { TGlobalServices }

constructor TGlobalServices.Create;
begin
  inherited Create;
  FServices := TDictionary<TGUID, IInterface>.Create;
end;

class function TGlobalServices.CurrentX: IGlobalServices;
begin
  if not Assigned(FInstance) then
    FInstance := Self.Create;
  Result := FInstance;
end;

destructor TGlobalServices.Destroy;
begin
  FreeAndNil(FServices);
  inherited;
end;

class procedure TGlobalServices.Fin;
begin
  FInstance := nil;
end;

function TGlobalServices.GetConnectionString(ConnectionStringType: TConnectionStringType): WideString;
begin
  Result := FConnectionStrings[ConnectionStringType];
end;

function TGlobalServices.GetMainConnectionString: WideString;
begin
  Result := FConnectionStrings[cstMain];
end;

function TGlobalServices.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  Pointer(Obj) := nil;
  Result := E_NOINTERFACE;
  if FServices.ContainsKey(IID) then
    if Supports(FServices[IID], IID, Obj) then
      Result := S_OK;

  if Result <> S_OK then
    if GetInterface(IID, Obj) then
      Result := S_OK;
end;

procedure TGlobalServices.RegisterService(const IID: TGUID; Service: IInterface);
  function GetClassName: string;
  begin
    if Service is TObject then
      Result := TObject(Service).ClassName
    else
      Result := '';
  end;

  function GetInterfaceName: string;
  begin
    Result := GUIDToString(IID);
  end;

var
  LService: IInterface;
  gs: IGlobalService;
begin
  if not FServices.ContainsKey(IID) then
  begin
    if Supports(Service, IID, LService) then
      FServices.Add(IID, Service)
    else
      if Service = nil then
        raise EArgumentNilException.Create('Trying to register nil as Global Service')at ReturnAddress
      else
        if Service is TObject then
          raise EArgumentException.CreateFmt('Class %s not supported interface %s', [GetClassName, GetInterfaceName])
            at ReturnAddress
        else
          raise EArgumentException.CreateFmt('Invalid argument in try to register service %s', [GetInterfaceName])
            at ReturnAddress;

    if Supports(Service, IGlobalService, gs) then
      gs.ReleaseLocalInstance;
  end
  else
    raise EListError.CreateFmt('Global service %s already exists', [GetInterfaceName])at ReturnAddress;
end;

procedure TGlobalServices.ReleaseLocalInstance;
begin
  Fin;
end;

procedure TGlobalServices.SetMainConnectionString(const Value: WideString);
begin
  FConnectionStrings[cstMain] := Value;
end;

function TGlobalServices.UpdateLetterConnection(const ConnectionString: WideString; ConnType: TTypeADOConnection)
  : WideString;
begin
  Result := UpdateLetterConnectionX(ConnectionString, LETTER_TYPE_CONNECTION[ConnType]);
end;

function TGlobalServices.UpdateLetterConnectionX(const ConnectionString, ConnType: WideString): WideString;
  procedure AddAppName(SL: TStringList);
  var
    ProgramSpecificData: IProgramSpecificData;
    UserInfo: IUserInfo;

    ProgrammDescription: string;
    UserLogin: string;
  begin
    if Supports(Self, IProgramSpecificData, ProgramSpecificData) then
      ProgrammDescription := ProgramSpecificData.ProgrammDescription
    else
      ProgrammDescription := 'UnknownRibbon';

    UserLogin := 'Unknown';
    if Supports(Self, IUserInfo, UserInfo) then
      if UserInfo.Login <> '' then
        UserLogin := UserInfo.Login;

    SL.Values['Application Name'] := Format(ProgrammDescription + ' v.%s /%s/ [%s]',
      [PlatformFunctions.GetProgramVersion, UserLogin, ConnType]);
  end;

  function TryReplaceAppName(SL: TStringList): Boolean;
  var
    i: Integer;
  begin
    Result := False;

    for i := SL.Count - 1 downto 0 do
      if AnsiSameText(SL.Names[i], 'application name') then
      begin
        SL.Delete(i);
        AddAppName(SL);
        Result := True;
        Break;
      end;
  end;

var
  SL: TStringList;
begin
  if ConnectionString = '' then
  begin
    Result := '';
    exit;
  end;
  SL := TStringList.Create;
  try
    SL.Delimiter := ';';
    SL.StrictDelimiter := True;
    SL.DelimitedText := ConnectionString;

    if not TryReplaceAppName(SL) then
      AddAppName(SL);

    Result := SL.DelimitedText;
  finally
    SL.Free;
  end;
end;

procedure TGlobalServices.SetConnectionString(ConnectionStringType: TConnectionStringType; const Value: WideString);
begin
  FConnectionStrings[ConnectionStringType] := Value;
end;

procedure TGlobalServices.SetExternal(AExternalServices: IGlobalServices);
begin
  ReleaseLocalInstance;
  if Assigned(AExternalServices) then
    FInstance := AExternalServices;
end;

function TGlobalServices._AddRef: Integer;
begin
  Result := TInterlocked.Increment(FRefCount);
end;

function TGlobalServices._Release: Integer;
begin
  Result := TInterlocked.Decrement(FRefCount);
  if Result = 0 then
    Destroy;
end;

initialization

finalization

FinalizeGlobalServices;

end.
