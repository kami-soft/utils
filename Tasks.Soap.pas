unit Tasks.Soap;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Generics.Collections,
  Xml.XMLIntf,
  pbInput,
  pbOutput,
  Soap.Rio,
  Soap.OpConvertOptions,
  Soap.SOAPHTTPTrans,
  Soap.SOAPHTTPClient,
  OtlTaskControl,
  OtlParallel,
  Tasks.Settings,
  Tasks.Manager;

type
  TLoginData = record
    Login: string;
    Pass: string;

    procedure Load(Node: IXMLNode);
    procedure Save(Node: IXMLNode);
  end;

  TAbstractSpecificSOAPSettings = class(TAbstractSpecificSettings)
  strict private
    FURL: string;
    FRequestLoginData: TLoginData;
    FUserAuthData: TLoginData;
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer)
      : Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;

    procedure intLoad(Node: IXMLNode); override;
    procedure intSave(Node: IXMLNode); override;
  public
    property URL: string read FURL write FURL;
    property SOAPAuthData: TLoginData read FRequestLoginData;
    property UserAuthData: TLoginData read FUserAuthData;
  end;

  TAbstractSOAPTask = class(TAbstractTask)
  strict private
    FHTTPRIO: IInterface;

    procedure HTTPRIOHTTPWebNodeBeforePost(const HTTPReqResp: THTTPReqResp; Data: Pointer);
    procedure HTTPRIOBeforeExecute(const MethodName: string; SOAPRequest: TStream);
    procedure HTTPRIOAfterExecute(const MethodName: string; SOAPResponse: TStream);

    function GetHTTPRIO: IRIOAccess;
  strict protected
    procedure Init; override;
    procedure Fin; override;
  public
    class function GetSOAPURL: string; virtual; abstract;
    class function GetSOAPAuthData: TLoginData; virtual; abstract;
  public
    property HTTPRIO: IRIOAccess read GetHTTPRIO;
  end;

  TSOAPTaskClass = class of TAbstractSOAPTask;

implementation

uses
  System.DateUtils,
  Winapi.WinInet,
  Winapi.ActiveX,
  Winapi.Windows,
  System.SyncObjs,
  IdCoderMIME,
  uXMLFunctions,
  Vcl.Forms,
  uThreadSafeLogger,
  Monitoring.Informer,
  Utils.Conversation;

{ TAbstractSOAPTask }

procedure TAbstractSOAPTask.Fin;
begin
  FHTTPRIO := nil;
  inherited;
end;

function TAbstractSOAPTask.GetHTTPRIO: IRIOAccess;
begin
  if not Supports(FHTTPRIO, IRIOAccess, Result) then
    Result := nil;
end;

procedure TAbstractSOAPTask.HTTPRIOAfterExecute(const MethodName: string; SOAPResponse: TStream);
var
  s: string;
  Bytes: TBytes;
begin
  SetLength(Bytes, SOAPResponse.Size);
  SOAPResponse.Seek(0, soBeginning);
  SOAPResponse.Read(Bytes[0], SOAPResponse.Size);
  s := TEncoding.UTF8.GetString(Bytes);
  LogFileX.Log('SOAP ANSWER ON : ' + MethodName);
  LogFileX.Log(s);
end;

procedure TAbstractSOAPTask.HTTPRIOBeforeExecute(const MethodName: string; SOAPRequest: TStream);
var
  s: string;
  Bytes: TBytes;
  SoapRequestPosition: Int64;
begin
  SoapRequestPosition := SOAPRequest.Position;
  try
    SetLength(Bytes, SOAPRequest.Size);
    SOAPRequest.Seek(0, soBeginning);
    SOAPRequest.Read(Bytes[0], SOAPRequest.Size);
    s := TEncoding.UTF8.GetString(Bytes);
    LogFileX.Lock;
    try
      LogFileX.Log('SOAP REQUEST ON : ' + MethodName);
      LogFileX.Log(s);
    finally
      LogFileX.Unlock;
    end;
  finally
    SOAPRequest.Position := SoapRequestPosition;
  end;
end;

procedure TAbstractSOAPTask.HTTPRIOHTTPWebNodeBeforePost(const HTTPReqResp: THTTPReqResp; Data: Pointer);
var
  auth: string;
  AuthData: TLoginData;
begin
  AuthData := GetSOAPAuthData;
  auth := 'Authorization: Basic ' + TIdEncoderMIME.EncodeString(AuthData.Login + ':' + AuthData.Pass);
  HttpAddRequestHeaders(Data, PChar(auth), Length(auth), HTTP_ADDREQ_FLAG_ADD);
end;

procedure TAbstractSOAPTask.Init;
var
  tmpRIO: THTTPRIO;
begin
  inherited;
  FHTTPRIO := THTTPRIO.Create(nil);
  tmpRIO := HTTPRIO.Rio as THTTPRIO;
  tmpRIO.URL := GetSOAPURL;
  tmpRIO.HTTPWebNode.Agent := 'RIVC_PULKOVO';
  tmpRIO.HTTPWebNode.UseUTF8InHeader := True;
  tmpRIO.HTTPWebNode.InvokeOptions := [soIgnoreInvalidCerts { , soAutoCheckAccessPointViaUDDI } ];
  tmpRIO.HTTPWebNode.WebNodeOptions := [];
  tmpRIO.HTTPWebNode.OnBeforePost := HTTPRIOHTTPWebNodeBeforePost;
  tmpRIO.OnBeforeExecute := HTTPRIOBeforeExecute;
  tmpRIO.OnAfterExecute := HTTPRIOAfterExecute;
  tmpRIO.Converter.Options := [soSendMultiRefObj, soTryAllSchema, soRootRefNodesToBody, soCacheMimeResponse,
    soUTF8EncodeXML];
end;

{ TAbstractSpecificSettings }

procedure TAbstractSpecificSOAPSettings.intLoad(Node: IXMLNode);
begin
  inherited;
  FURL := GetNodeValueByName(Node.ChildNodes, 'SOAPURL');

  FRequestLoginData.Load(Node.ChildNodes.FindNode('AuthInfo'));
  FUserAuthData.Load(Node.ChildNodes.FindNode('UserInfo'));
end;

procedure TAbstractSpecificSOAPSettings.intSave(Node: IXMLNode);
begin
  inherited;
  Node.AddChild('SOAPURL').Text := FURL;
  FRequestLoginData.Save(Node.AddChild('AuthInfo'));
  FUserAuthData.Save(Node.AddChild('UserInfo'));
end;

function TAbstractSpecificSOAPSettings.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput;
  FieldNumber, WireType: integer): Boolean;
begin
  Result := inherited;
end;

procedure TAbstractSpecificSOAPSettings.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
end;

{ TLoginData }

procedure TLoginData.Load(Node: IXMLNode);
begin
  if not Assigned(Node) then
    exit;
  Login := GetNodeValueByName(Node.ChildNodes, 'Login');
  Pass := GetNodeValueByName(Node.ChildNodes, 'Pass');
end;

procedure TLoginData.Save(Node: IXMLNode);
begin
  Node.AddChild('Login').Text := Login;
  Node.AddChild('Pass').Text := Pass;
end;

end.
