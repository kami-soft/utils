unit Utils.AlertWindows;

interface

uses
  System.Classes,
  Vcl.Graphics,
  Winapi.ActiveX;

type
  IAlertWindow = interface;

  IAlertCloseCallback = interface
    ['{A049B03F-0E7F-42E3-8F34-06E91E775941}']
    procedure AlertClosed(Caller: IAlertWindow); safecall;
  end;

  IAlertButtonClickCallback = interface
    ['{8F7EDC87-1728-45C0-9E12-CC1CCAB26F2B}']
    procedure ButtonClick(Caller: IAlertWindow; ButtonIndex: Integer); safecall;
  end;

  IAlertCaptionButtonClickCallback = interface
    ['{9EF71673-35D0-4DCD-903C-77AEB21F71D6}']
    procedure CaptionButtonClick(Caller: IAlertWindow; Button: Integer; var AHandled: LongBool); safecall;
  end;

  IAlertOwnered = interface
    ['{796BB0AC-CD9E-46CC-B561-2013D1E8DEB0}']
    function GetOwner: IInterface; safecall;
    property Owner: IInterface read GetOwner;
  end;

  IAlertMessage = interface
    ['{19A1A7B5-2C55-48F4-8924-4ABAA5EEC3B4}']
    function GetCaption: WideString; safecall;
    procedure SetCaption(const AValue: WideString); safecall;

    function GetText: WideString; safecall;
    procedure SetText(const AValue: WideString); safecall;

    function GetImageIndex: Integer; safecall;
    procedure SetImageIndex(const AValue: Integer); safecall;

    function GetTag: Integer; safecall;
    procedure SetTag(const AValue: Integer); safecall;

    function GetTagPointer: Pointer; safecall;
    procedure SetTagPointer(const AValue: Pointer); safecall;

    property Caption: WideString read GetCaption write SetCaption;
    property Text: WideString read GetText write SetText;
    property ImageIndex: Integer read GetImageIndex write SetImageIndex;

    property Tag: Integer read GetTag write SetTag;
    property TagPointer: Pointer read GetTagPointer write SetTagPointer;
  end;

  IAlertMessageList = interface
    ['{551834D1-3FD5-4E92-8E85-177D7B021088}']
    function Count: Integer; safecall;
    function Add: IAlertMessage; safecall;
    function AddAndFill(const Caption, Text: WideString; ImageIndex: Integer): IAlertMessage; safecall;
    /// <returns>ALWAYS return NEW INTERFACE, not same as function Add!!!</returns>
    function Item(index: Integer): IAlertMessage; safecall;
    procedure Delete(index: Integer); safecall;
  end;

  IAlertButton = interface
    ['{E1051D82-C234-40E6-AD66-FE1047A94178}']

    // do not activate Enabled property!!!
    // Enabled used in TdxAlertWindowX to prevent
    // buttons use in click event handler.
    //function GetEnabled: LongBool; safecall;
    //procedure SetEnabled(const AValue: LongBool); safecall;

    function GetHint: WideString; safecall;
    procedure SetHint(const AValue: WideString); safecall;

    function GetImageIndex: Integer; safecall;
    procedure SetImageIndex(const AValue: Integer); safecall;

    function GetVisible: LongBool; safecall;
    procedure SetVisible(const AValue: LongBool); safecall;

    function GetLeftMargin: Integer; safecall;
    procedure SetLeftMargin(const AValue: Integer); safecall;

    function GetRightMargin: Integer; safecall;
    procedure SetRightMargin(const AValue: Integer); safecall;

    property Hint: WideString read GetHint write SetHint;
    property ImageIndex: Integer read GetImageIndex write SetImageIndex;
    property Visible: LongBool read GetVisible write SetVisible;

    property LeftMargin: Integer read GetLeftMargin write SetLeftMargin;
    property RightMargin: Integer read GetRightMargin write SetRightMargin;
  end;

  IAlertButtons = interface
    ['{7C6D6D7E-34CF-4351-9774-497F6C98179E}']
    function GetWidth: Integer; safecall;
    //procedure SetWidth(const AValue: integer); safecall;

    function GetHeight: Integer; safecall;
    //procedure SetHeight(const AValue: integer); safecall;

    function GetCount: Integer; safecall;
    function GetButton(AIndex: Integer): IAlertButton;

    procedure Clear; safecall;
    procedure Delete(AIndex: Integer); safecall;
    /// <remarks>
    /// DO NOT STORE return value!!!
    ///  This is dangerous: if window is closed, then this interface
    ///  will points to wrong data!
    /// </remarks>
    function Add: IAlertButton; safecall;

    property Width: Integer read GetWidth;
    property Height: Integer read GetHeight;

    property Count: Integer read GetCount;
    /// <returns>ALWAYS return NEW INTERFACE, not same as function Add!!!</returns>
    property Button[AIndex: Integer]: IAlertButton read GetButton;
  end;

  IAlertWindowOptionsText = interface
    ['{E5D74581-1C79-4892-AC12-00672C307D1C}']
    function GetAlignHorz: Integer; safecall;
    procedure SetAlignHorz(const AValue: Integer); safecall;

    function GetAlignVert: Integer; safecall;
    procedure SetAlignVert(const AValue: Integer); safecall;

    function GetFont: IFont; safecall;
    /// <returns>0 = Left, 1 = Right, 2 = Center</returns>
    property AlignHorz: Integer read GetAlignHorz write SetAlignHorz;
    /// <returns>0 = Top, 1 = Bottom, 2 = Center</returns>
    property AlignVert: Integer read GetAlignVert write SetAlignVert;
    property Font: IFont read GetFont;
  end;

  IAlertWindowOptionsMessage = interface
    ['{5CAD3054-5284-40DA-A30A-4E1A1DB8FC00}']
    function GetOptionsCaption: IAlertWindowOptionsText; safecall;
    function GetOptionsText: IAlertWindowOptionsText; safecall;

    property OptionsCaption: IAlertWindowOptionsText read GetOptionsCaption;
    property OptionsText: IAlertWindowOptionsText read GetOptionsText;
  end;

  IAlertWindowOptionsSize = interface
    ['{DA74966D-1B0C-4A54-AF13-A95577372173}']
    function GetAutoHeight: LongBool; safecall;
    procedure SetAutoHeight(const AValue: LongBool); safecall;

    function GetAutoSizeAdjustment: LongBool; safecall;
    procedure SetAutoSizeAdjustment(const AValue: LongBool); safecall;

    function GetAutoWidth: LongBool; safecall;
    procedure SetAutoWidth(const AValue: LongBool); safecall;

    function GetHeight: Integer; safecall;
    procedure SetHeight(const AValue: Integer); safecall;

    function GetMinHeight: Integer; safecall;
    procedure SetMinHeight(const AValue: Integer); safecall;

    function GetMaxHeight: Integer; safecall;
    procedure SetMaxHeight(const AValue: Integer); safecall;

    function GetWidth: Integer; safecall;
    procedure SetWidht(const AValue: Integer); safecall;

    function GetMinWidth: Integer; safecall;
    procedure SetMinWidth(const AValue: Integer); safecall;

    function GetMaxWidth: Integer; safecall;
    procedure SetMaxWidth(const AValue: Integer); safecall;

    property AutoHeight: LongBool read GetAutoHeight write SetAutoHeight;
    property AutoSizeAdjustment: LongBool read GetAutoSizeAdjustment write SetAutoSizeAdjustment;
    property AutoWidth: LongBool read GetAutoWidth write SetAutoWidth;

    property Height: Integer read GetHeight write SetHeight;
    property MinHeight: Integer read GetMinHeight write SetMinHeight;
    property MaxHeight: Integer read GetMaxHeight write SetMaxHeight;

    property Width: Integer read GetWidth write SetWidht;
    property MinWidth: Integer read GetMinWidth write SetMinWidth;
    property MaxWidth: Integer read GetMaxWidth write SetMaxWidth;
  end;

  IAlertWindowOptionsBehavior = interface
    ['{19ADA361-CE38-45D0-8AF4-AA77945A5186}']
    function GetCloseOnRightClick: LongBool; safecall;
    procedure SetCloseOnRightClick(const AValue: LongBool); safecall;

    function GetDisplayTime: Integer; safecall;
    procedure SetDisplayTime(const AValue: Integer); safecall;

    function GetScreenSnap: LongBool; safecall;
    procedure SetScreenSnap(const AValue: LongBool); safecall;

    function GetScreenSnapBuffer: Integer; safecall;
    procedure SetScreenSnapBuffer(const AValue: Integer); safecall;

    property CloseOnRightClick: LongBool read GetCloseOnRightClick write SetCloseOnRightClick;
    property DisplayTime: Integer read GetDisplayTime write SetDisplayTime;
    property ScreenSnap: LongBool read GetScreenSnap write SetScreenSnap;
    property ScreenSnapBuffer: Integer read GetScreenSnapBuffer write SetScreenSnapBuffer;
  end;

  IAlertWindowOptionsCaptionButtons = interface
    ['{B70158DE-B816-4989-B671-F1995930EEDD}']
    function GetPinButton: LongBool; safecall;
    procedure SetPinButton(const AValue: LongBool); safecall;

    function GetCloseButton: LongBool; safecall;
    procedure SetCloseButton(const AValue: LongBool); safecall;

    //property DropdownButton: LongBool read GetDropdownButton write SetDropdownButton;
    property PinButton: LongBool read GetPinButton write SetPinButton;
    property CloseButton: LongBool read GetCloseButton write SetCloseButton;
  end;

  IAlertWindowOptionsNavigationPanel = interface
    ['{561369EE-08CA-4B50-80BE-8B079E53E34C}']
    function GetDisplayMask: WideString; safecall;
    procedure SetDisplayMask(const AValue: WideString); safecall;

    function GetFont: IFont; safecall;

    function GetVisibility: Integer; safecall;
    procedure SetVisibility(const AValue: Integer); safecall;

    function GetWidth: Integer; safecall;
    procedure SetWidth(const AValue: Integer); safecall;

    property DisplayMask: WideString read GetDisplayMask write SetDisplayMask;
    property Font: IFont read GetFont;
    property Visibility: Integer read GetVisibility write SetVisibility;
    property Width: Integer read GetWidth write SetWidth;
  end;

  IAlertWindow = interface
    ['{3AC021C6-8222-441C-B18C-EAC8B7FACFF6}']
    procedure CloseAlert; safecall;
    function GetCategory: WideString; safecall;
    function GetActiveMessageIndex: Integer; safecall;
    procedure SetActiveMessageIndex(const AIndex: Integer); safecall;
    function GetIsPinned: LongBool; safecall;
    procedure SetIsPinned(const AValue: LongBool); safecall;

    function GetButtons: IAlertButtons; safecall;
    function GetMessages: IAlertMessageList; safecall;

    function GetOptionsBehavior: IAlertWindowOptionsBehavior; safecall;
    function GetOptionsCaptionButtons: IAlertWindowOptionsCaptionButtons; safecall;
    function GetOptionsMessage: IAlertWindowOptionsMessage; safecall;
    function GetOptionsNavigationPanel: IAlertWindowOptionsNavigationPanel; safecall;
    function GetOptionsSize: IAlertWindowOptionsSize; safecall;

    procedure BeginUpdate; safecall;
    procedure EndUpdate; safecall;

    procedure ResetDisplayTimer; safecall;

    procedure RegisterCloseCallback(Callback: IAlertCloseCallback); safecall;
    procedure UnregisterCloseCallback(Callback: IAlertCloseCallback); safecall;

    procedure RegisterButtonClickCallback(Callback: IAlertButtonClickCallback); safecall;
    procedure UnregisterButtonClickCallback(Callback: IAlertButtonClickCallback); safecall;

    procedure RegisterCaptionButtonClickCallback(Callback: IAlertCaptionButtonClickCallback); safecall;
    procedure UnregisterCaptionButtonClickCallback(Callback: IAlertCaptionButtonClickCallback); safecall;

    property Category: WideString read GetCategory;
    property ActiveMessageIndex: Integer read GetActiveMessageIndex write SetActiveMessageIndex;
    property IsPinned: LongBool read GetIsPinned write SetIsPinned;

    property Buttons: IAlertButtons read GetButtons;
    property Messages: IAlertMessageList read GetMessages;

    property OptionsBehavior: IAlertWindowOptionsBehavior read GetOptionsBehavior;
    property OptionsCaptionButtons: IAlertWindowOptionsCaptionButtons read GetOptionsCaptionButtons;
    property OptionsMessage: IAlertWindowOptionsMessage read GetOptionsMessage;
    property OptionsNavigationPanel: IAlertWindowOptionsNavigationPanel read GetOptionsNavigationPanel;
    property OptionsSize: IAlertWindowOptionsSize read GetOptionsSize;
  end;

  IAlertWindowManager = interface
    ['{B8EF3DFF-21C4-453F-9784-AC3D0D2517A0}']
    function AddMessage(const Category, Caption, Text: WideString; ImgIndex: Integer): IAlertMessage; safecall;
    /// <remarks>
    /// To create IStream use System.Classes.TStreamAdapter
    /// </remarks>
    function AddButtonImage(AImageStream: IStream): Integer; safecall;
    function AddMessageImage(AImageStream: IStream): Integer; safecall;

    function FindWindow(const Category: WideString): IAlertWindow; safecall;
  end;

  IAlertWindowManagerSettings = interface
    ['{5E894940-A2CF-4583-AC40-5002F35C5240}']
    function GetWindowsPosition: Integer; safecall;
    procedure SetWindowsPosition(const AValue: Integer); safecall;

    function GetWindowMaxCount: Integer; safecall;
    procedure SetWindowMaxCount(const AValue: Integer); safecall;

    /// <summary>
    /// (awpAuto=0, awpTopLeft, awpTopRight, awpBottomLeft, awpBottomRight)
    /// </summary>
    property WindowsPosition: Integer read GetWindowsPosition write SetWindowsPosition;
    property WindowMaxCount: Integer read GetWindowMaxCount write SetWindowMaxCount;
  end;

  TAlertWindowsManager = class
    class function Instance: IAlertWindowManager;

    class function FindOwnerOf(AChild: IInterface; AParent: TGUID): IInterface;

    class function AddGraphicToMessageImages(AGraphic: TGraphic): Integer; overload;
    class function AddGraphicToMessageImages(AGraphicStream: TStream): Integer; overload;
    class function AddGraphicToMessageImages(const AGraphicResourceName: string): Integer; overload;

    class function AddGraphicToButtonImages(AGraphic: TGraphic): Integer; overload;
    class function AddGraphicToButtonImages(AGraphicStream: TStream): Integer; overload;
  end;

implementation

uses
  Winapi.Windows,
  System.SysUtils,
  Utils.GlobalServices;

{ TAlertWindowsManager }

class function TAlertWindowsManager.AddGraphicToButtonImages(AGraphic: TGraphic): Integer;
var
  Stream: TStream;
  AlertManager: IAlertWindowManager;
begin
  Result := -1;
  AlertManager := Instance;
  if not Assigned(AlertManager) then
    exit;
  Stream := TMemoryStream.Create;
  try
    AGraphic.SaveToStream(Stream);
    Stream.Seek(0, soBeginning);

    Result := AddGraphicToButtonImages(Stream);
  finally
    Stream.Free;
  end;
end;

class function TAlertWindowsManager.AddGraphicToButtonImages(AGraphicStream: TStream): Integer;
var
  StreamAdapter: IStream;
  AlertManager: IAlertWindowManager;
begin
  Result := -1;
  AlertManager := Instance;
  if not Assigned(AlertManager) then
    exit;
  StreamAdapter := TStreamAdapter.Create(AGraphicStream);
  Result := AlertManager.AddButtonImage(StreamAdapter);
end;

class function TAlertWindowsManager.AddGraphicToMessageImages(const AGraphicResourceName: string): Integer;
var
  ResStream: TResourceStream;
begin
  ResStream := TResourceStream.Create(HInstance, AGraphicResourceName, RT_RCDATA);
  try
    ResStream.Seek(0, soBeginning);
    Result := TAlertWindowsManager.AddGraphicToMessageImages(ResStream);
  finally
    ResStream.Free;
  end;
end;

class function TAlertWindowsManager.AddGraphicToMessageImages(AGraphic: TGraphic): Integer;
var
  Stream: TStream;
  AlertManager: IAlertWindowManager;
begin
  Result := -1;
  AlertManager := Instance;
  if not Assigned(AlertManager) then
    exit;
  Stream := TMemoryStream.Create;
  try
    AGraphic.SaveToStream(Stream);
    Stream.Seek(0, soBeginning);

    Result := AddGraphicToMessageImages(Stream);
  finally
    Stream.Free;
  end;
end;

class function TAlertWindowsManager.AddGraphicToMessageImages(AGraphicStream: TStream): Integer;
var
  StreamAdapter: IStream;
  AlertManager: IAlertWindowManager;
begin
  Result := -1;
  AlertManager := Instance;
  if not Assigned(AlertManager) then
    exit;
  StreamAdapter := TStreamAdapter.Create(AGraphicStream);
  try
    Result := AlertManager.AddMessageImage(StreamAdapter);
  finally
    StreamAdapter := nil;
  end;
end;

class function TAlertWindowsManager.FindOwnerOf(AChild: IInterface; AParent: TGUID): IInterface;
var
  intf: IAlertOwnered;
  tmpOwner: IInterface;
begin
  Result := nil;

  if not Supports(AChild, IAlertOwnered, intf) then
    exit;

  tmpOwner := AChild;
  while Assigned(tmpOwner) do
  begin
    if Supports(tmpOwner, AParent, Result) then
      Break;

    if Supports(tmpOwner, IAlertOwnered, intf) then
      tmpOwner := intf.Owner
    else
      tmpOwner := nil;
  end;
end;

class function TAlertWindowsManager.Instance: IAlertWindowManager;
begin
  if not Supports(GlobalServices, IAlertWindowManager, Result) then
    Result := nil;
end;

end.
