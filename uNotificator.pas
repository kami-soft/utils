unit uNotificator;

interface

uses
  System.Classes,
  System.Variants,
  System.Generics.Defaults,
  System.Generics.Collections;

type
  TNotificationEvent<T> = procedure(Sender: TObject; Notification: T) of object;

  IAbstractNotificator<T> = interface(IUnknown)
    procedure RegisterNotification(Notificator: TNotificationEvent<T>);
    procedure UnregisterNotification(Notificator: TNotificationEvent<T>);

    procedure DoNotify(Notification: T);
  end;

  IFreeNotificator = interface(IAbstractNotificator<TObject>)
    ['{1E58E582-186B-4E52-A875-5F24121FAC74}']
  end;



  TChangeAction = (caAdd, caChange, caDelete);

  TNotificationStructure = record
    NotifyObject: TObject;
    Action: TChangeAction;
    AdditionalInfo: string;

    constructor Create(ANotifyObject: TObject; AAction: TChangeAction; AAdditionalInfo: string);
  end;

  IChangeNotificator = interface(IAbstractNotificator<TNotificationStructure>)
    ['{F194BAB9-7E72-4E6B-80BB-4279195CE0A0}']
  end;

  TAbstractNotificator<T> = class(TInterfacedObject, IAbstractNotificator<T>)
  strict private
    FOwner: TObject;
    FNotifications: TList<TNotificationEvent<T>>;
  protected
    procedure RegisterNotification(Notificator: TNotificationEvent<T>);
    procedure UnregisterNotification(Notificator: TNotificationEvent<T>);

    procedure DoNotify(Notification: T);

    function GetOwner: TObject;
  public
    constructor Create(AOwner: TObject);
    destructor Destroy; override;
  end;

  TFreeNotificator = class(TAbstractNotificator<TObject>, IFreeNotificator)
  public
    destructor Destroy; override;
  end;

  TChangeNotificator = class(TAbstractNotificator<TNotificationStructure>, IChangeNotificator)
  public
    destructor Destroy; override;
  end;

implementation

uses
  System.SysUtils;

{ TNotificationStructure }

constructor TNotificationStructure.Create(ANotifyObject: TObject; AAction: TChangeAction; AAdditionalInfo: string);
begin
  NotifyObject := ANotifyObject;
  Action := AAction;
  AdditionalInfo := AAdditionalInfo;
end;

{ TAbstractNotificator<T> }

constructor TAbstractNotificator<T>.Create(AOwner: TObject);
begin
  inherited Create;
  FNotifications := TList<TNotificationEvent<T>>.Create(TComparer<TNotificationEvent<T>>.Construct(
    function(const Left, Right: TNotificationEvent<T>): integer
    begin
      if NativeUInt(TMethod(Left).Code) > NativeUInt(TMethod(Right).Code) then
        Result := 1
      else
        if NativeUInt(TMethod(Left).Code) < NativeUInt(TMethod(Right).Code) then
          Result := -1
        else
          if NativeUInt(TMethod(Left).Data) > NativeUInt(TMethod(Right).Data) then
            Result := 1
          else
            if NativeUInt(TMethod(Left).Data) < NativeUInt(TMethod(Right).Data) then
              Result := -1
            else
              Result := 0;
    end));

  FOwner := AOwner;
end;

destructor TAbstractNotificator<T>.Destroy;
begin
  FreeAndNil(FNotifications);
  inherited;
end;

procedure TAbstractNotificator<T>.DoNotify(Notification: T);
var
  i: integer;
  Event: TNotificationEvent<T>;
begin
  for i := 0 to FNotifications.Count - 1 do
    begin
      Event := FNotifications[i];
      Event(FOwner, Notification);
    end;
end;

function TAbstractNotificator<T>.GetOwner: TObject;
begin
  Result := FOwner;
end;

procedure TAbstractNotificator<T>.RegisterNotification(Notificator: TNotificationEvent<T>);
begin
  if not FNotifications.Contains(Notificator) then
    FNotifications.Add(Notificator);
end;

procedure TAbstractNotificator<T>.UnregisterNotification(Notificator: TNotificationEvent<T>);
begin
  FNotifications.Remove(Notificator);
end;

{ TFreeNotificator }

destructor TFreeNotificator.Destroy;
begin
  DoNotify(GetOwner);
  inherited;
end;

{ TChangeNotificator }

destructor TChangeNotificator.Destroy;
begin
  DoNotify(TNotificationStructure.Create(GetOwner, caDelete, NULL));
  inherited;
end;

end.
