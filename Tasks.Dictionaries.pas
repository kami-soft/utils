unit Tasks.Dictionaries;

interface

uses
  System.Classes,
  System.Generics.Defaults,
  System.Generics.Collections;

type
  TAbstractDictionary = class(TObject)
  public
    class procedure RegisterSelf;
    constructor Create; virtual;

    procedure Init; virtual;
    procedure Fin; virtual;
    function InitOrder: Integer; virtual; abstract;
  end;

  TDictionaryClass = class of TAbstractDictionary;

  TTaskDictionaryContainer = class(TObjectList<TAbstractDictionary>)
  strict private
    procedure SortByOrder;

    procedure Init;
    procedure Fin;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Reload;
  end;

implementation

type
  TDictionaryFactory = class(TList<TDictionaryClass>)
  strict private
    class var FInstance: TDictionaryFactory;

    class destructor Destroy;
  public
    class function Instance: TDictionaryFactory;
  end;

  { TAbstractDictionary }

constructor TAbstractDictionary.Create;
begin
  inherited Create;
end;

procedure TAbstractDictionary.Fin;
begin
  // empty
end;

procedure TAbstractDictionary.Init;
begin
  //empty
end;

class procedure TAbstractDictionary.RegisterSelf;
begin
  TDictionaryFactory.Instance.Add(Self);
end;

{ TDictionaryFactory }

class destructor TDictionaryFactory.Destroy;
begin
  FInstance.Free;
  FInstance := nil;
end;

class function TDictionaryFactory.Instance: TDictionaryFactory;
begin
  if not Assigned(FInstance) then
    FInstance := Self.Create;
  Result := FInstance;
end;

{ TDictionaryContainer }

constructor TTaskDictionaryContainer.Create;
var
  i: Integer;
begin
  inherited Create(True);
  for i := 0 to TDictionaryFactory.Instance.Count - 1 do
    Add(TDictionaryFactory.Instance[i].Create);
  SortByOrder;
  Init;
end;

destructor TTaskDictionaryContainer.Destroy;
begin
  Fin;
  inherited;
end;

procedure TTaskDictionaryContainer.Fin;
var
  i: Integer;
begin
  for i := Count - 1 downto 0 do
    Items[i].Fin;
end;

procedure TTaskDictionaryContainer.Init;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    Items[i].Init;
end;

procedure TTaskDictionaryContainer.Reload;
begin
  Fin;
  Init;
end;

procedure TTaskDictionaryContainer.SortByOrder;
begin
  Sort(TComparer<TAbstractDictionary>.Construct(
        function(const Left, Right: TAbstractDictionary): Integer
    begin
      Result := Left.InitOrder - Right.InitOrder;
    end));
end;

end.
