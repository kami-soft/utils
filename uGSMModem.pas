unit uGSMModem;

interface

uses
  Windows,
  Classes;

type
  TDeleteSMSFlag = (dsfOnlyOne, dsfAllReaded, dsfAllReadedAndSended, dsfAllReadedSendedAndUnsended, dsfAll);

  TGSMModem = class(TObject)
  private
    FComHandle: THandle;
    FSMSNum: integer;

    function GetConnected: boolean;

    procedure InitCommTimeouts;
    procedure InitCommState;

    procedure SendToModem(const Data; const Length: Cardinal); overload;
    procedure SendToModem(const Data: string); overload;

    function ReceiveFromModem: string;

    function GetResponse: string;
    procedure GetATCmdOkResponse(const ErrorMsg: string); overload;
    procedure GetATCmdOkResponse(var Response: string; const ErrorMsg: string); overload;
    procedure GetATCmdlinefeedResponse(const ErrorMsg: string);
  public
    constructor Create;
    destructor Destroy; override;

    function Connect(ComPortName: string): boolean;
    procedure Disconnect;

    function SendSMS(PhoneNum: string; SMS: string): boolean;
    procedure DeleteSMS(Index: integer; MemStorage: string; Flag: TDeleteSMSFlag);

    procedure GetSMSCount(MemStorage: string; out CurrentCount: integer; out MaxCount: integer);

    procedure ListAllSMSinPDUMode(MessageList: TStrings; MemStorage: string);

    property Connected: boolean read GetConnected;
  end;

implementation

uses
  SysUtils,
  StrUtils,
  DateUtils;

const
  MaxSMSLen = 64;
  MaxSMSPartCount = 3;
  DefaultMemStorage = '"MT"';

function ucs2(s: string): string;
var
  i: integer;
  { k: integer;
    ac: AnsiString; }
begin
  { kami: ���, ������ ���� - �� �����. �����, ���� ������ ��������� ��������, �� ���������
    �������� � ������� ��� �������.
    }
  Result := '';
  for i := 1 to Length(s) do
    begin
      Result := Result + IntToHex(Word(s[i]), 4);
    end;
  {
    ac := AnsiString(s);
    for i := 1 to Length(ac) do
    begin
    k := ord(ac[i]);
    if k >= 192 then
    k := k + 1040 - 192;
    Result := Result + inttohex(k, 4);
    end; }
end;

function BuildSinglePDUMessage(aSMSAddress: string; aMessage: string): string;
var
  i: integer;
  nmes, m, tel: string;
begin
  { kami: ���, ������ ���� - �� �����...
    ����������� 100% �� ���, ��� ������� (�� ��������� � ����������) ����� ���
    � ����� TP-User-Data-Length � ��������� ������.
    }
  m := aMessage;
  // ���������� ������������ ���������� �����, �, ��������, ����� ����������� ����������
  // ��� ������������ ���������� ���� � ������ ������: �� "72 38 88 09 00 F1" � "27 83 88 90 00 1F".
  // ����� ����������� ������ �������, ������� � ��������� ����� �������� F.
  if Length(aSMSAddress) mod 2 = 1 then
    aSMSAddress := aSMSAddress + 'F';
  for i := 1 to Length(aSMSAddress) do
    if i mod 2 = 0 then
      tel := tel + aSMSAddress[i] + aSMSAddress[i - 1];

  nmes := '00'; // ����� ���������� � SMSC. ����� - 0 ��������, ��� ��� �������� ��� ������ �������������� ����� SMSC, ����������� � ��������. ���� ����� �������� ��������������. ��� ���������� ��������� ���� ����� ������ ���� ������! (�� ��� ����� ����� ��������������� ����, ����������� � ��������.
  nmes := nmes + '11'; // ������ ����� SMS-SUBMIT
  nmes := nmes + '00'; // TP-Message-Reference. �������� 0�00 ��������� �� ��, ��� � �������� ������ �������� ����������� ����� �������������� �����.
  // nmes := nmes + '0B'; // ����� ������ ���������� (11)
  nmes := nmes + IntToHex(Length(aSMSAddress), 2);
  nmes := nmes + '91'; // ���-������. (91 ��������� ������������� ������ ����������� ������, 81 - ������� ������).
  nmes := nmes + tel; // ���������� ����� ���������� � ������������� ������� � ����������� (46708251358). ���� ������� ����� �������� � ������� ������� (Type-of-Address ����� 81 ������ 91), �� ��� �������� ������ �������� ����� ���� �� ������������ 10 ������� (0x0A) � ������ ���� �� ������������ ��� 7080523185 (0708251358).
  nmes := nmes + '00'; // TP-PID. ������������� ���������
  nmes := nmes + '08'; // TP-DCS.
  nmes := nmes + 'A8'; // TP-Validity-Period. "AA" �������� 4 ���. ���� ����� �������� ��������������, ��. 4 � 3 ������� ������
  nmes := nmes + IntToHex(Length(m) * 2, 2); // TP-User-Data-Length. ����� ���������.
  nmes := nmes + ucs2(m); // Encode7bit(m); // TP-User-Data. ��� ������ ������������ ��������� "hellohello", ��������������� � 7 �����.
  Result := nmes;
end;

function BuildPDUMessagePart(aSMSAddress, aMessage: string; msgNum, msgPart: integer): string;
var
  i: integer;
  nmes, m, tel: string;
  fullPartCount: integer;
begin
  { kami: �������� �������� ���� ������� - ��������� ��������� � ���� http://www.programmersforum.ru/showthread.php?t=108961&page=3 ,
    � ������ - raxp, 28.12.2010, 16:11
    ���������� ��������� ������� BuildSinglePDUMessage (��.����).
    }

  Result := '';
  fullPartCount := Length(aMessage) div MaxSMSLen;
  if (Length(aMessage) mod MaxSMSLen) <> 0 then
    inc(fullPartCount);

  m := Copy(aMessage, 1 + (msgPart - 1) * MaxSMSLen, MaxSMSLen);
  if m = '' then
    exit;
  // ���������� ������������ ���������� �����, �, ��������, ����� ����������� ����������
  // ��� ������������ ���������� ���� � ������ ������: �� "72 38 88 09 00 F1" � "27 83 88 90 00 1F".
  // ����� ����������� ������ �������, ������� � ��������� ����� �������� F.
  if Length(aSMSAddress) mod 2 = 1 then
    aSMSAddress := aSMSAddress + 'F';
  tel := '';
  for i := 1 to Length(aSMSAddress) do
    if i mod 2 = 0 then
      tel := tel + aSMSAddress[i] + aSMSAddress[i - 1];

  nmes := '00'; // ����� ���������� � SMSC. ����� - 0 ��������, ��� ��� �������� ��� ������ �������������� ����� SMSC, ����������� � ��������. ���� ����� �������� ��������������. ��� ���������� ��������� ���� ����� ������ ���� ������! (�� ��� ����� ����� ��������������� ����, ����������� � ��������.
  nmes := nmes + '41'; // ������ ����� SMS-SUBMIT
  nmes := nmes + '00'; // TP-Message-Reference. �������� 0�00 ��������� �� ��, ��� � �������� ������ �������� ����������� ����� �������������� �����.
  // nmes := nmes + '0B'; // ����� ������ ���������� (11)
  nmes := nmes + IntToHex(Length(aSMSAddress), 2);
  nmes := nmes + '91'; // ���-������. (91 ��������� ������������� ������ ����������� ������, 81 - ������� ������).
  nmes := nmes + tel; // ���������� ����� ���������� � ������������� ������� � ����������� (46708251358). ���� ������� ����� �������� � ������� ������� (Type-of-Address ����� 81 ������ 91), �� ��� �������� ������ �������� ����� ���� �� ������������ 10 ������� (0x0A) � ������ ���� �� ������������ ��� 7080523185 (0708251358).
  nmes := nmes + '00'; // TP-PID. ������������� ���������
  nmes := nmes + '08'; // TP-DCS.
  // nmes := nmes + 'A8'; // TP-Validity-Period. "AA" �������� 4 ���. ���� ����� �������� ��������������, ��. 4 � 3 ������� ������
  nmes := nmes + IntToHex(Length(m) * 2 + 7, 2); // TP-User-Data-Length. ����� ���������.
  nmes := nmes + '060804'; // ��, ��������� �������
  nmes := nmes + IntToHex(msgNum, 4);
  nmes := nmes + IntToHex(fullPartCount, 2) + IntToHex(msgPart, 2); // ����� ����� ���������
  nmes := nmes + ucs2(m); // Encode7bit(m); // TP-User-Data. ��� ������ ������������ ��������� "hellohello", ��������������� � 7 �����.
  Result := nmes;
end;

function BuildPDUMessage(aSMSAddress, aMessage: string; msgNum, msgPart: integer): string;
begin
  if Length(aMessage) <= MaxSMSLen then
    begin
      if msgPart = 1 then
        Result := BuildSinglePDUMessage(aSMSAddress, aMessage)
      else
        Result := '';
    end
  else
    Result := BuildPDUMessagePart(aSMSAddress, aMessage, msgNum, msgPart);
end;

{ TGSMModem }

function TGSMModem.Connect(ComPortName: string): boolean;
begin
  if Connected then
    Disconnect;
  FComHandle := CreateFile(@ComPortName[1], GENERIC_READ or GENERIC_WRITE, 0, nil, OPEN_EXISTING, 0, 0);
  Result := FComHandle <> INVALID_HANDLE_VALUE;

  if Result then
    begin
      { init the timeout for serial }
      InitCommTimeouts;

      { init the comm State for serial }
      InitCommState;
    end;
end;

constructor TGSMModem.Create;
begin
  inherited Create;
  FComHandle := INVALID_HANDLE_VALUE;
end;

procedure TGSMModem.DeleteSMS(Index: integer; MemStorage: string; Flag: TDeleteSMSFlag);
begin
  if not Connected then
    raise EReadError.Create('Not Connected!');

  { to reset completely previous request }
  SendToModem(#27);

  { starting }
  SendToModem('AT'#13);
  GetATCmdOkResponse('AT command error');

  { storage }
  if MemStorage = '' then
    MemStorage := DefaultMemStorage
  else
    if MemStorage[1] <> '"' then
      MemStorage := '"' + MemStorage + '"';
  SendToModem('AT+CPMS=' + MemStorage + #13);
  GetATCmdOkResponse('AT+CPMS command error!');

  { Message Format }
  if Flag <> dsfOnlyOne then
    SendToModem('AT+CMGD=' + IntToStr(Index) + ',' + IntToStr(integer(Flag)) + #13)
  else
    SendToModem('AT+CMGD=' + IntToStr(Index) + #13);
  GetATCmdOkResponse('AT+CMGD command error!');
end;

destructor TGSMModem.Destroy;
begin
  Disconnect;
  inherited;
end;

procedure TGSMModem.Disconnect;
begin
  if Connected then
    begin
      CloseHandle(FComHandle);
      FComHandle := INVALID_HANDLE_VALUE;
    end;
end;

procedure TGSMModem.GetATCmdOkResponse(const ErrorMsg: string);
var
  aResponse: string;
begin
  GetATCmdOkResponse(aResponse, ErrorMsg);
end;

procedure TGSMModem.GetATCmdlinefeedResponse(const ErrorMsg: string);
var
  aResponse: string;
  MaxTime: TDateTime;
begin
  aResponse := AnsiUppercase(GetResponse);

  MaxTime := IncSecond(Now, 5);
  While (Pos('>'#32, aResponse) <= 0) do
    begin
      if (Pos(#13#10'ERROR'#13#10, aResponse) > 0) then
        raise EReadError.Create(ErrorMsg + ' (' + Trim(StringReplace(aResponse, #13#10, ' ', [rfReplaceALL])) + ')');
      aResponse := aResponse + AnsiUppercase(GetResponse);

      if Now > MaxTime then
        raise EAbort.Create('Timeout on GetATCmdlinefeedResponse');
    end;
end;

procedure TGSMModem.GetATCmdOkResponse(var Response: string; const ErrorMsg: string);
var
  aTmpErrorMsg: string;
  P1: integer;

  MaxTime: TDateTime;
begin
  Response := AnsiUppercase(GetResponse);

  MaxTime := IncSecond(Now, 5);
  While (Pos(#13#10'OK'#13#10, Response) = 0) do
    begin
      if (Pos('ERROR', Response) <> 0) then
        begin
          P1 := Pos(#13#10'+CMS ERROR:', Response);
          if P1 > 0 then
            aTmpErrorMsg := ErrorMsg + ' (' + Trim(Copy(Response, P1, Maxint)) + ')' // +CMS ERROR: 38
          else
            aTmpErrorMsg := ErrorMsg + '  ' + Response;
          raise EWriteError.Create(aTmpErrorMsg);
        end;
      Response := Response + AnsiUppercase(GetResponse);

      if Now > MaxTime then
        raise EAbort.Create('Timeout on GetATCmdOkResponse');
    end;
end;

function TGSMModem.GetConnected: boolean;
begin
  Result := FComHandle <> INVALID_HANDLE_VALUE;
end;

function TGSMModem.GetResponse: string;
var
  tmpRes: string;

  MaxTime: TDateTime;
begin
  Result := '';
  MaxTime := IncSecond(Now, 5);
  repeat
    tmpRes := ReceiveFromModem;
    Result := Result + tmpRes;

    if Now > MaxTime then
      raise EAbort.Create('Timeout on GetResponse');

  until tmpRes = '';
end;

procedure TGSMModem.GetSMSCount(MemStorage: string; out CurrentCount, MaxCount: integer);
const
  CPMSStartStr = '+CPMS:';
var
  Response: string;
  iPos: integer;
begin
  if not Connected then
    raise EReadError.Create('Not Connected!');

  CurrentCount := 0;
  MaxCount := 0;

  { to reset completely previous request }
  SendToModem(#27);

  { starting }
  SendToModem('AT'#13);
  GetATCmdOkResponse('AT command error');

  { storage }
  if MemStorage = '' then
    MemStorage := DefaultMemStorage
  else
    if MemStorage[1] <> '"' then
      MemStorage := '"' + MemStorage + '"';
  SendToModem('AT+CPMS=' + MemStorage + #13);
  GetATCmdOkResponse(Response, 'AT+CPMS command error!');

  Delete(Response, 1, Pos(CPMSStartStr, Response) + Length(CPMSStartStr));
  // response:=  Copy(response, Pos(CPMSStartStr, response)+Length(CPMSStartStr), Length(response));
  Response := Trim(StringReplace(Response, 'OK', '', [rfIgnoreCase]));

  iPos := Pos(',', Response);
  if iPos <> 0 then
    begin
      CurrentCount := StrToInt(Copy(Response, 1, iPos - 1));

      Delete(Response, 1, iPos);

      iPos := Pos(',', Response);
      if iPos <> 0 then
        MaxCount := StrToInt(Copy(Response, 1, iPos - 1))
      else
        CurrentCount := 0;
    end;

end;

procedure TGSMModem.InitCommState;
Var
  MyDCB: TDCB;
Begin
  GetCommState(FComHandle, MyDCB);
  MyDCB.BaudRate := 9600;
  MyDCB.Parity := NOPARITY;
  MyDCB.StopBits := ONESTOPBIT;
  MyDCB.ByteSize := 8;
  SetCommState(FComHandle, MyDCB);
end;

procedure TGSMModem.InitCommTimeouts;
var
  CommTimeouts: TCommTimeouts;
begin
  { A value of MAXDWORD, combined with zero values for both the ReadTotalTimeoutConstant and ReadTotalTimeoutMultiplier members,
    specifies that the read operation is to return immediately with the characters that have already been received, even if no
    characters have been received. }
  CommTimeouts.ReadIntervalTimeout := MAXDWORD;
  CommTimeouts.ReadTotalTimeoutMultiplier := 0;
  CommTimeouts.ReadTotalTimeoutConstant := 0;
  CommTimeouts.WriteTotalTimeoutMultiplier := 0;
  CommTimeouts.WriteTotalTimeoutConstant := 60000;
  SetCommTimeouts(FComHandle, CommTimeouts);
end;

procedure TGSMModem.ListAllSMSinPDUMode(MessageList: TStrings; MemStorage: string);
{ ----------------------------------- }
  procedure InternalFulfillLstMessage;
  var
    P1, P2: integer;
    aIndex: integer;
    str: string;
  begin
    { Receive Message }
    SendToModem('AT+CMGL=4'#13);
    GetATCmdOkResponse(str, 'AT+CMGL command error!');

    { fullfill aLstMessage }
    P1 := Pos('+CMGL: ', str);
    While P1 > 0 do
      begin
        inc(P1, 7);
        P2 := PosEx(',', str, P1);
        if P2 <= 0 then
          raise EReadError.Create('AT+CMGL parse error!');
        aIndex := StrToInt(Copy(str, P1, P2 - P1));
        P1 := PosEx(#13#10, str, P2);
        if P1 <= 0 then
          raise EReadError.Create('AT+CMGL parse error!');
        P1 := P1 + 2;
        P2 := PosEx(#13#10, str, P1);
        if P2 <= 0 then
          raise EReadError.Create('AT+CMGL parse error!');
        MessageList.Add(IntToStr(aIndex) + '=' + Trim(Copy(str, P1, P2 - P1)));
        P1 := PosEx('+CMGL: ', str, P2);
      end;
  end;

begin
  if not Connected then
    raise EReadError.Create('Not Connected!');

  { to reset completely previous request }
  SendToModem(#27);

  { starting }
  SendToModem('AT'#13);
  GetATCmdOkResponse('AT command error');

  { Message Format }
  SendToModem('AT+CMGF=0'#13); // pdu mode
  GetATCmdOkResponse('AT+CMGF command error!');

  { storage }
  if MemStorage = '' then
    MemStorage := DefaultMemStorage
  else
    if MemStorage[1] <> '"' then
      MemStorage := '"' + MemStorage + '"';
  SendToModem('AT+CPMS=' + MemStorage + #13);
  GetATCmdOkResponse('AT+CPMS command error!');

  { list the SMS }
  InternalFulfillLstMessage;
end;

function TGSMModem.ReceiveFromModem: string;
var
  NumberOfBytesRead: Dword;
  Buff: AnsiString;
begin
  Sleep(10);
  SetLength(Buff, 1024);
  try
    if not ReadFile(FComHandle, Buff[1], 1023, NumberOfBytesRead, nil) then
      raise EReadError.Create('Receive from modem');
    if NumberOfBytesRead <> 0 then
      Result := Copy(string(Buff), 1, NumberOfBytesRead)
    else
      Result := '';
  finally
    Buff := '';
  end;
end;

function TGSMModem.SendSMS(PhoneNum: string; SMS: string): boolean;

  procedure SendPDU(PDU: string);
  var
    str: string;
    aLength: integer;
  begin
    str := Copy(PDU, 1, 2);
    if not TryStrToInt(str, aLength) then
      aLength := 0;
    aLength := (Length(PDU) div 2) - aLength - 1;

    SendToModem('AT+CMGS=' + IntToStr(aLength) + #13);
    GetATCmdlinefeedResponse('AT+CMGS linefeed command error!');
    SendToModem(PDU + #26);
    GetATCmdOkResponse('AT+CMGS command error!');
  end;

var
  sMessagePart: string;
  msgPart: integer;
begin
  Result := False;
  if not Connected then
    exit;
  SendToModem(#27);
  { starting }
  SendToModem('AT'#13);
  GetATCmdOkResponse('AT command error');

  { Message Format }
  SendToModem('AT+CMGF=0'#13); // pdu mode
  GetATCmdOkResponse('AT+CMGF command error!');

  { Send Message }
  SMS := Copy(SMS, 1, MaxSMSLen * MaxSMSPartCount);

  msgPart := 1;
  While msgPart <= MaxSMSPartCount do
    begin
      sMessagePart := BuildPDUMessage(PhoneNum, SMS, FSMSNum, msgPart);
      if sMessagePart <> '' then
        begin
          SendPDU(sMessagePart);
          inc(msgPart);
        end
      else
        begin
          Result := msgPart > 1;
          Break;
        end;
    end;
  inc(FSMSNum);
end;

procedure TGSMModem.SendToModem(const Data: string);
var
  s: AnsiString;
begin
  if Length(Data) <> 0 then
    begin
      s := AnsiString(Data);
      SendToModem(s[1], Length(s));
    end;
end;

procedure TGSMModem.SendToModem(const Data; const Length: Cardinal);
var
  iPos: Cardinal;
  iWritten: Cardinal;
  Buf: PAnsiChar;

  MaxTime: TDateTime;
begin
  if Length <= 0 then
    exit;
  iPos := 0;
  Buf := @Data;

  MaxTime := IncSecond(Now, 5);
  While (iPos < (Length - 1)) and (MaxTime > Now) do
    begin
      Sleep(10);
      if not WriteFile(FComHandle, Buf[iPos], Length - iPos, iWritten, nil) then
        raise EWriteError.Create('Can`t write to COM port');
      iPos := iPos + iWritten;
    end;
end;

end.
