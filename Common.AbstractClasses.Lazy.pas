unit Common.AbstractClasses.Lazy;

interface

uses
  System.Classes,
  System.RTTI,
  System.Generics.Collections,
  System.TypInfo,
  pbInput,
  pbOutput,
  Common.AbstractClasses,
  Common.AbstractClasses.Batch;

type
  TLazyObject = class(TBatchObject)
  strict private
  type
    TLazyData = class(TAbstractData)
    strict private
      FName: string;
      FTypeKind: TTypeKind;
      FClassName: string;
      FValue: TValue;
      procedure SetValue(const Value: TValue);
    strict protected
      function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer)
        : Boolean; override;
      procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
    public
      class function CanSave(Value: TValue): Boolean;

      property name: string read FName write FName;
      property Value: TValue read FValue write SetValue;
    end;
  strict private
    FDataList: TDictionary<string, TValue>;
    function GetData(const AName: string): TValue;
    procedure SetData(const AName: string; const Value: TValue);

    procedure FreeAllData;
  strict protected
    function NormalizeName(const AName: string): string;

    function DoGetData(const AName: string): TValue; virtual;
    procedure FreeData(const AName: string); virtual;

    procedure BeforeLoad; override;
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer)
      : Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    constructor Create(AOwner: TObject); override;
    destructor Destroy; override;

    property Data[const AName: string]: TValue read GetData write SetData;
  end;

  TLazyClass = class of TLazyObject;

implementation

uses
  System.SysUtils;

{ TLazyObject }

procedure TLazyObject.BeforeLoad;
begin
  inherited;
  FreeAllData;
  FDataList.Clear;
end;

constructor TLazyObject.Create(AOwner: TObject);
begin
  inherited;
  FDataList := TDictionary<string, TValue>.Create;
end;

destructor TLazyObject.Destroy;
begin
  FreeAllData;
  FreeAndNil(FDataList);
  inherited;
end;

function TLazyObject.DoGetData(const AName: string): TValue;
begin
  Result := TValue.Empty;
end;

procedure TLazyObject.FreeAllData;
var
  s: string;
begin
  for s in FDataList.Keys do
    FreeData(s);
end;

procedure TLazyObject.FreeData(const AName: string);
var
  NormalizedName: string;
begin
  NormalizedName := NormalizeName(AName);
  if FDataList.ContainsKey(NormalizedName) then
    FDataList[NormalizedName] := TValue.Empty;
end;

function TLazyObject.GetData(const AName: string): TValue;
var
  NormalizedName: string;
begin
  NormalizedName := NormalizeName(AName);

  if not FDataList.TryGetValue(NormalizedName, Result) then
    Result := TValue.Empty;

  if Result.IsEmpty then
  begin
    Result := DoGetData(NormalizedName);
    if not Result.IsEmpty then
      FDataList.AddOrSetValue(NormalizedName, Result);
  end;
end;

function TLazyObject.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer): Boolean;
var
  Pair: TLazyData;
begin
  Result := inherited;
  if Result then
    exit;

  if FieldNumber = 10000 then
  begin
    Pair := TLazyData.Create(Self);
    try
      Pair.LoadFromBufAsField(ProtoBuf, FieldNumber);
      if not Pair.Value.IsEmpty then
        Data[Pair.name] := Pair.Value;
      Result := True;
    finally
      Pair.Free;
    end;
  end;

end;

function TLazyObject.NormalizeName(const AName: string): string;
begin
  Result := AnsiUpperCase(AName);
end;

procedure TLazyObject.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
var
  name: string;
  Value: TValue;
  Pair: TLazyData;
begin
  inherited;
  Pair := TLazyData.Create(Self);
  try
    for name in FDataList.Keys do
    begin
      Value := Data[name];
      if not Value.IsEmpty then
        if TLazyData.CanSave(Value) then
        begin
          Pair.name := name;
          Pair.Value := Value;
          Pair.SaveToBufAsField(ProtoBuf, 10000);
        end;
    end;
  finally
    Pair.Free;
  end;
end;

procedure TLazyObject.SetData(const AName: string; const Value: TValue);
var
  NormalizedName: string;
begin
  NormalizedName := NormalizeName(AName);

  if FDataList.ContainsKey(NormalizedName) then
    FreeData(NormalizedName);
  FDataList.AddOrSetValue(NormalizedName, Value);
end;

{ TLazyObject.TLazyData }

class function TLazyObject.TLazyData.CanSave(Value: TValue): Boolean;
begin
  case Value.Kind of
    tkInteger:
      Result := True;
    tkChar:
      Result := True;
    tkEnumeration:
      Result := True;
    tkFloat:
      Result := True;
    tkString:
      Result := True;
    tkClass:
      Result := Value.AsObject is TAbstractData;
    tkWChar:
      Result := True;
    tkLString:
      Result := True;
    tkWString:
      Result := True;
    tkInt64:
      Result := True;
    tkUString:
      Result := True;
  else
    Result := False;
  end;
end;

function TLazyObject.TLazyData.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer)
  : Boolean;
var
  ValueClass: TAbstractDataClass;
begin
  Result := inherited;
  if Result then
    exit;
  case FieldNumber of
    1:
      FName := ProtoBuf.readString;
    2:
      FTypeKind := TTypeKind(ProtoBuf.readInt32);
    3:
      FClassName := ProtoBuf.readString;
    4:
      case FTypeKind of
        tkInteger:
          FValue := ProtoBuf.readInt32;
        tkChar:
          FValue := ProtoBuf.readString;
        tkEnumeration:
          FValue := ProtoBuf.readInt32;
        tkFloat:
          FValue := ProtoBuf.readDouble;
        tkString:
          FValue := ProtoBuf.readString;
        tkClass:
          begin
            ValueClass := TAbstractDataClassFactory.Current.GetClassByName(FClassName);
            if Assigned(ValueClass) then
            begin
              FValue := ValueClass.Create(Owner);
              TAbstractData(FValue.AsObject).LoadFromBufAsField(ProtoBuf, FieldNumber);
            end
            else
              exit; // we should return False to skip this field data
          end;
        tkWChar:
          FValue := ProtoBuf.readString;
        tkLString:
          FValue := ProtoBuf.readString;
        tkWString:
          FValue := ProtoBuf.readString;
        tkInt64:
          FValue := ProtoBuf.readInt64;
        tkUString:
          FValue := ProtoBuf.readString;
      else
        raise ENotSupportedException.Create('TValue type kind not supported');
      end;
  end;
  Result := FieldNumber in [1 .. 4];
end;

procedure TLazyObject.TLazyData.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
const
  FieldValueNum = 4;
begin
  inherited;
  ProtoBuf.writeString(1, FName);
  ProtoBuf.writeInt32(2, integer(FTypeKind));
  if FValue.IsObject then
    if FValue.AsObject is TAbstractData then
    begin
      FClassName := FValue.AsObject.ClassName;
      ProtoBuf.writeString(3, FClassName);
    end;
  case FValue.Kind of
    tkInteger:
      ProtoBuf.writeInt32(FieldValueNum, FValue.AsInteger);
    tkChar:
      ProtoBuf.writeString(FieldValueNum, FValue.AsString);
    tkEnumeration:
      if FValue.IsType<Boolean> then
        ProtoBuf.writeBoolean(FieldValueNum, FValue.AsBoolean)
      else
        ProtoBuf.writeInt32(FieldValueNum, FValue.AsInteger);
    tkFloat:
      ProtoBuf.writeDouble(FieldValueNum, FValue.AsExtended);
    tkString:
      ProtoBuf.writeString(FieldValueNum, FValue.AsString);
    tkClass:
      if FValue.IsObject then
        if FValue.AsObject is TAbstractData then
        begin
          TAbstractData(FValue.AsObject).SaveToBufAsField(ProtoBuf, FieldValueNum);
        end;
    tkWChar:
      ProtoBuf.writeString(FieldValueNum, FValue.AsString);
    tkLString:
      ProtoBuf.writeString(FieldValueNum, FValue.AsString);
    tkWString:
      ProtoBuf.writeString(FieldValueNum, FValue.AsString);
    tkInt64:
      ProtoBuf.writeInt64(FieldValueNum, FValue.AsInt64);
    tkUString:
      ProtoBuf.writeString(FieldValueNum, FValue.AsString);
  else
    raise ENotSupportedException.Create('TValue type kind not supported');
  end;
end;

procedure TLazyObject.TLazyData.SetValue(const Value: TValue);
begin
  FValue := Value;
  FTypeKind := FValue.Kind;
end;

end.
