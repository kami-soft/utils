unit Tasks.GlobalServices.Classes;

interface

implementation

uses
  System.SysUtils,
  System.Classes,
  System.Generics.Defaults,
  System.Generics.Collections,
  Data.DB,
  Data.Win.ADODB,
  Winapi.ActiveX,
  Common.Types,
  Common.Interfaces,
  Utils.Conversation,
  Utils.GlobalServices,
  Utils.AirportInfo,
  Utils.ConnectionPool,
  Utils.DataPresentation,
  Dll.Common.Interfaces,
  Dll.Common.Manager,
  RDSParams.UserParams,
  Common.ProgramSpecificData,
  Tasks.Dictionaries,
  Tasks.GlobalData;

type
  TUserInfo = class(TInterfacedObject, IUserInfo)
  private
    FLogin: string;
  strict private
    FName: string;
    FName_E: string;
    FCOP: integer;
    function GetLogin: WideString; safecall;
    function GetName: WideString; safecall;
    function GetName_E: WideString; safecall;
    function GetCOP: integer; safecall;
  public
    procedure ReadFromDB(Conn: TADOConnection);
  end;

  TAirCoInfo = class(TInterfacedObject, IAirCoInfo)
  strict private
  type
    TSinglePrefixes = class(TDictionary<TAirCoPrefix, string>)
    public
      procedure ReadNRPrefixes(DS: TDataSet);
    end;

    TPrefixes = class(TObjectList<TSinglePrefixes>)
    public
      procedure ReadNRPrefixes(Conn: TADOConnection);
    end;
  strict private
    FClientID: integer;
    FID: integer;
    FFullName: WideString;
    FFullNameEng: WideString;
    FShortName: WideString;
    FShortNameEng: WideString;
    FTinyName: WideString;
    FTinyNameEng: WideString;

    FPrefixes: TPrefixes;

    FBasePorts: TList<integer>;
    FCountryCode: WideString;

    FLogo: TStream;

    function GetClientID: integer; safecall;
    function GetID: integer; safecall;
    function GetFullName: WideString; safecall;
    function GetFullNameEng: WideString; safecall;
    function GetShortName: WideString; safecall;
    function GetShortNameEng: WideString; safecall;
    function GetTinyName: WideString; safecall;
    function GetTinyNameEng: WideString; safecall;

    function GetPrefixCount: integer; safecall;
    function GetPrefix(i: integer; Prefix: TAirCoPrefix): WideString; safecall;

    function GetBasePortsCount: integer; safecall;
    function GetBasePortID(i: integer): integer; safecall;

    function GetCountryCode: WideString; safecall;

    function GetidOK: integer; safecall;
    function GetLogo: IStream; safecall;
  public
    constructor Create;
    destructor Destroy; override;

    procedure ReadFromDB(Conn: TADOConnection);
  end;

type
  TTaskDictionaries = class(TAbstractDictionary)
  strict private
    FRDSParamsUser: IRDSParamsUser;
    FRDSParamsApp: IRDSParamsApplication;
    FRDSParamsGlobal: IRDSParamsGlobal;

    procedure RegisterGlobalServices;
  public
    procedure Init; override;
    procedure Fin; override;
    function InitOrder: integer; override;
  end;

const
  cRDS154ConnStr =
    'Provider=SQLOLEDB.1;Password=pulkovo294;Persist Security Info=True;User ID=pulkovo;Initial Catalog=RDS;Data Source=srvr154';

  { TUserInfo }

function TUserInfo.GetCOP: integer;
begin
  Result := FCOP;
end;

function TUserInfo.GetLogin: WideString;
begin
  Result := FLogin;
end;

function TUserInfo.GetName: WideString;
begin
  Result := FName;
end;

function TUserInfo.GetName_E: WideString;
begin
  Result := FName_E;
end;

procedure TUserInfo.ReadFromDB(Conn: TADOConnection);
var
  Qry: TADOQuery;
begin
  if FLogin = '' then
    raise EInvalidOperation.Create('Empty login on TUserInfo.ReadFromDB');

  Qry := TADOQuery.Create(nil);
  try
    Qry.Connection := Conn;
    Qry.SQL.Text := 'SELECT TOP 1 u.name, u.id_user, u.name_e ' +
      'FROM dbo.ac_p_users u (nolock) LEFT JOIN dbo.ac_p_groups g (nolock) ON u.id_group = g.id_group ' +
      'WHERE u.[Active]=1 AND u.uname = :UserLogin';
    Qry.Parameters.ParamByName('UserLogin').Value := FLogin;
    try
      Qry.Open;
      try
        FName := Qry.FieldByName('name').AsString;
        FCOP := Qry.FieldByName('id_user').AsInteger;
        FName_E := Qry.FieldByName('name_e').AsString;
      finally
        Qry.Close;
      end;
    except
      on e: Exception do
      begin
        Conn.Connected := False;
        raise;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

{ TAirCoInfo }

constructor TAirCoInfo.Create;
begin
  inherited Create;
  FPrefixes := TPrefixes.Create;
  FBasePorts := TList<integer>.Create;
  FLogo := TMemoryStream.Create;
end;

destructor TAirCoInfo.Destroy;
begin
  FreeAndNil(FLogo);
  FreeAndNil(FBasePorts);
  FreeAndNil(FPrefixes);
  inherited;
end;

function TAirCoInfo.GetBasePortID(i: integer): integer;
begin
  if (i >= 0) and (i < FBasePorts.Count) then
    Result := FBasePorts[i]
  else
    Result := 0;
end;

function TAirCoInfo.GetBasePortsCount: integer;
begin
  Result := FBasePorts.Count;
end;

function TAirCoInfo.GetClientID: integer;
begin
  Result := FClientID;
end;

function TAirCoInfo.GetCountryCode: WideString;
begin
  Result := FCountryCode;
end;

function TAirCoInfo.GetFullName: WideString;
begin
  Result := FFullName;
end;

function TAirCoInfo.GetFullNameEng: WideString;
begin
  Result := FFullNameEng;
end;

function TAirCoInfo.GetID: integer;
begin
  Result := FID;
end;

function TAirCoInfo.GetidOK: integer;
begin
  Result := 0;
end;

function TAirCoInfo.GetLogo: IStream;
begin
  Result := TStreamAdapter.Create(FLogo, soReference);
end;

function TAirCoInfo.GetPrefix(i: integer; Prefix: TAirCoPrefix): WideString;
var
  SinglePrefixes: TSinglePrefixes;
begin
  if (i >= 0) and (i < FPrefixes.Count) then
  begin
    SinglePrefixes := FPrefixes[i];
    Result := SinglePrefixes[Prefix];
  end
  else
    Result := '';
end;

function TAirCoInfo.GetPrefixCount: integer;
begin
  Result := FPrefixes.Count;
end;

function TAirCoInfo.GetShortName: WideString;
begin
  Result := FShortName;
end;

function TAirCoInfo.GetShortNameEng: WideString;
begin
  Result := FShortNameEng;
end;

function TAirCoInfo.GetTinyName: WideString;
begin
  Result := FTinyName;
end;

function TAirCoInfo.GetTinyNameEng: WideString;
begin
  Result := FTinyNameEng;
end;

procedure TAirCoInfo.ReadFromDB(Conn: TADOConnection);
var
  SP: TADOStoredProc;
begin
  SP := TADOStoredProc.Create(nil);
  try
    SP.Connection := Conn;
    SP.ProcedureName := 'dbo.spGetAirCoInfo';

    SP.Open;
    try
      if not SP.Eof then
      begin
        FID := SP.FieldByName('id').AsInteger;
        FFullName := SP.FieldByName('FullRuName').AsString;
        FFullNameEng := SP.FieldByName('FullEngName').AsString;
        FShortName := SP.FieldByName('ShortRuName').AsString;
        FShortNameEng := SP.FieldByName('ShortEngName').AsString;
        FTinyName := SP.FieldByName('TinyRuName').AsString;
        FTinyNameEng := SP.FieldByName('TinyEngName').AsString;

        FCountryCode := SP.FieldByName('cc').AsString;

        FClientID := SP.FieldByName('ak').AsInteger; { 22.12.2011 }

        FLogo.Size := 0;
        if SP.FieldByName('img_logo').IsBlob then
          TBlobField(SP.FieldByName('img_logo')).SaveToStream(FLogo);
      end;
    finally
      SP.Close;
    end;

    SP.ProcedureName := 'dbo.spReadBaseAirports';
    SP.Open;
    try
      while not SP.Eof do
      begin
        FBasePorts.Add(SP.FieldByName('id_port').AsInteger);
        SP.Next;
      end;
    finally
      SP.Close;
    end;
  finally
    SP.Free;
  end;

  FPrefixes.ReadNRPrefixes(Conn);
end;

{ TAirCoInfo.TSinglePrefixes }

procedure TAirCoInfo.TSinglePrefixes.ReadNRPrefixes(DS: TDataSet);
var
  Prefix: TAirCoPrefix;
  sName, sValue: string;
begin
  Clear;
  if not DS.Eof then
  begin
    for Prefix := low(TAirCoPrefix) to high(TAirCoPrefix) do
    begin
      sName := TEnumeration<TAirCoPrefix>.ToString(Prefix);
      if Assigned(DS.FindField(sName)) then
        sValue := DS.FieldByName(sName).AsString
      else
        sValue := '';
      Add(Prefix, sValue);
    end;
  end;
end;

{ TAirCoInfo.TPrefixes }

procedure TAirCoInfo.TPrefixes.ReadNRPrefixes(Conn: TADOConnection);
var
  SP: TADOStoredProc;
  Prefixes: TSinglePrefixes;
begin
  Clear;
  SP := TADOStoredProc.Create(nil);
  try
    SP.Connection := Conn;
    SP.ProcedureName := 'spGetAirCoPrefixes';
    SP.Open;
    try
      while not SP.Eof do
      begin
        Prefixes := TSinglePrefixes.Create;
        Add(Prefixes);
        Prefixes.ReadNRPrefixes(SP);
        SP.Next;
      end;
    finally
      SP.Close;
    end;
  finally
    SP.Free;
  end;
end;

{ TSchedToOpsDictionaries }

procedure TTaskDictionaries.Fin;
var
  dllm: IDllManager;
  LGlobalServices: IGlobalServices;
  Obj: TObject;
begin
  inherited;

  LGlobalServices := GlobalServices;
  try
    if Supports(LGlobalServices, IDllManager, dllm) then
    begin
      dllm.UnloadInstances;
      dllm := nil;
    end;
  finally
    LGlobalServices := nil;
  end;

  TConnectionPool.Instance.DangerDeleteAll;

  FinalizeGlobalServices;

  Obj := TObject(FRDSParamsUser);
  FRDSParamsUser := nil;
  Obj.Free;

  Obj := TObject(FRDSParamsApp);
  FRDSParamsApp := nil;
  Obj.Free;

  Obj := TObject(FRDSParamsGlobal);
  FRDSParamsGlobal := nil;
  Obj.Free;
end;

procedure TTaskDictionaries.Init;
begin
  inherited;
  RegisterGlobalServices;
end;

function TTaskDictionaries.InitOrder: integer;
begin
  Result := low(integer); //-2147483648, to initialize first and finalize last
end;

procedure TTaskDictionaries.RegisterGlobalServices;
var
  LGlobalServices: IGlobalServices;
  LUserInfo: IUserInfo;
  LAircoInfo: IAirCoInfo;
  LProgramSpecificData: IProgramSpecificData;
begin
  {$IFNDEF CONSOLE_TESTRUNNER}
  GlobalConnection.MainConnectionString := TGlobalData.Instance.ConnectionString;
  GlobalConnection.ConnectionString[cstRDS] := TGlobalData.Instance.ConnectionString;
  {$ELSE}
  GlobalConnection.MainConnectionString := cRDS154ConnStr;
  GlobalConnection.ConnectionString[cstRDS] := cRDS154ConnStr;
  {$ENDIF}
  LGlobalServices := GlobalServices;
  if not Supports(LGlobalServices, IAirportInfoList) then
    LGlobalServices.RegisterService(IAirportInfoList, AirportInfoList);
  if not Supports(LGlobalServices, IPresentation) then
  begin
    TDllPresentation.Init;
    LGlobalServices.RegisterService(IPresentation, Presentation);
  end;

  if not Supports(LGlobalServices, IProgramSpecificData) then
  begin
    LProgramSpecificData := ProgramSpecificData;
    LGlobalServices.RegisterService(IProgramSpecificData, LProgramSpecificData);
  end;

  if not Supports(LGlobalServices, IUserInfo) then
  begin
    LUserInfo := TUserInfo.Create;
    LGlobalServices.RegisterService(IUserInfo, LUserInfo);
  end;

  if not Supports(LGlobalServices, IAirCoInfo) then
  begin
    LAircoInfo := TAirCoInfo.Create;
    TAirCoInfo(LAircoInfo).ReadFromDB(TConnectionPool.Instance.GetThreadConnection(cstMain));
    LGlobalServices.RegisterService(IAirCoInfo, LAircoInfo);
  end;

  if not Supports(LGlobalServices, IRDSParamsUser) then
  begin
    FRDSParamsUser := TRDSUserParameters.Create(nil);
    TAbstractParameters(FRDSParamsUser).Connection := TConnectionPool.Instance.GetThreadConnection(cstMain);
    FRDSParamsUser.LoadParametersData(LProgramSpecificData.ProgrammRegisteredName, LUserInfo.COP);
    LGlobalServices.RegisterService(IRDSParamsUser, FRDSParamsUser);
  end;

  if not Supports(LGlobalServices, IRDSParamsApplication) then
  begin
    FRDSParamsApp := TRDSApplicationParameters.Create(nil);
    TAbstractParameters(FRDSParamsApp).Connection := TConnectionPool.Instance.GetThreadConnection(cstMain);
    FRDSParamsApp.LoadParametersData(LProgramSpecificData.ProgrammRegisteredName, LUserInfo.COP);
    LGlobalServices.RegisterService(IRDSParamsApplication, FRDSParamsApp);
  end;

  if not Supports(LGlobalServices, IRDSParamsGlobal) then
  begin
    FRDSParamsGlobal := TRDSGlobalParameters.Create(nil);
    TAbstractParameters(FRDSParamsGlobal).Connection := TConnectionPool.Instance.GetThreadConnection(cstMain);
    FRDSParamsGlobal.LoadParametersData(LProgramSpecificData.ProgrammRegisteredName, LUserInfo.COP);
    LGlobalServices.RegisterService(IRDSParamsGlobal, FRDSParamsGlobal);
  end;

  if not Supports(LGlobalServices, IDllManager) then
    LGlobalServices.RegisterService(IDllManager, TDllManager.Create);
end;

initialization

TTaskDictionaries.RegisterSelf;

end.
