unit ServiceMainEx;
/// this is extension for standart TService
/// new TService can work with HandlerEx service function,
/// which allow to use much more functions, for example - user logon/logout, device connections, etc...

/// author: kami (c) 2015
/// you can use this code without any limitations.
/// The code provided "as is", wtihout warranty of any kind.
interface

uses
  Windows,
  WinSVC,
  SvcMgr;

type
  TsvcLanAgent = class(TService)
  private
    { Private declarations }
  protected
    function DoCustomControl(CtrlCode: DWord): Boolean; override;
    function DoCustomControlEx(dwControl: DWORD; W, L: DWORD): Boolean; override;
    function DoSessionChange(w: WParam; l: LParam): Boolean;
  public
    function GetServiceController: TServiceController; override;
    function GetServiceControllerEx: TServiceControllerEx; override;
    { Public declarations }
  end;

var
  svcLanAgent: TsvcLanAgent;

implementation
uses
  uConsts,
  uDesktopConnector;

{$R *.DFM}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  svcLanAgent.Controller(CtrlCode);
end;

function ServiceControllerEx(dwControl, dwEventType: DWORD; lpEventData, lpContext: LParam): BOOL; stdcall;
begin
  Result := svcLanAgent.ControllerEx(dwControl, dwEventType, lpEventData, lpContext);
end;

function TsvcLanAgent.DoCustomControl(CtrlCode: DWord): Boolean;
begin
  Result := True;
  if CtrlCode = 253 then
    begin
      DoStop;
    end;
  if CtrlCode = 252 then
    begin

    end;
end;

function TsvcLanAgent.DoCustomControlEx(dwControl, W, L: DWORD): Boolean;
begin
  Result := True;
  case dwControl of
    SERVICE_CONTROL_STOP:
      Result := DoStop;
    SERVICE_CONTROL_PAUSE:
      Result := DoPause;
    SERVICE_CONTROL_CONTINUE:
      Result := DoContinue;
    SERVICE_CONTROL_INTERROGATE:
      DoInterrogate;
    SERVICE_CONTROL_SHUTDOWN:
      DoShutdown;

    SERVICE_CONTROL_SESSIONCHANGE:
      Result := DoSessionChange(w, l);

    252:; // ��� �������, ������������ ���������� uninstaller-��
      //ProtectTimer.Enabled := False;

    253: // ��� �������, ������������ ������� uninstaller-��
          // ��� ��������� �������
      begin
        //ProtectTimer.Enabled := False;
        Result := DoStop;
      end;
  end;
end;

function TsvcLanAgent.DoSessionChange(w: WParam; l: LParam): Boolean;
begin
  Result := True;
  { ��� W ��������� ��������:
WTS_CONSOLE_CONNECT     =1 A session was connected to the console session.
WTS_CONSOLE_DISCONNECT    =2 A session was disconnected from the console session.
WTS_REMOTE_CONNECT        =3 A session was connected to the remote session.
WTS_REMOTE_DISCONNECT     =4 A session was disconnected from the remote session.
WTS_SESSION_LOGON         =5 A user has logged on to the session.
WTS_SESSION_LOGOFF        =6 A user has logged off the session.
WTS_SESSION_LOCK          =7 A session has been locked.
WTS_SESSION_UNLOCK        =8 A session has been unlocked.
WTS_SESSION_REMOTE_CONTROL=9
   }

   CurrentSessionID := l; // ��� �� ������������� � ServiceControllerEx
  // l=pWTSSESSION_NOTIFICATION(lpEventData).dwSessionID
  // ��� ��� ��� ��������� � ������ ���������� ����� PostThreadMessage
  // �� ��������� TWTSSESSION_NOTIFICATION ����������� �� ����,
  // ��� �� �� ������.
  DesktopConnector.SetActiveSession(l, w);
  //LogMessage('������ ��������� ������ � MainService');
end;

function TsvcLanAgent.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

function TsvcLanAgent.GetServiceControllerEx: TServiceControllerEx;
begin
  Result := ServiceControllerEx;
end;

end.

