unit uVirtualTreeViewHelper;

interface

uses
  System.Classes,
  System.Generics.Collections,
  VirtualTrees;

type
  TVTHelper = class helper for TBaseVirtualTree
  private
    function intGetObjectByNode(Node: PVirtualNode): TObject;
  public
    function InsertNodeWithObject(Node: PVirtualNode; Mode: TVTNodeAttachMode; Obj: TObject): PVirtualNode;
    function InsertNodeWithInt(Node: PVirtualNode; Mode: TVTNodeAttachMode; Value: Integer): PVirtualNode;
    function GetObjectByNode<T: class>(Node: PVirtualNode): T;
    function GetIntegerByNode(Node: PVirtualNode): Integer;

    procedure ExpandAll;
    procedure CollapseAll;

    procedure FixHeader;

    procedure DelayedFixHeader;
  end;

implementation

{ TVTHelper }

procedure TVTHelper.CollapseAll;
var
  Node: PVirtualNode;
begin
  BeginUpdate;
  try
    Node := GetLast;
    while Assigned(Node) do
      begin
        Expanded[Node] := False;
        Node := GetPrevious(Node);
      end;
  finally
    EndUpdate;
  end;
end;

procedure TVTHelper.DelayedFixHeader;
begin
  TThread.CreateAnonymousThread(
    procedure
    begin
      TThread.Sleep(200);
      TThread.Queue(nil,
        procedure
        begin
          if HandleAllocated then
            FixHeader
          else
            DelayedFixHeader;
        end);
    end).Start;
end;

procedure TVTHelper.ExpandAll;
var
  Node: PVirtualNode;
begin
  BeginUpdate;
  try
    Node := GetFirst;
    while Assigned(Node) do
      begin
        Expanded[Node] := True;
        Node := GetNext(Node);
      end;
  finally
    EndUpdate;
  end;
end;

procedure TVTHelper.FixHeader;
begin
  Header.Height := Header.Height + 1;
  Header.Height := Header.Height - 1;
end;

function TVTHelper.GetIntegerByNode(Node: PVirtualNode): Integer;
var
  NodeData: Pointer;
begin
  Result := 0;
  if not Assigned(Node) then
    exit;
  NodeData := GetNodeData(Node);
  Result := Integer(NodeData^);
end;

function TVTHelper.GetObjectByNode<T>(Node: PVirtualNode): T;
var
  Obj: TObject;
begin
  Obj := intGetObjectByNode(Node);
  if Obj is T then
    Result := T(Obj)
  else
    Result := nil;
end;

function TVTHelper.InsertNodeWithInt(Node: PVirtualNode; Mode: TVTNodeAttachMode; Value: Integer): PVirtualNode;
begin
  Result := InsertNode(Node, Mode, Pointer(Value));
end;

function TVTHelper.InsertNodeWithObject(Node: PVirtualNode; Mode: TVTNodeAttachMode; Obj: TObject): PVirtualNode;
begin
  Result := InsertNode(Node, Mode, Obj);
end;

function TVTHelper.intGetObjectByNode(Node: PVirtualNode): TObject;
var
  NodeData: Pointer;
begin
  Result := nil;
  if not Assigned(Node) then
    exit;
  NodeData := GetNodeData(Node);
  if Assigned(NodeData) then
    Result := TObject(NodeData^);
end;

end.
