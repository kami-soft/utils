unit iFD.LightTimesClasses;

interface

uses
  System.Classes,
  System.Generics.Defaults,
  System.Generics.Collections,
  Xml.XMLIntf,
  iFD.CommonClasses;

type
  TLightTimeType = (ltDawn, ltSunrise, ltSunset, ltDark);

  TLightTimes = class(TObject)
  strict private
    FDark: TDateTime;
    FDawn: TDateTime;
    FSunrise: TDateTime;
    FSunset: TDateTime;

    FLight, FSun: Integer;

    function GetLightTime(LightTimeType: TLightTimeType): TDateTime;
  private
    function GetIsLightTimeChanged(LightTimeType: TLightTimeType): Boolean;
  public
    constructor Create; overload;
    constructor Create(ADawn, ASunrise, ASunset, ADark: TDateTime; ALight: Integer = 0; ASun: Integer = 0); overload;

    procedure Fill(ADawn, ASunrise, ASunset, ADark: TDateTime; ALight: Integer = 0; ASun: Integer = 0);

    procedure Load(ANode: IXMLNode);
    procedure Save(ANode: IXMLNode);

    procedure FillByAPData(APData: TAPDataX);

    property LightTime[LightTimeType: TLightTimeType]: TDateTime read GetLightTime;
    property IsLightTimeChanged[LightTimeType: TLightTimeType]: Boolean read GetIsLightTimeChanged;
  end;

  TLightTimesList = class(TObjectList<TLightTimes>)
  public
    procedure Assign(List: TLightTimesList);

    procedure Load(ANode: IXMLNode);
    procedure Save(ANode: IXMLNode);

    procedure DoSort;

    function GetNearestLightTimesForDate(dt: TDateTime): TLightTimes;

    procedure FillByAPData(APData: TAPDataX; DateDelta: Integer);
  end;

  // ������� ������ ����������, � ����� ������ �������
  // ���������� ������� � �������� ������������� ���������
function CalculateCrossTimeByLightLists(LightType: TLightTimeType; const StartTime, EndTime: TDateTime;
  StartAPTimes, EndAPTimes: TLightTimesList): TDateTime;
function CalculateCrossTime(LightType: TLightTimeType; const StartTime, EndTime: TDateTime;
  StartAPData, EndAPData: TAPDataX): TDateTime;

/// ����� ������� ������ ����� ������� �������� ������� �������
///  � ������������ ������� ������� uAirportInfo.InitAirportInfo(RDSConnStr);
function NightTime(const StartTime, EndTime: TDateTime; StartAPData, EndAPData: TAPDataX): TDateTime; overload;
{$IFDEF MSWINDOWS}
function NightTime(const StartTime, EndTime: TDateTime; const APName1, APName2: string): TDateTime; overload;
function TotalNightTime(const EngStart, ATOT, ATL, EngStop: TDateTime; const APName1, APName2: string): TDateTime;
{$ENDIF}

implementation

uses
  System.SysUtils,
  System.DateUtils,
  Xml.XMLDoc,
  uXMLFunctions,
  iFD.CommonFunctions{$IFDEF MSWINDOWS},
  Utils.AirportInfo{$ENDIF};

type
  TLightTimePair = record
    dTime1, dTime2: TDateTime;
  end;

  TLightTimePairList = class(TList<TLightTimePair>)
  public
    procedure FillFromAPTimes(LightType: TLightTimeType; APTimes1, APTimes2: TLightTimesList);
  end;

function CalculateCrossTimeByLightLists(LightType: TLightTimeType; const StartTime, EndTime: TDateTime;
  StartAPTimes, EndAPTimes: TLightTimesList): TDateTime;
var
  LightTimePairList: TLightTimePairList;
  i: Integer;
  tmpRes: TDateTime;
begin
  {
    1. ������� ���� ������ ���������� ������� LightType (�������, ������������ < ��� �� 12 �����)
    2. ������� ����� ����������� �� ������ ����
  }
  Result := 0;
  LightTimePairList := TLightTimePairList.Create;
  try
    LightTimePairList.FillFromAPTimes(LightType, StartAPTimes, EndAPTimes);

    for i := 0 to LightTimePairList.Count - 1 do
    begin
      tmpRes := iFD.CommonFunctions.LightTimeCross(StartTime, EndTime, LightTimePairList[i].dTime1,
        LightTimePairList[i].dTime2);
      if not SameDateTime(tmpRes, 0) then
      begin
        Result := tmpRes;
        Break;
      end;
    end;
  finally
    LightTimePairList.Free;
  end;
end;

function CalculateCrossTime(LightType: TLightTimeType; const StartTime, EndTime: TDateTime;
  StartAPData, EndAPData: TAPDataX): TDateTime;
var
  StartAPTimes: TLightTimesList;
  EndAPTimes: TLightTimesList;

  SourceAP1Date, SourceAP2Date: TDateTime;
begin
  // ������� ������ ����������, � ����� ������ �������
  // ���������� ������� � �������� ������������� ���������
  /// ��������:
  /// 1. �������� �� �� ������, ���� ����� ���� LightTime, �������� � ������� ������
  /// 2. ���� �� ������� - �� �� ��������, �� �� �� �������.
  /// 3. ���� ���� ���������� �������. ���� - ����� ���������������� ��, ��������� �� ����� ��� �� 12 �����
  /// 4. �������� ����������.

  SourceAP1Date := StartAPData.APDate;
  SourceAP2Date := EndAPData.APDate;
  try
    StartAPData.APDate := DateOf(StartTime);
    EndAPData.APDate := DateOf(EndTime);

    StartAPTimes := TLightTimesList.Create;
    EndAPTimes := TLightTimesList.Create;
    try
      StartAPTimes.FillByAPData(StartAPData, 5);
      EndAPTimes.FillByAPData(EndAPData, 5);

      Result := CalculateCrossTimeByLightLists(LightType, StartTime, EndTime, StartAPTimes, EndAPTimes);
    finally
      EndAPTimes.Free;
      StartAPTimes.Free;
    end;
  finally
    EndAPData.APDate := SourceAP2Date;
    StartAPData.APDate := SourceAP1Date;
  end;
end;

type
  TTypedLightTime = record
    dt: TDateTime;
    LightType: TLightTimeType;
    IsATOT: Boolean; // ��, ���������. �� ������� ��� �������� � TLightTimeType
    IsATL: Boolean; // �� ��� ������ �� �����.
  end;

  TTypedLightTimeList = class(TList<TTypedLightTime>)
  public
    procedure DoSort;
  end;

function NightTime(const StartTime, EndTime: TDateTime; StartAPData, EndAPData: TAPDataX): TDateTime;
var
  TypedLightTime: TTypedLightTime;
  TypedLightTimeList: TTypedLightTimeList;

  StartAPTimesList: TLightTimesList;
  tmpDT: TDateTime;
  i: Integer;
  ltt: TLightTimeType;
  isStarted: Boolean;
begin
  Result := 0;

  // 1. ������� ������, � ���� �������:
  // ����� ������, �����������
  // ��� ������� ���������, ��������, ������� � ������� �� ������ �� ������� ������!!!
  // 2. ��������� ��� ��� �� �����������
  // 3. �������� �� ������, ��������� ���� "�������� TLightType", ��������
  // � ��������� �������� ����� ������� �������� � ����������, ������� �� ������� ������
  // � ���������� �������� �������.

  TypedLightTimeList := TTypedLightTimeList.Create;
  try
    // ������� ����� ������
    TypedLightTime.dt := StartTime;
    TypedLightTime.LightType := low(TLightTimeType);
    TypedLightTime.IsATOT := True;
    TypedLightTime.IsATL := False;
    TypedLightTimeList.Add(TypedLightTime);

    // ����� �����������
    TypedLightTime.dt := EndTime;
    TypedLightTime.LightType := low(TLightTimeType);
    TypedLightTime.IsATOT := False;
    TypedLightTime.IsATL := True;
    TypedLightTimeList.Add(TypedLightTime);

    // ������� �������� ������������� ��������� ��������� ������,
    // �� ������ ��, ������� ������ ������� ������, ���� �� ������ �
    // ��������� �����������.
    StartAPTimesList := TLightTimesList.Create;
    try
      tmpDT := StartAPData.APDate;
      try
        StartAPData.APDate := StartTime;
        StartAPTimesList.FillByAPData(StartAPData, 5);
      finally
        StartAPData.APDate := tmpDT;
      end;

      for i := 0 to StartAPTimesList.Count - 1 do
        for ltt := low(TLightTimeType) to high(TLightTimeType) do
        begin
          TypedLightTime.dt := StartAPTimesList[i].LightTime[ltt];
          TypedLightTime.LightType := ltt;
          TypedLightTime.IsATOT := False;
          TypedLightTime.IsATL := False;
          if (not SameDateTime(TypedLightTime.dt, 0)) and (CompareDateTime(TypedLightTime.dt, StartTime) <= 0) then
            TypedLightTimeList.Add(TypedLightTime);
        end;
    finally
      StartAPTimesList.Free;
    end;

    // ������� ��� ����������� � ��������� ������������� ���������
    for ltt := low(TLightTimeType) to high(TLightTimeType) do
    begin
      tmpDT := CalculateCrossTime(ltt, StartTime, EndTime, StartAPData, EndAPData);
      if not SameDateTime(tmpDT, 0) then
      begin
        TypedLightTime.dt := tmpDT;
        TypedLightTime.LightType := ltt;
        TypedLightTime.IsATOT := False;
        TypedLightTime.IsATL := False;
        TypedLightTimeList.Add(TypedLightTime);
      end;
    end;

    TypedLightTimeList.DoSort;

    // �������� �� ������������� ������.
    // ���������� ������� ���������, � �� ����������� ��������
    //
    ltt := low(TLightTimeType);
    isStarted := False;
    for i := 1 to TypedLightTimeList.Count - 1 do
    begin
      if TypedLightTimeList[i - 1].IsATOT then
        isStarted := True;

      if not TypedLightTimeList[i - 1].IsATOT and not TypedLightTimeList[i - 1].IsATL then
        ltt := TypedLightTimeList[i - 1].LightType;

      if isStarted and (ltt = ltDark) then
        Result := Result + (TypedLightTimeList[i].dt - TypedLightTimeList[i - 1].dt);

      if TypedLightTimeList[i].IsATL then
        Break;
    end;
  finally
    TypedLightTimeList.Free;
  end;
end;

{$IFDEF MSWINDOWS}

function NightTime(const StartTime, EndTime: TDateTime; const APName1, APName2: string): TDateTime; overload;
var
  AP: IAirportInfo;
  APInfoList: IAirportInfoList;
  StartAPData, EndAPData: TAPDataX;
begin
  /// ����� ������� �������� ������� �������
  ///  � ������������ ������� ������� uAirportInfo.InitAirportInfo(RDSConnStr);
  ///
  Result := 0;
  StartAPData := TAPDataX.Create;
  try
    EndAPData := TAPDataX.Create;
    try
      APInfoList := AirportInfoList;
      AP := APInfoList.FindAPByAnyName(APName1);
      if Assigned(AP) then
        StartAPData.Fill(APName1, StartTime, AP.Latitude, AP.Longitude)
      else
        exit;
      AP := APInfoList.FindAPByAnyName(APName2);
      if Assigned(AP) then
        EndAPData.Fill(APName2, StartTime, AP.Latitude, AP.Longitude)
      else
        exit;

      Result := NightTime(StartTime, EndTime, StartAPData, EndAPData);
    finally
      EndAPData.Free;
    end;
  finally
    StartAPData.Free;
  end;
end;

function TotalNightTime(const EngStart, ATOT, ATL, EngStop: TDateTime; const APName1, APName2: string): TDateTime;
begin
  /// ����� ������� ������� �������� ������� �������
  ///  � ������������ ������� ������� uAirportInfo.InitAirportInfo(RDSConnStr);
  Result := NightTime(EngStart, ATOT, APName1, APName1) + NightTime(ATOT, ATL, APName1, APName2) +
    NightTime(ATL, EngStop, APName2, APName2);
end;
{$ENDIF}
{ TLightTimes }

constructor TLightTimes.Create;
begin
  Create(0, 0, 0, 0);
end;

constructor TLightTimes.Create(ADawn, ASunrise, ASunset, ADark: TDateTime; ALight, ASun: Integer);
begin
  inherited Create;
  Fill(ADawn, ASunrise, ASunset, ADark, ALight, ASun);
end;

procedure TLightTimes.Fill(ADawn, ASunrise, ASunset, ADark: TDateTime; ALight, ASun: Integer);
begin
  FDawn := ADawn;
  FSunrise := ASunrise;
  FSunset := ASunset;
  FDark := ADark;

  FLight := ALight;
  FSun := ASun;
end;

procedure TLightTimes.FillByAPData(APData: TAPDataX);
var
  Dawn, Sunrise, Sunset, Dark: TDateTime;
  Light, Sun: Integer;
begin
  Solaris(APData.APDate, APData.Latitude, APData.Longitude, Dawn, Sunrise, Sunset, Dark, Light, Sun);
  Fill(Dawn, Sunrise, Sunset, Dark, Light, Sun);
end;

function TLightTimes.GetIsLightTimeChanged(LightTimeType: TLightTimeType): Boolean;
begin
  Result := False;
  case LightTimeType of
    ltDawn:
      Result := FLight = 0; // ���������� ����� ��� � ���� ��� ������
    // �� ��������, �� ���� ���������� ����
    ltSunrise:
      Result := FSun = 0;
    // ���������� ����� ��� � ����
    // ��� ������ ������� �� ��������, �� �� ���� ��������� ����� �������� ����
    ltSunset:
      Result := FSun = 0;
    // ���������� ����� ��� � ����
    // ��� ������ ������� �� ��������, �� �� ���� ��������� ����� �������� ����
    ltDark:
      Result := FLight = 0; // ���������� ����� ��� � ���� ��� ������
    // �� ��������, �� ���� ���������� ����
  end;
end;

function TLightTimes.GetLightTime(LightTimeType: TLightTimeType): TDateTime;
begin
  Result := 0;
  if IsLightTimeChanged[LightTimeType] then
    case LightTimeType of
      ltDawn:
        Result := FDawn;
      ltSunrise:
        Result := FSunrise;
      ltSunset:
        Result := FSunset;
      ltDark:
        Result := FDark;
    end;
end;

procedure TLightTimes.Load(ANode: IXMLNode);
begin
  { <row dt="2015-04-27T10:00:00"
    dawn="2015-04-27T01:21:20.553"
    sunrise="2015-04-27T02:11:18.160"
    sunset="2015-04-27T17:41:44.570"
    dark="2015-04-27T18:31:42.173"
    light="0" sun="0"/>
  }
  FDawn := StrToDateTimeDef(GetAttributeValue(ANode, 'dawn'), 0, NormalizedFormatSettings);
  FSunrise := StrToDateTimeDef(GetAttributeValue(ANode, 'sunrise'), 0, NormalizedFormatSettings);
  FSunset := StrToDateTimeDef(GetAttributeValue(ANode, 'sunset'), 0, NormalizedFormatSettings);
  FDark := StrToDateTimeDef(GetAttributeValue(ANode, 'dark'), 0, NormalizedFormatSettings);

  FLight := StrToIntDef(GetAttributeValue(ANode, 'light'), 0);
  FSun := StrToIntDef(GetAttributeValue(ANode, 'sun'), 0);
end;

procedure TLightTimes.Save(ANode: IXMLNode);
begin
  // <row dt="2015-04-16T10:00:00" dawn="2015-04-16T01:57:14.787" sunrise="2015-04-16T02:42:57.150" sunset="2015-04-16T17:14:38.447" dark="2015-04-16T18:00:20.810"/>
  CreateAttribute(ANode, 'dawn', DateTimeToStr(FDawn, NormalizedFormatSettings));
  CreateAttribute(ANode, 'sunrise', DateTimeToStr(FSunrise, NormalizedFormatSettings));
  CreateAttribute(ANode, 'sunset', DateTimeToStr(FSunset, NormalizedFormatSettings));
  CreateAttribute(ANode, 'dark', DateTimeToStr(FDark, NormalizedFormatSettings));

  CreateAttribute(ANode, 'light', IntToStr(FLight));
  CreateAttribute(ANode, 'sun', IntToStr(FSun));
end;

{ TLightTimesList }

procedure TLightTimesList.Assign(List: TLightTimesList);
var
  Xml: IXMLDocument;
  Node: IXMLNode;
begin
  if Assigned(List) then
  begin
    Xml := NewXMLDocument;
    Node := Xml.AddChild('sol');
    List.Save(Node);
    Load(Node);
  end
  else
    Clear;
end;

procedure TLightTimesList.DoSort;
begin
  Sort(TComparer<TLightTimes>.Construct(
        function(const Left, Right: TLightTimes): Integer
    begin
      Result := CompareDateTime(Left.LightTime[ltDawn], Right.LightTime[ltDawn]);
    end));
end;

procedure TLightTimesList.FillByAPData(APData: TAPDataX; DateDelta: Integer);
var
  SourceDate: TDateTime;
  Times: TLightTimes;
  i: Integer;
begin
  SourceDate := APData.APDate;
  Clear;

  for i := -DateDelta to DateDelta do
  begin
    APData.APDate := IncDay(SourceDate, i);
    Times := TLightTimes.Create;
    Add(Times);
    Times.FillByAPData(APData);
  end;

  APData.APDate := SourceDate;
end;

function TLightTimesList.GetNearestLightTimesForDate(dt: TDateTime): TLightTimes;
var
  Closest: Int64;
  i: Integer;
begin
  /// 1. ������� ����� �������, ��� ������� < dt < �������
  /// 2. ���� ��� - ����� ���������, � �������� dt < �������

  Result := nil;
  for i := 0 to Count - 1 do
    if (CompareDateTime(Items[i].LightTime[ltDawn], dt) < 0) and (CompareDateTime(dt, Items[i].LightTime[ltDark]) < 0)
    then
    begin
      Result := Items[i];
      Break;
    end;

  if not Assigned(Result) then
  begin
    Closest := 0;
    for i := 0 to Count - 1 do
      if CompareDateTime(dt, Items[i].LightTime[ltDawn]) < 0 then
        if (Closest = 0) or (MilliSecondsBetween(dt, Items[i].LightTime[ltDawn]) < Closest) then
        begin
          Closest := MilliSecondsBetween(dt, Items[i].LightTime[ltDawn]);
          Result := Items[i];
        end;
  end;
end;

procedure TLightTimesList.Load(ANode: IXMLNode);
var
  Nodes: IXMLNodeList;
  i: Integer;
  LightTimes: TLightTimes;
begin
  (* <sol>
    <row dt="2015-04-16T10:00:00" dawn="2015-04-16T01:57:14.787" sunrise="2015-04-16T02:42:57.150" sunset="2015-04-16T17:14:38.447" dark="2015-04-16T18:00:20.810"/>
    <row dt="2015-04-15T10:00:00" dawn="2015-04-15T02:00:29.817" sunrise="2015-04-15T02:45:53.890" sunset="2015-04-15T17:12:11.983" dark="2015-04-15T17:57:36.057"/>
    <row dt="2015-04-17T10:00:00" dawn="2015-04-17T01:53:59.570" sunrise="2015-04-17T02:40:00.980" sunset="2015-04-17T17:17:05.163" dark="2015-04-17T18:03:06.570"/>
    </sol>
  *)
  Clear;
  if not Assigned(ANode) then
    exit;
  Nodes := ANode.ChildNodes;
  if not Assigned(Nodes) then
    exit;

  for i := 0 to Nodes.Count - 1 do
  begin
    if Nodes[i].NodeName = 'row' then
    begin
      LightTimes := TLightTimes.Create;
      Add(LightTimes);
      LightTimes.Load(Nodes[i]);
    end;
  end;
  DoSort;
end;

procedure TLightTimesList.Save(ANode: IXMLNode);
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    Items[i].Save(ANode.AddChild('row'));
end;

{ TLightTimePairList }

procedure TLightTimePairList.FillFromAPTimes(LightType: TLightTimeType; APTimes1, APTimes2: TLightTimesList);
  function FindNearest(SourceTime: TDateTime): TDateTime;
  var
    i: Integer;
    tmpDT: TDateTime;
    minDiff: Int64;
  begin
    Result := 0;
    if SameDateTime(SourceTime, 0) then
      exit;

    minDiff := MaxInt;
    for i := 0 to APTimes2.Count - 1 do
    begin
      tmpDT := APTimes2[i].LightTime[LightType];
      if SameDateTime(tmpDT, 0) then
        Continue;

      if (Result = 0) or (MilliSecondsBetween(tmpDT, SourceTime) < minDiff) then
      begin
        Result := tmpDT;
        minDiff := MilliSecondsBetween(tmpDT, SourceTime);
      end;
    end;

    if HoursBetween(SourceTime, Result) > 12 then
      Result := 0;

    if APTimes2.Count <> 0 then
      if (Result = 0) and (not APTimes2[0].IsLightTimeChanged[LightType]) then
        Result := SourceTime;
  end;

var
  i: Integer;
  Pair: TLightTimePair;
begin
  {
    ����� ������� �� ������� ������, �������� ����� ��� ���������� �� �������.
    ���� ������ - ������� ��� � ���� ������ � ������ ���� ������ ������� �� �������. (???)
  }

  for i := 0 to APTimes1.Count - 1 do
  begin
    Pair.dTime1 := APTimes1[i].LightTime[LightType];
    if SameDateTime(Pair.dTime1, 0) then
      Continue;

    Pair.dTime2 := FindNearest(Pair.dTime1);
    if SameDateTime(Pair.dTime2, 0) then
      Continue;
    Add(Pair);
  end;
end;

{ TTypedLightTimeList }

procedure TTypedLightTimeList.DoSort;
begin
  Sort(TComparer<TTypedLightTime>.Construct(
    function(const Left, Right: TTypedLightTime): Integer
    begin
      Result := CompareDateTime(Left.dt, Right.dt);
    end));
end;

end.
