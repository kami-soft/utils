unit uBigFileOperations;
/// module for operations with very big files (more than 2Gb)
/// see description for functions

/// author: kami (c) 2016
/// you can use this code without any limitations.
/// The code provided "as is", wtihout warranty of any kind.
interface

uses
  Windows,
  SysUtils,
  Classes,
  Generics.Defaults,
  Generics.Collections;

type
  TRemoveCallbackProc = procedure(const CurrentPos, TotalSize: int64; const CurrentLine: int64) of object;
  TDuplicateCallbackProc = procedure(const CurrentPos, TotalSize: int64; const CurrentLine, RemovedLines: int64) of object;

  TRemoveOptions = record
    RemoveStopStr: string;
    LineFeedStr: string;

    Callback: TRemoveCallbackProc;
  end;

  TSearchObj = class(TObject)
  strict private
    FSearchRecs: TList<RawByteString>;
    FtmpBuf: RawByteString;

    function GetMaxRecLength: integer;
  private
    function GetCount: integer;
    function GetItem(index: integer): RawByteString;
  public
    constructor Create;
    destructor Destroy; override;

    procedure AddToFind(Bytes: TBytes); overload;
    procedure AddToFind(Str: RawByteString); overload;

    // zero-based function result
    function FindInBuf(const Buf; const BufLen: integer; var FindedPos: integer; var RecIndex: integer): Boolean; overload;
    // zero-based function result
    function FindInBuf(const Buf: TBytes; var FindedPos: integer; var RecIndex: integer): Boolean; overload;
    // one-based function result
    function FindInBuf(const Buf: RawByteString; var FindedPos: integer; var RecIndex: integer): Boolean; overload;

    property MaxRecLength: integer read GetMaxRecLength;
    property Items[index: integer]: RawByteString read GetItem; default;
    property Count: integer read GetCount;
  end;

function FindInStreamEx(Stream: TStream; const FromPos: int64; StopRecs: TSearchObj; var FindedPos: int64; var RecIndex: integer): Boolean;
function GetStreamEncoding(Stream: TStream; DefaultEncoding: TEncoding): TEncoding;

/// remove all characters in every line in input file before RemoveOptions.RemoveStopStr
// ������ ��� ������� �� ������ ������ �������� �����, ������������� ����� RemoveOptions.RemoveStopStr
procedure DoRemoveBeforeAtEveryLine(InStream, OutStream: TStream; const RemoveOptions: TRemoveOptions; DefaultEncoding: TEncoding); overload;
procedure DoRemoveBeforeAtEveryLine(const InFileName, OutFileName: string; const RemoveOptions: TRemoveOptions; DefaultEncoding: TEncoding); overload;

/// remove all characters in every line in input file between RemoveOptions.RemoveStopStr and RemoveOptions.LineFeedStr
// � ������ ������ �������� ����� ������ ��, ��� ��������� ����� RemoveOptions.RemoveStopStr (� �� RemoveOptions.LineFeedStr)
procedure DoRemoveAfterAtEveryLine(const InStream, OutStream: TStream; const RemoveOptions: TRemoveOptions; DefaultEncoding: TEncoding); overload;
procedure DoRemoveAfterAtEveryLine(const InFileName, OutFileName: string; const RemoveOptions: TRemoveOptions; DefaultEncoding: TEncoding); overload;

/// copy all lines to out file/stream, except full duplicated lines
// ������ (�� ���������� � �������� ����) ��� ��������� ����� �� �������� �����
procedure DoRemoveDuplicates(const InStream, OutStream: TStream; const LineFeedStr: string; DefaultEncoding: TEncoding); overload;
procedure DoRemoveDuplicates(const InFileName, OutFileName: string; const LineFeedStr: string; DefaultEncoding: TEncoding); overload;

procedure DoReplace(const InStream, OutStream: TStream; const AFind, AReplace: string; ReplaceFlags: TReplaceFlags; DefaultEncoding: TEncoding);

implementation

uses
  System.StrUtils, System.Hash;

function Max(A, B: integer): integer;
begin
  if A > B then
    Result := A
  else
    Result := B;
end;

type
  TDuplicatesDictionary = class(TDictionary<integer, Byte>)
  public
    constructor Create;

    function KeyFromData(Data: RawByteString): integer;
  end;
{$REGION 'TSearchObj'}
  { TSearchObj }

procedure TSearchObj.AddToFind(Bytes: TBytes);
var
  s: RawByteString;
begin
  SetLength(s, Length(Bytes));
  Move(Bytes[0], s[1], Length(Bytes));
  AddToFind(s);
end;

procedure TSearchObj.AddToFind(Str: RawByteString);
begin
  FSearchRecs.Add(Str);
end;

constructor TSearchObj.Create;
begin
  inherited Create;
  FSearchRecs := TList<RawByteString>.Create;
end;

destructor TSearchObj.Destroy;
begin
  FreeAndNil(FSearchRecs);
  inherited;
end;

function TSearchObj.FindInBuf(const Buf: TBytes; var FindedPos, RecIndex: integer): Boolean;
begin
  Result := FindInBuf(Buf[0], Length(Buf), FindedPos, RecIndex);
end;

function TSearchObj.FindInBuf(const Buf; const BufLen: integer; var FindedPos, RecIndex: integer): Boolean;
begin
  if Length(FtmpBuf) <> BufLen then
    SetLength(FtmpBuf, BufLen);
  if BufLen <> 0 then
    begin
      Move(Buf, FtmpBuf[1], BufLen);
      Result := FindInBuf(FtmpBuf, FindedPos, RecIndex);
      if Result then
        Dec(FindedPos);
    end
  else
    begin
      Result := False;
      FindedPos := -1;
      RecIndex := 0;
    end;
end;

function TSearchObj.FindInBuf(const Buf: RawByteString; var FindedPos, RecIndex: integer): Boolean;
var
  i: integer;
  CurrFindedPos: integer;
begin
  Result := False;
  FindedPos := MaxInt;
  for i := 0 to FSearchRecs.Count - 1 do
    begin
      CurrFindedPos := Pos(FSearchRecs[i], Buf);
      if (CurrFindedPos <> 0) and (CurrFindedPos < FindedPos) then
        begin
          Result := True;
          RecIndex := i;
          FindedPos := CurrFindedPos;
        end;
    end;
  if not Result then
    begin
      FindedPos := -1;
      RecIndex := 0;
    end;
end;

function TSearchObj.GetCount: integer;
begin
  Result := FSearchRecs.Count;
end;

function TSearchObj.GetItem(index: integer): RawByteString;
begin
  Result := FSearchRecs[index];
end;

function TSearchObj.GetMaxRecLength: integer;
var
  i: integer;
begin
  Result := 0;
  for i := 0 to FSearchRecs.Count - 1 do
    if Length(FSearchRecs[i]) > Result then
      Result := Length(FSearchRecs[i]);
end;
{$ENDREGION}
//
{$REGION 'TDuplicatesDictionary'}
{ TDuplicatesDictionary }

constructor TDuplicatesDictionary.Create;
begin
  inherited Create;
  (* inherited Create(TEqualityComparer<RawByteString>.Construct( //
    function(const Left, Right: RawByteString): Boolean //
    begin //
    Result := CompareStr(Left, Right) = 0; //
    end, //
    function(const Value: RawByteString): integer //
    begin //
    if Length(Value) <> 0 then //
    Result := BobJenkinsHash(Value[1], Length(Value), -1) //
    else //
    Result := -1; //
    end //
    ));
  *)
end;

function TDuplicatesDictionary.KeyFromData(Data: RawByteString): integer;
{ var
  Bytes: TBytes; }
begin
  { FHash.Init;
    FHash.Update(Data[1], Length(Data));
    SetLength(Bytes, FHash.GetHashSize div 8);
    FHash.Final(Bytes[0]);

    SetLength(Result, Length(Bytes) * 2);
    BinToHex(Bytes[0], PAnsiChar(Result), Length(Bytes)); }

  Result := System.Hash.THashBobJenkins.GetHashValue(Data[1], Length(Data), -1);
end;
{$ENDREGION}
// ==================================
threadvar intBuf: TBytes;

function FindInStreamEx(Stream: TStream; const FromPos: int64; StopRecs: TSearchObj; var FindedPos: int64; var RecIndex: integer): Boolean;
var
  Readed: integer;
  iPos, BufLen: integer;
begin
  Result := False;
  FindedPos := -1;
  Stream.Seek(FromPos, soBeginning);

  BufLen := (StopRecs.MaxRecLength * 8 + 1024) and $FFFFFF00;
  if Length(intBuf) < BufLen then
    SetLength(intBuf, BufLen);
  repeat
    Readed := Stream.Read(intBuf[0], Length(intBuf));
    if Readed <> 0 then
      begin
        if StopRecs.FindInBuf(intBuf[0], Readed, iPos, RecIndex) then
          begin
            FindedPos := Stream.Seek(-Readed + iPos, soCurrent);
            Result := True;
            Break;
          end;
        Stream.Seek(-StopRecs.MaxRecLength + 1, soCurrent);
      end;
  until Readed < Length(intBuf);
end;

procedure DoCopy(FromStream, ToStream: TStream; StartPos, EndPos: int64);
begin
  FromStream.Seek(StartPos, soBeginning);
  ToStream.CopyFrom(FromStream, EndPos - StartPos);
end;

function GetStreamEncoding(Stream: TStream; DefaultEncoding: TEncoding): TEncoding;
var
  Buf: TBytes;
  OldStreamPos: int64;
begin
  Result := nil;
  SetLength(Buf, 1024);

  OldStreamPos := Stream.Seek(0, soCurrent);
  try
    Stream.Seek(0, soBeginning);
    Stream.Read(Buf[0], Length(Buf));
    TEncoding.GetBufferEncoding(Buf, Result, DefaultEncoding);
  finally
    Stream.Seek(OldStreamPos, soBeginning);
  end;
end;

function CopyToStr(Stream: TStream; FromPos, ToPos: int64): RawByteString;
begin
  Stream.Seek(FromPos, soBeginning);
  SetLength(Result, ToPos - FromPos);
  Stream.Read(Result[1], Length(Result));
end;

procedure DoRemoveBeforeAtEveryLine(InStream, OutStream: TStream; const RemoveOptions: TRemoveOptions; DefaultEncoding: TEncoding);
var
  ObjSym, ObjLF: TSearchObj;

  FindedPos, PrevPos: int64;
  LineIndex: int64;
  RecIndex: integer;

  Encoding: TEncoding;
begin
  Encoding := GetStreamEncoding(InStream, DefaultEncoding);

  ObjSym := TSearchObj.Create;
  ObjLF := TSearchObj.Create;
  try
    ObjSym.AddToFind(Encoding.GetBytes(RemoveOptions.RemoveStopStr));
    ObjSym.AddToFind(Encoding.GetBytes(RemoveOptions.LineFeedStr));

    ObjLF.AddToFind(Encoding.GetBytes(RemoveOptions.LineFeedStr));

    LineIndex := 0;
    PrevPos := Length(Encoding.GetPreamble);
    repeat
      if FindInStreamEx(InStream, PrevPos, ObjSym, FindedPos, RecIndex) then
        begin
          if RecIndex <> (ObjSym.Count - 1) then // ���� ��� �� ������� ������
            begin
              PrevPos := FindedPos + Length(ObjSym[RecIndex]);
              if not FindInStreamEx(InStream, PrevPos, ObjLF, FindedPos, RecIndex) then
                FindedPos := InStream.Size - Length(ObjLF[0]);

              Inc(FindedPos, Length(ObjLF[0]));
            end
          else
            begin
              Inc(FindedPos, Length(ObjSym[RecIndex]));
              Inc(LineIndex);
            end;
          DoCopy(InStream, OutStream, PrevPos, FindedPos);
          PrevPos := FindedPos;

          if Assigned(RemoveOptions.Callback) then
            RemoveOptions.Callback(FindedPos, InStream.Size, LineIndex);
        end;
    until FindedPos = -1;

    if PrevPos < InStream.Size then
      DoCopy(InStream, OutStream, PrevPos, InStream.Size);
  finally
    ObjLF.Free;
    ObjSym.Free;
  end;
end;

procedure DoRemoveBeforeAtEveryLine(const InFileName, OutFileName: string; const RemoveOptions: TRemoveOptions; DefaultEncoding: TEncoding);
var
  FS: TFileStream;
  NewFS: TFileStream;
begin
  FS := TFileStream.Create(InFileName, fmOpenRead or fmShareDenyWrite);
  try
    NewFS := TFileStream.Create(OutFileName, fmCreate);
    try
      DoRemoveBeforeAtEveryLine(FS, NewFS, RemoveOptions, DefaultEncoding);
    finally
      NewFS.Free;
    end;
  finally
    FS.Free;
  end;
end;

procedure DoRemoveAfterAtEveryLine(const InStream, OutStream: TStream; const RemoveOptions: TRemoveOptions; DefaultEncoding: TEncoding);
var
  ObjSym, ObjLF: TSearchObj;

  FindedPos, PrevPos: int64;
  LineIndex: int64;
  RecIndex: integer;

  Encoding: TEncoding;
begin
  Encoding := GetStreamEncoding(InStream, DefaultEncoding);

  ObjSym := TSearchObj.Create;
  ObjLF := TSearchObj.Create;
  try
    ObjSym.AddToFind(Encoding.GetBytes(RemoveOptions.RemoveStopStr));
    // ObjSym.AddToFind(LineFeedBytes);

    ObjLF.AddToFind(Encoding.GetBytes(RemoveOptions.LineFeedStr));

    LineIndex := 0;
    PrevPos := Length(Encoding.GetPreamble);
    repeat
      if FindInStreamEx(InStream, PrevPos, ObjSym, FindedPos, RecIndex) then // ���� ���������
        begin
          DoCopy(InStream, OutStream, PrevPos, FindedPos); // �������� �� ������ ������ �� ��������� ������������������
          // (���� ���� ��� ������������������ ����������� �� ������ ������ - ������ ���������, � ������ ���������
          // ��� ����������)
          PrevPos := FindedPos + Length(ObjSym[RecIndex]); // ���������� ���������
          if FindInStreamEx(InStream, PrevPos, ObjLF, FindedPos, RecIndex) then // ���� ����� ������ (�� �� ���� ���� ����������)
            begin
              PrevPos := FindedPos;
              Inc(LineIndex);
            end
          else
            PrevPos := InStream.Size;
        end;
      if Assigned(RemoveOptions.Callback) then
        RemoveOptions.Callback(FindedPos, InStream.Size, LineIndex);
    until FindedPos = -1;

    if PrevPos < InStream.Size then
      DoCopy(InStream, OutStream, PrevPos, InStream.Size);
  finally
    ObjLF.Free;
    ObjSym.Free;
  end;
end;

procedure DoRemoveAfterAtEveryLine(const InFileName, OutFileName: string; const RemoveOptions: TRemoveOptions; DefaultEncoding: TEncoding);
var
  FS: TFileStream;
  NewFS: TFileStream;
begin
  FS := TFileStream.Create(InFileName, fmOpenRead or fmShareDenyWrite);
  try
    NewFS := TFileStream.Create(OutFileName, fmCreate);
    try
      DoRemoveAfterAtEveryLine(FS, NewFS, RemoveOptions, DefaultEncoding);
    finally
      NewFS.Free;
    end;
  finally
    FS.Free;
  end;
end;

procedure DoRemoveDuplicates(const InStream, OutStream: TStream; const LineFeedStr: string; DefaultEncoding: TEncoding);
  procedure AddOrIgnore(StartPos, EndPos: int64; HashList: TDuplicatesDictionary);
  var
    Str: RawByteString;
    iKey: integer;
  begin
    Str := CopyToStr(InStream, StartPos, EndPos);

    // ��������, ���� �� ����� ������ � �������
    iKey := HashList.KeyFromData(Str);
    if not HashList.ContainsKey(iKey) then
      begin
        HashList.Add(iKey, 0);
        DoCopy(InStream, OutStream, StartPos, EndPos);
      end;
  end;

var
  ObjLF: TSearchObj;
  HashList: TDuplicatesDictionary;
  PrevPos, NewLinePos: int64;
  RecIndex: integer;

  Encoding: TEncoding;
begin
  Encoding := GetStreamEncoding(InStream, DefaultEncoding);

  HashList := TDuplicatesDictionary.Create;
  try
    ObjLF := TSearchObj.Create;
    try
      ObjLF.AddToFind(Encoding.GetBytes(LineFeedStr));

      PrevPos := 0;
      repeat
        if FindInStreamEx(InStream, PrevPos, ObjLF, NewLinePos, RecIndex) then
          begin
            // �� �� PrevPos �� NewLinePos - ��������� ������
            Inc(NewLinePos, Length(ObjLF[0])); // �������� ����� �������� ������
            AddOrIgnore(PrevPos, NewLinePos, HashList);
            PrevPos := NewLinePos;
          end;
      until NewLinePos = -1;
      if PrevPos < InStream.Size then
        begin
          // �� �� PrevPos �� FS.Size - ��������� ������
          AddOrIgnore(PrevPos, InStream.Size, HashList);
        end;
    finally
      ObjLF.Free;
    end;
  finally
    HashList.Free;
  end;
end;

procedure DoRemoveDuplicates(const InFileName, OutFileName: string; const LineFeedStr: string; DefaultEncoding: TEncoding);
var
  FS: TFileStream;
  NewFS: TFileStream;
begin
  FS := TFileStream.Create(InFileName, fmOpenRead or fmShareDenyWrite);
  try
    NewFS := TFileStream.Create(OutFileName, fmCreate);
    try
      DoRemoveDuplicates(FS, NewFS, LineFeedStr, DefaultEncoding);
    finally
      NewFS.Free;
    end;
  finally
    FS.Free;
  end;
end;

procedure DoReplace(const InStream, OutStream: TStream; const AFind, AReplace: string; ReplaceFlags: TReplaceFlags; DefaultEncoding: TEncoding);
var
  RealEncoding: TEncoding;

  procedure CopyPreamble;
  var
    PreambleSize: integer;
    PreambleBuf: TBytes;
  begin
    // Copy Encoding preamble
    SetLength(PreambleBuf, 100);
    InStream.Read(PreambleBuf, Length(PreambleBuf));
    InStream.Seek(0, soBeginning);

    RealEncoding := nil;
    PreambleSize := TEncoding.GetBufferEncoding(PreambleBuf, RealEncoding, DefaultEncoding);
    if PreambleSize <> 0 then
      OutStream.CopyFrom(InStream, PreambleSize);
  end;

  function GetLastIndex(const Str, SubStr: string): integer;
  var
    i: integer;
    tmpSubStr, tmpStr: string;
  begin
    if not(rfIgnoreCase in ReplaceFlags) then
      begin
        i := Pos(SubStr, Str);
        Result := i;
        while i > 0 do
          begin
            i := PosEx(SubStr, Str, i + 1);
            if i > 0 then
              Result := i;
          end;
        if Result > 0 then
          Inc(Result, Length(SubStr) - 1);
      end
    else
      begin
        tmpStr := UpperCase(Str);
        tmpSubStr := UpperCase(SubStr);
        i := Pos(tmpSubStr, tmpStr);
        Result := i;
        while i > 0 do
          begin
            i := PosEx(tmpSubStr, tmpStr, i + 1);
            if i > 0 then
              Result := i;
          end;
        if Result > 0 then
          Inc(Result, Length(tmpSubStr) - 1);
      end;
  end;

var
  SourceSize: int64;

  procedure ParseBuffer(Buf: TBytes; var IsReplaced: Boolean);
  var
    i: integer;
    ReadedBufLen: integer;
    BufStr: string;
    DestBytes: TBytes;
    LastIndex: integer;
  begin
    if IsReplaced and (not(rfReplaceAll in ReplaceFlags)) then
      begin
        OutStream.Write(Buf, Length(Buf));
        Exit;
      end;

    // 1. Get chars from buffer
    ReadedBufLen := 0;
    for i := Length(Buf) downto 0 do
      if RealEncoding.GetCharCount(Buf, 0, i) <> 0 then
        begin
          ReadedBufLen := i;
          Break;
        end;
    if ReadedBufLen = 0 then
      raise EEncodingError.Create('Cant convert bytes to str');

    InStream.Seek(ReadedBufLen - Length(Buf), soCurrent);

    BufStr := RealEncoding.GetString(Buf, 0, ReadedBufLen);
    if rfIgnoreCase in ReplaceFlags then
      IsReplaced := ContainsText(BufStr, AFind)
    else
      IsReplaced := ContainsStr(BufStr, AFind);

    if IsReplaced then
      begin
        LastIndex := GetLastIndex(BufStr, AFind);
        LastIndex := Max(LastIndex, Length(BufStr) - Length(AFind) + 1);
      end
    else
      LastIndex := Length(BufStr);

    SetLength(BufStr, LastIndex);
    InStream.Seek(RealEncoding.GetByteCount(BufStr) - ReadedBufLen, soCurrent);

    BufStr := StringReplace(BufStr, AFind, AReplace, ReplaceFlags);
    DestBytes := RealEncoding.GetBytes(BufStr);
    OutStream.Write(DestBytes, Length(DestBytes));
  end;

var
  Buf: TBytes;
  BufLen: integer;
  bReplaced: Boolean;
begin
  InStream.Seek(0, soBeginning);
  OutStream.Size := 0;
  CopyPreamble;

  SourceSize := InStream.Size;
  BufLen := Max(RealEncoding.GetByteCount(AFind) * 5, 2048);
  BufLen := Max(RealEncoding.GetByteCount(AReplace) * 5, BufLen);
  SetLength(Buf, BufLen);

  bReplaced := False;
  while InStream.Position < SourceSize do
    begin
      BufLen := InStream.Read(Buf, Length(Buf));
      SetLength(Buf, BufLen);
      ParseBuffer(Buf, bReplaced);
    end;

  InStream.Size := 0;
  InStream.CopyFrom(OutStream, 0);
end;

end.
