unit Ribbon.ProgressWnd;
{$DEFINE BlinkingRect}

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Types,
  System.Classes,
  System.Generics.Collections,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.ExtCtrls,
  Vcl.StdCtrls,
  cxGroupBox,
  cxButtons;

type
  TProgressPanel = class(TcxGroupBox)
  strict private
  const
    PenWidth = 8;
    CaptionYCenterOffset = 20;
    cl = $005B391E;
    MaxTagCount = 23;
    DashLength = 12;
  strict private
    FTimer: TTimer;
    procedure OnTimer(Sender: TObject);
  private
    FProgressText: string;
    procedure SetProgressText(const Value: string);
  protected
    procedure Paint; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    property ProgressText: string read FProgressText write SetProgressText;
  end;

  TCancelPanel = class(TProgressPanel)
  strict private
    Fbtn: TcxButton;
    FOnCancel: TNotifyEvent;
    procedure SetOnCancel(const Value: TNotifyEvent);
    procedure OnButtonClick(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;

    property OnCancel: TNotifyEvent read FOnCancel write SetOnCancel;
  end;

  TProgressPanelClass = class of TCancelPanel;

  TLongOperationProgress = class
  public
    class function Win8OrLater: Boolean;
    class function Show<T: TCancelPanel, constructor>(Parent: TWinControl; OnCancel: TNotifyEvent;
      AlphaBlendValue: Byte = 220): T;
  end;

function ShowLongOperationProgress(Parent: TWinControl; OnCancel: TNotifyEvent; AlphaBlendValue: Byte = 220;
  ProgressPanelClass: TProgressPanelClass = nil): TControl;

implementation

uses
  uAlphaBlendControl;

type
  TTransparentPanel = class(TCancelPanel)
    //  http://delphidabbler.com/tips/210 + ������� ���� �����������
  private
    FBackground: TBitmap;
    FInShow: Boolean;
    FRealParent: TWinControl;
    FHWND: THandle;

    procedure WMEraseBkGnd(var msg: TWMEraseBkGnd); message WM_ERASEBKGND;

    procedure WndProcForDelayedActions(var Message: TMessage);
  protected
    procedure CaptureBackground;
    procedure SetParent(AParent: TWinControl); override;
  public
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    { Published declarations }
  end;

function Win8OrLater: Boolean;
begin
  Result := TOSVersion.Major > 6;
  if not Result and (TOSVersion.Major = 6) then
    Result := TOSVersion.Minor > 1;
end;

function ShowLongOperationProgress(Parent: TWinControl; OnCancel: TNotifyEvent; AlphaBlendValue: Byte;
  ProgressPanelClass: TProgressPanelClass): TControl;
begin
  if not Assigned(Parent) then
    Exit(nil);
  if not Win8OrLater then
    AlphaBlendValue := 255;
  ProgressPanelClass := TCancelPanel;

  Result := ProgressPanelClass.Create(nil);
  Result.Parent := Parent;
  TCancelPanel(Result).OnCancel := OnCancel;

  TCancelPanel(Result).HandleNeeded;
  Result.SetBounds(0, 0, Parent.ClientWidth, Parent.ClientHeight);
  Result.Anchors := [akLeft, akTop, akRight, akBottom];
  Result.Visible := True;
  Result.BringToFront;
  Result.Repaint;
  if AlphaBlendValue <> 255 then
    TCancelPanel(Result).SetAlpha(AlphaBlendValue);
end;

{ TPanel }

constructor TCancelPanel.Create(AOwner: TComponent);
begin
  inherited;
  Fbtn := TcxButton.Create(Self);
  Fbtn.Parent := Self;
  Fbtn.Left := 50;
  Fbtn.Top := 50;
  Fbtn.Width := 200;
  Fbtn.Caption := '������';
  Fbtn.OnClick := OnButtonClick;
  Fbtn.Visible := False;
end;

procedure TCancelPanel.OnButtonClick(Sender: TObject);
begin
  Fbtn.Caption := '����������';
  if Assigned(FOnCancel) then
    FOnCancel(Self);
end;

procedure TCancelPanel.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
var
  i: Integer;
begin
  inherited;
  if Assigned(Fbtn) then
    Fbtn.SetBounds(AWidth div 2 - Fbtn.Width div 2, AHeight div 2 - Fbtn.Height div 2, Fbtn.Width, Fbtn.Height);
  for i := 0 to ControlCount - 1 do
    if Controls[i] is TWinControl then
      if TWinControl(Controls[i]).HandleAllocated then
        PostMessage(TWinControl(Controls[i]).Handle, CM_INVALIDATE, 0, 0);
end;

procedure TCancelPanel.SetOnCancel(const Value: TNotifyEvent);
begin
  FOnCancel := Value;
  Fbtn.Visible := Assigned(FOnCancel);
end;

{ TTransparentPanel }

procedure TTransparentPanel.CaptureBackground;
var
  dc: HDC;
  sourcerect: TRect;
begin
  FreeAndNil(FBackground);
  FBackground := TBitmap.Create;
  FBackground.SetSize(FRealParent.ClientWidth, FRealParent.ClientHeight);
  sourcerect.TopLeft := FRealParent.ClientToScreen(FRealParent.ClientRect.TopLeft);
  sourcerect.BottomRight := FRealParent.ClientToScreen(FRealParent.ClientRect.BottomRight);

  dc := GetDC(FRealParent.Handle);
  try
    BitBlt(FBackground.Canvas.Handle, 0, 0, FBackground.Width, FBackground.Height, dc, 0, 0, SRCCOPY);
  finally
    ReleaseDC(FRealParent.Handle, dc);
  end;

  FBackground.Canvas.Brush.Color := clWhite;
  FBackground.Canvas.Brush.Style := bsFDiagonal;
  FBackground.Canvas.Pen.Style := psClear;
  FBackground.Canvas.Rectangle(BoundsRect.Left, BoundsRect.Top, BoundsRect.Right, BoundsRect.Bottom);
end;

constructor TTransparentPanel.Create(AOwner: TComponent);
begin
  inherited;
  FHWND := AllocateHWnd(WndProcForDelayedActions);
end;

destructor TTransparentPanel.Destroy;
begin
  DeallocateHWnd(FHWND);
  FBackground.Free;
  inherited;
end;

procedure TTransparentPanel.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  if FInShow then
  begin
    inherited;
    Exit;
  end;

  if Visible and HandleAllocated and not(csDesigning in ComponentState) then
  begin
    Parent := nil;
    inherited;
    FRealParent.Update;

    PostMessage(FHWND, WM_USER + 10, 0, 0);
  end
  else
    inherited;

  PostMessage(FHWND, WM_USER + 11, 0, 0);
end;

procedure TTransparentPanel.SetParent(AParent: TWinControl);
begin
  if Assigned(AParent) then
    FRealParent := AParent;
  inherited;
end;

procedure TTransparentPanel.WMEraseBkGnd(var msg: TWMEraseBkGnd);
begin
  if csDesigning in ComponentState then
    inherited
  else
  begin
    if not Assigned(FBackground) then
      CaptureBackground;
    BitBlt(msg.dc, 0, 0, FBackground.Width, FBackground.Height, FBackground.Canvas.Handle, 0, 0, SRCCOPY);
    msg.Result := 1;
  end;
end;

procedure TTransparentPanel.WndProcForDelayedActions(var Message: TMessage);
begin
  case message.msg of
    WM_USER + 10:
      begin
        FInShow := True;
        try
          CaptureBackground;
          Parent := FRealParent;
        finally
          FInShow := False;
        end;
      end;
    WM_USER + 11:
      begin
        if Assigned(Parent) then
          if (Left <> 0) or (Top <> 0) or (Width <> Parent.ClientWidth) or (Height <> Parent.ClientHeight) then
            SetBounds(0, 0, Parent.ClientWidth, Parent.ClientHeight);
      end
  else
    message.Result := DefWindowProc(FHWND, message.msg, message.WParam, message.LParam);
  end;
end;

{ TProgressPanel }

constructor TProgressPanel.Create(AOwner: TComponent);
begin
  inherited;
  ControlStyle := ControlStyle - [csSetCaption];

  FTimer := TTimer.Create(nil);
  {$IFDEF BlinkingRect}
  FTimer.Enabled := True;
  {$ELSE}
  FTimer.Enabled := False;
  {$ENDIF}
  FTimer.OnTimer := OnTimer;
  FTimer.Interval := 50;

  PanelStyle.Active := True;
  Caption:='';
end;

destructor TProgressPanel.Destroy;
begin
  FreeAndNil(FTimer);
  inherited;
end;

procedure TProgressPanel.OnTimer(Sender: TObject);
begin
  FTimer.Tag := FTimer.Tag + 1;
  if FTimer.Tag > MaxTagCount then
    FTimer.Tag := 0;
  Invalidate;
end;

procedure TProgressPanel.Paint;
var
  {$IFDEF BlinkingRect}
  lbrush: LOGBRUSH;
  userstyle: array of DWORD;
  {$ENDIF}
  w: Integer;
  p: TPoint;
begin
  //if csDesigning in ComponentState then
  inherited;

  if (Trim(FProgressText) <> '') { and ShowCaption } then
  begin
    w := Canvas.TextWidth(FProgressText);
    p.X := Width div 2 - w div 2;
    p.y := Height div 2 + CaptionYCenterOffset;
    Canvas.Brush.Style := bsClear;
    Canvas.TextOut(p.X, p.y, FProgressText);
  end;

  {$IFDEF BlinkingRect}
  lbrush.lbStyle := BS_SOLID;
  lbrush.lbColor := cl;
  lbrush.lbHatch := 0;

  setlength(userstyle, 2);
  userstyle[0] := DashLength;
  userstyle[1] := DashLength;

  Canvas.Pen.Handle := ExtCreatePen(PS_GEOMETRIC or PS_USERSTYLE or PS_ENDCAP_FLAT, PenWidth, lbrush, Length(userstyle),
    userstyle);

  Canvas.Polyline([Point(FTimer.Tag, 0), Point(Width, 0), Point(Width, Height), Point(0, Height), Point(0, 0)]);
  {$ENDIF}
end;

procedure TProgressPanel.SetProgressText(const Value: string);
begin
  if FProgressText <> Value then
  begin
    FProgressText := Value;
    Invalidate;
  end;
end;

{ TLongOperationProgress }

class function TLongOperationProgress.Show<T>(Parent: TWinControl; OnCancel: TNotifyEvent; AlphaBlendValue: Byte): T;
begin
  if not Assigned(Parent) then
    Exit(nil);
  if not Win8OrLater then
    AlphaBlendValue := 255;

  Result := (T as TControlClass).Create(nil) as T;
  Result.Parent := Parent;
  TCancelPanel(Result).OnCancel := OnCancel;

  TCancelPanel(Result).HandleNeeded;
  Result.SetBounds(0, 0, Parent.ClientWidth, Parent.ClientHeight);
  Result.Anchors := [akLeft, akTop, akRight, akBottom];
  Result.Visible := True;
  Result.BringToFront;
  Result.Repaint;
  if AlphaBlendValue <> 255 then
    Result.SetAlpha(AlphaBlendValue);
end;

class function TLongOperationProgress.Win8OrLater: Boolean;
begin
  Result := TOSVersion.Major > 6;
  if not Result and (TOSVersion.Major = 6) then
    Result := TOSVersion.Minor > 1;
end;

end.
