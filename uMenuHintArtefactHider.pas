unit uMenuHintArtefactHider;
/// <summary>
/// ������, �������������� �������������� ������� ������ ��� ������ �������.
///  ���� ������� RegisterFormForHideHints - � ������������ ������� ����
///  ����������������� (UnregisterFormForHideHints) � �����������.
/// </summary>

interface

uses
  Vcl.Forms;

procedure RegisterFormForHideHints(fm: TCustomForm);
procedure UnregisterFormForHideHints(fm: TCustomForm);

implementation

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Generics.Collections;

type
  TMenuHintArthefactHider = class(TList<TCustomForm>)
  private
    FHintsActive: Boolean;
    procedure SetHintsActive(const Value: Boolean);
  public
    constructor Create;
    destructor Destroy; override;

    property HintsActive: Boolean read FHintsActive write SetHintsActive;
  end;

var
  FHintArthefactHider: TMenuHintArthefactHider;

var
  FHook: HHOOK;

function CallWndRetProc(Code, Flag, PData: Integer): Integer; stdcall;
var
  msg: Cardinal;
begin
  if Assigned(FHintArthefactHider) and (Code>=0) then
  begin
    msg := PCWPRetStruct(PData)^.message;
    if (msg = WM_EXITMENULOOP) or (msg = WM_ENTERMENULOOP) then
      FHintArthefactHider.HintsActive := msg = WM_EXITMENULOOP;
  end;

  Result := CallNextHookEx(FHook, Code, Flag, PData)
end;

procedure RegisterFormForHideHints(fm: TCustomForm);
begin
  if not Assigned(FHintArthefactHider) then
    FHintArthefactHider := TMenuHintArthefactHider.Create;
  if not FHintArthefactHider.Contains(fm) then
    FHintArthefactHider.Add(fm);
end;

procedure UnregisterFormForHideHints(fm: TCustomForm);
begin
  if Assigned(FHintArthefactHider) then
    FHintArthefactHider.Remove(fm);
  if FHintArthefactHider.Count = 0 then
    FreeAndNil(FHintArthefactHider);
end;

{ THintArthefactHider }

constructor TMenuHintArthefactHider.Create;
begin
  inherited Create;
  FHook := SetWindowsHookEx(WH_CALLWNDPROCRET, @CallWndRetProc, 0, GetCurrentThreadId);
end;

destructor TMenuHintArthefactHider.Destroy;
begin
  UnhookWindowsHookEx(FHook);
  FHook := 0;
  inherited;
end;

procedure TMenuHintArthefactHider.SetHintsActive(const Value: Boolean);
var
  i: Integer;
begin
  FHintsActive := Value;
  for i := 0 to Count - 1 do
    Items[i].ShowHint := Value;
end;

initialization
  FHintArthefactHider:=nil;

finalization
  FreeAndNil(FHintArthefactHider);

end.
