unit Common.AbstractClasses.Async;

interface

uses
  System.Classes,
  System.Generics.Collections,
  System.SysUtils,
  System.RTTI,
  Common.AbstractClasses.Lazy;

type
  TAsyncThreadValues = TDictionary<string, TValue>;

  TLazyAsyncObject = class(TLazyObject)
  private
    FDataInProcess: TDictionary<string, Boolean>;
    function GetDataInProcessCount: Integer;
  protected
    /// <summary>
    /// This procedure called in separated thread for prepare (if necessary)
    /// common values can used in every DoAsyncLoadData call
    /// for example - create single connection for whole thread (and maybe - stored procedure)
    /// instead of create connection for every DoAsyncLoadData call
    ///  Do not forget to call CoInitialize, if necessary!
    /// </summary>
    class procedure InitValuesForAsync(ThreadValues: TAsyncThreadValues); virtual;
    /// <summary>
    /// this procedure called in separated thread for finalize all data
    ///  created by InitValuesForAsync
    ///  Do not forget to call CoUninitialize, if you init COM in InitValuesForAsync
    /// </summary>
    class procedure FinValuesForAsync(ThreadValues: TAsyncThreadValues); virtual;
    /// <summary>
    /// start task for load data. Can be used in any thread
    /// Another thread will call function DoAsyncLoadData with AName value
    /// </summary>
    procedure StartAsyncLoadData(const AName: string; LimitSpeed: Boolean);
    /// <summary>
    /// This function called in separate thread. (the call initated by StartAsyncLoadData)
    /// Object must get value named AName and return it in Result.
    /// Remember that this is separate thread context, so think about thread-safe
    /// </summary>
    function DoAsyncLoadData(const AName: string; ThreadValues: TAsyncThreadValues): TValue; virtual; abstract;
    /// <summary>
    /// Called in MAIN thread throught TThread.Queue when data succesfully loaded
    /// AName contains value passed from StartAsyncLoadData
    /// ErrorDescription - small information about exception (basically - E.ToString)
    /// </summary>
    procedure OnAsyncException(const AName: string; const ErrorDescription: string); virtual;
    /// <summary>
    /// Called in MAIN thread throught TThread.Queue when data succesfully loaded
    /// Retrieved value at this moment already contains in Data[] property
    /// and can be used as Data[AName] everywhere
    /// </summary>
    procedure OnAsyncDataLoaded(const AName: string); virtual;
  public
    destructor Destroy; override;

    property DataInProcessCount: Integer read GetDataInProcessCount;
  end;

  TLazyAsyncObjectClass = class of TLazyAsyncObject;

procedure FinalizeAsyncLoad;

implementation

uses
  System.SyncObjs,
  System.StrUtils,
  System.DateUtils,
  Vcl.ExtCtrls,
  Common.Interfaces;

type
  TValueRequest = record
    Owner: TLazyAsyncObject;
    Name: string;
    OutputValue: TValue;
    ErrorDescription: string;

    LimitSpeed: Boolean;
  end;

  TLoaderQueue = class(TList<TValueRequest>)
  end;

  TValueLoader = class(TThread)
  strict private
  const
    cNotUsedIntervalMin = 1;
  strict private
    FInQueue: TLoaderQueue;
    FOutQueue: TLoaderQueue;
    FEvent: TEvent;
    FInCriticalSection: TCriticalSection;
    FOutCriticalSection: TCriticalSection;

    FAsyncClass: TLazyAsyncObjectClass;
    FThreadedValues: TAsyncThreadValues;

    FLastActivity: TDateTime;

    procedure MainThreadReturnValues;

    procedure intProcessQueues;
  strict protected
    procedure Execute; override;
  public
    constructor Create(AClass: TLazyAsyncObjectClass; const LoaderName: string); reintroduce;
    destructor Destroy; override;

    procedure ThreadSafeAddRequest(const Request: TValueRequest);
    procedure DeleteRequestsByObject(AObject: TLazyAsyncObject);

    function IsNotUsed: Boolean;
  end;

  TValueLoaderContainer = class(TObject)
  strict private
    class var FInstance: TValueLoaderContainer;
    class function Instance: TValueLoaderContainer;
  strict private
    FCriticalSection: TCriticalSection;
    FScanTimer: TTimer;

    FLoaders: TObjectDictionary<string, TValueLoader>;
    function GenerateLoaderName(const Request: TValueRequest): string;

    constructor Create;

    function FindLoader(const Request: TValueRequest): TValueLoader;
    function GetLoader(const Request: TValueRequest): TValueLoader;

    procedure intDeleteLoader(const ALoaderName: string);
    procedure intThreadSafeDeleteRequestByObject(AObject: TLazyAsyncObject);
    procedure intScanForNotUsedLoaders(Sender: TObject);

    procedure CreateScanTimer;
  public
    class procedure ThreadSafeAddRequest(const Request: TValueRequest);
    class procedure ThreadSafeDeleteRequestByObject(AObject: TLazyAsyncObject);

    class procedure Fin;

    destructor Destroy; override;
  end;

procedure FinalizeAsyncLoad;
begin
  TValueLoaderContainer.Fin;
end;

{ TLazyAsyncObject }

destructor TLazyAsyncObject.Destroy;
begin
  if Assigned(FDataInProcess) then
    if FDataInProcess.Count <> 0 then
      TValueLoaderContainer.ThreadSafeDeleteRequestByObject(Self);
  FreeAndNil(FDataInProcess);
  inherited;
end;

class procedure TLazyAsyncObject.FinValuesForAsync(ThreadValues: TAsyncThreadValues);
begin

end;

function TLazyAsyncObject.GetDataInProcessCount: Integer;
begin
  if Assigned(FDataInProcess) then
    Result := FDataInProcess.Count
  else
    Result := 0;
end;

class procedure TLazyAsyncObject.InitValuesForAsync(ThreadValues: TAsyncThreadValues);
begin

end;

procedure TLazyAsyncObject.OnAsyncDataLoaded(const AName: string);
begin
  if Assigned(FDataInProcess) then
    FDataInProcess.Remove(NormalizeName(AName));
end;

procedure TLazyAsyncObject.OnAsyncException(const AName: string; const ErrorDescription: string);
begin
  if Assigned(FDataInProcess) then
    FDataInProcess.Remove(NormalizeName(AName));
end;

procedure TLazyAsyncObject.StartAsyncLoadData(const AName: string; LimitSpeed: Boolean);
var
  Request: TValueRequest;
  NormalizedName: string;
begin
  if not Assigned(FDataInProcess) then
    FDataInProcess := TDictionary<string, Boolean>.Create;

  NormalizedName := NormalizeName(AName);
  if FDataInProcess.ContainsKey(NormalizedName) then
    exit;

  Request.Owner := Self;
  Request.Name := AName;
  Request.OutputValue := TValue.Empty;
  Request.ErrorDescription := '';
  Request.LimitSpeed := LimitSpeed;
  FDataInProcess.Add(NormalizedName, False);
  TValueLoaderContainer.ThreadSafeAddRequest(Request);
end;

{ TValueLoadThread }

constructor TValueLoader.Create(AClass: TLazyAsyncObjectClass; const LoaderName: string);
begin
  inherited Create(False);
  FAsyncClass := AClass;
  FInCriticalSection := TCriticalSection.Create;
  FOutCriticalSection := TCriticalSection.Create;
  FEvent := TEvent.Create(nil, True, True, 'AsyncLoader' + LoaderName);
  FInQueue := TLoaderQueue.Create;
  FOutQueue := TLoaderQueue.Create;
end;

procedure TValueLoader.DeleteRequestsByObject(AObject: TLazyAsyncObject);
var
  i: Integer;
begin
  FInCriticalSection.Enter;
  try
    for i := FInQueue.Count - 1 downto 0 do
      if FInQueue[i].Owner = AObject then
        FInQueue.Delete(i);
  finally
    FInCriticalSection.Leave;
  end;

  FOutCriticalSection.Enter;
  try
    for i := FOutQueue.Count - 1 downto 0 do
      if FOutQueue[i].Owner = AObject then
        FOutQueue.Delete(i);
  finally
    FOutCriticalSection.Leave;
  end;

  FLastActivity := Now;
end;

destructor TValueLoader.Destroy;
begin
  Terminate;
  FEvent.SetEvent;
  while not Finished do
    Sleep(10);

  FOutCriticalSection.Enter;
  try
    FreeAndNil(FOutQueue);
  finally
    FOutCriticalSection.Leave;
  end;

  FInCriticalSection.Enter;
  try
    FreeAndNil(FInQueue);
  finally
    FInCriticalSection.Leave;
  end;

  FreeAndNil(FEvent);
  FreeAndNil(FOutCriticalSection);
  FreeAndNil(FInCriticalSection);
  inherited;
end;

procedure TValueLoader.Execute;
begin
  FThreadedValues := TAsyncThreadValues.Create;
  try
    while not Terminated do
    begin
      if FEvent.WaitFor(INFINITE) = wrSignaled then
      begin
        if not Terminated then
          intProcessQueues;
      end
      else
        Break;

      FEvent.ResetEvent;
    end;
  finally
    FThreadedValues.Free;
  end;
end;

procedure TValueLoader.intProcessQueues;
  function GetRequest(out Request: TValueRequest): Boolean;
  begin
    Result := False;
    FInCriticalSection.Enter;
    try
      if FInQueue.Count <> 0 then
      begin
        Request := FInQueue[0];
        Result := True;
      end;
    finally
      FInCriticalSection.Leave;
    end;
  end;

  procedure AddToOutQueue(const Request: TValueRequest);
  begin
    FInCriticalSection.Enter;
    try
      if FInQueue.Count <> 0 then
        if (FInQueue[0].Owner = Request.Owner) and (FInQueue[0].Name = Request.Name) then
        // object not deleted in another thread while retrieve data
        begin
          FInQueue.Delete(0);
          FOutCriticalSection.Enter;
          try
            FOutQueue.Add(Request);
          finally
            FOutCriticalSection.Leave;
          end;
          Queue(MainThreadReturnValues);
        end;
    finally
      FInCriticalSection.Leave;
    end;
  end;

var
  Request: TValueRequest;
begin
  FAsyncClass.InitValuesForAsync(FThreadedValues);
  try
    while not Terminated do
    begin
      if GetRequest(Request) then
        try
          try
            Request.OutputValue := Request.Owner.DoAsyncLoadData(Request.Name, FThreadedValues);
          except
            on e: Exception do
            begin
              Request.OutputValue := TValue.Empty;
              Request.ErrorDescription := e.ToString;
            end;
          end;
        finally
          AddToOutQueue(Request);
        end
      else
        Break;
    end;
  finally
    FAsyncClass.FinValuesForAsync(FThreadedValues);
    FThreadedValues.Clear;
  end;
end;

function TValueLoader.IsNotUsed: Boolean;
begin
  Result := False;
  FInCriticalSection.Enter;
  try
    FOutCriticalSection.Enter;
    try
      if FLastActivity < IncMinute(Now, -cNotUsedIntervalMin) then
        if (FInQueue.Count = 0) and (FOutQueue.Count = 0) then
          Result := True;
    finally
      FOutCriticalSection.Leave;
    end;
  finally
    FInCriticalSection.Leave;
  end;
end;

procedure TValueLoader.MainThreadReturnValues;
  function GetRequest(out Request: TValueRequest): Boolean;
  begin
    Result := False;
    FOutCriticalSection.Enter;
    try
      if FOutQueue.Count <> 0 then
      begin
        Request := FOutQueue[0];
        FOutQueue.Delete(0);
        Result := True;
      end;
    finally
      FOutCriticalSection.Leave;
    end;
  end;

var
  Request: TValueRequest;
begin
  while GetRequest(Request) do
  begin
    if Request.ErrorDescription = '' then
    begin
      Request.Owner.Data[Request.Name] := Request.OutputValue;
      Request.Owner.OnAsyncDataLoaded(Request.Name);
    end
    else
      Request.Owner.OnAsyncException(Request.Name, Request.ErrorDescription);
  end;
end;

procedure TValueLoader.ThreadSafeAddRequest(const Request: TValueRequest);
begin
  FInCriticalSection.Enter;
  try
    FInQueue.Add(Request);
  finally
    FInCriticalSection.Leave;
  end;
  FEvent.SetEvent;
  FLastActivity := Now;
end;

{ TValueLoaderContainer }

constructor TValueLoaderContainer.Create;
begin
  inherited Create;
  FCriticalSection := TCriticalSection.Create;
  FLoaders := TObjectDictionary<string, TValueLoader>.Create([doOwnsValues]);
end;

procedure TValueLoaderContainer.CreateScanTimer;
begin
  if not Assigned(FScanTimer) then
  begin
    FScanTimer := TTimer.Create(nil);
    FScanTimer.Interval := 1000 * 10;
    FScanTimer.OnTimer := intScanForNotUsedLoaders;
    FScanTimer.Enabled := True;
  end;
end;

destructor TValueLoaderContainer.Destroy;
begin
  FreeAndNil(FScanTimer);
  FCriticalSection.Enter;
  try
    FreeAndNil(FLoaders);
  finally
    FCriticalSection.Leave;
  end;
  FreeAndNil(FCriticalSection);
  inherited;
end;

class procedure TValueLoaderContainer.Fin;
begin
  FreeAndNil(FInstance);
end;

function TValueLoaderContainer.FindLoader(const Request: TValueRequest): TValueLoader;
var
  sLoaderName: string;
begin
  sLoaderName := GenerateLoaderName(Request);
  FCriticalSection.Enter;
  try
    if FLoaders.ContainsKey(sLoaderName) then
      Result := FLoaders[sLoaderName]
    else
      Result := nil;
  finally
    FCriticalSection.Leave;
  end;
end;

function TValueLoaderContainer.GenerateLoaderName(const Request: TValueRequest): string;
begin
  if not Request.LimitSpeed then
    Result := Request.Owner.ClassName + '_RequestName=' + Request.Name
  else
    Result := Request.Owner.ClassName;
end;

function TValueLoaderContainer.GetLoader(const Request: TValueRequest): TValueLoader;
var
  sLoaderName: string;
begin
  FCriticalSection.Enter;
  try
    Result := FindLoader(Request);
    if not Assigned(Result) then
    begin
      sLoaderName := GenerateLoaderName(Request);
      Result := TValueLoader.Create(TLazyAsyncObjectClass(Request.Owner.ClassType), sLoaderName);
      FLoaders.Add(sLoaderName, Result);
      Result.Priority := tpIdle;

      TThread.Queue(nil, CreateScanTimer);
    end;
  finally
    FCriticalSection.Leave;
  end;
end;

class function TValueLoaderContainer.Instance: TValueLoaderContainer;
begin
  if not Assigned(FInstance) then
    FInstance := TValueLoaderContainer.Create;
  Result := FInstance;
end;

procedure TValueLoaderContainer.intDeleteLoader(const ALoaderName: string);
var
  Loader: TValueLoader;
begin
  FCriticalSection.Enter;
  try
    if FLoaders.ContainsKey(ALoaderName) then
      Loader := FLoaders.ExtractPair(ALoaderName).Value
    else
      Loader := nil;
  finally
    FCriticalSection.Leave;
  end;
  Loader.Free;
end;

procedure TValueLoaderContainer.intScanForNotUsedLoaders(Sender: TObject);
var
  NotUsedLoaders: TStringList;
  sLoaderName: string;
  i: Integer;
begin
  NotUsedLoaders := TStringList.Create;
  try
    NotUsedLoaders.Duplicates := dupIgnore;
    NotUsedLoaders.Sorted := True;
    NotUsedLoaders.CaseSensitive := False;
    FCriticalSection.Enter;
    try
      for sLoaderName in FLoaders.Keys do
        if FLoaders[sLoaderName].IsNotUsed then
          NotUsedLoaders.Add(sLoaderName);

      for i := 0 to NotUsedLoaders.Count - 1 do
        intDeleteLoader(NotUsedLoaders[i]);
    finally
      FCriticalSection.Leave;
    end;
  finally
    NotUsedLoaders.Free;
  end;
end;

procedure TValueLoaderContainer.intThreadSafeDeleteRequestByObject(AObject: TLazyAsyncObject);
var
  Loader: TValueLoader;
  s: string;
  sClassName: string;
begin
  sClassName := AObject.ClassName;
  FCriticalSection.Enter;
  try
    for s in FLoaders.Keys do
    begin
      if AnsiStartsText(sClassName, s) then
      begin
        Loader := FLoaders[s];
        if Assigned(Loader) then
          Loader.DeleteRequestsByObject(AObject);
      end;
    end;
  finally
    FCriticalSection.Leave;
  end;
end;

class procedure TValueLoaderContainer.ThreadSafeAddRequest(const Request: TValueRequest);
begin
  Instance.GetLoader(Request).ThreadSafeAddRequest(Request);
end;

class procedure TValueLoaderContainer.ThreadSafeDeleteRequestByObject(AObject: TLazyAsyncObject);
begin
  if Assigned(FInstance) then
    Instance.intThreadSafeDeleteRequestByObject(AObject);
end;

initialization

finalization

FinalizeAsyncLoad;

end.
