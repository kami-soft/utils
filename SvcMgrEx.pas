{ ******************************************************* }
{ }
{ Delphi Visual Component Library }
{ }
{ Copyright(c) 1995-2010 Embarcadero Technologies, Inc. }
{ }
{ ******************************************************* }
/// this is extension for standart TService
/// new TService can work with HandlerEx service function,
/// which allow to use much more functions, for example - user logon/logout, device connections, etc...

/// author: kami (c) 2015
/// you can use this code without any limitations.
/// The code provided "as is", wtihout warranty of any kind.
unit SvcMgrEx;
{$J+,H+,X+}

interface

uses
  {$IF DEFINED(CLR)}
  System.Security.Permissions,
  System.ComponentModel.Design.Serialization,
  {$IFEND}
  Windows,
  Messages,
  WinSvc,
  SysUtils,
  Classes;

type

  { TEventLogger }

  TEventLogger = class(TObject)
  private
    FName: String;
    FEventLog: Integer;
    {$IF DEFINED(CLR)}
  strict protected
    procedure Finalize; override;
    {$IFEND}
  public
    constructor Create(Name: String);
    destructor Destroy; override;
    procedure LogMessage(Message: String; EventType: DWord = 1; Category: Word = 0; ID: DWord = 0);
  end;

  { TDependency }

  TDependency = class(TCollectionItem)
  private
    FName: String;
    FIsGroup: Boolean;
  protected
    function GetDisplayName: String; override;
  published
    property Name: String read FName write FName;
    property IsGroup: Boolean read FIsGroup write FIsGroup;
  end;

  { TDependencies }

  TDependencies = class(TCollection)
  private
    FOwner: TPersistent;
    function GetItem(Index: Integer): TDependency;
    procedure SetItem(Index: Integer; Value: TDependency);
  protected
    function GetOwner: TPersistent; override;
  public
    constructor Create(Owner: TPersistent);
    property Items[Index: Integer]: TDependency read GetItem write SetItem; default;
  end;

  { TServiceThread }

const

  CM_SERVICE_CONTROL_CODE = WM_USER + 1;
  CM_SERVICE_CONTROL_CODE_EX = WM_USER + WM_USER;

  // ����, ������� ����� ������������ ������ � DoCustomControlEx
  // ����� ��������� ��� ��� - ������������ ��������� TService.AllowAdditionalControls
const
  SERVICE_ACCEPT_SESSIONCHANGE = $80;
  SERVICE_ACCEPT_HARDWAREPROFILECHANGE = $20;
  SERVICE_ACCEPT_POWEREVENT = $40;
  SERVICE_ACCEPT_TIMECHANGE = $200;
  SERVICE_ACCEPT_TRIGGEREVENT = $400;

  // ����, ������� ��� ������ �������� � DoCustomControlEx.
  // �����������, ���� ����������� �� ������ ���������� � TService.AllowAdditionalControls
const
  SERVICE_CONTROL_DEVICEEVENT = $0B;
  SERVICE_CONTROL_HARDWAREPROFILECHANGE = $0C;
  SERVICE_CONTROL_POWEREVENT = $0D;
  SERVICE_CONTROL_SESSIONCHANGE = $0E;
  SERVICE_CONTROL_TIMECHANGE = $10;
  SERVICE_CONTROL_TRIGGEREVENT = $20;

  // ����, �������������� � ChangeServiceConfig2, �������� dwInfoLevel
const
  SERVICE_CONFIG_DESCRIPTION = 1;
  SERVICE_CONFIG_FAILURE_ACTIONS = 2;
  SERVICE_CONFIG_DELAYED_AUTO_START_INFO = 3;
  SERVICE_CONFIG_FAILURE_ACTIONS_FLAG = 4;
  SERVICE_CONFIG_SERVICE_SID_INFO = 5;
  SERVICE_CONFIG_REQUIRED_PRIVILEGES_INFO = 6;
  SERVICE_CONFIG_PRESHUTDOWN_INFO = 7;

type

  TService = class;
  {$IF DEFINED(CLR)}

  TServiceThread = class(TWin32Thread)
    {$ELSE}
    TServiceThread = class(TThread)
    {$IFEND}
    private FService: TService;

    procedure ServiceControlEx(msg: TMsg);
  protected
    procedure Execute; override;
    {$IF DEFINED(CLR)}
    property Terminated;
    {$IFEND}
  public
    constructor Create(Service: TService);
    procedure ProcessRequests(WaitForMessage: Boolean);
  end;

  { TService }
  {$IF DEFINED(CLR)}

  TServiceController = WinSvc.THandlerFunction;
  {$ELSE}
  // WinSvc.THandlerFunction is declared as TFarProc so we need to declare
  // the actual signature here to maintain backwards compatiblity.
  TServiceController = procedure(CtrlCode: DWord); stdcall;
  {$IFEND}
  TServiceControllerEx = function(dwControl: DWord; dwEventType: DWord; lpEventData, lpContext: pointer): BOOL; stdcall;

  TServiceExMsg = record
    lpEventData: pointer;
    lpContext: pointer;
  end;

  PServiceExMsg = ^TServiceExMsg;

  // ==================================================================================
  // =================== ����� ���������� ����� WinAPI ��� ChangeConfigService2 =======
  // ==================================================================================

  _SERVICE_DELAYED_AUTO_START_INFO = record
    fDelayedAutostart: BOOL;
  end;
  SERVICE_DELAYED_AUTO_START_INFO = _SERVICE_DELAYED_AUTO_START_INFO;
  TServiceDelayedAutoStartInfo = _SERVICE_DELAYED_AUTO_START_INFO;
  PSERVICE_DELAYED_AUTO_START_INFO = ^_SERVICE_DELAYED_AUTO_START_INFO;
  PServiceDelayedAutoStartInfo = ^_SERVICE_DELAYED_AUTO_START_INFO;

  _SERVICE_DESCRIPTION = record
    lpDescription: PChar;
  end;
  SERVICE_DESCRIPTION = _SERVICE_DESCRIPTION;
  TServiceDescription = _SERVICE_DESCRIPTION;
  PServiceDescription = ^_SERVICE_DESCRIPTION;

  SC_ACTION_TYPE = (SC_ACTION_NONE, SC_ACTION_RESTART, SC_ACTION_REBOOT, SC_ACTION_RUN_COMMAND);
  _SC_ACTION = record
    Type_:SC_ACTION_TYPE;
    Delay: DWord;
  end;

  SC_ACTION = _SC_ACTION;
  TSC_Action = _SC_ACTION;
  PSC_Action = ^_SC_ACTION;
  _SERVICE_FAILURE_ACTIONS = record
    dwResetPeriod: DWord;
    lpRebootMsg: PChar;
    lpCommand: PChar;
    cActions: DWord;
    lpsaActions: PSC_Action;
  end;
  SERVICE_FAILURE_ACTIONS = _SERVICE_FAILURE_ACTIONS;
  TServiceFailureActions = _SERVICE_FAILURE_ACTIONS;
  P_SERVICE_FAILURE_ACTIONS = ^_SERVICE_FAILURE_ACTIONS;
  PServiceFailureActions = ^_SERVICE_FAILURE_ACTIONS;

  _SERVICE_FAILURE_ACTIONS_FLAG = record
    fFailureActionsOnNonCrashFailures: BOOL;
  end;
  SERVICE_FAILURE_ACTIONS_FLAG = _SERVICE_FAILURE_ACTIONS_FLAG;
  TServiceFailureActionsFlag = _SERVICE_FAILURE_ACTIONS_FLAG;
  PSERVICE_FAILURE_ACTIONS_FLAG = ^_SERVICE_FAILURE_ACTIONS_FLAG;
  PServiceFailureActionsFlag = ^_SERVICE_FAILURE_ACTIONS_FLAG;

  _SERVICE_PRESHUTDOWN_INFO = record
    dwPreshutdownTimeout: DWord;
  end;
  SERVICE_PRESHUTDOWN_INFO = _SERVICE_PRESHUTDOWN_INFO;
  TServicePreshutdownInfo = _SERVICE_PRESHUTDOWN_INFO;
  PSERVICE_PRESHUTDOWN_INFO = ^_SERVICE_PRESHUTDOWN_INFO;
  PServicePreshutdownInfo = ^_SERVICE_PRESHUTDOWN_INFO;

  _SERVICE_REQUIRED_PRIVILEGES_INFO = record
    pmszRequiredPrivileges: PChar;
  end;
  SERVICE_REQUIRED_PRIVILEGES_INFO = _SERVICE_REQUIRED_PRIVILEGES_INFO;
  TServiceRequiredPrivilegesInfo = _SERVICE_REQUIRED_PRIVILEGES_INFO;
  PSERVICE_REQUIRED_PRIVILEGES_INFO = ^_SERVICE_REQUIRED_PRIVILEGES_INFO;
  PServiceRequiredPrivilegesInfo = ^_SERVICE_REQUIRED_PRIVILEGES_INFO;

  _SERVICE_SID_INFO = record
    dwServiceSidType: DWord;
  end;
  SERVICE_SID_INFO = _SERVICE_SID_INFO;
  TServiceSIDInfo = _SERVICE_SID_INFO;
  PSERVICE_SID_INFO = ^_SERVICE_SID_INFO;
  PServiceSIDInfo = ^_SERVICE_SID_INFO;

  // ================================================================================
  // ===================== ��������� ���� lpEventData �� HandlerEx ==================
  // ================================================================================
  tagWTSSESSION_NOTIFICATION = record
    cbSize: DWORD;
    dwSessionId: DWORD;
  end;
  WTSSESSION_NOTIFICATION = tagWTSSESSION_NOTIFICATION;
  PWTSSESSION_NOTIFICATION = ^tagWTSSESSION_NOTIFICATION;
  TWtsSessionNotification = tagWTSSESSION_NOTIFICATION;
  PWtsSessionNotification = PWTSSESSION_NOTIFICATION;

  _SERVICE_TIMECHANGE_INFO = record
    liNewTime: LARGE_INTEGER;
    liOldTime: LARGE_INTEGER;
  end;
  SERVICE_TIMECHANGE_INFO = _SERVICE_TIMECHANGE_INFO;
  TServiceTimechangeInfo = _SERVICE_TIMECHANGE_INFO;
  PServiceTimechangeInfo = ^_SERVICE_TIMECHANGE_INFO;
  PSERVICE_TIMECHANGE_INFO = ^_SERVICE_TIMECHANGE_INFO;

  // ==================================================================================
  // ======================= ����� ����� ���������� �����=============================
  // ==================================================================================

  TAdditionalControls = (acSessionChange, acHardwareProfileChange, acPowerEvent, acTimeChange, acTriggerEvent);
  TAdditionalControlSet = set of TAdditionalControls;

  TServiceType = (stWin32, stDevice, stFileSystem);

  TCurrentStatus = (csStopped, csStartPending, csStopPending, csRunning, csContinuePending, csPausePending, csPaused);

  TErrorSeverity = (esIgnore, esNormal, esSevere, esCritical);

  TStartType = (stBoot, stSystem, stAuto, stManual, stDisabled);

  TServiceEvent = procedure(Sender: TService) of object;
  TContinueEvent = procedure(Sender: TService; var Continued: Boolean) of object;
  TPauseEvent = procedure(Sender: TService; var Paused: Boolean) of object;
  TStartEvent = procedure(Sender: TService; var Started: Boolean) of object;
  TStopEvent = procedure(Sender: TService; var Stopped: Boolean) of object;

  [SecurityPermission(SecurityAction.Demand, Unrestricted = True)]

  TService = class(TDataModule)
  private
    FAllowStop: Boolean;
    FAllowPause: Boolean;
    FDependencies: TDependencies;
    FDisplayName: String;
    FErrCode: DWord;
    FErrorSeverity: TErrorSeverity;
    FEventLogger: TEventLogger;
    FInteractive: Boolean;
    FLoadGroup: String;
    FParams: TStringList;
    FPassword: String;
    FServiceStartName: String;
    FServiceThread: TServiceThread;
    FServiceType: TServiceType;
    FStartType: TStartType;
    FStatus: TCurrentStatus;
    FStatusHandle: THandle;
    FTagID: DWord;
    FWaitHint: Integer;
    FWin32ErrorCode: DWord;
    FBeforeInstall: TServiceEvent;
    FAfterInstall: TServiceEvent;
    FBeforeUninstall: TServiceEvent;
    FAfterUninstall: TServiceEvent;
    FOnContinue: TContinueEvent;
    FOnExecute: TServiceEvent;
    FOnPause: TPauseEvent;
    FOnShutdown: TServiceEvent;
    FOnStart: TStartEvent;
    FOnStop: TStopEvent;
    FAdditionalControls: TAdditionalControlSet;
    FDescription: String;
    function GetDisplayName: String;
    function GetParamCount: Integer;
    function GetParam(Index: Integer): String;
    procedure SetStatus(Value: TCurrentStatus);
    procedure SetDependencies(Value: TDependencies);
    function GetNTDependencies: String;
    function GetNTServiceType: Integer;
    function GetNTStartType: Integer;
    function GetNTErrorSeverity: Integer;
    function GetNTControlsAccepted: Integer;
    procedure SetOnContinue(Value: TContinueEvent);
    procedure SetOnPause(Value: TPauseEvent);
    procedure SetOnStop(Value: TStopEvent);
    function GetTerminated: Boolean;
    function AreDependenciesStored: Boolean;
    procedure SetInteractive(Value: Boolean);
    procedure SetPassword(const Value: String);
    procedure SetServiceStartName(const Value: String);
    procedure SetDescription(const Value: String);
    procedure SetAdditionalControls(const Value: TAdditionalControlSet);
  protected
    procedure Controller(CtrlCode: DWord);
    function ControllerEx(dwControl, dwEventType: DWord; lpEventData, lpContext: pointer): Boolean;
    function GetServiceHandles(var SCManagerHandle: THandle; var ServiceHandle: THandle): Boolean;
    procedure FreeServiceHandles(SCManagerHandle: THandle; ServiceHandle: THandle);
    procedure DoStart; virtual;
    function DoStop: Boolean; virtual;
    function DoPause: Boolean; virtual;
    function DoContinue: Boolean; virtual;
    procedure DoInterrogate; virtual;
    procedure DoShutdown; virtual;
    function DoCustomControl(CtrlCode: DWord): Boolean; virtual;
    function DoCustomControlEx(dwControl, dwEventType: DWord; lpEventData, lpContext: pointer): Boolean; virtual;
    {$IF DEFINED(CLR)}
    procedure Main(Argc: DWord; Argv: IntPtr);
    {$ELSE}
    procedure Main(Argc: DWord; Argv: PLPSTR);
    {$IFEND}
  public
    constructor CreateNew(AOwner: TComponent; Dummy: Integer = 0); override;
    destructor Destroy; override;
    function GetServiceController: TServiceController; virtual; abstract;
    function GetServiceControllerEx: TServiceControllerEx; virtual;
    procedure ReportStatus;
    procedure LogMessage(Message: String; EventType: DWord = 1; Category: Integer = 0; ID: Integer = 0);

    function SetFailureActions(ResetPeriod: DWord; RebootMSG: String; Command: String; Actions: array of TSC_Action): Boolean;

    property AllowAdditionalControls: TAdditionalControlSet read FAdditionalControls write SetAdditionalControls default
      [];
    property Description: String read FDescription write SetDescription;

    property ErrCode: DWord read FErrCode write FErrCode;
    property ParamCount: Integer read GetParamCount;
    property Param[Index: Integer]: String read GetParam;
    property ServiceThread: TServiceThread read FServiceThread;
    property Status: TCurrentStatus read FStatus write SetStatus;
    property Terminated: Boolean read GetTerminated;
    property Win32ErrCode: DWord read FWin32ErrorCode write FWin32ErrorCode;
  published
    property AllowStop: Boolean read FAllowStop write FAllowStop default True;
    property AllowPause: Boolean read FAllowPause write FAllowPause default True;
    property Dependencies: TDependencies read FDependencies write SetDependencies stored AreDependenciesStored;
    property DisplayName: String read GetDisplayName write FDisplayName;
    property ErrorSeverity: TErrorSeverity read FErrorSeverity write FErrorSeverity default esNormal;
    property Interactive: Boolean read FInteractive write SetInteractive default False;
    property LoadGroup: String read FLoadGroup write FLoadGroup;
    property Password: String read FPassword write SetPassword;
    property ServiceStartName: String read FServiceStartName write SetServiceStartName;
    property ServiceType: TServiceType read FServiceType write FServiceType default stWin32;
    property StartType: TStartType read FStartType write FStartType default stAuto;
    property TagID: DWord read FTagID write FTagID default 0;
    property WaitHint: Integer read FWaitHint write FWaitHint default 5000;
    property BeforeInstall: TServiceEvent read FBeforeInstall write FBeforeInstall;
    property AfterInstall: TServiceEvent read FAfterInstall write FAfterInstall;
    property BeforeUninstall: TServiceEvent read FBeforeUninstall write FBeforeUninstall;
    property AfterUninstall: TServiceEvent read FAfterUninstall write FAfterUninstall;
    property OnContinue: TContinueEvent read FOnContinue write SetOnContinue;
    property OnExecute: TServiceEvent read FOnExecute write FOnExecute;
    property OnPause: TPauseEvent read FOnPause write SetOnPause;
    property OnShutdown: TServiceEvent read FOnShutdown write FOnShutdown;
    property OnStart: TStartEvent read FOnStart write FOnStart;
    property OnStop: TStopEvent read FOnStop write SetOnStop;
  end;

  { TServiceApplication }

  [RootDesignerSerializerAttribute('', '', False)]
  [SecurityPermission(SecurityAction.Demand, Unrestricted = True)]

  TServiceApplication = class(TComponent)
  private
    FDelayInitialize: Boolean;
    FEventLogger: TEventLogger;
    FInitialized: Boolean;
    FTitle: String;
    procedure OnExceptionHandler(Sender: TObject; E: Exception);
    function GetServiceCount: Integer;
  protected
    procedure DoHandleException(E: Exception); dynamic;
    procedure RegisterServices(Install, Silent: Boolean);
    function Hook(var Message: TMessage): Boolean;
    {$IF DEFINED(CLR)}
    procedure DispatchServiceMain(Argc: DWord; Argv: IntPtr);
    {$ELSE}
    procedure DispatchServiceMain(Argc: DWord; Argv: PLPSTR);
    {$IFEND}
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property DelayInitialize: Boolean read FDelayInitialize write FDelayInitialize;
    property ServiceCount: Integer read GetServiceCount;
    // The following uses the current behaviour of the IDE module manager
    procedure CreateForm(InstanceClass: TComponentClass; var Reference); virtual;
    procedure Initialize; virtual;
    function Installing: Boolean;
    procedure Run; virtual;
    property Title: String read FTitle write FTitle;
  end;

  TRegisterServiceCtrlHandlerEx = function(lpServiceName: PChar; lpHandlerProc: TFarProc;
    lpContext: pointer): SERVICE_STATUS_HANDLE; stdcall;
  TChangeServiceConfig2 = function(scHandle: SC_HANDLE; dwInfoLevel: DWord; lpInfo: pointer): BOOL; stdcall;

var
  advapi32: THandle;
  RegisterServiceCtrlHandlerEx: TRegisterServiceCtrlHandlerEx;
  ChangeServiceConfig2: TChangeServiceConfig2;

var
  Application: TServiceApplication = nil;

implementation

uses
  {$IF DEFINED(CLR)}
  System.Runtime.InteropServices,
  System.IO,
  {$IFEND}
  Forms,
  Dialogs,
  Consts;

{ TEventLogger }

constructor TEventLogger.Create(Name: String);
begin
  inherited Create;
  FName := Name;
  FEventLog := 0;
end;
{$IF DEFINED(CLR)}

procedure TEventLogger.Finalize;
begin
  if FEventLog <> 0 then
    DeregisterEventSource(FEventLog);
  inherited;
end;
{$IFEND}

destructor TEventLogger.Destroy;
begin
  if FEventLog <> 0 then
    begin
      DeregisterEventSource(FEventLog);
      FEventLog := 0;
    end;
  {$IF DEFINED(CLR)}
  System.GC.SuppressFinalize(self);
  {$IFEND}
  inherited Destroy;
end;

procedure TEventLogger.LogMessage(Message: String; EventType: DWord; Category: Word; ID: DWord);
{$IF DEFINED(CLR)}
var
  P, PP: IntPtr;
begin
  if FEventLog = 0 then
    FEventLog := RegisterEventSource(nil, FName);
  P := Marshal.StringToHGlobalAuto(Message);
  try
    PP := Marshal.AllocHGlobal(SizeOf(IntPtr));
    try
      Marshal.WriteIntPtr(PP, P);
      ReportEvent(FEventLog, EventType, Category, ID, nil, 1, 0, PP, nil);
    finally
      Marshal.FreeHGlobal(PP);
    end;
  finally
    Marshal.FreeHGlobal(P);
  end;
end;
{$ELSE}

var
  P: pointer;
begin
  P := PChar(Message);
  if FEventLog = 0 then
    FEventLog := RegisterEventSource(nil, PChar(FName));
  ReportEvent(FEventLog, EventType, Category, ID, nil, 1, 0, @P, nil);
end;
{$IFEND}
{ TDependency }

function TDependency.GetDisplayName: String;
begin
  if Name <> '' then
    Result := Name
  else
    Result := inherited GetDisplayName;
end;

{ TDependencies }

constructor TDependencies.Create(Owner: TPersistent);
begin
  FOwner := Owner;
  inherited Create(TDependency);
end;

function TDependencies.GetItem(Index: Integer): TDependency;
begin
  Result := TDependency( inherited GetItem(Index));
end;

procedure TDependencies.SetItem(Index: Integer; Value: TDependency);
begin
  inherited SetItem(Index, TCollectionItem(Value));
end;

function TDependencies.GetOwner: TPersistent;
begin
  Result := FOwner;
end;

{ TServiceThread }

constructor TServiceThread.Create(Service: TService);
begin
  {$IF DEFINED(CLR)}
  inherited Create(True);
  FService := Service;
  {$ELSE}
  FService := Service;
  inherited Create(True);
  {$IFEND}
end;

procedure TServiceThread.Execute;
var
  msg: TMsg;
  Started: Boolean;
begin
  PeekMessage(msg, 0, WM_USER, WM_USER, PM_NOREMOVE); { Create message queue }
  try
    // Allow initialization of the Application object after
    // StartServiceCtrlDispatcher to prevent conflicts under
    // Windows 2003 Server when registering a class object with OLE.
    if Application.DelayInitialize then
      Application.Initialize;
    FService.Status := csStartPending;
    Started := True;
    if Assigned(FService.OnStart) then
      FService.OnStart(FService, Started);
    if not Started then
      Exit;
    try
      FService.Status := csRunning;
      if Assigned(FService.OnExecute) then
        FService.OnExecute(FService)
      else
        ProcessRequests(True);
      ProcessRequests(False);
    except
      on E: Exception do
        FService.LogMessage(Format(SServiceFailed, [SExecute, E.Message]));
    end;
  except
    on E: Exception do
      FService.LogMessage(Format(SServiceFailed, [SStart, E.Message]));
  end;
end;

const
  ActionStr: array [1 .. 5] of String = (SStop, SPause, SContinue, SInterrogate, SShutdown);

procedure TServiceThread.ProcessRequests(WaitForMessage: Boolean);
var
  msg: TMsg;
  OldStatus: TCurrentStatus;
  ErrorMsg: String;
  ActionOK, Rslt: Boolean;
begin
  while True do
    begin
      if Terminated and WaitForMessage then
        break;
      if WaitForMessage then
        Rslt := GetMessage(msg, 0, 0, 0)
      else
        Rslt := PeekMessage(msg, 0, 0, 0, PM_REMOVE);
      if not Rslt then
        break;
      if msg.hwnd = 0 then { Thread message }
        begin
          if msg.Message = CM_SERVICE_CONTROL_CODE then
            begin
              OldStatus := FService.Status;
              try
                ActionOK := True;
                (* {$IF DEFINED(CLR)}
                  case msg.wParam.ToInt64 of
                  {$ELSE} *)
                case msg.wParam of
                  // {$IFEND}
                  SERVICE_CONTROL_STOP:
                    ActionOK := FService.DoStop;
                  SERVICE_CONTROL_PAUSE:
                    ActionOK := FService.DoPause;
                  SERVICE_CONTROL_CONTINUE:
                    ActionOK := FService.DoContinue;
                  SERVICE_CONTROL_SHUTDOWN:
                    FService.DoShutdown;
                  SERVICE_CONTROL_INTERROGATE:
                    FService.DoInterrogate;
                else
                  ActionOK := FService.DoCustomControl(msg.wParam);
                end;
                if not ActionOK then
                  FService.Status := OldStatus;
              except
                on E: Exception do
                  begin
                    if msg.wParam <> SERVICE_CONTROL_SHUTDOWN then
                      FService.Status := OldStatus;
                    {$IF DEFINED(CLR)}
                    if msg.wParam.ToInt64 in [1 .. 5] then
                      {$ELSE}
                      if msg.wParam in [1 .. 5] then
                        {$IFEND}
                        ErrorMsg := Format(SServiceFailed, [ActionStr[Integer(msg.wParam)], E.Message])
                      else
                        ErrorMsg := Format(SCustomError, [msg.wParam, E.Message]);
                    FService.LogMessage(ErrorMsg);
                  end;
              end;
            end
          else
            if msg.Message >= CM_SERVICE_CONTROL_CODE_EX then
              ServiceControlEx(msg)
            else
              DispatchMessage(msg);
        end
      else
        DispatchMessage(msg);
    end;
end;

procedure TServiceThread.ServiceControlEx(msg: TMsg);
var
  svcMsg: PServiceExMsg;
  OldStatus: TCurrentStatus;
  ActionOK: Boolean;
  ErrorMsg: String;
begin
  msg.Message := msg.Message - CM_SERVICE_CONTROL_CODE_EX;
  svcMsg := PServiceExMsg(msg.lParam);
  try
    OldStatus := FService.Status;
    try
      ActionOK := True;
      case msg.Message of
        SERVICE_CONTROL_STOP:
          ActionOK := FService.DoStop;
        SERVICE_CONTROL_PAUSE:
          ActionOK := FService.DoPause;
        SERVICE_CONTROL_CONTINUE:
          ActionOK := FService.DoContinue;
        SERVICE_CONTROL_SHUTDOWN:
          FService.DoShutdown;
        SERVICE_CONTROL_INTERROGATE:
          FService.DoInterrogate;
      else
        ActionOK := FService.DoCustomControlEx(msg.Message, msg.wParam, svcMsg.lpEventData, svcMsg.lpContext);
      end;
      if not ActionOK then
        FService.Status := OldStatus;
    except
      on E: Exception do
        begin
          if msg.Message <> SERVICE_CONTROL_SHUTDOWN then
            FService.Status := OldStatus;
          if msg.Message in [1 .. 5] then
            ErrorMsg := Format(SServiceFailed, [ActionStr[Integer(msg.Message)], E.Message])
          else
            ErrorMsg := Format(SCustomError, [msg.Message, E.Message]);
          FService.LogMessage(ErrorMsg);
        end;
    end;
  finally
    if Assigned(svcMsg.lpEventData) then
      FreeMem(svcMsg.lpEventData);
    FreeMem(svcMsg);
  end;
end;

{ TService }

constructor TService.CreateNew(AOwner: TComponent; Dummy: Integer = 0);
begin
  inherited CreateNew(AOwner, Dummy);
  FWaitHint := 5000;
  FInteractive := False;
  FServiceType := stWin32;
  FParams := TStringList.Create;
  FDependencies := TDependencies.Create(self);
  FErrorSeverity := esNormal;
  FStartType := stAuto;
  FTagID := 0;
  FAllowStop := True;
  FAllowPause := True;
end;

destructor TService.Destroy;
begin
  FDependencies.Free;
  FParams.Free;
  FEventLogger.Free;
  inherited Destroy;
end;

function TService.GetDisplayName: String;
begin
  if FDisplayName <> '' then
    Result := FDisplayName
  else
    Result := Name;
end;

procedure TService.SetInteractive(Value: Boolean);
begin
  if Value = FInteractive then
    Exit;
  if Value then
    begin
      Password := '';
      ServiceStartName := '';
    end;
  FInteractive := Value;
end;

procedure TService.SetPassword(const Value: String);
begin
  if Value = FPassword then
    Exit;
  if Value <> '' then
    Interactive := False;
  FPassword := Value;
end;

procedure TService.SetServiceStartName(const Value: String);
begin
  if Value = FServiceStartName then
    Exit;
  if Value <> '' then
    Interactive := False;
  FServiceStartName := Value;
end;

procedure TService.SetAdditionalControls(const Value: TAdditionalControlSet);
begin
  FAdditionalControls := Value;
  ReportStatus;
end;

procedure TService.SetDependencies(Value: TDependencies);
begin
  FDependencies.Assign(Value);
end;

procedure TService.SetDescription(const Value: String);
var
  Desc: TServiceDescription;
  hSCM, hService: THandle;
begin
  if Assigned(ChangeServiceConfig2) then
    if GetServiceHandles(hSCM, hService) then
      begin
        Desc.lpDescription := AllocMem(Length(Description) * SizeOf(Char) + SizeOf(Char));

        Move(Description[1], Desc.lpDescription^, Length(Description) * SizeOf(Char));
        if ChangeServiceConfig2(hService, SERVICE_CONFIG_DESCRIPTION, @Desc) then
          FDescription := Value;

        FreeMem(Desc.lpDescription);

        FreeServiceHandles(hSCM, hService);
      end;
end;

function TService.SetFailureActions(ResetPeriod: DWord; RebootMSG, Command: String; Actions: array of TSC_Action): boolean;
var
  sfa: TServiceFailureActions;
  hSCM, hService: THandle;
  tmpActions: array of TSC_Action;
  i: Integer;
begin
  Result:=False;
  if Assigned(ChangeServiceConfig2) then
    if GetServiceHandles(hSCM, hService) then
      begin
        // � ���������, Actions �������� ����� ����� ������.
        // ���� ���������� � ChangeServiceConfig2 ��������������� �������� Actions,
        // �� ����� ���������� "�������� ��������". ������ - ����� ���������
        // ���������� ����� ������ TSC_Action ������.
        SetLength(tmpActions, Length(Actions));
        if Length(Actions)<>0 then
          begin
            FillChar(tmpActions[0], Length(Actions)*SizeOf(TSC_Action), 0);
            for i := 0 to Length(Actions)-1 do
              begin
                tmpActions[i].Type_ := Actions[i].Type_;
                tmpActions[i].Delay := Actions[i].Delay;
              end;
          end;

        sfa.dwResetPeriod := ResetPeriod;
        sfa.lpRebootMsg := PChar(RebootMSG);
        sfa.lpCommand := PChar(Command);
        sfa.cActions := Length(tmpActions);
        sfa.lpsaActions := Pointer(tmpActions);

        Result:=ChangeServiceConfig2(hService, SERVICE_CONFIG_FAILURE_ACTIONS, @sfa);

        FreeServiceHandles(hSCM, hService);
      end;
end;

function TService.AreDependenciesStored: Boolean;
begin
  Result := FDependencies.Count > 0;
end;

function TService.GetParamCount: Integer;
begin
  Result := FParams.Count;
end;

function TService.GetServiceControllerEx: TServiceControllerEx;
begin
  Result := nil;
end;

function TService.GetServiceHandles(var SCManagerHandle, ServiceHandle: THandle): Boolean;
begin
  Result := False;
  SCManagerHandle := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
  if SCManagerHandle <> 0 then
    begin
      ServiceHandle := OpenService(SCManagerHandle, PChar(Name), SERVICE_ALL_ACCESS);
      if ServiceHandle <> 0 then
        Result := True
      else
        CloseServiceHandle(SCManagerHandle);
    end;
end;

function TService.GetParam(Index: Integer): String;
begin
  Result := FParams[Index];
end;

procedure TService.SetOnContinue(Value: TContinueEvent);
begin
  FOnContinue := Value;
  AllowPause := True;
end;

procedure TService.SetOnPause(Value: TPauseEvent);
begin
  FOnPause := Value;
  AllowPause := True;
end;

procedure TService.SetOnStop(Value: TStopEvent);
begin
  FOnStop := Value;
  AllowStop := True;
end;

function TService.GetTerminated: Boolean;
begin
  Result := False;
  if Assigned(FServiceThread) then
    Result := FServiceThread.Terminated;
end;

function TService.GetNTDependencies: String;
var
  {$IF DEFINED(CLR)}
  I, J, Len: Integer;
  Pos: Integer;
  Temp: String;
  {$ELSE}
  I, Len: Integer;
  P: PChar;
  {$IFEND}
begin
  Result := '';
  Len := 0;
  for I := 0 to Dependencies.Count - 1 do
    begin
      Inc(Len, Length(Dependencies[I].Name) + 1); // For null-terminator
      if Dependencies[I].IsGroup then
        Inc(Len);
    end;
  {$IF DEFINED(CLR)}
  if Len <> 0 then
    begin
      Inc(Len); // For final null-terminator;
      SetLength(Result, Len);
      Pos := 1;
      for I := 0 to Dependencies.Count - 1 do
        begin
          if Dependencies[I].IsGroup then
            begin
              Result[Pos] := SC_GROUP_IDENTIFIER;
              Inc(Pos);
            end;
          Temp := Dependencies[I].Name;
          Len := Length(Temp) + 1;
          SetLength(Temp, Len); // add one for null-terminator
          for J := 1 to Len do
            begin
              Result[Pos] := Temp[J];
              Inc(Pos);
            end;
        end;
      Result[Pos] := #0;
    end;
  {$ELSE}
  if Len <> 0 then
    begin
      Inc(Len); // For final null-terminator;
      SetLength(Result, Len);
      P := @Result[1];
      for I := 0 to Dependencies.Count - 1 do
        begin
          if Dependencies[I].IsGroup then
            begin
              P^ := SC_GROUP_IDENTIFIER;
              Inc(P);
            end;
          P := StrECopy(P, PChar(Dependencies[I].Name));
          Inc(P);
        end;
      P^ := #0;
    end;
  {$IFEND}
end;

const
  NTServiceType: array [TServiceType] of Integer = (SERVICE_WIN32_OWN_PROCESS, SERVICE_KERNEL_DRIVER,
    SERVICE_FILE_SYSTEM_DRIVER);

function TService.GetNTServiceType: Integer;
begin
  Result := NTServiceType[FServiceType];
  if (FServiceType = stWin32) and Interactive then
    Result := Result or SERVICE_INTERACTIVE_PROCESS;
  if (FServiceType = stWin32) and (Application.ServiceCount > 1) then
    Result := (Result xor SERVICE_WIN32_OWN_PROCESS) or SERVICE_WIN32_SHARE_PROCESS;
end;

const
  NTStartType: array [TStartType] of Integer = (SERVICE_BOOT_START, SERVICE_SYSTEM_START, SERVICE_AUTO_START,
    SERVICE_DEMAND_START, SERVICE_DISABLED);

function TService.GetNTStartType: Integer;
begin
  Result := NTStartType[FStartType];
  if (FStartType in [stBoot, stSystem]) and (FServiceType <> stDevice) then
    Result := SERVICE_AUTO_START;
end;

function TService.GetNTErrorSeverity: Integer;
const
  NTErrorSeverity: array [TErrorSeverity] of Integer = (SERVICE_ERROR_IGNORE, SERVICE_ERROR_NORMAL,
    SERVICE_ERROR_SEVERE, SERVICE_ERROR_CRITICAL);
begin
  Result := NTErrorSeverity[FErrorSeverity];
end;

function TService.GetNTControlsAccepted: Integer;
const
  AddControlsAccepted: array [ Low(TAdditionalControls) .. High(TAdditionalControls)] of Integer =
    (SERVICE_ACCEPT_SESSIONCHANGE, SERVICE_ACCEPT_HARDWAREPROFILECHANGE, SERVICE_ACCEPT_POWEREVENT,
    SERVICE_ACCEPT_TIMECHANGE, SERVICE_ACCEPT_TRIGGEREVENT);
var
  I: TAdditionalControls;
  ControllerEx: TServiceControllerEx;
begin
  Result := SERVICE_ACCEPT_SHUTDOWN;
  if AllowStop then
    Result := Result or SERVICE_ACCEPT_STOP;
  if AllowPause then
    Result := Result or SERVICE_ACCEPT_PAUSE_CONTINUE;
  ControllerEx := GetServiceControllerEx();
  if Assigned(ControllerEx) then
    for I := Low(TAdditionalControls) to High(TAdditionalControls) do
      if I in FAdditionalControls then
        Result := Result or AddControlsAccepted[I];
end;

procedure TService.LogMessage(Message: String; EventType: DWord; Category, ID: Integer);
begin
  if FEventLogger = nil then
    FEventLogger := TEventLogger.Create(Name);
  FEventLogger.LogMessage(Message, EventType, Category, ID);
end;

const
  LastStatus: TCurrentStatus = csStartPending;
  NTServiceStatus: array [TCurrentStatus] of Integer = (SERVICE_STOPPED, SERVICE_START_PENDING, SERVICE_STOP_PENDING,
    SERVICE_RUNNING, SERVICE_CONTINUE_PENDING, SERVICE_PAUSE_PENDING, SERVICE_PAUSED);
  PendingStatus: set of TCurrentStatus = [csStartPending, csStopPending, csContinuePending, csPausePending];

procedure TService.ReportStatus;
var
  ServiceStatus: TServiceStatus;
begin
  with ServiceStatus do
    begin
      dwWaitHint := FWaitHint;
      dwServiceType := GetNTServiceType;
      if FStatus = csStartPending then
        dwControlsAccepted := 0
      else
        dwControlsAccepted := GetNTControlsAccepted;
      if (FStatus in PendingStatus) and (FStatus = LastStatus) then
        Inc(dwCheckPoint)
      else
        dwCheckPoint := 0;
      LastStatus := FStatus;
      dwCurrentState := NTServiceStatus[FStatus];
      dwWin32ExitCode := Win32ErrCode;
      dwServiceSpecificExitCode := ErrCode;
      if ErrCode <> 0 then
        dwWin32ExitCode := ERROR_SERVICE_SPECIFIC_ERROR;
      if not SetServiceStatus(FStatusHandle, ServiceStatus) then
        LogMessage(SysErrorMessage(GetLastError));
    end;
end;

procedure TService.SetStatus(Value: TCurrentStatus);
begin
  FStatus := Value;
  if not(csDesigning in ComponentState) then
    ReportStatus;
end;
{$IF DEFINED(CLR)}

procedure TService.Main(Argc: DWord; Argv: IntPtr);
var
  I: Integer;
  Controller: TServiceController;
  PStr: IntPtr;
begin
  for I := 0 to Argc - 1 do
    begin
      PStr := Marshal.ReadIntPtr(Argv, I * SizeOf(IntPtr));
      FParams.Add(Marshal.PtrToStringAuto(PStr));
    end;
  Controller := self.GetServiceController();
  FStatusHandle := RegisterServiceCtrlHandler(Name, Controller);
  if (FStatusHandle = 0) then
    LogMessage(SysErrorMessage(GetLastError))
  else
    DoStart;
end;
{$ELSE}

procedure TService.Main(Argc: DWord; Argv: PLPSTR);
type
  PPCharArray = ^TPCharArray;
  TPCharArray = array [0 .. 1024] of PChar;
var
  I: Integer;
  Controller: TServiceController;
  ControllerEx: TServiceControllerEx;
begin
  for I := 0 to Argc - 1 do
    FParams.Add(PPCharArray(Argv)[I]);
  ControllerEx := GetServiceControllerEx();
  if Assigned(ControllerEx) and Assigned(RegisterServiceCtrlHandlerEx) then
    begin
      FStatusHandle := RegisterServiceCtrlHandlerEx(PChar(Name), @ControllerEx, nil);
    end
  else
    begin
      Controller := GetServiceController();
      FStatusHandle := RegisterServiceCtrlHandler(PChar(Name), @Controller);
    end;
  if (FStatusHandle = 0) then
    LogMessage(SysErrorMessage(GetLastError))
  else
    DoStart;
end;
{$IFEND}

procedure TService.Controller(CtrlCode: DWord);
begin
  PostThreadMessage(ServiceThread.ThreadID, CM_SERVICE_CONTROL_CODE, CtrlCode, 0);
  if ServiceThread.Suspended then
    ServiceThread.Resume;
end;

function TService.ControllerEx(dwControl, dwEventType: DWord; lpEventData, lpContext: pointer): Boolean;
var
  CM_Message: DWord;
  pSvcMsg: PServiceExMsg;
  Size: DWord;
  PPowSettings: PPOWERBROADCAST_SETTING;
begin
  CM_Message := CM_SERVICE_CONTROL_CODE_EX + dwControl;

  pSvcMsg := AllocMem(SizeOf(TServiceExMsg));
  pSvcMsg.lpContext := lpContext;

  if Assigned(lpEventData) then
    begin
      case dwControl of
        SERVICE_CONTROL_DEVICEEVENT:
          begin
            Move(lpEventData^, Size, SizeOf(DWord)); // � ��������� DEV_BROADCAST_HDR
            // ������ ���� ������ ���������, DWORD
            pSvcMsg.lpEventData := AllocMem(Size);
            Move(lpEventData^, pSvcMsg.lpEventData^, Size);
          end;
        SERVICE_CONTROL_POWEREVENT:
          begin
            if (dwEventType = PBT_POWERSETTINGCHANGE) then
              begin
                PPowSettings := lpEventData;
                Size := SizeOf(POWERBROADCAST_SETTING) + PPowSettings.DataLength - 1;
                pSvcMsg.lpEventData := AllocMem(Size);
                Move(PPowSettings^, pSvcMsg.lpEventData^, Size);
              end;
          end;
        SERVICE_CONTROL_SESSIONCHANGE:
          begin
            Move(lpEventData^, Size, SizeOf(DWord));
            // � ��������� WTSSESSION_NOTIFICATION
            // ������ ���� ������ ���������, DWORD
            pSvcMsg.lpEventData := AllocMem(Size);
            Move(lpEventData^, pSvcMsg.lpEventData^, Size);
          end;
        SERVICE_CONTROL_TIMECHANGE:
          begin
            Size := SizeOf(_SERVICE_TIMECHANGE_INFO);
            pSvcMsg.lpEventData := AllocMem(Size);
            Move(lpEventData^, pSvcMsg.lpEventData^, Size);
          end;
      else
        pSvcMsg.lpEventData := nil;
      end;
    end
  else
    pSvcMsg.lpEventData := nil;

  PostThreadMessage(ServiceThread.ThreadID, CM_Message, dwEventType, Integer(pSvcMsg));
  if ServiceThread.Suspended then
    ServiceThread.Resume;
  Result := True;
end;

procedure TService.DoStart;
begin
  try
    Status := csStartPending;
    try
      FServiceThread := TServiceThread.Create(self);
      FServiceThread.Resume;
      FServiceThread.WaitFor;
      FreeAndNil(FServiceThread);
    finally
      Status := csStopped;
    end;
  except
    on E: Exception do
      LogMessage(Format(SServiceFailed, [SExecute, E.Message]));
  end;
end;

function TService.DoStop: Boolean;
begin
  Result := True;
  Status := csStopPending;
  if Assigned(FOnStop) then
    FOnStop(self, Result);
  if Result then
    ServiceThread.Terminate;
end;

procedure TService.FreeServiceHandles(SCManagerHandle, ServiceHandle: THandle);
begin
  CloseServiceHandle(ServiceHandle);
  CloseServiceHandle(SCManagerHandle);
end;

function TService.DoPause: Boolean;
begin
  Result := True;
  Status := csPausePending;
  if Assigned(FOnPause) then
    FOnPause(self, Result);
  if Result then
    begin
      Status := csPaused;
      ServiceThread.Suspend;
    end;
end;

function TService.DoContinue: Boolean;
begin
  Result := True;
  Status := csContinuePending;
  if Assigned(FOnContinue) then
    FOnContinue(self, Result);
  if Result then
    Status := csRunning;
end;

procedure TService.DoInterrogate;
begin
  ReportStatus;
end;

procedure TService.DoShutdown;
begin
  Status := csStopPending;
  try
    if Assigned(FOnShutdown) then
      FOnShutdown(self);
  finally
    { Shutdown cannot abort, it must stop regardless of any exception }
    ServiceThread.Terminate;
  end;
end;

function TService.DoCustomControl(CtrlCode: DWord): Boolean;
begin
  Result := True;
end;

function TService.DoCustomControlEx(dwControl, dwEventType: DWord; lpEventData, lpContext: pointer): Boolean;
begin
  Result := True;
end;

{ TServiceApplication }

type
  TServiceClass = class of TService;

  (* {$IF DEFINED(CLR)}

    procedure ServiceMain(Argc: DWord; Argv: IntPtr);
    {$ELSE} *)
procedure ServiceMain(Argc: DWord; Argv: PLPSTR); stdcall;
// {$IFEND}
begin
  Application.DispatchServiceMain(Argc, Argv);
end;
{$IF DEFINED(CLR)}

const
  ServiceMainDelegate: TServiceMainFunction = @ServiceMain;
  {$IFEND}

procedure DoneServiceApplication;
begin
  with Forms.Application do
    begin
      if Handle <> 0 then
        ShowOwnedPopups(Handle, False);
      ShowHint := False;
      Destroying;
      DestroyComponents;
    end;
  with Application do
    begin
      Destroying;
      DestroyComponents;
    end;
end;

constructor TServiceApplication.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FDelayInitialize := False;
  FEventLogger := TEventLogger.Create(ExtractFileName(ParamStr(0)));
  FInitialized := False;
  Forms.Application.HookMainWindow(Hook);
end;

destructor TServiceApplication.Destroy;
begin
  FreeAndNil(FEventLogger);
  Forms.Application.OnException := nil;
  Forms.Application.UnhookMainWindow(Hook);
  inherited Destroy;
end;
{$IF DEFINED(CLR)}

procedure TServiceApplication.DispatchServiceMain(Argc: DWord; Argv: IntPtr);
var
  I: Integer;
  PStr: IntPtr;
begin
  for I := 0 to ComponentCount - 1 do
    if (Components[I] is TService) then
      begin
        PStr := Marshal.ReadIntPtr(Argv, 0);
        if SameText(Marshal.PtrToStringAuto(PStr), Components[I].Name) then
          begin
            TService(Components[I]).Main(Argc, Argv);
            break;
          end;
      end;
end;
{$ELSE}

procedure TServiceApplication.DispatchServiceMain(Argc: DWord; Argv: PLPSTR);
var
  I: Integer;
begin
  for I := 0 to ComponentCount - 1 do
    if (Components[I] is TService) and (AnsiCompareText(PChar(Argv^), Components[I].Name) = 0) then
      begin
        TService(Components[I]).Main(Argc, Argv);
        break;
      end;
end;
{$IFEND}

function TServiceApplication.GetServiceCount: Integer;
var
  I: Integer;
begin
  Result := 0;
  for I := 0 to ComponentCount - 1 do
    if Components[I] is TService then
      Inc(Result);
end;

procedure TServiceApplication.RegisterServices(Install, Silent: Boolean);

  procedure InstallService(Service: TService; SvcMgr: Integer);
  var
    {$IF DEFINED(CLR)}
    TmpTagID: DWord;
    Svc: Integer;
    {$ELSE}
    TmpTagID, Svc: Integer;
    PTag, PSSN: pointer;
    {$IFEND}
    Path: String;
  begin
    Path := ParamStr(0);
    with Service do
      begin
        if Assigned(BeforeInstall) then
          BeforeInstall(Service);
        TmpTagID := TagID;
        {$IF DEFINED(CLR)}
        if (TagID <> 0) and (ServiceStartName <> '') then
          Svc := CreateService(SvcMgr, Name, DisplayName, SERVICE_ALL_ACCESS, GetNTServiceType, GetNTStartType,
            GetNTErrorSeverity, Path, LoadGroup, TmpTagID, GetNTDependencies, ServiceStartName, Password)
        else
          if TagID <> 0 then
            Svc := CreateService(SvcMgr, Name, DisplayName, SERVICE_ALL_ACCESS, GetNTServiceType, GetNTStartType,
              GetNTErrorSeverity, Path, LoadGroup, TmpTagID, GetNTDependencies, nil, Password)
          else
            if ServiceStartName <> '' then
              Svc := CreateService(SvcMgr, Name, DisplayName, SERVICE_ALL_ACCESS, GetNTServiceType, GetNTStartType,
                GetNTErrorSeverity, Path, LoadGroup, nil, GetNTDependencies, ServiceStartName, Password)
            else
              Svc := CreateService(SvcMgr, Name, DisplayName, SERVICE_ALL_ACCESS, GetNTServiceType, GetNTStartType,
                GetNTErrorSeverity, Path, LoadGroup, nil, GetNTDependencies, nil, Password);
        {$ELSE}
        if TmpTagID > 0 then
          PTag := @TmpTagID
        else
          PTag := nil;
        if ServiceStartName = '' then
          PSSN := nil
        else
          PSSN := PChar(ServiceStartName);
        Svc := CreateService(SvcMgr, PChar(Name), PChar(DisplayName), SERVICE_ALL_ACCESS, GetNTServiceType,
          GetNTStartType, GetNTErrorSeverity, PChar(Path), PChar(LoadGroup), PTag, PChar(GetNTDependencies), PSSN,
          PChar(Password));
        {$IFEND}
        TagID := TmpTagID;
        if Svc = 0 then
          RaiseLastOSError;
        try
          try
            if Assigned(AfterInstall) then
              AfterInstall(Service);
          except
            on E: Exception do
              begin
                DeleteService(Svc);
                raise ;
              end;
          end;
        finally
          CloseServiceHandle(Svc);
        end;
      end;
  end;

  procedure UninstallService(Service: TService; SvcMgr: Integer);
  var
    Svc: Integer;
  begin
    with Service do
      begin
        if Assigned(BeforeUninstall) then
          BeforeUninstall(Service);
        {$IF DEFINED(CLR)}
        Svc := OpenService(SvcMgr, Name, SERVICE_ALL_ACCESS);
        {$ELSE}
        Svc := OpenService(SvcMgr, PChar(Name), SERVICE_ALL_ACCESS);
        {$IFEND}
        if Svc = 0 then
          RaiseLastOSError;
        try
          if not DeleteService(Svc) then
            RaiseLastOSError;
        finally
          CloseServiceHandle(Svc);
        end;
        if Assigned(AfterUninstall) then
          AfterUninstall(Service);
      end;
  end;

var
  SvcMgr: Integer;
  I: Integer;
  Success: Boolean;
  msg: String;
begin
  Success := True;
  {$IF DEFINED(CLR)}
  SvcMgr := OpenSCManager('', nil, SC_MANAGER_ALL_ACCESS);
  {$ELSE}
  SvcMgr := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
  {$IFEND}
  if SvcMgr = 0 then
    RaiseLastOSError;
  try
    for I := 0 to ComponentCount - 1 do
      if Components[I] is TService then
        try
          if Install then
            InstallService(TService(Components[I]), SvcMgr)
          else
            UninstallService(TService(Components[I]), SvcMgr) except on E: Exception do begin Success := False;
          if Install then
            msg := SServiceInstallFailed
          else
            msg := SServiceUninstallFailed;
          with TService(Components[I]) do
            MessageDlg(Format(msg, [DisplayName, E.Message]), mtError, [mbOK], 0);
        end;
  end;
  if Success and not Silent then
    if Install then
      MessageDlg(SServiceInstallOK, mtInformation, [mbOK], 0)
    else
      MessageDlg(SServiceUninstallOK, mtInformation, [mbOK], 0);
  finally
    CloseServiceHandle(SvcMgr);
  end;
end;

function TServiceApplication.Hook(var Message: TMessage): Boolean;
begin
  Result := Message.msg = WM_ENDSESSION;
end;

procedure TServiceApplication.CreateForm(InstanceClass: TComponentClass; var Reference);
begin
  if InstanceClass.InheritsFrom(TService) then
    begin
      {$IF DEFINED(CLR)}
      try
        Reference := TServiceClass(InstanceClass).Create(self);
      except
        Reference := nil;
        raise ;
      end;
      {$ELSE}
      try
        TComponent(Reference) := InstanceClass.Create(self);
      except
        TComponent(Reference) := nil;
        raise ;
      end;
      {$IFEND}
    end
  else
    Forms.Application.CreateForm(InstanceClass, Reference);
end;

procedure TServiceApplication.DoHandleException(E: Exception);
begin
  FEventLogger.LogMessage(E.Message);
end;

procedure TServiceApplication.Initialize;
begin
  if not FInitialized then
    begin
      FInitialized := True;
      Forms.Application.ShowMainForm := False;
      Forms.Application.Initialize;
    end;
end;

function FindSwitch(const Switch: String): Boolean;
begin
  Result := FindCmdLineSwitch(Switch, ['-', '/'], True);
end;

function TServiceApplication.Installing: Boolean;
begin
  Result := FindSwitch('INSTALL') or FindSwitch('UNINSTALL');
end;

procedure TServiceApplication.OnExceptionHandler(Sender: TObject; E: Exception);
begin
  DoHandleException(E);
end;

type
  TServiceTableEntryArray = array of TServiceTableEntry;

  TServiceStartThread = class(TThread)
  private
    FServiceStartTable: TServiceTableEntryArray;
  protected
    procedure DoTerminate; override;
    procedure Execute; override;
    {$IF DEFINED(CLR)}
    property ReturnValue;
    {$IFEND}
  public
    constructor Create(Services: TServiceTableEntryArray);
  end;

constructor TServiceStartThread.Create(Services: TServiceTableEntryArray);
begin
  {$IF DEFINED(CLR)}
  inherited Create(True);
  FreeOnTerminate := False;
  ReturnValue := 0;
  FServiceStartTable := Services;
  Resume;
  {$ELSE}
  FreeOnTerminate := False;
  ReturnValue := 0;
  FServiceStartTable := Services;
  inherited Create(False);
  {$IFEND}
end;

procedure TServiceStartThread.DoTerminate;
begin
  inherited DoTerminate;
  PostMessage(Forms.Application.Handle, WM_QUIT, 0, 0);
end;

procedure TServiceStartThread.Execute;
begin
  if StartServiceCtrlDispatcher(FServiceStartTable[0]) then
    ReturnValue := 0
  else
    ReturnValue := GetLastError;
end;

procedure TServiceApplication.Run;
var
  ServiceStartTable: TServiceTableEntryArray;
  ServiceCount, I, J: Integer;
  StartThread: TServiceStartThread;
begin
  {$IF NOT DEFINED(CLR)}
  AddExitProc(DoneServiceApplication);
  {$IFEND}
  if FindSwitch('INSTALL') then
    RegisterServices(True, FindSwitch('SILENT'))
  else
    if FindSwitch('UNINSTALL') then
      RegisterServices(False, FindSwitch('SILENT'))
    else
      begin
        Forms.Application.OnException := OnExceptionHandler;
        ServiceCount := 0;
        for I := 0 to ComponentCount - 1 do
          if Components[I] is TService then
            Inc(ServiceCount);
        SetLength(ServiceStartTable, ServiceCount + 1);
        {$IF NOT DEFINED(CLR)}
        FillChar(ServiceStartTable[0], SizeOf(TServiceTableEntry) * (ServiceCount + 1), 0);
        {$IFEND}
        J := 0;
        for I := 0 to ComponentCount - 1 do
          if Components[I] is TService then
            begin
              {$IF DEFINED(CLR)}
              ServiceStartTable[J].lpServiceName := Components[I].Name;
              ServiceStartTable[J].lpServiceProc := ServiceMainDelegate;
              {$ELSE}
              ServiceStartTable[J].lpServiceName := PChar(Components[I].Name);
              ServiceStartTable[J].lpServiceProc := @ServiceMain;
              {$IFEND}
              Inc(J);
            end;
        StartThread := TServiceStartThread.Create(ServiceStartTable);
        try
          while not Forms.Application.Terminated do
            try
              Forms.Application.HandleMessage;
            except
              on E: Exception do
                DoHandleException(E);
            end;
          Forms.Application.Terminate;
          {$IF DEFINED(CLR)}
          DoneServiceApplication;
          {$IFEND}
          if StartThread.ReturnValue <> 0 then
            FEventLogger.LogMessage(SysErrorMessage(StartThread.ReturnValue));
        finally
          StartThread.Free;
        end;
      end;
end;

procedure InitApplication;
begin
  Application := TServiceApplication.Create(nil);
end;

procedure DoneApplication;
begin
  Application.Free;
  Application := nil;
end;

initialization

advapi32 := LoadLibrary('advapi32.dll');
if advapi32 <> 0 then
  begin
    RegisterServiceCtrlHandlerEx := GetProcAddress(advapi32, 'RegisterServiceCtrlHandlerExW');
    ChangeServiceConfig2 := GetProcAddress(advapi32, 'ChangeServiceConfig2W');
  end
else
  begin
    RegisterServiceCtrlHandlerEx := nil;
    ChangeServiceConfig2 := nil;
  end;
InitApplication;

finalization

DoneApplication;
FreeLibrary(advapi32);

end.
