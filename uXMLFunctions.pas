unit uXMLFunctions;
{$IFDEF VER230}
  {$DEFINE XE2}
  {$DEFINE XE2Up}
{$ENDIF}
{$IFDEF VER280}
  {$DEFINE XE7}
  {$DEFINE XE2Up}
  {$DEFINE XE7Up}
{$ENDIF}
{$IFDEF VER290}
  {$DEFINE XE8}
  {$DEFINE XE2Up}
  {$DEFINE XE7Up}
  {$DEFINE XE8Up}
{$ENDIF}
{$IFDEF VER300}
  {$DEFINE DX}
  {$DEFINE XE2Up}
  {$DEFINE XE7Up}
  {$DEFINE XE8Up}
  {$DEFINE DXUp}
{$ENDIF}
{$IFDEF VER310}
  {$DEFINE DX1}
  {$DEFINE XE2Up}
  {$DEFINE XE7Up}
  {$DEFINE XE8Up}
  {$DEFINE DXUp}
  {$DEFINE DX1Up}
{$ENDIF}
{$IFDEF VER320}
{$DEFINE DX}
{$DEFINE XE2Up}
{$DEFINE XE7Up}
{$DEFINE XE8Up}
{$DEFINE DXUp}
{$DEFINE DX1Up} //Berlin
{$DEFINE DX2Up} //Tokio
{$ENDIF}

interface

uses
  XMLIntf;

type
  TSimpleXMLProps = class(TObject)
  private
    FRootNodeName: string;
    FXML: IXMLDocument;
    FxmlDocumentStr: string;

    procedure CreateXML;

    function GetXMLTagValueStr(const TagName: string): string;
    procedure SetXMLTagValueStr(const TagName: string; const Value: string);
    function GetXMLTagValueInt(const TagName: string; DefaultValue: integer): integer;
    procedure SetXMLTagValueInt(const TagName: string; DefaultValue: integer; const Value: integer);
    function GetXMLTagValueFloat(const TagName: string; DefaultValue: Double): Double;
    procedure SetXMLTagValueFloat(const TagName: string; DefaultValue: Double; const Value: Double);
    procedure SetXMLDocumentStr(const Value: string);
    function GetContainsTag(const TagName: string): Boolean;
  public
    constructor Create(const RootNodeName: string; const XmlDocument: string = '');
    destructor Destroy; override;

    procedure Clear; virtual;

    procedure Append(Props: TSimpleXMLProps; OverwriteExisting: Boolean); overload;
    procedure Append(const strProps: string; OverwriteExisting: Boolean); overload;
    procedure DeleteTag(const TagName: string);
    procedure SaveXml;

    property ContainsTag[const TagName: string]: Boolean read GetContainsTag;

    property XMLTagValueStr[const TagName: string]: string read GetXMLTagValueStr write SetXMLTagValueStr; default;
    property XMLTagValueInt[const TagName: string; DefaultValue: integer]: integer read GetXMLTagValueInt write SetXMLTagValueInt;
    property XMLTagValueFloat[const TagName: string; DefaultValue: Double]: Double read GetXMLTagValueFloat write SetXMLTagValueFloat;

    property XMLDocumentStr: string read FxmlDocumentStr write SetXMLDocumentStr;

    property XML: IXMLDocument read FXML;
  end;

function CreateAttribute(Node: IXMLNode; const AttrName, AttrValue: string): IXMLNode;
function GetXMLNodeList(XML: IXMLDocument; const RootNodeName: string): IXMLNodeList;
function SelectNodesX(xnRoot: IXMLNode; const nodePath: string): IXMLNodeList;
function SelectNodeX(xnRoot: IXMLNode; const nodePath: string): IXMLNode;
function GetNodeValueByName(XMLNodes: IXMLNodeList; const sNodeName: string): string;
function GetNodeValue(XMLNode: IXMLNode): string;
function GetAttributeValue(XMLNode: IXMLNode; const AttrName: string): string;

function GetRootNode(XML: IXMLDocument; const RootNodeName: string): IXMLNode;

function LinearizeXML(XML: IXMLDocument): IXMLDocument; overload;
function LinearizeXML(const sXML: string): string; overload;

function SafeLoadXMLDocument(const XMLFileName: string): IXMLDocument;
function SafeLoadXMLData(const Data: string): IXMLDocument;

{$IFDEF MSWINDOWS}
procedure SetSelectionNamespaces(XML: IXMLDocument; const Namespaces: string);
{$ENDIF}

implementation

uses
  SysUtils,
  System.StrUtils,
  Classes,
  XML.XMLDoc,
  {$IFDEF XE7Up} // ���� �� XE2
  {$IFNDEF MSWINDOWS}
  XML.omnixmldom, // � ����� ��� ��������������� ������ MSXML, �� ���� �� ���������
  {$ENDIF}
  {$ENDIF}
  XML.xmldom // ������ � ������������� �� �������������� ������� �� omnixml
  {$IFDEF MSWINDOWS}
  ,Winapi.msxml
  ,Xml.Win.msxmldom
  {$ENDIF}
  ;

function TryStrToFloatFull(const Str: string; out Value: Double): Boolean;
var
  FS: TFormatSettings;
begin
  FS := TFormatSettings.Create;
  FS.DecimalSeparator := '.';
  Result := TryStrToFloat(Str, Value, FS);
  if not Result then
    begin
      FS.DecimalSeparator := ',';
      Result := TryStrToFloat(Str, Value, FS);
    end;
end;

function CreateAttribute(Node: IXMLNode; const AttrName, AttrValue: string): IXMLNode;
begin
  Result := Node.OwnerDocument.CreateNode(AttrName, ntAttribute);
  Result.Text := AttrValue;
  Node.AttributeNodes.Add(Result);
  { Node.Attributes[AttrName]:=AttrValue;
    Result:=Node.AttributeNodes.FindNode(AttrName); }
end;

function GetRootNode(XML: IXMLDocument; const RootNodeName: string): IXMLNode;
begin
  Result := nil;
  if Assigned(XML) then
    if Assigned(XML.Node) then
      if Assigned(XML.Node.ChildNodes) then
        Result := XML.Node.ChildNodes.FindNode(RootNodeName);
end;

function GetXMLNodeList(XML: IXMLDocument; const RootNodeName: string): IXMLNodeList;
var
  root: IXMLNode;
begin
  // CoInitializeEx(nil, COINIT_MULTITHREADED) ���������� � ������������.
  Result := nil;
  root := GetRootNode(XML, RootNodeName);
  if Assigned(root) then
    Result := root.ChildNodes;
end;

function SelectNodesX(xnRoot: IXMLNode; const nodePath: string): IXMLNodeList;
var
  intfSelect: IDomNodeSelect;
  intfAccess: IXmlNodeAccess;
  dnlResult: IDomNodeList;
  intfDocAccess: IXmlDocumentAccess;
  doc: TXmlDocument;
  i: integer;
  dn: IDomNode;
begin
  Result := nil;
  if not Assigned(xnRoot) or not Supports(xnRoot, IXmlNodeAccess, intfAccess) or not Supports(xnRoot.DOMNode, IDomNodeSelect, intfSelect) then
    Exit;

  dnlResult := intfSelect.SelectNodes(nodePath);
  if Assigned(dnlResult) then
    begin
      Result := TXmlNodeList.Create(intfAccess.GetNodeObject, '', nil);
      if Supports(xnRoot.OwnerDocument, IXmlDocumentAccess, intfDocAccess) then
        doc := intfDocAccess.DocumentObject
      else
        doc := nil;

      for i := 0 to dnlResult.length - 1 do
        begin
          dn := dnlResult.item[i];
          Result.Add(TXmlNode.Create(dn, nil, doc));
        end;
    end;
end;

function SelectNodeX(xnRoot: IXMLNode; const nodePath: string): IXMLNode;
var
  intfSelect: IDomNodeSelect;
  dnResult: IDomNode;
  intfDocAccess: IXmlDocumentAccess;
  doc: TXmlDocument;
begin
  Result := nil;
  if not Assigned(xnRoot) or not Supports(xnRoot.DOMNode, IDomNodeSelect, intfSelect) then
    Exit;
  dnResult := intfSelect.SelectNode(nodePath);
  if Assigned(dnResult) then
    begin
      if Supports(xnRoot.OwnerDocument, IXmlDocumentAccess, intfDocAccess) then
        doc := intfDocAccess.DocumentObject
      else
        doc := nil;
      Result := TXmlNode.Create(dnResult, nil, doc);
    end;
end;

function GetNodeValueByName(XMLNodes: IXMLNodeList; const sNodeName: string): string;
var
  Node: IXMLNode;
begin
  Result := '';
  if Assigned(XMLNodes) then
    begin
      Node := XMLNodes.FindNode(sNodeName);
      Result := GetNodeValue(Node);
    end;
end;

function GetNodeValue(XMLNode: IXMLNode): string;
var
  i: integer;
  ChildNode: IXMLNode;
begin
  Result := '';
  if Assigned(XMLNode) then
    if (XMLNode.IsTextElement) or (XMLNode.NodeType in [ntAttribute, ntCData])  then
      Result := XMLNode.Text
    else
      for i := 0 to XMLNode.ChildNodes.Count - 1 do
      begin
        ChildNode:=XMLNode.ChildNodes[i];
        if (ChildNode.IsTextElement) or (ChildNode.NodeType in [ntAttribute, ntCData]) then
          Result := ChildNode.Text;
      end;
end;

function GetAttributeValue(XMLNode: IXMLNode; const AttrName: string): string;
var
  AttrNode: IXMLNode;
begin
  Result := '';
  if not Assigned(XMLNode) then
    Exit;
  if not Assigned(XMLNode.AttributeNodes) then
    Exit;
  AttrNode := XMLNode.AttributeNodes.FindNode(AttrName);
  if not Assigned(AttrNode) then
    Exit;
  Result := GetNodeValue(AttrNode);
end;

function LinearizeXML(XML: IXMLDocument): IXMLDocument; overload;
var
  SL: TStringList;
  i: integer;
  s: string;
  sFirstTag: string;
begin
  SL := TStringList.Create;
  try
    SL.Assign(XML.XML);
    for i := SL.Count - 1 downto 0 do
      begin
        s := Trim(SL[i]);
        if StartsStr('<', s) and EndsStr('>', s) then
          SL[i]:=s;
      end;

    s := '';
    if SL.Count > 0 then
      begin
        sFirstTag := SL[0];
        if Pos('<?xml', sFirstTag) > 0 then
          begin
            SL.Delete(0);
            s := sFirstTag + #13#10;
          end;
      end;
    if (s <> '') and (SL.Count = 0) then
      s := '';
    Result := LoadXMLData(s + StringReplace(SL.Text, '>'#13#10'<', '><', [rfReplaceAll]));
  finally
    SL.Free;
  end;
end;

function LinearizeXML(const sXML: string): string; overload;
var
  XML: IXMLDocument;
begin
  XML := LinearizeXML(LoadXMLData(sXML));
  XML.SaveToXML(Result);
end;

function SafeLoadXMLData(const Data: string): IXMLDocument;
begin
  try
    Result:=LoadXMLData(Data);
  except
    on e: Exception do
      Result:=nil;
  end;
end;

function SafeLoadXMLDocument(const XMLFileName: string): IXMLDocument;
begin
  try
    Result:=LoadXMLDocument(XMLFileName);
  except
    on e: Exception do
      Result:=nil;
  end;
end;

{$IFDEF MSWINDOWS}
procedure SetSelectionNamespaces(XML: IXMLDocument; const Namespaces: string);
var
  doc2: IXMLDOMDocument2;
begin
  if Xml.DOMDocument is TMSDOMDocument then
    if Supports(TMSDOMDocument(Xml.DOMDocument).MSDocument, IXMLDOMDocument2, doc2) then
      doc2.setProperty('SelectionNamespaces', 'xmlns:NS1="env" xmlns:NS2="http://www.jetplan.com/jetplan/services/Plan" xmlns:NS3="http://www.w3.org/2001/XMLSchema"');
end;
{$ENDIF}

{ TSimpleXMLProps }

procedure TSimpleXMLProps.Append(Props: TSimpleXMLProps; OverwriteExisting: Boolean);
var
  NewNodes: IXMLNodeList;
  NewNode: IXMLNode;
  i: integer;
begin
  CreateXML;
  NewNodes := GetXMLNodeList(Props.XML, Props.FRootNodeName);
  if Assigned(NewNodes) then
    begin
      for i := 0 to NewNodes.Count - 1 do
        begin
          NewNode := NewNodes[i];
          if (not ContainsTag[NewNode.NodeName]) or (OverwriteExisting) then
            XMLTagValueStr[NewNode.NodeName] := GetNodeValue(NewNode);
        end;
    end;
end;

procedure TSimpleXMLProps.Append(const strProps: string; OverwriteExisting: Boolean);
var
  NewProps: TSimpleXMLProps;
begin
  NewProps := TSimpleXMLProps.Create('root', strProps);
  try
    Append(NewProps, OverwriteExisting);
  finally
    NewProps.Free;
  end;
end;

procedure TSimpleXMLProps.Clear;
begin
  XMLDocumentStr := '';
end;

constructor TSimpleXMLProps.Create(const RootNodeName: string; const XmlDocument: string);
begin
  inherited Create;
  FRootNodeName := RootNodeName;
  FxmlDocumentStr := XmlDocument;
  CreateXML;
end;

procedure TSimpleXMLProps.CreateXML;
begin
  if not Assigned(FXML) then
    begin
      FXML := NewXMLDocument;
      if FxmlDocumentStr <> '' then
        begin
          FXML.LoadFromXML(FxmlDocumentStr);
        end
      else
        begin
          FXML.AddChild(FRootNodeName);
        end;
    end;
end;

procedure TSimpleXMLProps.DeleteTag(const TagName: string);
var
  Nodes: IXMLNodeList;
  Node: IXMLNode;
begin
  CreateXML;
  Nodes := GetXMLNodeList(FXML, FRootNodeName);
  if Assigned(Nodes) then
    begin
      Node := Nodes.FindNode(TagName);
      if Assigned(Node) then
        begin
          Nodes.Remove(Node);
          FXML.SaveToXML(FxmlDocumentStr);
        end;
    end;
end;

destructor TSimpleXMLProps.Destroy;
begin
  FXML := nil;
  inherited;
end;

function TSimpleXMLProps.GetContainsTag(const TagName: string): Boolean;
var
  Nodes: IXMLNodeList;
begin
  Result := False;
  Nodes := GetXMLNodeList(FXML, FRootNodeName);
  if Assigned(Nodes) then
    Result := Assigned(Nodes.FindNode(TagName));
end;

function TSimpleXMLProps.GetXMLTagValueStr(const TagName: string): string;
begin
  CreateXML;
  Result := GetNodeValueByName(GetXMLNodeList(FXML, FRootNodeName), TagName);
end;

function TSimpleXMLProps.GetXMLTagValueFloat(const TagName: string; DefaultValue: Double): Double;
var
  s: string;
begin
  s := GetXMLTagValueStr(TagName);
  if not TryStrToFloatFull(s, Result) then
    Result := DefaultValue;
end;

function TSimpleXMLProps.GetXMLTagValueInt(const TagName: string; DefaultValue: integer): integer;
var
  s: string;
begin
  s := GetXMLTagValueStr(TagName);
  if not TryStrToInt(s, Result) then
    Result := DefaultValue;
end;

procedure TSimpleXMLProps.SaveXml;
begin
  CreateXML;
  FXML.SaveToXML(FxmlDocumentStr);
end;

procedure TSimpleXMLProps.SetXMLDocumentStr(const Value: string);
begin
  FxmlDocumentStr := Value;
  FXML := nil;
  CreateXML;
end;

procedure TSimpleXMLProps.SetXMLTagValueStr(const TagName: string; const Value: string);
var
  RootNode: IXMLNode;
  TargetNode: IXMLNode;
begin
  CreateXML;
  RootNode := FXML.ChildNodes.FindNode(FRootNodeName);
  TargetNode := RootNode.ChildNodes.FindNode(TagName);
  if not Assigned(TargetNode) then
    TargetNode := RootNode.AddChild(TagName);

  TargetNode.Text := Value;
  FXML.SaveToXML(FxmlDocumentStr);
  //FxmlDocumentStr := StringReplace(FxmlDocumentStr, '&gt;', '>', [rfReplaceAll]);
  //FxmlDocumentStr := StringReplace(FxmlDocumentStr, '&lt;', '<', [rfReplaceAll]);
end;

procedure TSimpleXMLProps.SetXMLTagValueFloat(const TagName: string; DefaultValue: Double; const Value: Double);
begin
  SetXMLTagValueStr(TagName, FloatToStr(Value));
end;

procedure TSimpleXMLProps.SetXMLTagValueInt(const TagName: string; DefaultValue: integer; const Value: integer);
begin
  SetXMLTagValueStr(TagName, IntToStr(Value));
end;

initialization

{$IFDEF XE7Up}
  {$IFNDEF MSWINDOWS}
  DefaultDOMVendor := XML.omnixmldom.sOmniXmlVendor;
  {$ENDIF}
{$ENDIF}

end.
