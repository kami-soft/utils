unit Utils.Conversation;

interface

uses
  System.SysUtils,
  System.TypInfo,
  System.Math,
  System.Generics.Collections;

type
  // from http://stackoverflow.com/a/31604647/4908529
  // code by David Heffernan
  {$WARNINGS OFF}
  TEnumeration<T: record > = class
  strict private
    class function TypeInfo: PTypeInfo; inline; static;
    class function TypeData: PTypeData; inline; static;
  public
    class function IsEnumeration: Boolean; static;
    class function ToOrdinal(Enum: T): Integer; inline; static;
    class function FromOrdinal(Value: Integer): T; inline; static;
    class function ToString(Enum: T): string; inline; static;
    class function FromString(const S: string): T; inline; static;
    class function FromStringDef(const S: string; Default: T): T; inline; static;
    class function MinValue: Integer; inline; static;
    class function MaxValue: Integer; inline; static;
    class function InRange(Value: Integer): Boolean; inline; static;
    class function EnsureRange(Value: Integer): Integer; inline; static;
  end;

  TSet<T> = class
  public
    class function ToBytes(const Value: T): TBytes;
    class function FromBytes(const Bytes: TBytes): T;
  end;
  {$WARNINGS ON}

  //
  // CompareStringOrdinal ���������� ��� ������ �� ������� ����������, �.�.
  // "����� ����� (3)" < "����� ����� (103)"
  //
  // ���������� ��������� ��������
  // -1     - ������ ������ ������ ������
  // 0      - ������ ������������
  // 1      - ������ ������ ������ ������
  //  https://habr.com/post/181760/
  // =============================================================================
function CompareStringOrdinal(const S1, S2: string): Integer;

implementation

type
  TConversions<T: record > = class
  public
    class function StringToEnumeration(const S: string): T;
    class function EnumerationToString(Value: T): string;
  end;

  { TConversions<T> }

class function TConversions<T>.EnumerationToString(Value: T): string;
var
  P: PTypeInfo;
begin
  P := PTypeInfo(TypeInfo(T));
  case P^.Kind of
    tkEnumeration:
      case GetTypeData(P)^.OrdType of
        otSByte, otUByte:
          Result := GetEnumName(P, PByte(@Value)^);
        otSWord, otUWord:
          Result := GetEnumName(P, PWord(@Value)^);
        otSLong, otULong:
          Result := GetEnumName(P, PCardinal(@Value)^);
      end;
  else
    raise EArgumentException.CreateFmt('Type %s is not enumeration', [P^.Name]);
  end;
end;

class function TConversions<T>.StringToEnumeration(const S: string): T;
var
  P: PTypeInfo;
begin
  P := PTypeInfo(TypeInfo(T));
  case P^.Kind of
    tkEnumeration:
      case GetTypeData(P)^.OrdType of
        otSByte, otUByte:
          PByte(@Result)^ := GetEnumValue(P, S);
        otSWord, otUWord:
          PWord(@Result)^ := GetEnumValue(P, S);
        otSLong, otULong:
          PCardinal(@Result)^ := GetEnumValue(P, S);
      end;
  else
    raise EArgumentException.CreateFmt('Type %s is not enumeration', [P^.Name]);
  end;
end;

{ TEnumeration<T> }

class function TEnumeration<T>.EnsureRange(Value: Integer): Integer;
var
  ptd: PTypeData;
begin
  Assert(IsEnumeration);
  ptd := TypeData;
  Result := System.Math.EnsureRange(Value, ptd.MinValue, ptd.MaxValue);
end;

class function TEnumeration<T>.FromOrdinal(Value: Integer): T;
begin
  Assert(IsEnumeration);
  Assert(InRange(Value));
  Assert(SizeOf(Result) <= SizeOf(Value));
  Move(Value, Result, SizeOf(Result));
end;

class function TEnumeration<T>.FromString(const S: string): T;
begin
  Result := FromOrdinal(GetEnumValue(TypeInfo, S));
end;

class function TEnumeration<T>.FromStringDef(const S: string; Default: T): T;
begin
  try
    Result := FromString(S);
  except
    on e: Exception do
      Result := default;
  end;
end;

class function TEnumeration<T>.InRange(Value: Integer): Boolean;
var
  ptd: PTypeData;
begin
  Assert(IsEnumeration);
  ptd := TypeData;
  Result := System.Math.InRange(Value, ptd.MinValue, ptd.MaxValue);
end;

class function TEnumeration<T>.IsEnumeration: Boolean;
begin
  Result := TypeInfo.Kind = tkEnumeration;
end;

class function TEnumeration<T>.MaxValue: Integer;
begin
  Assert(IsEnumeration);
  Result := TypeData.MaxValue;
end;

class function TEnumeration<T>.MinValue: Integer;
begin
  Assert(IsEnumeration);
  Result := TypeData.MinValue;
end;

class function TEnumeration<T>.ToOrdinal(Enum: T): Integer;
begin
  Assert(IsEnumeration);
  Assert(SizeOf(Enum) <= SizeOf(Result));
  Result := 0; // needed when SizeOf(Enum) < SizeOf(Result)
  Move(Enum, Result, SizeOf(Enum));
  Assert(InRange(Result));
end;

class function TEnumeration<T>.ToString(Enum: T): string;
begin
  Result := GetEnumName(TypeInfo, ToOrdinal(Enum));
end;

class function TEnumeration<T>.TypeData: PTypeData;
begin
  Result := System.TypInfo.GetTypeData(TypeInfo);
end;

class function TEnumeration<T>.TypeInfo: PTypeInfo;
begin
  Result := System.TypeInfo(T);
end;

function CompareStringOrdinal(const S1, S2: string): Integer;

// ������� CharInSet ��������� ������� � Delphi 2009,
// ��� ����� ������ ������ ��������� �� ������
  function CharInSet(AChar: Char; ASet: array of Char): Boolean;
  var
    ch: Char;
  begin
    Result := False;
    for ch in ASet do
      if AChar = ch then
      begin
        Result := True;
        Break;
      end;
  end;

var
  S1IsInt, S2IsInt: Boolean;
  S1Cursor, S2Cursor: PChar;
  S1Int, S2Int, Counter, S1IntCount, S2IntCount: Integer;
  SingleByte: Byte;
begin
  // �������� �� ������ ������
  if S1 = '' then
    if S2 = '' then
    begin
      Result := 0;
      Exit;
    end
    else
    begin
      Result := -1;
      Exit;
    end;

  if S2 = '' then
  begin
    Result := 1;
    Exit;
  end;

  S1Cursor := @AnsiLowerCase(S1)[1];
  S2Cursor := @AnsiLowerCase(S2)[1];

  while True do
  begin
    // �������� �� ����� ������ ������
    if S1Cursor^ = #0 then
      if S2Cursor^ = #0 then
      begin
        Result := 0;
        Exit;
      end
      else
      begin
        Result := -1;
        Exit;
      end;

    // �������� �� ����� ������ ������
    if S2Cursor^ = #0 then
    begin
      Result := 1;
      Exit;
    end;

    // �������� �� ������ ����� � ����� �������
    S1IsInt := CharInSet(S1Cursor^, ['0','1', '2', '3', '4', '5', '6', '7', '8', '9']);
    S2IsInt := CharInSet(S2Cursor^, ['0','1', '2', '3', '4', '5', '6', '7', '8', '9']);
    if S1IsInt and not S2IsInt then
    begin
      Result := -1;
      Exit;
    end;
    if not S1IsInt and S2IsInt then
    begin
      Result := 1;
      Exit;
    end;

    // ������������ ���������
    if not(S1IsInt and S2IsInt) then
    begin
      if S1Cursor^ = S2Cursor^ then
      begin
        Inc(S1Cursor);
        Inc(S2Cursor);
        Continue;
      end;
      if S1Cursor^ < S2Cursor^ then
      begin
        Result := -1;
        Exit;
      end
      else
      begin
        Result := 1;
        Exit;
      end;
    end;

    // ����������� ����� �� ����� ����� � ����������
    S1Int := 0;
    Counter := 1;
    S1IntCount := 0;
    repeat
      Inc(S1IntCount);
      SingleByte := Byte(S1Cursor^) - Byte('0');
      S1Int := S1Int * Counter + SingleByte;
      Inc(S1Cursor);
      Counter := 10;
    until not CharInSet(S1Cursor^, ['0','1', '2', '3', '4', '5', '6', '7', '8', '9']);

    S2Int := 0;
    Counter := 1;
    S2IntCount := 0;
    repeat
      SingleByte := Byte(S2Cursor^) - Byte('0');
      Inc(S2IntCount);
      S2Int := S2Int * Counter + SingleByte;
      Inc(S2Cursor);
      Counter := 10;
    until not CharInSet(S2Cursor^, ['0','1', '2', '3', '4', '5', '6', '7', '8', '9']);

    if S1Int = S2Int then
    begin
      if S1Int = 0 then
      begin
        if S1IntCount < S2IntCount then
        begin
          Result := -1;
          Exit;
        end;
        if S1IntCount > S2IntCount then
        begin
          Result := 1;
          Exit;
        end;
      end;
      Continue;
    end;
    if S1Int < S2Int then
    begin
      Result := -1;
      Exit;
    end
    else
    begin
      Result := 1;
      Exit;
    end;
  end;
end;

{ TSet<T> }

class function TSet<T>.FromBytes(const Bytes: TBytes): T;
var
  tSize: integer;
begin
  tSize:=SizeOf(Result);
  if Length(Bytes)<>tSize then
    raise EConvertError.Create('Cant convert bytes to set: wrong size');
  Move(Bytes[0], Result, tSize);
end;

class function TSet<T>.ToBytes(const Value: T): TBytes;
var
  tSize: integer;
begin
  tSize:=SizeOf(Value);
  SetLength(Result, tSize);
  if tSize <> 0 then
    Move(Value, Result[0], tSize);
end;

end.
