unit Common.Adapters;

interface

uses
  Vcl.Graphics,
  pbInput,
  pbOutput,
  Common.AbstractClasses;

type
  TCommonFontAdapter = class(TAbstractData)
  strict private
    FName: string;
    FCharset: integer;
    FSize: integer;
    FStyle: TFontStyles;
    FColor: TColor;
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer)
      : boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    constructor Create(AOwner: TObject); override;

    procedure ApplyFont(AFont: TFont);
    procedure SaveFont(AFont: TFont);
  end;

implementation

{ TFontAdapter }

procedure TCommonFontAdapter.ApplyFont(AFont: TFont);
begin
  AFont.Name := FName;
  AFont.Charset := FCharset;
  AFont.Size := FSize;
  AFont.Style := FStyle;
  AFont.Color := FColor;
end;

constructor TCommonFontAdapter.Create(AOwner: TObject);
var
  tmpFont: TFont;
begin
  inherited;
  tmpFont := TFont.Create;
  try
    SaveFont(tmpFont);
  finally
    tmpFont.Free;
  end;
end;

function TCommonFontAdapter.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer): boolean;
begin
  Result := inherited;
  if Result then
    exit;
  case FieldNumber of
    1:
      FName := ProtoBuf.readString;
    2:
      FCharset := ProtoBuf.readInt32;
    3:
      FSize := ProtoBuf.readInt32;
    4:
      FStyle := TFontStyles(Byte(ProtoBuf.readInt32));
    5:
      FColor := ProtoBuf.readInt32;
  end;
  Result := FieldNumber in [1 .. 5];
end;

procedure TCommonFontAdapter.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeString(1, FName);
  ProtoBuf.writeInt32(2, FCharset);
  ProtoBuf.writeInt32(3, FSize);
  ProtoBuf.writeInt32(4, integer(Byte(FStyle)));
  ProtoBuf.writeInt32(5, FColor);
end;

procedure TCommonFontAdapter.SaveFont(AFont: TFont);
begin
  FName := AFont.Name;
  FCharset := AFont.Charset;
  FSize := AFont.Size;
  FStyle := AFont.Style;
  FColor := AFont.Color;
end;

end.
