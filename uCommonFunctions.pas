unit uCommonFunctions;

interface
uses
  Windows,
  SysUtils,
  Classes;

function Min(a, b: integer): integer; inline;
function Max(a, b: Integer): Integer; inline;

procedure ReadStringFromStream(Stream: TStream; var str: string);
procedure WriteStringToStream(Stream: TStream; const str: string);

function TryStrToFloatFull(const str: string; out Value: Double): Boolean;

function Hex(const Digit: Integer): AnsiChar;

function UTCToLocalTime(AValue: TDateTime): TDateTime;
function LocalTimeToUTC(AValue: TDateTime): TDateTime;

implementation

function Min(a, b: integer): integer; inline;
begin
  if a < b then
    Result := a
  else
    Result := b;
end;

function Max(a, b: Integer): Integer; inline;
begin
  if a > b then
    Result := a
  else
    Result := b;
end;

const
  MaxStrLength = 65535;

procedure ReadStringFromStream(Stream: TStream; var str: string);
var
  i: integer;
begin
  str := '';
  if Stream.read(i, SizeOf(integer)) = SizeOf(integer) then
    begin
      if (i > 0) and (i < MaxStrLength) then
        begin
          SetLength(str, i);
          if Stream.read(str[1], i * SizeOf(Char)) <> (i * SizeOf(Char)) then
            raise EStreamError.Create('������ ��� ������ ������ �� ������');
        end
      else
        if (i < 0) or (i >= MaxStrLength) then
          raise EStreamError.Create('������ ��� ������ ������� ������ �� ������');
    end
  else
    raise EStreamError.Create('������ ��� ������ ������� ������ �� ������');
end;

procedure WriteStringToStream(Stream: TStream; const str: string);
var
  i: integer;
begin
  i := Length(str);
  if i < MaxStrLength then
    begin
      Stream.write(i, SizeOf(integer));
      if i > 0 then
        Stream.write(str[1], i * SizeOf(Char));
    end
  else
    raise EStreamError.Create('������! ������ ������ ��������� ���������� (' + IntToStr(i) + ')');
end;

function TryStrToFloatFull(const str: string; out Value: Double): Boolean;
var
  FS: TFormatSettings;
begin
  Result := True;
  GetLocaleFormatSettings(GetThreadLocale, FS);
  if not TryStrToFloat(str, Value, FS) then
    begin
      if FS.DecimalSeparator = '.' then
        FS.DecimalSeparator := ','
      else
        FS.DecimalSeparator := '.';
      if not TryStrToFloat(str, Value, FS) then
        begin
          Value := 0;
          Result := False;
        end;
    end;
end;

const
  HexData: array [0 .. 15] of AnsiChar = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F');

function Hex(const Digit: Integer): AnsiChar;
begin
  Result := HexData[Digit];
end;

function TzSpecificLocalTimeToSystemTime(lpTimeZoneInformation: PTimeZoneInformation; var lpLocalTime, lpUniversalTime: TSystemTime): BOOL; stdcall;
external kernel32 name 'TzSpecificLocalTimeToSystemTime';

function UTCToLocalTime(AValue: TDateTime): TDateTime;
// AValue - ����� UTC
// Result - ����� � ������ ���������� GMT-�������� � ��������� �������� �� ������ �����
var
  ST1, ST2: TSystemTime;
  TZ: TTimeZoneInformation;
begin
  // TZ - ��������� ��������� Windows
  GetTimeZoneInformation(TZ);
  // �������������� TDateTime � WindowsSystemTime
  DateTimeToSystemTime(AValue, ST1);
  // ���������� ��������� �������� �� �������
  SystemTimeToTzSpecificLocalTime(@TZ, ST1, ST2);
  // ���������� SystemTime � TDateTime
  Result := SystemTimeToDateTime(ST2);
end;

function LocalTimeToUTC(AValue: TDateTime): TDateTime;
var
  ST1, ST2: TSystemTime;
  TZ: TTimeZoneInformation;
begin
  // TZ - ��������� ��������� Windows
  GetTimeZoneInformation(TZ);
  // �������������� TDateTime � WindowsSystemTime
  DateTimeToSystemTime(AValue, ST1);
  // ���������� ��������� �������� �� �������
  TzSpecificLocalTimeToSystemTime(@TZ, ST1, ST2);
  // ���������� SystemTime � TDateTime
  Result := SystemTimeToDateTime(ST2);
end;


end.
