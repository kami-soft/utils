unit uWatchFileSystem;

/// <author> from everybody in the world</author>
/// <summary>
/// this unit perform file system monitoring.
/// it looks for changes in specified directories
/// and call event.
/// can watch for different, independed folders.
/// The event calls in application main thread
/// ������, �������������� ���������� �� ����������� �������� �������.
/// �������� � ��������� �������, ��� ����������� ��������� - ��������
/// ����������� �������.
/// ����������� ���������� �� �����������, �� ���������� �������.
/// ������������ ������ ����� ����� TFileSystemWatcherList !
/// </summary>


/// author: kami (c) 2016
/// you can use this code without any limitations.
/// The code provided "as is", wtihout warranty of any kind.

/// how to use:
/// 1. Create instance of TFileSystemWatcherList:
///    myFileSystemWatcher := TFileSystemWatcherList.Create;

/// 2. Create method for receive notifications.
///    for example:
///    TForm1 = class(TForm)
///    .....
///    private
///      procedure FolderChangeNotification(Sender: TObject; Info: TInfoCallBack);
///
/// 3. Add folders to monitoring:
///    myFileSystemWatcher.StartWatch(
///      'c:\Temp', - folder name
///      FILE_NOTIFY_CHANGE_FILE_NAME or FILE_NOTIFY_CHANGE_SIZE, - combination of FILE_NOTIFY_XXX constants
///      True, - we need subfolder monitoring
///      Form1.FolderChangeNotification); - method from any object with signature TWatchChangesEvent
///  
/// !!!NOTE!!!
/// the WatchChangesEvent called in context of additional thread, not main thread!!!
/// if you need synchronization (for example - change something in visual components)
/// you should use synchronization methods.

interface

uses
  System.Classes,
  System.Generics.Collections;

type
  /// <summary>
  ///  This structure used in fired event and contains information abort file system changes.
  /// ��������� � ����������� �� ��������� � �������� ������� (���������� � callback ���������)
  /// </summary>
  TInfoCallBack = record
    Action: Integer; // Type of change ��� ��������� (��������� FILE_ACTION_XXX)
    Root: string; //  root folder �������� ����������, �� ������� ������� ����������
    OldFileName: string; // file name before renaming ��� ����� �� ��������������
    NewFileName: string; // file name after renaming ��� ����� ����� ��������������
  end;

  /// <summary>
  ///  callback procedure, called then folder changes
  ///  works in MAIN THREAD
  /// callback ���������, ���������� ��� ��������� � �������� �������
  /// ������������ � ������� ������.
  /// </summary>
  TWatchChangesEvent = procedure(Sender: TObject; Info: TInfoCallBack) of object;

  TFileSystemWatcherList = class;

  /// <summary>
  ///  Abstract class. Do not create directly! Use only TFileSystemWatcherList.StartWatch method!
  /// ����������� ����� - ���������� ���������� ����� (�������� - � ����������)
  /// ��� �� ��������� �������� ������� ����������, � ������������� �� ��������� �����
  /// ������������ ����������� ������������
  /// �� ���������! ������������ ������� TFileSystemWatcherList.StartWatch
  /// </summary>
  TFileSystemWatcher = class(TThread)
  strict private
    FWatchDirRoot: string;
    FFilter: Cardinal;
    FWatchSubTree: Boolean;
    FOnChanged: TWatchChangesEvent;
  private
    FOwner: TFileSystemWatcherList;
  public
    constructor Create(const AWatchDir: string; AFilter: Cardinal; AWatchSubTree: Boolean; WatchChangesEvent: TWatchChangesEvent); virtual;

    procedure BeforeDestruction; override;

    procedure StopWatch; virtual; abstract;

    property WatchDir: string read FWatchDirRoot;
    property Filter: Cardinal read FFilter;
    property WatchSubTree: Boolean read FWatchSubTree;

    property OnChanged: TWatchChangesEvent read FOnChanged write FOnChanged;
  end;

  TOnStopWatchEvent = procedure(Sender: TObject; Watcher: TFileSystemWatcher) of object;

  /// <summary>
  ///  Contains all watchers. For add new watcher use StartWatch method.
  ///  if watch interrupted (for different reasons) - the OnStopWatch event fired.
  ///
  /// ����� - ���������, �������� ���� ������������ �� �������� ��������.
  /// ���������� ������ ����������� - ����� ����� StartWatch
  /// ���������� ����� ���������� �� ������ ��������, ����� ��������� ��� -
  /// ������������ ������� TFileSystenWatcherList.OnStopWatch.
  /// </summary>
  TFileSystemWatcherList = class(TObjectList<TFileSystemWatcher>)
  private
    FOnStopWatch: TOnStopWatchEvent;
  protected
    procedure Notify(const Value: TFileSystemWatcher; Action: TCollectionNotification); override;
  public
    { ������ ����������� �������� �������
      ���������:
      FolderName    - folder to watch changes. ��� ����� ��� �����������
      AFilter  - use combination of FILE_NOTIFY_XXX constants. ���������� �������� FILE_NOTIFY_XXX
      WatchSubTree - Watch for subfolder changes. ���������� �� ��� �������� �������� �����
      WatchChangesEvent - callback �����, ���������� ��� ��������� � �������� ������� }
    function StartWatch(const FolderName: string; AFilter: Cardinal; WatchSubTree: Boolean; WatchChangesEvent: TWatchChangesEvent): TFileSystemWatcher;

    /// <summary>
    ///  please, do not restore watcher directly in this event.
    ///  use some delay (not Sleep! something like timer, PostMessage, etc...)
    /// ������ ��������� � ���� �������� - ��� ���������� � �������� �������� �� ������
    /// ��������, ������� ���������� ������� ������ ����� ������� ����� ���� �����������.
    /// ������ ����� - �� ����� ����� �������� ������������ ���������� �� ������,
    /// ����� ������� ��������� ������.
    /// </summary>
    property OnStopWatch: TOnStopWatchEvent read FOnStopWatch write FOnStopWatch;
  end;

implementation

uses
  Winapi.Windows,
  System.SysUtils;

const
  FILE_LIST_DIRECTORY = $0001;

type
  PFileNotifyInformation = ^TFileNotifyInformation;

  TFileNotifyInformation = record
    NextEntryOffset: DWORD;
    Action: DWORD;
    FileNameLength: DWORD;
    FileName: array [0 .. 0] of WideChar;
  end;

  WFSError = class(Exception);

  TWFS = class(TFileSystemWatcher)
  strict private
    FWatchDirHandle: THandle;
    FWatchBuf: array [0 .. 4096] of Byte;
    FOverLapp: TOverlapped;
    FPOverLapp: POverlapped;
    FBytesWritte: DWORD;
    FCompletionPort: THandle;
    FNumBytes: Cardinal;
    FOldFileName: string;
    function CreateDirHandle(aDir: string): THandle;
    procedure WatchEvent;
    procedure HandleEvent;
  protected
    procedure Execute; override;
  public
    constructor Create(const AWatchDir: string; AFilter: Cardinal; AWatchSubTree: Boolean; WatchChangesEvent: TWatchChangesEvent); override;
    destructor Destroy; override;

    procedure StopWatch; override;
  end;

constructor TWFS.Create(const AWatchDir: string; AFilter: Cardinal; AWatchSubTree: Boolean; WatchChangesEvent: TWatchChangesEvent);
begin
  inherited;
  FOldFileName := '';
  ZeroMemory(@FOverLapp, SizeOf(TOverlapped));
  FPOverLapp := @FOverLapp;
  ZeroMemory(@FWatchBuf, SizeOf(FWatchBuf));
end;

destructor TWFS.Destroy;
begin
  FreeOnTerminate := False;
  PostQueuedCompletionStatus(FCompletionPort, 0, 0, nil);
  CloseHandle(FWatchDirHandle);
  FWatchDirHandle := 0;
  CloseHandle(FCompletionPort);
  FCompletionPort := 0;

  inherited Destroy;
end;

function TWFS.CreateDirHandle(aDir: string): THandle;
begin
  Result := CreateFile(PChar(aDir), FILE_LIST_DIRECTORY, FILE_SHARE_READ + FILE_SHARE_DELETE + FILE_SHARE_WRITE, nil, OPEN_EXISTING,
    FILE_FLAG_BACKUP_SEMANTICS or FILE_FLAG_OVERLAPPED, 0);
end;

procedure TWFS.Execute;
begin
  FWatchDirHandle := CreateDirHandle(WatchDir);
  WatchEvent;
end;

procedure TWFS.HandleEvent;
var
  FileNotifyInfo: PFileNotifyInformation;
  InfoCallBack: TInfoCallBack;
  Offset: Longint;
begin
  Pointer(FileNotifyInfo) := @FWatchBuf[0];
  repeat
    Offset := FileNotifyInfo^.NextEntryOffset;
    InfoCallBack.Action := FileNotifyInfo^.Action;
    InfoCallBack.Root := WatchDir;
    SetString(InfoCallBack.NewFileName, FileNotifyInfo^.FileName, FileNotifyInfo^.FileNameLength);
    InfoCallBack.NewFileName := Trim(InfoCallBack.NewFileName);
    case FileNotifyInfo^.Action of
      FILE_ACTION_RENAMED_OLD_NAME:
        FOldFileName := Trim(WideCharToString(@(FileNotifyInfo^.FileName[0])));
      FILE_ACTION_RENAMED_NEW_NAME:
        InfoCallBack.OldFileName := FOldFileName;
    end;

    if Assigned(OnChanged) then
      OnChanged(Self, InfoCallBack);
    PChar(FileNotifyInfo) := PChar(FileNotifyInfo) + Offset;
  until (Offset = 0) or Terminated;
end;

procedure TWFS.StopWatch;
begin
  PostQueuedCompletionStatus(FCompletionPort, 0, 0, nil);
  Terminate;
end;

procedure TWFS.WatchEvent;
var
  CompletionKey: UIntPtr;
begin
  FCompletionPort := CreateIoCompletionPort(FWatchDirHandle, 0, Longint(Pointer(Self)), 0);
  ZeroMemory(@FWatchBuf, SizeOf(FWatchBuf));
  if not ReadDirectoryChanges(FWatchDirHandle, @FWatchBuf, SizeOf(FWatchBuf), WatchSubTree, Filter, @FBytesWritte, @FOverLapp, nil) then
    begin
      ReturnValue := GetLastError;
      Terminate;
    end
  else
    begin
      while not Terminated do
        begin
          if not GetQueuedCompletionStatus(FCompletionPort, FNumBytes, CompletionKey, FPOverLapp, INFINITE) then
            begin
              ReturnValue := GetLastError;
              Terminate;
            end;
          if CompletionKey <> 0 then
            begin
              Synchronize(HandleEvent);
              ZeroMemory(@FWatchBuf, SizeOf(FWatchBuf));
              FBytesWritte := 0;
              if not ReadDirectoryChanges(FWatchDirHandle, @FWatchBuf, SizeOf(FWatchBuf), WatchSubTree, Filter, @FBytesWritte, @FOverLapp, nil) then
                begin
                  ReturnValue := GetLastError;
                  Terminate;
                end;
            end
          else
            Terminate;
        end;
    end;
end;

{ TFileSystemWatcher }

procedure TFileSystemWatcher.BeforeDestruction;
begin
  if Assigned(FOwner) then
    FOwner.Extract(Self);
  inherited;
end;

constructor TFileSystemWatcher.Create(const AWatchDir: string; AFilter: Cardinal; AWatchSubTree: Boolean; WatchChangesEvent: TWatchChangesEvent);
begin
  inherited Create(False);
  FreeOnTerminate := True;
  FWatchDirRoot := AWatchDir;
  FFilter := AFilter;
  FWatchSubTree := AWatchSubTree;
  FOnChanged := WatchChangesEvent;
end;

{ TFileSystemWatcherList }

procedure TFileSystemWatcherList.Notify(const Value: TFileSystemWatcher; Action: TCollectionNotification);
begin
  case Action of
    cnAdded:
      Value.FOwner := Self;
    cnRemoved, cnExtracted:
      begin
        Value.FOwner := nil;
        if Assigned(FOnStopWatch) then
          FOnStopWatch(Self, Value);
      end;
  end;
  inherited;
end;

function TFileSystemWatcherList.StartWatch(const FolderName: string; AFilter: Cardinal; WatchSubTree: Boolean; WatchChangesEvent: TWatchChangesEvent)
  : TFileSystemWatcher;
begin
  if not Assigned(WatchChangesEvent) then
    Exit(nil);

  Result := TWFS.Create(FolderName, AFilter, WatchSubTree, WatchChangesEvent);
  Add(Result);
end;

end.
