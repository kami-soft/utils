unit Ribbon.dxHelpers;

interface
uses
  System.Classes,
  cxBarEditItem;

type
  TcxComboEditHelper = class helper for TcxBarEditItem
  private
    function GetItemIndex: integer;
    procedure SetItemIndex(const Value: integer);
    function GetItems: TStrings;
  public
    property ItemIndex: integer read GetItemIndex write SetItemIndex;
    property Items: TStrings read GetItems;
  end;

implementation
uses
  System.Variants,
  cxDropDownEdit;

{ TcxComboEditHelper }

function TcxComboEditHelper.GetItemIndex: integer;
var
  p: TcxComboBoxProperties;
begin
  Result := -1;
  if Properties is TcxComboBoxProperties then
  begin
    p := TcxComboBoxProperties(Properties);
    Result := p.Items.IndexOf(VarToStr(EditValue));
  end;
end;

function TcxComboEditHelper.GetItems: TStrings;
var
  p: TcxComboBoxProperties;
begin
  Result := nil;
  if Properties is TcxComboBoxProperties then
  begin
    p := TcxComboBoxProperties(Properties);
    Result := p.Items;
  end;
end;

procedure TcxComboEditHelper.SetItemIndex(const Value: integer);
var
  p: TcxComboBoxProperties;
begin
  if Properties is TcxComboBoxProperties then
  begin
    p := TcxComboBoxProperties(Properties);
    if Value < p.Items.Count then
      EditValue := p.Items[Value];
  end;
end;

end.
