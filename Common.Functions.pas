unit Common.Functions;

interface

uses
  System.Classes,
  System.Generics.Collections,
  System.SysUtils;

function NormalizedFormatSettings: TFormatSettings;

function DateTimeBetween(dtCompared, dtStart, dtEnd: TDateTime): Boolean;
function TimeBetween(dtCompared, dtStart, dtEnd: TDateTime): Boolean;
// deprecated 'use DateTimeInRange from System.SysUtils';
function DateTimeIntersects(dtStart1, dtEnd1, dtStart2, dtEnd2: TDateTime): Boolean;

procedure ForceQueue(proc: TThreadProcedure);

implementation

uses
  System.SyncObjs,
  System.DateUtils,
  Winapi.Windows;

function NormalizedFormatSettings: TFormatSettings;
begin
  Result := TFormatSettings.Create;
  Result.DateSeparator := '-';
  Result.TimeSeparator := ':';
  Result.ShortDateFormat := 'yyyy-mm-dd';
  Result.LongDateFormat := Result.ShortDateFormat;
  Result.ShortTimeFormat := 'hh:nn:ss';
  Result.LongTimeFormat := Result.ShortTimeFormat;
  Result.DecimalSeparator := '.';
end;

function TimeBetween(dtCompared, dtStart, dtEnd: TDateTime): Boolean;
begin
  Result := (CompareTime(dtStart, dtCompared) <= 0) and (CompareTime(dtCompared, dtEnd) <= 0);
end;

function DateTimeBetween(dtCompared, dtStart, dtEnd: TDateTime): Boolean;
begin
  Result := (CompareDateTime(dtStart, dtCompared) <= 0) and (CompareDateTime(dtCompared, dtEnd) <= 0);
end;

function DateTimeIntersects(dtStart1, dtEnd1, dtStart2, dtEnd2: TDateTime): Boolean;
begin
  Result := (CompareDateTime(dtStart1, dtEnd2) <= 0) and (CompareDateTime(dtEnd1, dtStart2) >= 0);
end;

function CoolTimeToTime(const CoolTime: string): TDateTime;
var
  sHour, sMin: string;
  hour, min: Integer;
begin
  sMin := Copy(CoolTime, Length(CoolTime) - 1, 2);
  sHour := Copy(CoolTime, 1, Length(CoolTime) - 2);
  hour := StrToIntDef(sHour, 0);
  min := StrToIntDef(sMin, 0);
  Result := IncHour(0, hour);
  Result := IncMinute(Result, min);
end;

procedure GetIDNDates(dt: TDateTime; out SummerStart, WinterStart: TDateTime);
begin
  // ��������� ����������� �������. ����� ����� ������ DayOfWeek, ������ ���
  // ����� ������� ����������� ������� ������.
  WinterStart:=EncodeDate(YearOf(dt), 10, 31);
  SummerStart:=EncodeDate(YearOf(dt), 03, 31);

  WinterStart := WinterStart - DayOfWeek(WinterStart) + 1;
  // ��������� ����������� �����
  SummerStart := SummerStart - DayOfWeek(SummerStart) + 1;
end;

resourcestring
  sErrorQueuePerformData = '������ ����������� ����������. ������������� ����������';
  sErrorCaption = '������';

type
  TSync = record
    FProcedure: TThreadProcedure;
  end;

  TForceQueue = class(TList<TSync>)
  strict private
    FCriticalSection: TCriticalSection;
  public
    constructor Create;
    destructor Destroy; override;

    procedure Lock;
    procedure Unlock;
  end;

var
  FQueuedList: TForceQueue;

procedure ForceQueue(proc: TThreadProcedure);
var
  LSync: TSync;
  queuedSync: TSync;
begin
  if not Assigned(FQueuedList) then
    FQueuedList := TForceQueue.Create;
  LSync.FProcedure := proc;
  FQueuedList.Lock;
  try
    FQueuedList.Add(LSync);
  finally
    FQueuedList.Unlock;
  end;

  TThread.CreateAnonymousThread(
      procedure
    begin
      TThread.Queue(nil,
          procedure
        begin
          if Assigned(FQueuedList) then
          begin
            FQueuedList.Lock;
            try
              if FQueuedList.Count <> 0 then
              begin
                queuedSync := FQueuedList.First;
                FQueuedList.Delete(0);
                try
                  queuedSync.FProcedure();
                except
                  // block exceptions
                  on e: Exception do
                    MessageBox(0, PChar(sErrorQueuePerformData + #13#10 + e.ToString), PChar(sErrorCaption),
                      MB_OK or MB_ICONERROR);
                end;
              end;
            finally
              FQueuedList.Unlock;
            end;
          end;
        end);
    end).Start;
end;

{ TForceQueue }

constructor TForceQueue.Create;
begin
  inherited Create;
  FCriticalSection := TCriticalSection.Create;
end;

destructor TForceQueue.Destroy;
begin
  FCriticalSection.Free;
  inherited;
end;

procedure TForceQueue.Lock;
begin
  FCriticalSection.Enter;
end;

procedure TForceQueue.Unlock;
begin
  FCriticalSection.Leave;
end;

initialization

finalization

FreeAndNil(FQueuedList);

end.
