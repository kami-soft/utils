unit Tasks.SourceRequests;

interface

uses
  System.Classes,
  System.Generics.Collections,
  pbInput,
  pbOutput,
  uAbstractProtoBufClasses,
  Common.Interfaces,
  Common.AbstractClasses;

type
  TSourceRequestState = (srsUnknown, srsReaded, srsInProcess, srsFinished, srsFinalError);

  TOutputData = class(TDictionary<string, string>)
  end;

  TAbstractSourceRequest = class(TAbstractData)
  public const
    cOutputDataErrorDescr = 'ErrorDescription';
  strict private
    FState: TSourceRequestState;
    FTryCounter: integer;
    FLastTry: TDateTime;

    FOutputData: TOutputData;

    procedure SetState(const Value: TSourceRequestState);
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer)
      : Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  strict private
    function GetErrorDescription: string;
  strict protected
    procedure DoFinished; virtual; abstract;
    property OutputData: TOutputData read FOutputData;
  public
    constructor Create(AOwner: TObject); override;
    destructor Destroy; override;
    function ToString: string; override;

    procedure SetFailed(const AErrorDescription: string);

    function IsSame(ARequest: TAbstractSourceRequest): Boolean; virtual; abstract;

    property State: TSourceRequestState read FState write SetState;
    property TryCounter: integer read FTryCounter;
    property LastTry: TDateTime read FLastTry;
    property ErrorDescription: string read GetErrorDescription;
  end;

  TSourceRequestList = class(TAbstractDataList<TAbstractSourceRequest>)
  public
    function RequestCount(AState: TSourceRequestState): Integer;
    function HasSame(SourceRequest: TAbstractSourceRequest): Boolean;
  end;

  // ������:
  //  �������� �������� SourceRequest-��,
  //  ���������� �� �������
  //  �������������� ����� �������� ��� ���������
  //  ��� ��� ����� �� ����� - �������� ������ � ��������� ��� ��� ���
  //    ������� ����� ��� �������, ����� ���� ������� ������� "��������� ���������"
  //    ����������� �������� ������ ����� ����������� - ����� �� ��������� ������ � �������
  //    ��� �� ��� ����.
  TAbstractSourceRequestManager = class(TAbstractData)
  private
    FLastErrorDT: TDateTime;
  public
    procedure AppendRequests(ListForRequests: TSourceRequestList); virtual; abstract;

    property LastErrorDt: TDateTime read FLastErrorDT write FLastErrorDT;
  end;

  TSourceRequestsManagerList = class(TAbstractDataList<TAbstractSourceRequestManager>)
  strict private
    procedure CreateManagers;
  public
    constructor Create(AOwner: TObject; AOwnsObjects: Boolean = True); override;
    procedure AppendRequests(ListForRequests: TSourceRequestList);
  end;

  TSourceRequestsOperator = class(TAbstractData)
  strict private
    FManagers: TSourceRequestsManagerList;
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer)
      : Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    constructor Create(AOwner: TObject); override;
    destructor Destroy; override;

    procedure GetRequests(ListForRequests: TSourceRequestList);
  end;

implementation

uses
  System.SysUtils,
  System.DateUtils,
  uThreadSafeLogger,
  Monitoring.Informer,
  Utils.Conversation;

{ TAbstractSourceRequest }

constructor TAbstractSourceRequest.Create(AOwner: TObject);
begin
  inherited;
  FOutputData := TOutputData.Create;
end;

destructor TAbstractSourceRequest.Destroy;
begin
  TMonitoringInformer.ImLive('Free finished task');
  LogFileX.Log('Free finished task: ' + ToString);
  try
    case FState of
      srsFinished, srsFinalError:
        DoFinished;
    else
      // do nothing;
    end;
  except
    on e: Exception do
    begin
      LogFileX.LogException(e);
      TMonitoringInformer.ImInException(e, True);
    end;
  end;
  FreeAndNil(FOutputData);
  inherited;
end;

function TAbstractSourceRequest.GetErrorDescription: string;
begin
  Result := '';
  if State = srsFinalError then
    FOutputData.TryGetValue(cOutputDataErrorDescr, Result);
end;

function TAbstractSourceRequest.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput;
  FieldNumber, WireType: integer): Boolean;
begin
  Result := inherited;
  if Result then
    exit;

  case FieldNumber of
    1:
      FState := TSourceRequestState(ProtoBuf.readInt32);
    2:
      FTryCounter := ProtoBuf.readInt32;
  end;

  Result := FieldNumber in [1 .. 2];
end;

procedure TAbstractSourceRequest.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeInt32(1, integer(FState));
  ProtoBuf.writeInt32(2, FTryCounter);
end;

procedure TAbstractSourceRequest.SetFailed(const AErrorDescription: string);
begin
  FOutputData.AddOrSetValue(cOutputDataErrorDescr, AErrorDescription);
  State := srsFinalError;
end;

procedure TAbstractSourceRequest.SetState(const Value: TSourceRequestState);
begin
  if FState <> Value then
  begin
    if Value < FState then
    begin
      Inc(FTryCounter);
      FLastTry := Now;
    end;

    FState := Value;
    ChangeNotificator.NotifyForSelfChanges;
  end;
end;

function TAbstractSourceRequest.ToString: string;
begin
  if ErrorDescription <> '' then
    Result :=TEnumeration<TSourceRequestState>.ToString(State) +' '+ ErrorDescription
  else
    Result := TEnumeration<TSourceRequestState>.ToString(State) +' ';
end;

{ TSourceRequestsManagerList }

procedure TSourceRequestsManagerList.AppendRequests(ListForRequests: TSourceRequestList);
var
  i: integer;
begin
  for i := 0 to Count - 1 do
    try
      if SecondsBetween(Items[i].LastErrorDt, Now) > 60 then
      begin
        Items[i].AppendRequests(ListForRequests);
        Items[i].LastErrorDt := 0;
      end;
    except
      on e: Exception do
      begin
        LogFileX.LogException(e);
        TMonitoringInformer.ImInException(e, True);
        Items[i].LastErrorDt := Now;
      end;
    end;
end;

{ TSourceRequestsManager }

constructor TSourceRequestsOperator.Create(AOwner: TObject);
begin
  inherited;
  FManagers := TSourceRequestsManagerList.Create(Self);
end;

destructor TSourceRequestsOperator.Destroy;
begin
  FreeAndNil(FManagers);
  inherited;
end;

procedure TSourceRequestsOperator.GetRequests(ListForRequests: TSourceRequestList);
begin
  FManagers.AppendRequests(ListForRequests);
end;

function TSourceRequestsOperator.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput;
  FieldNumber, WireType: integer): Boolean;
begin
  Result := inherited;
  if Result then
    exit;

  case FieldNumber of
    1:
      FManagers.AddFromBuf(ProtoBuf, FieldNumber);
  end;

  Result := FieldNumber in [1 .. 1];
end;

procedure TSourceRequestsOperator.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  FManagers.SaveToBuf(ProtoBuf, 1);
end;

constructor TSourceRequestsManagerList.Create(AOwner: TObject; AOwnsObjects: Boolean);
begin
  inherited;
  CreateManagers;
end;

procedure TSourceRequestsManagerList.CreateManagers;
var
  i: integer;
begin
  Clear;
  for i := 0 to TAbstractDataClassFactory.Current.Count - 1 do
    if TAbstractDataClassFactory.Current[i].InheritsFrom(TAbstractSourceRequestManager) then
      Add(TAbstractSourceRequestManager(TAbstractDataClassFactory.Current[i].Create(Self)));
end;

{ TSourceRequestList }

function TSourceRequestList.HasSame(SourceRequest: TAbstractSourceRequest): Boolean;
var
  i: integer;
begin
  Result := False;
  for i := 0 to Count - 1 do
    if Items[i].ClassType = SourceRequest.ClassType then
      if Items[i].IsSame(SourceRequest) then
      begin
        Result := True;
        Break;
      end;
end;

function TSourceRequestList.RequestCount(AState: TSourceRequestState): Integer;
var
  i: Integer;
begin
  Result:=0;
  for i := 0 to Count-1 do
    if Items[i].State = AState then
      Inc(Result);
end;

initialization

TSourceRequestsOperator.RegisterSelf;

end.
