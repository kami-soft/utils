unit uLargeFileOperations;

interface

uses
  System.Classes,
  System.SysUtils,
  System.Generics.Collections;

type
  TOperationCallback = procedure(Sender: TObject; const CurrentPos, TotalSize: int64) of object;

  TSearchObject = class(TObject)
  private
    FSearchRecs: TStrings;
  public
    constructor Create;
    destructor Destroy; override;

    function FindInString(const Buf: string; IgnoreCase: Boolean; var FindedPos: integer; var RecIndex: integer): Boolean;
    function FindInBuf(const Buf: TBytes; FromBufPos, BytesCount: integer; Encoding: TEncoding; IgnoreCase: Boolean; var FindedPos: integer;
      var RecIndex: integer): Boolean;

    property SearchRecords: TStrings read FSearchRecs;

    function GetMaxRecSize(Encoding: TEncoding): integer;
    function GetMaxRecLength: integer;
  end;

  TFileSearchReplace = class(TObject)
  private
    FOwnSourceStream: Boolean;
    FSourceStream: TStream;
    FtmpStream: TFileStream;
    FEncoding: TEncoding;
    FDefaultEncoding: TEncoding;
    FOperationCallback: TOperationCallback;
    FFindReplaceFlags: TReplaceFlags;

    procedure GetStreamEncoding(out SourceBOMLength: integer); inline;
    function GetMaxCharBytes(const Buf: TBytes): integer;

    procedure DoOperationCallback(const CurrentPos, TotalSize: int64);
  public
    constructor Create(const AFileName: string; DefaultEncoding: TEncoding); overload;
    constructor Create(SourceStream: TStream; DefaultEncoding: TEncoding); overload;
    destructor Destroy; override;

    function FindEx(FromPos: int64; StopRecs: TSearchObject; var FindedPos: int64; var RecIndex: integer): Boolean;
    procedure Replace(const AFrom, ATo: string);

    property FindReplaceFlags: TReplaceFlags read FFindReplaceFlags write FFindReplaceFlags;
    property OnOperationCallback: TOperationCallback read FOperationCallback write FOperationCallback;
  end;

implementation

uses
  System.IOUtils,
  System.StrUtils;

function Max(const A, B: integer): integer; inline;
begin
  if A > B then
    Result := A
  else
    Result := B;
end;

function ArraysEqual(const A, B: TBytes): Boolean; inline;
var
  i: integer;
begin
  if Length(A) <> Length(B) then
    Exit(False);
  Result := True;
  for i := 0 to Length(A) do
    if A[i] <> B[i] then
      begin
        Result := False;
        Break;
      end;
end;

{ TFileSearchReplace }

constructor TFileSearchReplace.Create(const AFileName: string; DefaultEncoding: TEncoding);
begin
  FOwnSourceStream := True;
  Create(TFileStream.Create(AFileName, fmOpenReadWrite), DefaultEncoding);
end;

constructor TFileSearchReplace.Create(SourceStream: TStream; DefaultEncoding: TEncoding);
var
  Foo: integer;
begin
  inherited Create();
  FSourceStream:=SourceStream;
  FtmpStream := TFileStream.Create(TPath.GetTempFileName, fmCreate);
  FDefaultEncoding := DefaultEncoding;
  GetStreamEncoding(Foo);
end;

destructor TFileSearchReplace.Destroy;
var
  tmpFileName: string;
begin
  if Assigned(FtmpStream) then
    tmpFileName := FtmpStream.FileName;

  FreeAndNil(FtmpStream);
  if FOwnSourceStream then
    FreeAndNil(FSourceStream);

  TFile.Delete(tmpFileName);

  inherited;
end;

procedure TFileSearchReplace.DoOperationCallback(const CurrentPos, TotalSize: int64);
begin
  if Assigned(FOperationCallback) then
    FOperationCallback(Self, CurrentPos, TotalSize);
end;

function TFileSearchReplace.FindEx(FromPos: int64; StopRecs: TSearchObject; var FindedPos: int64; var RecIndex: integer): Boolean;
var
  BOMSize: integer;
  ReadedBufLen: integer;
  iPos, BufLen: integer;
  Buf: TBytes;
  tmpStr: string;
begin
  if FromPos = 0 then
    begin
      GetStreamEncoding(BOMSize);
      if BOMSize <> 0 then
        Inc(FromPos, BOMSize);
    end;
  Result := False;
  FindedPos := -1;
  FSourceStream.Seek(FromPos, soBeginning);

  BufLen := (StopRecs.GetMaxRecSize(FEncoding) * 8 + 1024) and $FFFFFF00;
  SetLength(Buf, BufLen);
  repeat
    ReadedBufLen := FSourceStream.Read(Buf[0], Length(Buf));
    if ReadedBufLen <> 0 then
      begin
        SetLength(Buf, ReadedBufLen);
        ReadedBufLen := GetMaxCharBytes(Buf);
        if ReadedBufLen = 0 then
          raise EEncodingError.Create('can`t translate bytes to chars');
        FSourceStream.Seek(- Length(Buf) + ReadedBufLen, soCurrent);

        if StopRecs.FindInBuf(Buf, 0, ReadedBufLen, FEncoding, rfIgnoreCase in FFindReplaceFlags, iPos, RecIndex) then
          begin
            FindedPos := FSourceStream.Seek(-Length(Buf) + iPos, soCurrent);
            Result := True;
            Break;
          end;
        tmpStr:=FEncoding.GetString(Buf, 0, ReadedBufLen);
        tmpStr:=Copy(tmpStr, 1, Length(tmpStr) - StopRecs.GetMaxRecLength + 1);
        FSourceStream.Seek( - ReadedBufLen + FEncoding.GetByteCount(tmpStr), soCurrent);
      end;
  until ReadedBufLen = 0;
end;

function TFileSearchReplace.GetMaxCharBytes(const Buf: TBytes): integer;
var
  i: integer;
begin
  Result := 0;
  for i := Length(Buf) downto 0 do
    if FEncoding.GetCharCount(Buf, 0, i) <> 0 then
      begin
        Result := i;
        Break;
      end;
end;

procedure TFileSearchReplace.GetStreamEncoding(out SourceBOMLength: integer);
var
  BOMSize: integer;
  BOMBuf: TBytes;
begin
  SourceBOMLength := 0;
  SetLength(BOMBuf, 1024);

  FSourceStream.Seek(0, soBeginning);
  FSourceStream.Read(BOMBuf, Length(BOMBuf));
  FSourceStream.Seek(0, soBeginning);

  FEncoding := nil;
  BOMSize := TEncoding.GetBufferEncoding(BOMBuf, FEncoding, FDefaultEncoding);
  if BOMSize <> 0 then
    begin
      SetLength(BOMBuf, BOMSize);
      if ArraysEqual(FEncoding.GetPreamble, BOMBuf) then
        SourceBOMLength := BOMSize;
      FSourceStream.Seek(0, soBeginning);
    end;
end;

procedure TFileSearchReplace.Replace(const AFrom, ATo: string);
  procedure CopyPreamble;
  var
    PreambleSize: integer;
  begin
    // Copy Encoding preamble
    GetStreamEncoding(PreambleSize);
    if PreambleSize <> 0 then
      FtmpStream.CopyFrom(FSourceStream, PreambleSize);
  end;

  function GetLastIndex(const Str, SubStr: string): integer;
  var
    i: integer;
    tmpSubStr, tmpStr: string;
  begin
    if rfIgnoreCase in FFindReplaceFlags then
      begin
        tmpStr := UpperCase(Str);
        tmpSubStr := UpperCase(SubStr);
      end
    else
      begin
        tmpStr := Str;
        tmpSubStr := SubStr;
      end;

    i := Pos(tmpSubStr, tmpStr);
    Result := i;
    while i > 0 do
      begin
        i := PosEx(tmpSubStr, tmpStr, i + 1);
        if i > 0 then
          Result := i;
      end;
    if Result > 0 then
      Inc(Result, Length(tmpSubStr) - 1);
  end;

var
  SourceSize: int64;

  procedure ParseBuffer(Buf: TBytes; var IsReplaced: Boolean);
  var
    ReadedBufLen: integer;
    bufStr: string;
    DestBytes: TBytes;
    LastIndex: integer;
  begin
    if IsReplaced and (not(rfReplaceAll in FFindReplaceFlags)) then
      begin
        FtmpStream.Write(Buf, Length(Buf));
        Exit;
      end;

    ReadedBufLen := GetMaxCharBytes(Buf);
    if ReadedBufLen = 0 then
      raise EEncodingError.Create('Cant convert bytes to str');

    FSourceStream.Seek(ReadedBufLen - Length(Buf), soCurrent);

    bufStr := FEncoding.GetString(Buf, 0, ReadedBufLen);
    if rfIgnoreCase in FFindReplaceFlags then
      IsReplaced := ContainsText(bufStr, AFrom)
    else
      IsReplaced := ContainsStr(bufStr, AFrom);

    if IsReplaced then
      begin
        LastIndex := GetLastIndex(bufStr, AFrom);
        LastIndex := Max(LastIndex, Length(bufStr) - Length(AFrom) + 1);
      end
    else
      LastIndex := Length(bufStr);

    SetLength(bufStr, LastIndex);
    FSourceStream.Seek(FEncoding.GetByteCount(bufStr) - ReadedBufLen, soCurrent);

    bufStr := StringReplace(bufStr, AFrom, ATo, FFindReplaceFlags);
    DestBytes := FEncoding.GetBytes(bufStr);
    FtmpStream.Write(DestBytes, Length(DestBytes));
  end;

var
  Buf: TBytes;
  BufLen: integer;
  bReplaced: Boolean;
begin
  FSourceStream.Seek(0, soBeginning);
  FtmpStream.Size := 0;
  CopyPreamble;

  SourceSize := FSourceStream.Size;
  BufLen := Max(FEncoding.GetByteCount(AFrom) * 5, 2048);
  BufLen := Max(FEncoding.GetByteCount(ATo) * 5, BufLen);
  SetLength(Buf, BufLen);

  DoOperationCallback(0, SourceSize);

  bReplaced := False;
  while FSourceStream.Position < SourceSize do
    begin
      BufLen := FSourceStream.Read(Buf, Length(Buf));
      SetLength(Buf, BufLen);
      ParseBuffer(Buf, bReplaced);
      DoOperationCallback(FSourceStream.Position, SourceSize);
    end;

  FSourceStream.Size := 0;
  FSourceStream.CopyFrom(FtmpStream, 0);
end;

{ TSearchObject }

constructor TSearchObject.Create;
begin
  inherited Create;
  FSearchRecs := TStringList.Create;
end;

destructor TSearchObject.Destroy;
begin
  FreeAndNil(FSearchRecs);
  inherited;
end;

function TSearchObject.FindInBuf(const Buf: TBytes; FromBufPos, BytesCount: integer; Encoding: TEncoding; IgnoreCase: Boolean;
  var FindedPos, RecIndex: integer): Boolean;
var
  tmpStr: string;
begin
  tmpStr := Encoding.GetString(Buf, FromBufPos, BytesCount);
  Result := FindInString(tmpStr, IgnoreCase, FindedPos, RecIndex);
  if Result then
    { TODO : Need research: zero-based or one-based string used. }
    FindedPos := FromBufPos + Encoding.GetByteCount(tmpStr, 1, FindedPos - 1);
end;

function TSearchObject.FindInString(const Buf: string; IgnoreCase: Boolean; var FindedPos, RecIndex: integer): Boolean;
var
  i: integer;
  CurrFindedPos: integer;

  tmpBuf: string;
begin
  Result := False;
  FindedPos := MaxInt;
  if IgnoreCase then
    tmpBuf := AnsiUpperCase(Buf)
  else
    tmpBuf := Buf;
  for i := 0 to FSearchRecs.Count - 1 do
    begin
      if IgnoreCase then
        CurrFindedPos := Pos(AnsiUpperCase(FSearchRecs[i]), tmpBuf)
      else
        CurrFindedPos := Pos(FSearchRecs[i], tmpBuf);
      if (CurrFindedPos <> 0) and (CurrFindedPos < FindedPos) then
        begin
          Result := True;
          RecIndex := i;
          FindedPos := CurrFindedPos;
        end;
    end;
  if not Result then
    begin
      FindedPos := -1;
      RecIndex := 0;
    end;
end;

function TSearchObject.GetMaxRecLength: integer;
var
  i: Integer;
begin
  Result:=0;
  for i := 0 to FSearchRecs.Count-1 do
    if Length(FSearchRecs[i])>Result then
      Result:=Length(FSearchRecs[i]);
end;

function TSearchObject.GetMaxRecSize(Encoding: TEncoding): integer;
var
  i: integer;
  tmp: integer;
begin
  Result := 0;
  for i := 0 to FSearchRecs.Count - 1 do
    begin
      tmp := Encoding.GetByteCount(FSearchRecs[i]);
      if tmp > Result then
        Result := tmp;
    end;
end;

end.
