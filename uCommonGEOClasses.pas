unit uCommonGEOClasses;

interface

uses
  Windows,
  Classes,
  Generics.Defaults,
  Generics.Collections;

type
  TGEOPoint = record
    Latitude: Double;
    Longitude: Double;

    procedure Assign(GeoPoint: TGEOPoint);
    procedure LoadFromStream(Stream: TStream);
    procedure SaveToStream(Stream: TStream);

    function TrySetCoordsLatLon(sLatLon: string): Boolean;
    function TrySetCoordsLonLat(sLonLat: string): Boolean;

    procedure SetCoords(Lat, Lon: Double);

    function IsEquial(GeoPoint: TGEOPoint; MaxDiffMeters: integer = 0): Boolean;

    function ToStringLatLon: string;
    function ToStringLonLat: string;
  end;

  PGEOPoint = ^TGEOPoint;

  TGeoPointList = class(TList<TGEOPoint>)
  public
    procedure AddNew(Latitude, Longitude: Double);
  end;

  TSegment = class(TGeoPointList)
  private
    FSegmentLength: integer;
    FSegmentTime: integer;
    function GetDestPoint: TGEOPoint;
    function GetSourcePoint: TGEOPoint;
  public
    procedure LoadFromStream(Stream: TStream);
    procedure SaveToStream(Stream: TStream);

    property SegmentLength: integer read FSegmentLength write FSegmentLength; // ������������� �������� � ������
    property SegmentTime: integer read FSegmentTime write FSegmentTime; // ������������� �������� � ��������

    property SourcePoint: TGEOPoint read GetSourcePoint;
    property DestPoint: TGEOPoint read GetDestPoint;
  end;

  TSegments = class(TObjectList<TSegment>)
  public
    function AddNew: TSegment;
    procedure Assign(Segments: TSegments);
    procedure SaveToStream(Stream: TStream);
    procedure LoadFromStream(Stream: TStream);

    function SegmentsLength: integer;
    function SegmentsTime: integer;
  end;

  TRouteInterval = class(TObject)
  private
    FIntervalLength: integer; // ������������� ��������� �������� � ������
    FIntervalTime: integer; // ������������� ��������� �������� � ��������
    FSourcePoint: TGEOPoint;
    FDestPoint: TGEOPoint;
    FSegments: TSegments;
    FTag: integer; // "��������� ���������" ��������.
    // ������������ ����� ������������� ����� � �����������, ������� ����� ��������,
    // ����� ��������� �� �������� �� ����� ����������
  public
    constructor Create;
    destructor Destroy; override;

    procedure SaveToStream(Stream: TStream);
    procedure LoadFromStream(Stream: TStream);

    procedure Assign(Interval: TRouteInterval);
    procedure Clear;

    property SourcePoint: TGEOPoint read FSourcePoint write FSourcePoint;
    property DestPoint: TGEOPoint read FDestPoint write FDestPoint;
    property IntervalLength: integer read FIntervalLength write FIntervalLength;
    property IntervalTime: integer read FIntervalTime write FIntervalTime;
    property Segments: TSegments read FSegments;

    property Tag: integer read FTag write FTag;
  end;

  TRouteIntervals = class(TObjectList<TRouteInterval>)
  public
    function AddNew(IntervalLength, IntervalTime: integer; SourcePoint, DestPoint: TGEOPoint; Segments: TSegments = nil): TRouteInterval;

    procedure Assign(Intervals: TRouteIntervals);
    procedure SaveToStream(Stream: TStream);
    procedure LoadFromStream(Stream: TStream);

    function GetRouteLength: integer;
    function GetRouteTime: integer;
  end;

function GeoPoint(Lat, Lon: Double): TGEOPoint;
function GeoPointDistance(GeoPoint1, GeoPoint2: TGEOPoint): integer; // � ������, ����������� ������� �������
function GeoPointDistance2(GeoPoint1, GeoPoint2: TGEOPoint): Double; // � ������

function EncodeCoordListForYandex(Source: TGeoPointList): string;

function GradToRad(const Grad: Double): Double; inline;
function RadToGrad(const Rad: Double): Double; inline;
function GradToRadGeoPoint(const GradGEOPoint: TGEOPoint): TGEOPoint; inline;
function RadToGradGeoPoint(const RadGEOPoint: TGEOPoint): TGEOPoint; inline;

implementation

uses
  SysUtils,
  Math,
  uCommonFunctions;

const
  Version = 1;

function GeoPoint(Lat, Lon: Double): TGEOPoint;
begin
  Result.Latitude := Lat;
  Result.Longitude := Lon;
end;

function GeoPointDistance2(GeoPoint1, GeoPoint2: TGEOPoint): Double;
var
  a, b, c, d: Extended;
  PiX: Extended;
begin
  PiX := Pi;
  a := Sqr(COS(PiX * GeoPoint2.Latitude / 180) * SIN(PiX * GeoPoint2.Longitude / 180 - PiX * GeoPoint1.Longitude / 180));
  b := Sqr(COS(PiX * GeoPoint1.Latitude / 180) * SIN(PiX * GeoPoint2.Latitude / 180) - SIN(PiX * GeoPoint1.Latitude / 180) * COS
      (PiX * GeoPoint2.Latitude / 180) * COS(ABS(PiX * GeoPoint2.Longitude / 180 - PiX * GeoPoint1.Longitude / 180)));
  c := SIN(PiX * GeoPoint1.Latitude / 180) * SIN(PiX * GeoPoint2.Latitude / 180);
  d := COS(PiX * GeoPoint1.Latitude / 180) * COS(PiX * GeoPoint2.Latitude / 180) * COS
    (ABS(PiX * GeoPoint2.Longitude / 180 - PiX * GeoPoint1.Longitude / 180));
  Result := ArcTan2(Sqrt(a + b), c + d) * 6372795;
end;

function GeoPointDistance(GeoPoint1, GeoPoint2: TGEOPoint): integer; // � ������
begin
  Result := Round(1609.344 * 3958.75 * arccos(SIN(GeoPoint1.Latitude / 57.2958) * SIN(GeoPoint2.Latitude / 57.2958) + COS
        (GeoPoint1.Latitude / 57.2958) * COS(GeoPoint2.Latitude / 57.2958) * COS(GeoPoint2.Longitude / 57.2958 - GeoPoint1.Longitude / 57.2958)
      ));
end;

function Base64EncodeYa(const Input: PAnsiChar; Len: integer): AnsiString;
const
  Base64OutA: array [0 .. 64] of AnsiChar = ('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U',
    'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
    'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '_', '=');
var
  Count: integer;
  i: integer;
begin
  Count := 0;
  i := Len;
  while (i mod 3) > 0 do
    Inc(i);
  i := (i div 3) * 4;
  SetLength(Result, i);
  i := 0;
  while Count < Len do
    begin
      Inc(i);
      Result[i] := Base64OutA[(Byte(Input[Count]) and $FC) shr 2];
      if (Count + 1) < Len then
        begin
          Inc(i);
          Result[i] := Base64OutA[((Byte(Input[Count]) and $03) shl 4) + ((Byte(Input[Count + 1]) and $F0) shr 4)];
          if (Count + 2) < Len then
            begin
              Inc(i);
              Result[i] := Base64OutA[((Byte(Input[Count + 1]) and $0F) shl 2) + ((Byte(Input[Count + 2]) and $C0) shr 6)];
              Inc(i);
              Result[i] := Base64OutA[(Byte(Input[Count + 2]) and $3F)];
            end
          else
            begin
              Inc(i);
              Result[i] := Base64OutA[(Byte(Input[Count + 1]) and $0F) shl 2];
              Inc(i);
              Result[i] := '=';
            end
        end
      else
        begin
          Inc(i);
          Result[i] := Base64OutA[(Byte(Input[Count]) and $03) shl 4];
          Inc(i);
          Result[i] := '=';
          Inc(i);
          Result[i] := '=';
        end;
      Inc(Count, 3);
    end;
  // SetLength(Result, I);
end;
/// http://api.yandex.ru/maps/doc/jsapi/1.x/dg/tasks/how-to-add-polyline.xml#encoding-polyline-points

function EncodeCoordListForYandex(Source: TGeoPointList): string;
var
  DispArray: TList<TPoint>;
  PrevIntPoint: TPoint;
  CurrIntPoint: TPoint;
  DiffPoint: TPoint;
  i: integer;
  GeoPoint: TGEOPoint;
  sa: AnsiString;
begin
  DispArray := TList<TPoint>.Create;
  try
    PrevIntPoint.X := 0;
    PrevIntPoint.Y := 0;

    for i := 0 to Source.Count - 1 do
      begin
        GeoPoint := Source[i];
        CurrIntPoint.X := Round(GeoPoint.Latitude * 1000000);
        CurrIntPoint.Y := Round(GeoPoint.Longitude * 1000000);
        DiffPoint.X := CurrIntPoint.X - PrevIntPoint.X;
        DiffPoint.Y := CurrIntPoint.Y - PrevIntPoint.Y;

        DispArray.Add(DiffPoint);

        PrevIntPoint := CurrIntPoint;
      end;

    // ��� ����� ����� � ���������� �������.
    // �������� ����������� � ������.
    if DispArray.Count > 1 then
      begin
        SetLength(sa, DispArray.Count * SizeOf(TPoint));
        for i := 0 to DispArray.Count - 1 do
          begin
            DiffPoint := DispArray[i];
            Move(DiffPoint.X, sa[i * SizeOf(TPoint) + 1], SizeOf(TPoint));
          end;
        Result := string(Base64EncodeYa(@sa[1], Length(sa)));
      end;
  finally
    DispArray.Free;
  end;
end;

function GradToRad(const Grad: Double): Double; inline;
begin
  Result := Grad * Pi / 180;
end;

function RadToGrad(const Rad: Double): Double; inline;
begin
  Result := Rad * 180 / Pi;
end;

function GradToRadGeoPoint(const GradGEOPoint: TGEOPoint): TGEOPoint; inline;
begin
  Result.SetCoords(GradToRad(GradGEOPoint.Latitude), GradToRad(GradGEOPoint.Longitude));
end;

function RadToGradGeoPoint(const RadGEOPoint: TGEOPoint): TGEOPoint; inline;
begin
  Result.SetCoords(RadToGrad(RadGEOPoint.Latitude), RadToGrad(RadGEOPoint.Longitude));
end;

{ TGEOPoint }

procedure TGEOPoint.Assign(GeoPoint: TGEOPoint);
var
  Stream: TStream;
begin
  Stream := TMemoryStream.Create;
  try
    GeoPoint.SaveToStream(Stream);
    Stream.Seek(0, soBeginning);
    LoadFromStream(Stream);
  finally
    Stream.Free;
  end;
end;

function TGEOPoint.IsEquial(GeoPoint: TGEOPoint; MaxDiffMeters: integer): Boolean;
begin
  if MaxDiffMeters <= 0 then
    Result := (Latitude = GeoPoint.Latitude) and (Longitude = GeoPoint.Longitude)
  else
    Result := GeoPointDistance(Self, GeoPoint) <= MaxDiffMeters;
end;

procedure TGEOPoint.LoadFromStream(Stream: TStream);
begin
  Stream.read(Latitude, SizeOf(Double));
  Stream.read(Longitude, SizeOf(Double));
end;

procedure TGEOPoint.SaveToStream(Stream: TStream);
begin
  Stream.write(Latitude, SizeOf(Double));
  Stream.write(Longitude, SizeOf(Double));
end;

procedure TGEOPoint.SetCoords(Lat, Lon: Double);
begin
  Latitude := Lat;
  Longitude := Lon;
end;

function TGEOPoint.ToStringLatLon: string;
var
  FS: TFormatSettings;
begin
  GetLocaleFormatSettings(GetThreadLocale, FS);
  FS.DecimalSeparator := '.';
  Result := FloatToStr(Latitude, FS) + ',' + FloatToStr(Longitude, FS);
end;

function TGEOPoint.ToStringLonLat: string;
var
  FS: TFormatSettings;
begin
  GetLocaleFormatSettings(GetThreadLocale, FS);
  FS.DecimalSeparator := '.';
  Result := FloatToStr(Longitude, FS) + ',' + FloatToStr(Latitude, FS);
end;

function TGEOPoint.TrySetCoordsLatLon(sLatLon: string): Boolean;
var
  iPos: integer;
  sLat, sLon: string;
begin
  Result := False;
  iPos := Pos(',', sLatLon);
  if iPos = 0 then
    exit;
  sLat := copy(sLatLon, 1, iPos - 1);
  sLon := copy(sLatLon, iPos + 1, Length(sLatLon));

  Result := TryStrToFloatFull(sLat, Latitude) and TryStrToFloatFull(sLon, Longitude);
end;

function TGEOPoint.TrySetCoordsLonLat(sLonLat: string): Boolean;
var
  iPos: integer;
  sLat, sLon: string;
begin
  Result := False;
  iPos := Pos(',', sLonLat);
  if iPos = 0 then
    exit;
  sLon := copy(sLonLat, 1, iPos - 1);
  sLat := copy(sLonLat, iPos + 1, Length(sLonLat));

  Result := TryStrToFloatFull(sLat, Latitude) and TryStrToFloatFull(sLon, Longitude);
end;

{ TGeoPointList }

procedure TGeoPointList.AddNew(Latitude, Longitude: Double);
var
  tmp: TGEOPoint;
begin
  tmp.SetCoords(Latitude, Longitude);
  inherited Add(tmp);
end;

{ TSegment }

function TSegment.GetDestPoint: TGEOPoint;
begin
  if Count > 0 then
    Result := Last
  else
    begin
      Result.Latitude := 0;
      Result.Longitude := 0;
    end;
end;

function TSegment.GetSourcePoint: TGEOPoint;
begin
  if Count > 0 then
    Result := First
  else
    begin
      Result.Latitude := 0;
      Result.Longitude := 0;
    end;
end;

procedure TSegment.LoadFromStream(Stream: TStream);
var
  Ver, i, iCount: integer;
  tmpPoint: TGEOPoint;
begin
  Stream.read(Ver, SizeOf(integer));
  if Ver > Version then
    raise EStreamError.Create('Wrong TSegment version.');
  Clear;

  Stream.read(FSegmentLength, SizeOf(integer));
  Stream.read(FSegmentTime, SizeOf(integer));

  Stream.read(iCount, SizeOf(integer));
  Capacity := iCount;
  for i := 0 to iCount - 1 do
    begin
      tmpPoint.LoadFromStream(Stream);
      Add(tmpPoint);
    end;
end;

procedure TSegment.SaveToStream(Stream: TStream);
var
  Ver, i, iCount: integer;
begin
  Ver := Version;

  Stream.write(Ver, SizeOf(integer));

  Stream.write(FSegmentLength, SizeOf(integer));
  Stream.write(FSegmentTime, SizeOf(integer));

  iCount := Count;
  Stream.write(iCount, SizeOf(integer));
  for i := 0 to Count - 1 do
    Items[i].SaveToStream(Stream);
end;

{ TSegments }

function TSegments.AddNew: TSegment;
begin
  Result := TSegment.Create;
  Add(Result);
end;

procedure TSegments.Assign(Segments: TSegments);
var
  Stream: TStream;
begin
  Stream := TMemoryStream.Create;
  try
    Segments.SaveToStream(Stream);
    Stream.Seek(0, soBeginning);
    LoadFromStream(Stream);
  finally
    Stream.Free;
  end;
end;

procedure TSegments.LoadFromStream(Stream: TStream);
var
  Ver, i, iCount: integer;
  tmp: TSegment;
begin
  Stream.read(Ver, SizeOf(integer));
  if Ver > Version then
    raise EStreamError.Create('Wrong TSegments version.');
  Clear;
  Stream.read(iCount, SizeOf(integer));
  Capacity := iCount;
  for i := 0 to iCount - 1 do
    begin
      tmp := TSegment.Create;
      try
        tmp.LoadFromStream(Stream);
        Add(tmp);
      except
        tmp.Free;
        raise ;
      end;
    end;
end;

procedure TSegments.SaveToStream(Stream: TStream);
var
  Ver, i, iCount: integer;
begin
  Ver := Version;
  Stream.write(Ver, SizeOf(integer));

  iCount := Count;
  Stream.write(iCount, SizeOf(integer));
  for i := 0 to iCount - 1 do
    Items[i].SaveToStream(Stream);
end;

function TSegments.SegmentsLength: integer;
var
  i: integer;
begin
  Result := 0;
  for i := 0 to Count - 1 do
    Result := Result + Items[i].SegmentLength;
end;

function TSegments.SegmentsTime: integer;
var
  i: integer;
begin
  Result := 0;
  for i := 0 to Count - 1 do
    Result := Result + Items[i].SegmentTime;
end;

{ TRouteInterval }

procedure TRouteInterval.Assign(Interval: TRouteInterval);
var
  Stream: TStream;
begin
  Stream := TMemoryStream.Create;
  try
    Interval.SaveToStream(Stream);
    Stream.Seek(0, soBeginning);
    LoadFromStream(Stream);
  finally
    Stream.Free;
  end;
end;

procedure TRouteInterval.Clear;
begin
  FIntervalLength := 0;
  FIntervalTime := 0;
  FSourcePoint.SetCoords(0, 0);
  FDestPoint.SetCoords(0, 0);
  FSegments.Clear;
end;

constructor TRouteInterval.Create;
begin
  inherited Create;
  FSegments := TSegments.Create(True);
end;

destructor TRouteInterval.Destroy;
begin
  FreeAndNil(FSegments);
  inherited;
end;

procedure TRouteInterval.LoadFromStream(Stream: TStream);
var
  Ver: integer;
begin
  Stream.read(Ver, SizeOf(integer));
  if Ver > Version then
    raise EStreamError.Create('Wrong TRouteInterval version.');
  Stream.read(FIntervalLength, SizeOf(integer));
  Stream.read(FIntervalTime, SizeOf(integer));
  FSourcePoint.LoadFromStream(Stream);
  FDestPoint.LoadFromStream(Stream);

  FSegments.LoadFromStream(Stream);
end;

procedure TRouteInterval.SaveToStream(Stream: TStream);
var
  Ver: integer;
begin
  Ver := Version;
  Stream.write(Ver, SizeOf(integer));

  Stream.write(FIntervalLength, SizeOf(integer));
  Stream.write(FIntervalTime, SizeOf(integer));
  FSourcePoint.SaveToStream(Stream);
  FDestPoint.SaveToStream(Stream);
  FSegments.SaveToStream(Stream);
end;

{ TRouteIntervals }

function TRouteIntervals.AddNew(IntervalLength, IntervalTime: integer; SourcePoint, DestPoint: TGEOPoint; Segments: TSegments): TRouteInterval;
begin
  Result := TRouteInterval.Create;

  Result.FIntervalLength := IntervalLength;
  Result.FIntervalTime := IntervalTime;
  Result.FSourcePoint := SourcePoint;
  Result.FDestPoint := DestPoint;

  if Assigned(Segments) then
    begin
      Result.FSegments.Assign(Segments);
    end;
  inherited Add(Result);
end;

procedure TRouteIntervals.Assign(Intervals: TRouteIntervals);
var
  tmpStream: TStream;
begin
  tmpStream := TMemoryStream.Create;
  try
    Intervals.SaveToStream(tmpStream);
    tmpStream.Seek(0, soBeginning);
    LoadFromStream(tmpStream);
  finally
    tmpStream.Free;
  end;
end;

function TRouteIntervals.GetRouteLength: integer;
var
  i: integer;
begin
  Result := 0;
  for i := 0 to Count - 1 do
    Result := Result + Items[i].IntervalLength;
end;

function TRouteIntervals.GetRouteTime: integer;
var
  i: integer;
begin
  Result := 0;
  for i := 0 to Count - 1 do
    Result := Result + Items[i].IntervalTime;
end;

procedure TRouteIntervals.LoadFromStream(Stream: TStream);
var
  Ver, i, iCount: integer;
  tmp: TRouteInterval;
begin
  Stream.read(Ver, SizeOf(integer));
  if Ver > Version then
    raise EStreamError.Create('Wrong TRouteIntervals version.');

  Stream.read(iCount, SizeOf(integer));
  Clear;
  Capacity := iCount;
  for i := 0 to iCount - 1 do
    begin
      tmp := TRouteInterval.Create;
      try
        tmp.LoadFromStream(Stream);
        inherited Add(tmp);
      except
        tmp.Free;
        raise ;
      end;
    end;
end;

procedure TRouteIntervals.SaveToStream(Stream: TStream);
var
  Ver, i, iCount: integer;
begin
  Ver := Version;
  Stream.write(Ver, SizeOf(integer));

  iCount := Count;
  Stream.write(iCount, SizeOf(integer));
  for i := 0 to iCount - 1 do
    Items[i].SaveToStream(Stream);
end;

end.
