unit Common.Interfaces;

interface

uses
  System.Classes,
  Xml.XMLIntf,
  Winapi.ActiveX,
  System.JSON,
  uAbstractProtoBufClasses;

type
  IOwneredIntf = interface
    ['{ADF563F3-B4CE-4E96-9559-F0FFC2936D5E}']
    function GetOwner: TObject;
    procedure SetOwner(const Value: TObject);

    property Owner: TObject read GetOwner write SetOwner;
  end;

  IActionCallback = interface
    ['{F624355C-C89D-4581-B5FB-730E71A3B93C}']
    procedure intOnCurrentActionCallback(Sender: TObject; const Action: string);
  end;

  IXMLStorage = interface
    ['{577BF821-1589-4928-A43E-EB45313775D1}']
    procedure Load(Node: IXMLNode);
    procedure Save(Node: IXMLNode);
  end;

  IJSONStorage = interface
    ['{A8C203EC-1F21-44BA-89D5-D7B5000AB51A}']
    procedure JSONLoad(AJSON: TJSONObject);
    procedure JSONSave(AJSON: TJSONObject);
  end;

  IProtoBufStorage = interface
    ['{5F1BF76F-7550-479E-AB37-B9F464DA36D6}']
    procedure Assign(ProtoBuf: TAbstractProtoBuf);

    procedure LoadFromStream(Stream: TStream);
    procedure SaveToStream(Stream: TStream);
  end;

  IVariousDataPool = interface
    ['{9B448649-AC34-4235-A33E-EC82EF4E463D}']
    function GetValues(const ValueName: WideString): Variant; safecall;
    procedure SetValues(const ValueName: WideString; const Value: Variant); safecall;

    property Values[const ValueName: WideString]: Variant read GetValues write SetValues;
  end;

type
  TChangeAction = (caAdd, caChange, caDelete);

  TChangeNotificationStructure = record
    NotifyObject: TObject;
    Action: TChangeAction;
    Description: WideString;

    constructor Create(ANotifyObject: TObject; AAction: TChangeAction; const ADescription: string);
  end;

  TChangeNotificationEvent = procedure(Sender: TObject; Notification: TChangeNotificationStructure) of object;

  IChangeNotificator = interface
    ['{B3C61D61-1A28-46A8-8775-A651CC117820}']
    procedure RegisterNotification(Notificator: TChangeNotificationEvent);
    procedure UnregisterNotification(Notificator: TChangeNotificationEvent);

    procedure Lock;
    procedure Unlock(const ADescription: string = '');
    procedure DoNotify(Notification: TChangeNotificationStructure);
    procedure ReRaiseNotification(Notification: TChangeNotificationStructure);
    procedure NotifyForSelfChanges(const ADescription: string = '');
  end;

  IGlobalService = interface
    ['{E43B207D-9709-428F-BD12-1AC2CC7447E0}']
    procedure ReleaseLocalInstance; safecall;
  end;

  IGlobalServices = interface
    ['{998BE719-DE99-4577-98BE-4B1F28267284}']
    procedure RegisterService(const IID: TGUID; Service: IInterface); safecall;

    procedure SetExternal(AExternalServices: IGlobalServices); safecall;
  end;

  TConnectionStringType = (cstMain = 0, cstRDS = 1, cstNSI = 2, cstPNSI = 3, cstPR = 4, cstRDSAlien = 5);

  TTypeADOConnection = (tcGeneral, tcPaxLoading, tcBrif, tcTablet, tcMap, tcDelayActs, tcDelay1, tcDelay2, tcAccess,
    tcAlerts, tcFleet, tcAirport, tcRestrict, tcRepresent, tcDesigner, tcReport, tcTransfer, tcSeatsUsing, tcFlightInfo,
    tcOOSMain, tcCrew, tcReportCancelled, tcLicence, tcDelayRep, tcAirco, tcSSIM, tcCargoBox, tcOtherReports,
    tcContracts, tcCargoUn, tcBaggageNorm, tcDelayDuty, tcNalCh, tcDrivingTimes, tcFPL, tcPPL, tcFuel, tcAutoOFP,
    tcAFTN, tcSOFI, tcZitoChain, tcRouteAlts);

  IGlobalConnection = interface
    ['{4C2E81EE-DAD9-4070-8642-0E91048C929E}']
    function GetMainConnectionString: WideString; safecall;
    procedure SetMainConnectionString(const Value: WideString); safecall;

    function GetConnectionString(ConnectionStringType: TConnectionStringType): WideString; safecall;
    function UpdateLetterConnection(const ConnectionString: WideString; ConnType: TTypeADOConnection)
      : WideString; safecall;
    function UpdateLetterConnectionX(const ConnectionString: WideString; const ConnType: WideString)
      : WideString; safecall;

    procedure SetConnectionString(ConnectionStringType: TConnectionStringType; const Value: WideString); safecall;

    property MainConnectionString: WideString read GetMainConnectionString write SetMainConnectionString;
    property ConnectionString[ConnectionStringType: TConnectionStringType]: WideString read GetConnectionString
      write SetConnectionString;
  end;

  IUserInfo = interface
    ['{5968DD15-0E31-4762-A94F-B0D3C063E7CF}']
    function GetLogin: WideString; safecall;
    /// <returns>Name, as listed in dbo.ac_p_users, field Name</returns>
    function GetName: WideString; safecall;
    /// <returns>English name, as listed in dbo.ac_p_users, field Name_E</returns>
    function GetName_E: WideString; safecall;
    /// <returns>ID from dbo.ac_p_users. COP = Code of Operator, the common abbreviature in DB RDS</returns>
    function GetCOP: Integer; safecall;

    property Login: WideString read GetLogin;
    property name: WideString read GetName;
    property Name_E: WideString read GetName_E;
    property COP: Integer read GetCOP;
  end;

  Trsi = 0 .. 3; // rs2007, rs2010, rs2013, rs2016

  TVisualSettings = record
    MainSkinName: WideString; // use same values, as listed in dxSkinController. 1:1
    RibbonStyleInt: Trsi;
  end;

  IProgramSpecificData = interface
    ['{8AFB557B-B5E7-4FE7-B5AE-6B5B8B1A0F39}']
    /// <returns>free string, used only in UI.
    /// In Login window, in MainForm caption, etc...
    /// For example = 'OpenSky/Ops'</returns>
    function GetProgrammDescription: WideString; safecall;
    /// <returns>program name, as listed in ac_p_programs, field pname_exe</returns>
    function GetProgrammRegisteredName: WideString; safecall;
    /// <returns>free string, for example - '����������� ���������� ��������'</returns>
    function GetSubSystemDescription: WideString; safecall;
    function GetVisualSettings: TVisualSettings; safecall;

    property SubSystemDescription: WideString read GetSubSystemDescription;
    property ProgrammRegisteredName: WideString read GetProgrammRegisteredName;
    property ProgrammDescription: WideString read GetProgrammDescription;
    property VisualSettings: TVisualSettings read GetVisualSettings;
  end;

  IProgramSpecificDataWritable = interface(IProgramSpecificData)
    ['{8A93A943-079A-415C-BE07-EF60CBD5F907}']
    // see comments for IProgramSpecificData
    procedure SetSubSystemDescription(const ASSDescription: WideString); safecall;
    procedure SetProgrammRegisteredName(const ARegisteredName: WideString); safecall;
    procedure SetProgrammDescription(const ADescription: WideString); safecall;
    procedure SetVisualSettings(const AVisualSettings: TVisualSettings); safecall;
  end;

  IDefaultPresentationSettings = interface
    ['{3455191B-F7A0-4C79-BF08-ABB458F26DD2}']
    function GetDefaultAPViewMode: Integer; safecall;
    function GetDefaultNRViewMode: Integer; safecall;
    function GetDefaultTimeViewMode: Integer; safecall;

    property DefaultAPViewMode: Integer read GetDefaultAPViewMode;
    property DefaultNRViewMode: Integer read GetDefaultNRViewMode;
    property DefaultTimeViewMode: Integer read GetDefaultTimeViewMode;
  end;

  IApplicationInfo = interface
    ['{1E090180-DE16-4B0F-8C03-383FF73E22A6}']
    function GetApplicationHandle: THandle; safecall;
    function GetMainWindowHandle: THandle; safecall;
    function GetApplicationIcon: THandle; safecall;

    property ApplicationHandle: THandle read GetApplicationHandle;
    property MainWindowHandle: THandle read GetMainWindowHandle;
    property ApplicationIcon: THandle read GetApplicationIcon;
  end;

  TAirCoPrefix = (acpIATA, acpICAO, acpIATARus, acpICAORus);

  IAirCoInfo = interface
    ['{0EA6E611-0389-4391-87E2-2C2BF7B68E94}']
    /// <returns>Unique client identifier.
    /// Contained in [dbo].[Ac_p_params_system],
    /// ID_App = 0, IDParam = 1, ValueInt
    /// For Example: Rossiya = 1, Oren = 13, Azimuth = 17</returns>
    function GetClientID: Integer; safecall;
    /// <returns>ID aviaCompany in airco_info_main, which marked as Main</returns>
    function GetID: Integer; safecall;
    /// <returns>various fields from airco_info_main</returns>
    function GetFullName: WideString; safecall;
    function GetFullNameEng: WideString; safecall;
    function GetShortName: WideString; safecall;
    function GetShortNameEng: WideString; safecall;
    function GetTinyName: WideString; safecall;
    function GetTinyNameEng: WideString; safecall;

    /// <returns>count of possible flight number prefixes.
    /// For example: FV, SU, SDM, etc...</returns>
    function GetPrefixCount: Integer; safecall;
    /// <returns>concrete flight number prefix. Zero-based indexes should be MAIN indexes</returns>
    function GetPrefix(i: Integer; Prefix: TAirCoPrefix): WideString; safecall;

    /// <returns>count of base aviacompany airports</returns>
    function GetBasePortsCount: Integer; safecall;
    /// <returns>concrete base airport ID. Zero-based port should be MAIN port
    /// To get airport properties - use IAirportInfoList </returns>
    function GetBasePortID(i: Integer): Integer; safecall;

    function GetCountryCode: WideString; safecall;
    /// <returns>not used now, reserved for future</returns>
    function GetidOK: Integer; safecall;
    /// <returns>content of graphic file whith AirCo logo. Contained in airco_info_main, field img_logo
    /// to convert to TStream - use IStreamToStream below</returns>
    function GetLogo: IStream; safecall;

    property ClientID: Integer read GetClientID;
    property ID: Integer read GetID;
    property FullName: WideString read GetFullName;
    property FullNameEng: WideString read GetFullNameEng;
    property ShortName: WideString read GetShortName;
    property ShortNameEng: WideString read GetShortNameEng;
    property TinyName: WideString read GetTinyName;
    property TinyNameEng: WideString read GetTinyNameEng;

    /// <remarks>
    /// Total count of prefixes groups (acpIATA, acpICAO, acpIATARus, acpICAORus)
    /// </remarks>
    property PrefixSetCount: Integer read GetPrefixCount;
    /// <remarks>
    /// return single prefix value.
    ///  zero-based group = MainPrefixes
    /// </remarks>
    property Prefix[i: Integer; Prefix: TAirCoPrefix]: WideString read GetPrefix;

    /// <remarks>
    /// Total base port count of our AirCompany
    /// </remarks>
    property BasePortsCount: Integer read GetBasePortsCount;
    /// <remarks>
    /// returned value can be used to get IAirportInfo from AirportInfoList
    /// zero-based PortID is MAIN AirCoPort
    /// </remarks>
    property BasePortID[i: Integer]: Integer read GetBasePortID;

    property CountryCode: WideString read GetCountryCode;

    property idOK: Integer read GetidOK;
    property Logo: IStream read GetLogo;
  end;

  IRDSParamsRead = interface
    ['{98ACEEF8-2335-4572-8019-5621A90A23E1}']
    function ReadBoolParam(const IDParameter: OleVariant; Default: LongBool = False): LongBool; safecall;
    function ReadIntParam(const IDParameter: OleVariant; Default: Integer = 0): Integer; safecall;
    function ReadFloatParam(const IDParameter: OleVariant; Default: Double = 0): Double; safecall;
    function ReadStringParam(const IDParameter: OleVariant; const Default: WideString = ''): WideString; safecall;
    function ReadDateParam(const IDParameter: OleVariant; Default: TDateTime = 0): TDateTime; safecall;
    function ReadGUIDParam(const IDParameter: OleVariant; Default: TGUID): TGUID; safecall;
    function ReadXMLParam(const IDParameter: OleVariant; const Default: WideString = ''): WideString; safecall;
    procedure ReadStreamParam(const IDParameter: OleVariant; Default, Target: IStream); safecall;

    function IsExists(const IDParameter: OleVariant): LongBool; safecall;

    function LoadParametersData(const AppName: WideString; IDUser: Integer = 0): LongBool; safecall;
  end;

  IRDSParamsWrite = interface(IRDSParamsRead)
    ['{EEE35A3B-1A09-4581-BCE0-8C889115DB3E}']
    procedure WriteBoolParam(const IDParameter: OleVariant; Value: LongBool); safecall;
    procedure WriteIntParam(const IDParameter: OleVariant; Value: Integer); safecall;
    procedure WriteFloatParam(const IDParameter: OleVariant; Value: Double); safecall;
    procedure WriteStringParam(const IDParameter: OleVariant; const Value: WideString); safecall;
    procedure WriteDateParam(const IDParameter: OleVariant; Value: TDateTime); safecall;
    procedure WriteGUIDParam(const IDParameter: OleVariant; Value: TGUID); safecall;
    procedure WriteXMLParam(const IDParameter: OleVariant; const Value: WideString); safecall;
    procedure WriteStreamParam(const IDParameter: OleVariant; Value: IStream); safecall;

    function SaveParametersData(const AppName: WideString; IDUser: Integer = 0): LongBool; safecall;
  end;

  IRDSParamsUser = interface(IRDSParamsWrite)
    ['{E2BF67E3-DA9A-49C1-AFAE-74E3D9AFEA80}']
  end;

  IRDSParamsApplication = interface(IRDSParamsRead)
    ['{E91DA838-0F86-487D-89AC-7EEF2DA56F40}']
  end;

  IRDSParamsGlobal = interface(IRDSParamsRead)
    ['{8E514DE1-0743-4AF3-92D8-A6966270D89D}']
  end;

  IObserver = interface
    ['{FA6FF750-EBC3-4CF9-AD6D-8655656F2ACE}']
    procedure Changed(Action: TChangeAction; Data: Pointer; Description: WideString); safecall;
  end;

  ISubscriber = interface
    ['{208579E7-6170-498B-B2E1-8705229A393F}']
    procedure Subscribe(Observer: IObserver); safecall;
    procedure Unsubscribe(Observer: IObserver); safecall;
    procedure Notify(Action: TChangeAction; Data: Pointer; Description: WideString); safecall;
  end;

  ITOFilter = interface
    ['{16D6E0A6-0A50-4B71-B9EF-1E5A239DB3AE}']
    function Used: LongBool; safecall;
    function TypeVSUsed(const AnyTypeVS: WideString): LongBool; safecall;
  end;

  ITOAmosFilter = interface(ITOFilter)
    ['{FF7F8B02-975A-40E8-94D0-0FAAE51A2A28}']
  end;

  ITOPreopsFilter = interface(ITOFilter)
   ['{9C28DFF6-D148-4346-89A1-7FCE0B4A433C}']
  end;

procedure IStreamToStream(InStream: IStream; OutStream: TStream);
procedure StreamToIStream(InStream: TStream; OutStream: IStream);

implementation

uses
  System.SysUtils;

{ TNotificationStructure }

constructor TChangeNotificationStructure.Create(ANotifyObject: TObject; AAction: TChangeAction;
  const ADescription: string);
begin
  NotifyObject := ANotifyObject;
  Action := AAction;
  Description := ADescription;
end;

procedure IStreamToStream(InStream: IStream; OutStream: TStream);
const
  ReadSize = 65536;
var
  {$IFDEF VER230}
  i: Int64; // XE2
  {$ELSE}
  i: UInt64; //RIO
  {$ENDIF}
  readed: LongInt;
  tmpData: Pointer;
begin
  InStream.Seek(0, STREAM_SEEK_END, i);
  OutStream.Size := i;
  InStream.Seek(0, STREAM_SEEK_SET, i);

  tmpData := AllocMem(ReadSize);
  try
    readed := ReadSize;
    while readed = ReadSize do
      if Succeeded(InStream.Read(tmpData, ReadSize, @readed)) then
        OutStream.Write(tmpData^, readed)
      else
        raise EInvalidContainer.Create('Cant read from IStream');
  finally
    FreeMem(tmpData);
  end;

  OutStream.Seek(0, soBeginning);
end;

procedure StreamToIStream(InStream: TStream; OutStream: IStream);
const
  ReadSize = 65536;
var
  {$IFDEF VER230}
  i: Int64; // XE2
  {$ELSE}
  i: UInt64; //RIO
  {$ENDIF}
  readed: LongInt;
  tmpData: Pointer;
begin
  OutStream.SetSize(0);
  OutStream.SetSize(InStream.Size);

  InStream.Seek(0, soBeginning);

  tmpData := AllocMem(ReadSize);
  try
    readed := ReadSize;
    while readed = ReadSize do
    begin
      readed := InStream.Read(tmpData^, ReadSize);
      OutStream.Write(tmpData, readed, @i);
    end;
  finally
    FreeMem(tmpData);
  end;

  OutStream.Seek(0, STREAM_SEEK_SET, i);
end;

end.
