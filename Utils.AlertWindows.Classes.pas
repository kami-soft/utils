unit Utils.AlertWindows.Classes;

interface

uses
  System.Classes,
  System.Generics.Collections,
  System.SysUtils,
  Winapi.ActiveX,
  dxAlertWindow,
  cxGraphics,
  Utils.AlertWindows;

type
  TAlertWindowManager = class(TInterfacedObject, IAlertWindowManager, IAlertWindowManagerSettings)
  strict private
    FButtonImageList: TcxImageList;
    FMessageImageList: TcxImageList;
    FAlertManager: TdxAlertWindowManager;
    { IAlertWindowManager }
    function AddMessage(const Category, Caption, Text: WideString; ImgIndex: integer): IAlertMessage; safecall;
    function AddButtonImage(AImageStream: IStream): integer; safecall;
    function AddMessageImage(AImageStream: IStream): integer; safecall;
    function FindWindow(const Category: WideString): IAlertWindow; safecall;

    { IAlertWindowManagerSettings }
    function GetWindowsPosition: integer; safecall;
    procedure SetWindowsPosition(const AValue: integer); safecall;
    function GetWindowMaxCount: integer; safecall;
    procedure SetWindowMaxCount(const AValue: integer); safecall;
  strict private
    function intAddImage(AImageStream: IStream; AImageList: TcxImageList): integer;
    function InternalAdd(const Category, Caption, Text: WideString; ImgIndex: integer): IAlertMessage;
  public
    constructor Create;
    destructor Destroy; override;
  end;

implementation

uses
  Winapi.Windows,
  Winapi.Messages,
  Vcl.AxCtrls,
  Vcl.Controls,
  cxClasses,
  dxGDIPlusClasses,
  Common.Interfaces,
  Utils.GlobalServices;

type
  TAlertMessage = class(TInterfacedObject, IAlertMessage, IAlertOwnered)
  strict private
    FdxAlertWindowMessage: TdxAlertWindowMessage;
    FOwner: IInterface;

    { IAlertMessage }
    function GetCaption: WideString; safecall;
    procedure SetCaption(const AValue: WideString); safecall;

    function GetText: WideString; safecall;
    procedure SetText(const AValue: WideString); safecall;

    function GetImageIndex: integer; safecall;
    procedure SetImageIndex(const AValue: integer); safecall;

    function GetTag: integer; safecall;
    procedure SetTag(const AValue: integer); safecall;

    function GetTagPointer: Pointer; safecall;
    procedure SetTagPointer(const AValue: Pointer); safecall;

    { IAlertOwnered }
    function GetOwner: IInterface; safecall;
  public
    constructor Create(msg: TdxAlertWindowMessage; AOwner: IInterface);
  end;

  TAlertMessageList = class(TInterfacedObject, IAlertMessageList, IAlertOwnered)
  strict private
    FMSGList: TdxAlertWindowMessageList;
    FOwner: IInterface;

    { IAlertMessageList }
    function Count: integer; safecall;
    function Add: IAlertMessage; safecall;
    function AddAndFill(const Caption, Text: WideString; ImageIndex: integer): IAlertMessage; safecall;
    function Item(index: integer): IAlertMessage; safecall;
    procedure Delete(index: integer); safecall;

    { IAlertOwnered }
    function GetOwner: IInterface; safecall;
  public
    constructor Create(MsgList: TdxAlertWindowMessageList; AOwner: IInterface);
  end;

  TAlertButton = class(TInterfacedObject, IAlertButton, IAlertOwnered)
  strict private
    FButton: TdxAlertWindowButton;
    FOwner: IInterface;

    { IAlertButton }
    function GetHint: WideString; safecall;
    procedure SetHint(const AValue: WideString); safecall;

    function GetImageIndex: integer; safecall;
    procedure SetImageIndex(const AValue: integer); safecall;

    function GetVisible: LongBool; safecall;
    procedure SetVisible(const AValue: LongBool); safecall;

    function GetLeftMargin: integer; safecall;
    procedure SetLeftMargin(const AValue: integer); safecall;

    function GetRightMargin: integer; safecall;
    procedure SetRightMargin(const AValue: integer); safecall;

    { IAlertOwnered }
    function GetOwner: IInterface; safecall;
  public
    constructor Create(AButton: TdxAlertWindowButton; AOwner: IInterface);
  end;

  TAlertButtons = class(TInterfacedObject, IAlertButtons, IAlertOwnered)
  strict private
    FOptionsButtons: TdxAlertWindowOptionsButtons;
    FOwner: IInterface;

    { IAlertButtons }
    function GetWidth: integer; safecall;
    function GetHeight: integer; safecall;

    function GetCount: integer; safecall;
    function GetButton(AIndex: integer): IAlertButton;

    procedure Clear; safecall;
    procedure Delete(AIndex: integer); safecall;
    function Add: IAlertButton; safecall;

    { IAlertOwnered }
    function GetOwner: IInterface; safecall;
  public
    constructor Create(AOptionsButtons: TdxAlertWindowOptionsButtons; AOwner: IInterface);
  end;

  TAlertWindowOptionsText = class(TInterfacedObject, IAlertWindowOptionsText, IAlertOwnered)
  strict private
    FWindowOptionsText: TdxAlertWindowOptionsText;
    FOwner: IInterface;
    { IAlertWindowOptionsText }
    function GetAlignHorz: integer; safecall;
    procedure SetAlignHorz(const AValue: integer); safecall;

    function GetAlignVert: integer; safecall;
    procedure SetAlignVert(const AValue: integer); safecall;

    function GetFont: IFont; safecall;

    { IAlertOwnered }
    function GetOwner: IInterface; safecall;
  public
    constructor Create(AWindowOptionsText: TdxAlertWindowOptionsText; AOwner: IInterface);
  end;

  TAlertWindowOptionsMessage = class(TInterfacedObject, IAlertWindowOptionsMessage, IAlertOwnered)
  strict private
    FWindowOptionsMessage: TdxAlertWindowOptionsMessage;
    FOwner: IInterface;

    { IAlertWindowOptionsMessage }
    function GetOptionsCaption: IAlertWindowOptionsText; safecall;
    function GetOptionsText: IAlertWindowOptionsText; safecall;

    { IAlertOwnered }
    function GetOwner: IInterface; safecall;
  public
    constructor Create(AWindowOptionsMessage: TdxAlertWindowOptionsMessage; AOwner: IInterface);
  end;

  TAlertWindowOptionsSize = class(TInterfacedObject, IAlertWindowOptionsSize, IAlertOwnered)
  strict private
    FWindowOptionsSize: TdxAlertWindowOptionsSize;
    FOwner: IInterface;

    { IAlertWindowOptionsSize }
    function GetAutoHeight: LongBool; safecall;
    procedure SetAutoHeight(const AValue: LongBool); safecall;

    function GetAutoSizeAdjustment: LongBool; safecall;
    procedure SetAutoSizeAdjustment(const AValue: LongBool); safecall;

    function GetAutoWidth: LongBool; safecall;
    procedure SetAutoWidth(const AValue: LongBool); safecall;

    function GetHeight: integer; safecall;
    procedure SetHeight(const AValue: integer); safecall;

    function GetMinHeight: integer; safecall;
    procedure SetMinHeight(const AValue: integer); safecall;

    function GetMaxHeight: integer; safecall;
    procedure SetMaxHeight(const AValue: integer); safecall;

    function GetWidth: integer; safecall;
    procedure SetWidht(const AValue: integer); safecall;

    function GetMinWidth: integer; safecall;
    procedure SetMinWidth(const AValue: integer); safecall;

    function GetMaxWidth: integer; safecall;
    procedure SetMaxWidth(const AValue: integer); safecall;

    { IAlertOwnered }
    function GetOwner: IInterface; safecall;
  public
    constructor Create(AWindowOptionsSize: TdxAlertWindowOptionsSize; AOwner: IInterface);
  end;

  TAlertWindowOptionsBehavior = class(TInterfacedObject, IAlertWindowOptionsBehavior, IAlertOwnered)
  strict private
    FWindowOptionsBehavior: TdxAlertWindowOptionsBehavior;
    FOwner: IInterface;

    { IAlertWindowOptionsBehavior }
    function GetCloseOnRightClick: LongBool; safecall;
    procedure SetCloseOnRightClick(const AValue: LongBool); safecall;

    function GetDisplayTime: integer; safecall;
    procedure SetDisplayTime(const AValue: integer); safecall;

    function GetScreenSnap: LongBool; safecall;
    procedure SetScreenSnap(const AValue: LongBool); safecall;

    function GetScreenSnapBuffer: integer; safecall;
    procedure SetScreenSnapBuffer(const AValue: integer); safecall;

    { IAlertOwnered }
    function GetOwner: IInterface; safecall;
  public
    constructor Create(AWindowOptionsBehavior: TdxAlertWindowOptionsBehavior; AOwner: IInterface);
  end;

  TAlertWindowOptionsCaptionButtons = class(TInterfacedObject, IAlertWindowOptionsCaptionButtons, IAlertOwnered)
  strict private
    FOptionsCaptionButtons: TdxAlertWindowOptionsCaptionButtons;
    FOwner: IInterface;

    { IAlertWindowOptionsCaptionButtons }
    function GetPinButton: LongBool; safecall;
    procedure SetPinButton(const AValue: LongBool); safecall;

    function GetCloseButton: LongBool; safecall;
    procedure SetCloseButton(const AValue: LongBool); safecall;

    { IAlertOwnered }
    function GetOwner: IInterface; safecall;
  public
    constructor Create(AWindowOptionsCaptionButtons: TdxAlertWindowOptionsCaptionButtons; AOwner: IInterface);
  end;

  TAlertWindowOptionsNavigationPanel = class(TInterfacedObject, IAlertWindowOptionsNavigationPanel, IAlertOwnered)
  strict private
    FOptionsNavigationPanel: TdxAlertWindowOptionsNavigationPanel;
    FOwner: IInterface;
    { IAlertWindowOptionsNavigationPanel }
    function GetDisplayMask: WideString; safecall;
    procedure SetDisplayMask(const AValue: WideString); safecall;

    function GetFont: IFont; safecall;

    function GetVisibility: integer; safecall;
    procedure SetVisibility(const AValue: integer); safecall;

    function GetWidth: integer; safecall;
    procedure SetWidth(const AValue: integer); safecall;

    { IAlertOwnered }
    function GetOwner: IInterface; safecall;
  public
    constructor Create(AOptionsNavigationPanel: TdxAlertWindowOptionsNavigationPanel; AOwner: IInterface);
  end;

const
  WM_USER_REACTIVATE = WM_USER + 4;

type
  TdxAlertWindowX = class(TdxAlertWindow, IAlertWindow)
  strict private
    FCategory: WideString;
    FButtons: IAlertButtons;
    FMessageList: IAlertMessageList;

    FOptionsBehavior: IAlertWindowOptionsBehavior;
    FOptionsCaptionButtons: IAlertWindowOptionsCaptionButtons;
    FOptionsMessage: IAlertWindowOptionsMessage;
    FOptionsNavigationPanel: IAlertWindowOptionsNavigationPanel;
    FOptionsSize: IAlertWindowOptionsSize;

    FCloseCallbackList: TList<IAlertCloseCallback>;
    FButtonClickCallbackList: TList<IAlertButtonClickCallback>;
    FCaptionButtonClickCallbackList: TList<IAlertCaptionButtonClickCallback>;

    procedure WMUserReactivate(var Message: TMessage); message WM_USER_REACTIVATE;

    procedure WMCancelMode(var Message: TWMCancelMode); message WM_CANCELMODE;

    { IAlertWindow }
    procedure CloseAlert; safecall;
    function GetCategory: WideString; safecall;

    function GetActiveMessageIndex: integer; safecall;
    procedure SetActiveMessageIndex(const AIndex: integer); safecall;

    function GetIsPinned: LongBool; safecall;
    procedure SetIsPinned(const AValue: LongBool); safecall;

    function GetButtons: IAlertButtons; safecall;
    function GetMessages: IAlertMessageList; safecall;

    function GetOptionsBehavior: IAlertWindowOptionsBehavior; safecall;
    function GetOptionsCaptionButtons: IAlertWindowOptionsCaptionButtons; safecall;
    function GetOptionsMessage: IAlertWindowOptionsMessage; safecall;
    function GetOptionsNavigationPanel: IAlertWindowOptionsNavigationPanel; safecall;
    function GetOptionsSize: IAlertWindowOptionsSize; safecall;

    procedure BeginUpdate; safecall;
    procedure EndUpdate; safecall;

    procedure ResetDisplayTimer; safecall;

    procedure RegisterCloseCallback(Callback: IAlertCloseCallback); safecall;
    procedure UnregisterCloseCallback(Callback: IAlertCloseCallback); safecall;

    procedure RegisterButtonClickCallback(Callback: IAlertButtonClickCallback); safecall;
    procedure UnregisterButtonClickCallback(Callback: IAlertButtonClickCallback); safecall;

    procedure RegisterCaptionButtonClickCallback(Callback: IAlertCaptionButtonClickCallback); safecall;
    procedure UnregisterCaptionButtonClickCallback(Callback: IAlertCaptionButtonClickCallback); safecall;
  strict private
    FDenyClose: integer;
    FOldDisplayTime: Cardinal;
    FOldCaptionButtons: TdxAlertWindowCaptionButtons;
    procedure CMRelease(var Message: TMessage); message CM_RELEASE;
    procedure DenyClose;
    procedure AllowClose;
  private
    procedure SetCategory(const AValue: WideString);
  strict protected
    procedure DoHide; override;
    procedure DoMeasureMessageText(var AWidth, AHeight: integer); override;
    function DoCaptionButtonClick(AButton: TdxAlertWindowCaptionButton): Boolean; override;
    procedure DoButtonClick(AButtonIndex: integer); override;
    procedure MessagesChanged(Sender: TObject); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure Changed; override;
  end;

  TdxAlertWindowManagerX = class(TdxAlertWindowManager)
  protected
    function GetAlertWindowClass: TdxAlertWindowClass; override;
  end;

  { TdxAlertWindowManagerX }

function TdxAlertWindowManagerX.GetAlertWindowClass: TdxAlertWindowClass;
begin
  Result := TdxAlertWindowX;
end;

{ TdxAlertWindowX }

constructor TdxAlertWindowX.Create(AOwner: TComponent);
begin
  inherited;
  FCloseCallbackList := TList<IAlertCloseCallback>.Create;
  FButtonClickCallbackList := TList<IAlertButtonClickCallback>.Create;
  FCaptionButtonClickCallbackList := TList<IAlertCaptionButtonClickCallback>.Create;
end;

procedure TdxAlertWindowX.DenyClose;
var
  i: integer;
begin
  inc(FDenyClose);
  if FDenyClose <> 1 then
    exit;
  FOldDisplayTime := OptionsBehavior.DisplayTime;
  FOldCaptionButtons := OptionsCaptionButtons.CaptionButtons;
  OptionsBehavior.DisplayTime := MaxInt;
  OptionsCaptionButtons.CaptionButtons := FOldCaptionButtons - [awcbClose];

  for i := 0 to OptionsButtons.Buttons.Count - 1 do
    OptionsButtons.Buttons[i].Enabled := False;
end;

destructor TdxAlertWindowX.Destroy;
begin
  FreeAndNil(FCaptionButtonClickCallbackList);
  FreeAndNil(FButtonClickCallbackList);
  FreeAndNil(FCloseCallbackList);
  inherited;
end;

procedure TdxAlertWindowX.DoButtonClick(AButtonIndex: integer);
var
  i: integer;
begin
  DenyClose;
  try
    OptionsBehavior.DisplayTime := MaxInt;
    for i := FButtonClickCallbackList.Count - 1 downto 0 do
      FButtonClickCallbackList[i].ButtonClick(Self, AButtonIndex);
    inherited;
  finally
    AllowClose;
  end;
end;

function TdxAlertWindowX.DoCaptionButtonClick(AButton: TdxAlertWindowCaptionButton): Boolean;
var
  i: integer;
  AHandled: LongBool;
begin
  DenyClose;
  try
    AHandled := False;
    if Assigned(FCaptionButtonClickCallbackList) then
      for i := FCaptionButtonClickCallbackList.Count - 1 downto 0 do
      begin
        FCaptionButtonClickCallbackList[i].CaptionButtonClick(Self, integer(AButton), AHandled);
        if AHandled then
          Break;
      end;
    Result := AHandled;
  finally
    AllowClose;
  end;
end;

procedure TdxAlertWindowX.DoHide;
var
  i: integer;
begin
  if Assigned(FCloseCallbackList) then
    for i := FCloseCallbackList.Count - 1 downto 0 do
      FCloseCallbackList[i].AlertClosed(Self);
  inherited;
end;

procedure TdxAlertWindowX.DoMeasureMessageText(var AWidth, AHeight: integer);
var
  sz: TSize;
  r: TRect;
  s: string;
begin
  inherited;
  r := TRect.Empty;
  if (CurrentMessageIndex > -1) and (CurrentMessageIndex < MessageList.Count) then
    s := MessageList[CurrentMessageIndex].Text
  else
    s := '';
  sz := Canvas.TextExtent(s);
  r.Width := sz.cx;
  r.Height := sz.cy;
  DrawText(Canvas.Handle, s, -1, r, DT_CALCRECT);

  AWidth := r.Width;
  AHeight := r.Height;
end;

procedure TdxAlertWindowX.EndUpdate;
begin
  inherited EndUpdate;
end;

function TdxAlertWindowX.GetActiveMessageIndex: integer;
begin
  Result := CurrentMessageIndex;
end;

function TdxAlertWindowX.GetButtons: IAlertButtons;
begin
  if not Assigned(FButtons) then
    FButtons := TAlertButtons.Create(OptionsButtons, Self);
  Result := FButtons;
end;

procedure TdxAlertWindowX.AllowClose;
var
  i: integer;
begin
  Dec(FDenyClose);
  if FDenyClose <> 0 then
    exit;
  OptionsBehavior.DisplayTime := FOldDisplayTime;
  OptionsCaptionButtons.CaptionButtons := FOldCaptionButtons;

  for i := 0 to OptionsButtons.Buttons.Count - 1 do
    OptionsButtons.Buttons[i].Enabled := True;
end;

procedure TdxAlertWindowX.BeginUpdate;
begin
  inherited BeginUpdate;
end;

procedure TdxAlertWindowX.Changed;
begin
  inherited;

end;

procedure TdxAlertWindowX.CloseAlert;
begin
  Close;
end;

procedure TdxAlertWindowX.CMRelease(var Message: TMessage);
begin
  if FDenyClose = 0 then
    inherited;
end;

function TdxAlertWindowX.GetCategory: WideString;
begin
  Result := FCategory;
end;

function TdxAlertWindowX.GetIsPinned: LongBool;
begin
  Result := Pinned;
end;

function TdxAlertWindowX.GetMessages: IAlertMessageList;
begin
  if not Assigned(FMessageList) then
    FMessageList := TAlertMessageList.Create(MessageList, Self);
  Result := FMessageList;
end;

function TdxAlertWindowX.GetOptionsBehavior: IAlertWindowOptionsBehavior;
begin
  if not Assigned(FOptionsBehavior) then
    FOptionsBehavior := TAlertWindowOptionsBehavior.Create(OptionsBehavior, Self);
  Result := FOptionsBehavior;
end;

function TdxAlertWindowX.GetOptionsCaptionButtons: IAlertWindowOptionsCaptionButtons;
begin
  if not Assigned(FOptionsCaptionButtons) then
    FOptionsCaptionButtons := TAlertWindowOptionsCaptionButtons.Create(OptionsCaptionButtons, Self);
  Result := FOptionsCaptionButtons;
end;

function TdxAlertWindowX.GetOptionsMessage: IAlertWindowOptionsMessage;
begin
  if not Assigned(FOptionsMessage) then
    FOptionsMessage := TAlertWindowOptionsMessage.Create(OptionsMessage, Self);
  Result := FOptionsMessage;
end;

function TdxAlertWindowX.GetOptionsNavigationPanel: IAlertWindowOptionsNavigationPanel;
begin
  if not Assigned(FOptionsNavigationPanel) then
    FOptionsNavigationPanel := TAlertWindowOptionsNavigationPanel.Create(OptionsNavigationPanel, Self);
  Result := FOptionsNavigationPanel;
end;

function TdxAlertWindowX.GetOptionsSize: IAlertWindowOptionsSize;
begin
  if not Assigned(FOptionsSize) then
    FOptionsSize := TAlertWindowOptionsSize.Create(OptionsSize, Self);
  Result := FOptionsSize;
end;

procedure TdxAlertWindowX.MessagesChanged(Sender: TObject);
begin
  inherited;
  if Assigned(MessageList) and (MessageList.Count = 0) then
    Close;
end;

procedure TdxAlertWindowX.RegisterButtonClickCallback(Callback: IAlertButtonClickCallback);
begin
  if not FButtonClickCallbackList.Contains(Callback) then
    FButtonClickCallbackList.Add(Callback);
end;

procedure TdxAlertWindowX.RegisterCaptionButtonClickCallback(Callback: IAlertCaptionButtonClickCallback);
begin
  if not FCaptionButtonClickCallbackList.Contains(Callback) then
    FCaptionButtonClickCallbackList.Add(Callback);
end;

procedure TdxAlertWindowX.RegisterCloseCallback(Callback: IAlertCloseCallback);
begin
  if not FCloseCallbackList.Contains(Callback) then
    FCloseCallbackList.Add(Callback);
end;

procedure TdxAlertWindowX.ResetDisplayTimer;
begin
  RestartDisplayTimer;
end;

procedure TdxAlertWindowX.SetActiveMessageIndex(const AIndex: integer);
begin
  CurrentMessageIndex := AIndex;
end;

procedure TdxAlertWindowX.SetCategory(const AValue: WideString);
begin
  FCategory := AValue;
end;

procedure TdxAlertWindowX.SetIsPinned(const AValue: LongBool);
begin
  Pinned := AValue;
end;

procedure TdxAlertWindowX.UnregisterButtonClickCallback(Callback: IAlertButtonClickCallback);
begin
  FButtonClickCallbackList.Remove(Callback);
end;

procedure TdxAlertWindowX.UnregisterCaptionButtonClickCallback(Callback: IAlertCaptionButtonClickCallback);
begin
  FCaptionButtonClickCallbackList.Remove(Callback);
end;

procedure TdxAlertWindowX.UnregisterCloseCallback(Callback: IAlertCloseCallback);
begin
  FCloseCallbackList.Remove(Callback);
end;

procedure TdxAlertWindowX.WMCancelMode(var Message: TWMCancelMode);
begin
  message.Result := -1;
  PostMessage(Handle, WM_USER_REACTIVATE, 0, 0);
end;

procedure TdxAlertWindowX.WMUserReactivate(var Message: TMessage);
begin
  EnableWindow(Handle, True);
end;

{ TAlertMessage }

constructor TAlertMessage.Create(msg: TdxAlertWindowMessage; AOwner: IInterface);
begin
  FdxAlertWindowMessage := msg;
  FOwner := AOwner;
end;

function TAlertMessage.GetCaption: WideString;
begin
  Result := FdxAlertWindowMessage.Caption;
end;

function TAlertMessage.GetImageIndex: integer;
begin
  Result := FdxAlertWindowMessage.ImageIndex;
end;

function TAlertMessage.GetOwner: IInterface;
begin
  Result := FOwner;
end;

function TAlertMessage.GetTag: integer;
begin
  Result := FdxAlertWindowMessage.Tag;
end;

function TAlertMessage.GetTagPointer: Pointer;
begin
  Result := FdxAlertWindowMessage.TagPointer;
end;

function TAlertMessage.GetText: WideString;
begin
  Result := FdxAlertWindowMessage.Text;
end;

procedure TAlertMessage.SetCaption(const AValue: WideString);
begin
  FdxAlertWindowMessage.Caption := AValue;
end;

procedure TAlertMessage.SetImageIndex(const AValue: integer);
begin
  FdxAlertWindowMessage.ImageIndex := AValue;
end;

procedure TAlertMessage.SetTag(const AValue: integer);
begin
  FdxAlertWindowMessage.Tag := AValue;
end;

procedure TAlertMessage.SetTagPointer(const AValue: Pointer);
begin
  FdxAlertWindowMessage.TagPointer := AValue;
end;

procedure TAlertMessage.SetText(const AValue: WideString);
begin
  FdxAlertWindowMessage.Text := AValue;
end;

{ TAlertMessageList }

function TAlertMessageList.Add: IAlertMessage;
begin
  Result := TAlertMessage.Create(FMSGList.Add, Self);
end;

function TAlertMessageList.AddAndFill(const Caption, Text: WideString; ImageIndex: integer): IAlertMessage;
begin
  Result := TAlertMessage.Create(FMSGList.Add(Caption, Text, ImageIndex), Self);
end;

function TAlertMessageList.Count: integer;
begin
  Result := FMSGList.Count;
end;

constructor TAlertMessageList.Create(MsgList: TdxAlertWindowMessageList; AOwner: IInterface);
begin
  inherited Create;
  FMSGList := MsgList;
  FOwner := AOwner;
end;

procedure TAlertMessageList.Delete(index: integer);
begin
  FMSGList.FreeAndDelete(index);
end;

function TAlertMessageList.GetOwner: IInterface;
begin
  Result := FOwner;
end;

function TAlertMessageList.Item(index: integer): IAlertMessage;
begin
  Result := TAlertMessage.Create(FMSGList[index], Self);
end;

{ TAlertWindowManager }

function TAlertWindowManager.AddButtonImage(AImageStream: IStream): integer;
begin
  Result := intAddImage(AImageStream, FButtonImageList);
end;

function TAlertWindowManager.AddMessage(const Category, Caption, Text: WideString; ImgIndex: integer): IAlertMessage;
var
  Wnd: IAlertWindow;
begin
  Wnd := FindWindow(Category);
  if not Assigned(Wnd) then
    Result := InternalAdd(Category, Caption, Text, ImgIndex)
  else
  begin
    Result := Wnd.GetMessages.AddAndFill(Caption, Text, ImgIndex);
    Wnd.ResetDisplayTimer;
  end;
end;

function TAlertWindowManager.AddMessageImage(AImageStream: IStream): integer;
begin
  Result := intAddImage(AImageStream, FMessageImageList);
end;

constructor TAlertWindowManager.Create;
var
  psd: IProgramSpecificData;
begin
  inherited Create;
  FButtonImageList := TcxImageList.Create(nil);
  FButtonImageList.SetSize(24, 24);

  FMessageImageList := TcxImageList.Create(nil);
  FMessageImageList.SetSize(32, 32);

  FAlertManager := TdxAlertWindowManagerX.Create(nil);
  FAlertManager.OptionsButtons.Images := FButtonImageList;
  FAlertManager.OptionsMessage.Images := FMessageImageList;

  FAlertManager.OptionsCaptionButtons.CaptionButtons := [awcbPin, awcbClose];
  FAlertManager.OptionsBehavior.CloseOnRightClick := False;

  FAlertManager.OptionsAnimate.CollapseEmptySlots := True;
  FAlertManager.OptionsAnimate.HidingAnimationTime := 300;
  FAlertManager.OptionsAnimate.HotTrackAlphaBlendValue := 255;
  FAlertManager.OptionsAnimate.HotTrackFadeInTime := 10;
  FAlertManager.OptionsAnimate.HotTrackFadeOutTime := 300;

  FAlertManager.LookAndFeel.NativeStyle := False;
  if Supports(GlobalServices, IProgramSpecificData, psd) then
    FAlertManager.LookAndFeel.SkinName := psd.VisualSettings.MainSkinName;

  FAlertManager.OptionsSize.AutoWidth := True;
  FAlertManager.OptionsSize.AutoHeight := True;
  FAlertManager.OptionsSize.MaxWidth := 400;
  FAlertManager.OptionsSize.MinWidth := 200;
  FAlertManager.OptionsSize.Height := 100;
  FAlertManager.OptionsSize.Width := 300;
  FAlertManager.OptionsSize.AutoSizeAdjustment := True;
end;

destructor TAlertWindowManager.Destroy;
begin
  FreeAndNil(FAlertManager);
  FreeAndNil(FButtonImageList);
  FreeAndNil(FMessageImageList);
  inherited;
end;

function TAlertWindowManager.FindWindow(const Category: WideString): IAlertWindow;
var
  i: integer;
  AlertWindow: IAlertWindow;
begin
  Result := nil;

  for i := 0 to FAlertManager.Count - 1 do
    if Supports(FAlertManager.Items[i], IAlertWindow, AlertWindow) then
      if SameText(AlertWindow.Category, Category) then
      begin
        Result := AlertWindow;
        Break;
      end;
end;

function TAlertWindowManager.GetWindowMaxCount: integer;
begin
  Result := FAlertManager.WindowMaxCount;
end;

function TAlertWindowManager.GetWindowsPosition: integer;
begin
  Result := integer(FAlertManager.WindowPosition);
end;

function TAlertWindowManager.intAddImage(AImageStream: IStream; AImageList: TcxImageList): integer;
var
  dxImage: TdxSmartImage;
  Stream: TStream;
begin
  // ToDo: implement Addimage method
  Stream := TMemoryStream.Create;
  try
    IStreamToStream(AImageStream, Stream);
    Stream.Seek(0, soBeginning);
    dxImage := TdxSmartImage.CreateFromStream(Stream);
    try
      if (dxImage.Width <> AImageList.Width) or (dxImage.Height <> AImageList.Height) then
        dxImage.Resize(AImageList.Width, AImageList.Height);

      Result := AImageList.Add(dxImage);
    finally
      dxImage.Free;
    end;
  finally
    Stream.Free;
  end;
end;

function TAlertWindowManager.InternalAdd(const Category, Caption, Text: WideString; ImgIndex: integer): IAlertMessage;
var
  fm: TdxAlertWindowX;
  ifm: IAlertWindow;
begin
  fm := FAlertManager.Show(Caption, Text, ImgIndex) as TdxAlertWindowX;
  fm.SetCategory(Category);
  ifm := fm;
  Result := ifm.Messages.Item(ifm.Messages.Count - 1);
end;

procedure TAlertWindowManager.SetWindowMaxCount(const AValue: integer);
begin
  FAlertManager.WindowMaxCount := AValue;
end;

procedure TAlertWindowManager.SetWindowsPosition(const AValue: integer);
begin
  FAlertManager.WindowPosition := TdxAlertWindowPosition(AValue);
end;

{ TAlertButton }

constructor TAlertButton.Create(AButton: TdxAlertWindowButton; AOwner: IInterface);
begin
  inherited Create;
  FButton := AButton;
  FOwner := AOwner;
end;

function TAlertButton.GetHint: WideString;
begin
  Result := FButton.Hint;
end;

function TAlertButton.GetImageIndex: integer;
begin
  Result := FButton.ImageIndex;
end;

function TAlertButton.GetLeftMargin: integer;
begin
  Result := FButton.LeftMargin;
end;

function TAlertButton.GetOwner: IInterface;
begin
  Result := FOwner;
end;

function TAlertButton.GetRightMargin: integer;
begin
  Result := FButton.RightMargin;
end;

function TAlertButton.GetVisible: LongBool;
begin
  Result := FButton.Visible;
end;

procedure TAlertButton.SetHint(const AValue: WideString);
begin
  FButton.Hint := AValue;
end;

procedure TAlertButton.SetImageIndex(const AValue: integer);
begin
  FButton.ImageIndex := AValue;
end;

procedure TAlertButton.SetLeftMargin(const AValue: integer);
begin
  FButton.LeftMargin := AValue;
end;

procedure TAlertButton.SetRightMargin(const AValue: integer);
begin
  FButton.RightMargin := AValue;
end;

procedure TAlertButton.SetVisible(const AValue: LongBool);
begin
  FButton.Visible := AValue;
end;

{ TAlertButtons }

function TAlertButtons.Add: IAlertButton;
begin
  Result := TAlertButton.Create(FOptionsButtons.Buttons.Add, Self);
end;

procedure TAlertButtons.Clear;
begin
  FOptionsButtons.Buttons.Clear;
end;

constructor TAlertButtons.Create(AOptionsButtons: TdxAlertWindowOptionsButtons; AOwner: IInterface);
begin
  inherited Create;
  FOptionsButtons := AOptionsButtons;
  FOwner := AOwner;
end;

procedure TAlertButtons.Delete(AIndex: integer);
begin
  FOptionsButtons.Buttons.Delete(AIndex);
end;

function TAlertButtons.GetButton(AIndex: integer): IAlertButton;
begin
  Result := TAlertButton.Create(FOptionsButtons.Buttons[AIndex], Self);
end;

function TAlertButtons.GetCount: integer;
begin
  Result := FOptionsButtons.Buttons.Count;
end;

function TAlertButtons.GetHeight: integer;
begin
  Result := FOptionsButtons.Height;
end;

function TAlertButtons.GetOwner: IInterface;
begin
  Result := FOwner;
end;

function TAlertButtons.GetWidth: integer;
begin
  Result := FOptionsButtons.Width;
end;

{ TAlertWindowOptionsText }

constructor TAlertWindowOptionsText.Create(AWindowOptionsText: TdxAlertWindowOptionsText; AOwner: IInterface);
begin
  inherited Create;
  FWindowOptionsText := AWindowOptionsText;
  FOwner := AOwner;
end;

function TAlertWindowOptionsText.GetAlignHorz: integer;
begin
  Result := integer(FWindowOptionsText.AlignHorz);
end;

function TAlertWindowOptionsText.GetAlignVert: integer;
begin
  Result := integer(FWindowOptionsText.AlignVert);
end;

function TAlertWindowOptionsText.GetFont: IFont;
var
  FontDisp: IFontDisp;
begin
  GetOleFont(FWindowOptionsText.Font, FontDisp);
  if not Supports(FontDisp, IFont, Result) then
    Result := nil;
end;

function TAlertWindowOptionsText.GetOwner: IInterface;
begin
  Result := FOwner;
end;

procedure TAlertWindowOptionsText.SetAlignHorz(const AValue: integer);
begin
  FWindowOptionsText.AlignHorz := TAlignment(AValue);
end;

procedure TAlertWindowOptionsText.SetAlignVert(const AValue: integer);
begin
  FWindowOptionsText.AlignVert := TcxAlignmentVert(AValue);
end;

{ TAlertWindowOptionsMessage }

constructor TAlertWindowOptionsMessage.Create(AWindowOptionsMessage: TdxAlertWindowOptionsMessage; AOwner: IInterface);
begin
  inherited Create;
  FWindowOptionsMessage := AWindowOptionsMessage;
  FOwner := AOwner;
end;

function TAlertWindowOptionsMessage.GetOptionsCaption: IAlertWindowOptionsText;
begin
  Result := TAlertWindowOptionsText.Create(FWindowOptionsMessage.Caption, Self);
end;

function TAlertWindowOptionsMessage.GetOptionsText: IAlertWindowOptionsText;
begin
  Result := TAlertWindowOptionsText.Create(FWindowOptionsMessage.Text, Self);
end;

function TAlertWindowOptionsMessage.GetOwner: IInterface;
begin
  Result := FOwner;
end;

{ TAlertWindowOptionsSize }

constructor TAlertWindowOptionsSize.Create(AWindowOptionsSize: TdxAlertWindowOptionsSize; AOwner: IInterface);
begin
  inherited Create;
  FWindowOptionsSize := AWindowOptionsSize;
  FOwner := AOwner;
end;

function TAlertWindowOptionsSize.GetAutoHeight: LongBool;
begin
  Result := FWindowOptionsSize.AutoHeight;
end;

function TAlertWindowOptionsSize.GetAutoSizeAdjustment: LongBool;
begin
  Result := FWindowOptionsSize.AutoSizeAdjustment;
end;

function TAlertWindowOptionsSize.GetAutoWidth: LongBool;
begin
  Result := FWindowOptionsSize.AutoWidth;
end;

function TAlertWindowOptionsSize.GetHeight: integer;
begin
  Result := FWindowOptionsSize.Height;
end;

function TAlertWindowOptionsSize.GetMaxHeight: integer;
begin
  Result := FWindowOptionsSize.MaxHeight;
end;

function TAlertWindowOptionsSize.GetMaxWidth: integer;
begin
  Result := FWindowOptionsSize.MaxWidth;
end;

function TAlertWindowOptionsSize.GetMinHeight: integer;
begin
  Result := FWindowOptionsSize.MinHeight;
end;

function TAlertWindowOptionsSize.GetMinWidth: integer;
begin
  Result := FWindowOptionsSize.MinWidth;
end;

function TAlertWindowOptionsSize.GetOwner: IInterface;
begin
  Result := FOwner;
end;

function TAlertWindowOptionsSize.GetWidth: integer;
begin
  Result := FWindowOptionsSize.Width;
end;

procedure TAlertWindowOptionsSize.SetAutoHeight(const AValue: LongBool);
begin
  FWindowOptionsSize.AutoHeight := AValue;
end;

procedure TAlertWindowOptionsSize.SetAutoSizeAdjustment(const AValue: LongBool);
begin
  FWindowOptionsSize.AutoSizeAdjustment := AValue;
end;

procedure TAlertWindowOptionsSize.SetAutoWidth(const AValue: LongBool);
begin
  FWindowOptionsSize.AutoWidth := AValue;
end;

procedure TAlertWindowOptionsSize.SetHeight(const AValue: integer);
begin
  FWindowOptionsSize.Height := AValue;
end;

procedure TAlertWindowOptionsSize.SetMaxHeight(const AValue: integer);
begin
  FWindowOptionsSize.MaxHeight := AValue;
end;

procedure TAlertWindowOptionsSize.SetMaxWidth(const AValue: integer);
begin
  FWindowOptionsSize.MaxWidth := AValue;
end;

procedure TAlertWindowOptionsSize.SetMinHeight(const AValue: integer);
begin
  FWindowOptionsSize.MinHeight := AValue;
end;

procedure TAlertWindowOptionsSize.SetMinWidth(const AValue: integer);
begin
  FWindowOptionsSize.MinWidth := AValue;
end;

procedure TAlertWindowOptionsSize.SetWidht(const AValue: integer);
begin
  FWindowOptionsSize.Width := AValue;
end;

{ TAlertWindowOptionsBehavior }

constructor TAlertWindowOptionsBehavior.Create(AWindowOptionsBehavior: TdxAlertWindowOptionsBehavior;
  AOwner: IInterface);
begin
  inherited Create;
  FWindowOptionsBehavior := AWindowOptionsBehavior;
  FOwner := AOwner;
end;

function TAlertWindowOptionsBehavior.GetCloseOnRightClick: LongBool;
begin
  Result := FWindowOptionsBehavior.CloseOnRightClick;
end;

function TAlertWindowOptionsBehavior.GetDisplayTime: integer;
begin
  Result := FWindowOptionsBehavior.DisplayTime;
end;

function TAlertWindowOptionsBehavior.GetOwner: IInterface;
begin
  Result := FOwner;
end;

function TAlertWindowOptionsBehavior.GetScreenSnap: LongBool;
begin
  Result := FWindowOptionsBehavior.ScreenSnap;
end;

function TAlertWindowOptionsBehavior.GetScreenSnapBuffer: integer;
begin
  Result := FWindowOptionsBehavior.ScreenSnapBuffer;
end;

procedure TAlertWindowOptionsBehavior.SetCloseOnRightClick(const AValue: LongBool);
begin
  FWindowOptionsBehavior.CloseOnRightClick := AValue;
end;

procedure TAlertWindowOptionsBehavior.SetDisplayTime(const AValue: integer);
begin
  FWindowOptionsBehavior.DisplayTime := AValue;
end;

procedure TAlertWindowOptionsBehavior.SetScreenSnap(const AValue: LongBool);
begin
  FWindowOptionsBehavior.ScreenSnap := AValue;
end;

procedure TAlertWindowOptionsBehavior.SetScreenSnapBuffer(const AValue: integer);
begin
  FWindowOptionsBehavior.ScreenSnapBuffer := AValue;
end;

{ TAlertWindowOptionsCaptionButtons }

constructor TAlertWindowOptionsCaptionButtons.Create(AWindowOptionsCaptionButtons: TdxAlertWindowOptionsCaptionButtons;
  AOwner: IInterface);
begin
  inherited Create;
  FOptionsCaptionButtons := AWindowOptionsCaptionButtons;
  FOwner := AOwner;
end;

function TAlertWindowOptionsCaptionButtons.GetCloseButton: LongBool;
begin
  Result := awcbClose in FOptionsCaptionButtons.CaptionButtons;
end;

function TAlertWindowOptionsCaptionButtons.GetOwner: IInterface;
begin
  Result := FOwner;
end;

function TAlertWindowOptionsCaptionButtons.GetPinButton: LongBool;
begin
  Result := awcbPin in FOptionsCaptionButtons.CaptionButtons;
end;

procedure TAlertWindowOptionsCaptionButtons.SetCloseButton(const AValue: LongBool);
begin
  if AValue then
    FOptionsCaptionButtons.CaptionButtons := FOptionsCaptionButtons.CaptionButtons + [awcbClose]
  else
    FOptionsCaptionButtons.CaptionButtons := FOptionsCaptionButtons.CaptionButtons - [awcbClose];
end;

procedure TAlertWindowOptionsCaptionButtons.SetPinButton(const AValue: LongBool);
begin
  if AValue then
    FOptionsCaptionButtons.CaptionButtons := FOptionsCaptionButtons.CaptionButtons + [awcbPin]
  else
    FOptionsCaptionButtons.CaptionButtons := FOptionsCaptionButtons.CaptionButtons - [awcbPin];
end;

{ TAlertWindowOptionsNavigationPanel }

constructor TAlertWindowOptionsNavigationPanel.Create(AOptionsNavigationPanel: TdxAlertWindowOptionsNavigationPanel;
  AOwner: IInterface);
begin
  inherited Create;
  FOptionsNavigationPanel := AOptionsNavigationPanel;
  FOwner := AOwner;
end;

function TAlertWindowOptionsNavigationPanel.GetDisplayMask: WideString;
begin
  Result := FOptionsNavigationPanel.DisplayMask;
end;

function TAlertWindowOptionsNavigationPanel.GetFont: IFont;
var
  FontDisp: IFontDisp;
begin
  GetOleFont(FOptionsNavigationPanel.Font, FontDisp);
  if not Supports(FontDisp, IFont, Result) then
    Result := nil;
end;

function TAlertWindowOptionsNavigationPanel.GetOwner: IInterface;
begin
  Result := FOwner;
end;

function TAlertWindowOptionsNavigationPanel.GetVisibility: integer;
begin
  Result := integer(FOptionsNavigationPanel.Visibility);
end;

function TAlertWindowOptionsNavigationPanel.GetWidth: integer;
begin
  Result := FOptionsNavigationPanel.Width;
end;

procedure TAlertWindowOptionsNavigationPanel.SetDisplayMask(const AValue: WideString);
begin
  FOptionsNavigationPanel.DisplayMask := AValue;
end;

procedure TAlertWindowOptionsNavigationPanel.SetVisibility(const AValue: integer);
begin
  FOptionsNavigationPanel.Visibility := TdxAlertWindowNavigationPanelVisibility(AValue);
end;

procedure TAlertWindowOptionsNavigationPanel.SetWidth(const AValue: integer);
begin
  FOptionsNavigationPanel.Width := AValue;
end;

end.
