unit dxExtendedColorEdit;

interface

uses
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.ExtCtrls,
  dxColorEdit,
  cxButtons;

type
  TdxColorEdit = class(dxColorEdit.TdxColorEdit)
  strict private
    Fbtn: TcxButton;
    FPanel: TPanel;
    FOldGalleryParent: TWinControl;
    procedure OnChooseColorButtonClick(Sender: TObject);
  protected
    procedure DropDown; override;
    procedure PopupWindowClosed(Sender: TObject); override;
  public
    destructor Destroy; override;
  end;

implementation

uses
  dxColorDialog,
  dxCoreGraphics,
  Vcl.Imaging.pngimage;

{$R 'ColorButton.res' 'ColorButton.rc'}
{ TdxColorEdit }

type
  TDialog = TdxColorDialog;

destructor TdxColorEdit.Destroy;
begin
  FPanel.Free;
  inherited;
end;

procedure TdxColorEdit.DropDown;
var
  png: TPNGImage;
begin
  inherited;

  if not Assigned(FPanel) then
  begin
    FPanel := TPanel.Create(nil);
    FPanel.BevelOuter := bvNone;

    Self.FreeNotification(FPanel);

    Fbtn := TcxButton.Create(nil);
    Fbtn.Parent := FPanel;

    Fbtn.Align := alBottom;
    Fbtn.Caption := '������� ����...';
    Fbtn.SpeedButtonOptions.AllowAllUp := True;
    Fbtn.SpeedButtonOptions.CanBeFocused := False;
    Fbtn.SpeedButtonOptions.Flat := True;
    Fbtn.OnClick := OnChooseColorButtonClick;
    Fbtn.Colors.Normal := clWhite; // cxLookAndFeelPainters.procedure TcxCustomLookAndFeelPainter.DrawGalleryBackground
    Fbtn.Colors.Default := clWhite; // hardcoded background color to clWhite
    Fbtn.Colors.Hot := clWhite; // so we can`t use anything except hardcoded color value.
    Fbtn.LookAndFeel.NativeStyle := False;
    Fbtn.LookAndFeel.SkinName := '';

    png := TPNGImage.Create;
    try
      png.LoadFromResourceName(HInstance, 'CHOOSE_COLOR_BUTTON');
      Fbtn.OptionsImage.Glyph.Assign(png);
    finally
      png.Free;
    end;
  end;

  FOldGalleryParent := FColorGallery.Parent;

  FPanel.Parent := PopupWindow;
  FColorGallery.Parent := FPanel;
  FColorGallery.Align := alTop;

  FPanel.SetBounds(0, 0, FColorGallery.Width, FColorGallery.Height + Fbtn.Height);

  ActiveProperties.PopupControl := FPanel;
end;

procedure TdxColorEdit.OnChooseColorButtonClick(Sender: TObject);
var
  dlg: TDialog;
begin
  Fbtn.SpeedButtonOptions.Down := False;

  dlg := TDialog.Create(nil);
  try
    dlg.Color :=dxColorToAlphaColor(ColorValue);
    dlg.Options.ColorPicker.AllowEditAlpha:=False;
    if dlg.Execute then
    begin
      ColorValue := dxAlphaColorToColor(dlg.Color);
      if Assigned(Properties.OnChange) then
        Properties.OnChange(Self);
    end;
  finally
    dlg.Free;
  end;

  PopupWindow.ClosePopup;
end;

procedure TdxColorEdit.PopupWindowClosed(Sender: TObject);
begin
  FColorGallery.Parent := FOldGalleryParent;
  ActiveProperties.PopupControl := FColorGallery;

  FPanel.Parent := nil;
  inherited;
end;

end.
