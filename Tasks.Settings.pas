unit Tasks.Settings;

interface

uses
  System.Classes,
  System.Generics.Collections,
  Xml.XMLIntf,
  pbInput,
  pbOutput,
  Common.AbstractClasses;

type
  TAbstractSpecificSettings = class(TAbstractData)
  private
    FTryIntervalMs: integer;
    FMaxRetryCount: integer;
  protected
    procedure intLoad(Node: IXMLNode); //override;
    procedure intSave(Node: IXMLNode); //override;
  public
    property MaxRetryCount: integer read FMaxRetryCount write FMaxRetryCount;
    property TryIntervalMs: integer read FTryIntervalMs;
  end;

  TSpecificSettingsClass = class of TAbstractSpecificSettings;

  TSpecificSettingsList = class(TAbstractDataList<TAbstractSpecificSettings>)
  private
    procedure CreateUnexistingSettings;
  public
    function SettingsByType<T: TAbstractSpecificSettings>: T;
  end;

  TSpecificSettingsContainer = class(TAbstractData)
  strict private
    class var FInstance: TSpecificSettingsContainer;
  strict private
    FSettingsList: TSpecificSettingsList;
    FLogDepthDays: integer;
    FReloadDictionariesIntervalMin: integer;
  protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer)
      : Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;

    procedure BeforeLoad; override;
    procedure AfterLoad; override;

    procedure intLoad(Node: IXMLNode); //override;
    procedure intSave(Node: IXMLNode); //override;
  public
    class function Instance: TSpecificSettingsContainer;

    constructor Create(AOwner: TObject); override;
    destructor Destroy; override;
    class destructor Destroy;

    function SettingsByType<T: TAbstractSpecificSettings>: T;

    property LogDepthDays: integer read FLogDepthDays write FLogDepthDays;
    property ReloadDictionariesIntervalMin: integer read FReloadDictionariesIntervalMin
      write FReloadDictionariesIntervalMin;
  end;

implementation

uses
  System.SysUtils,
  uXMLFunctions;
{ TSpecificSettingsList }

procedure TSpecificSettingsList.CreateUnexistingSettings;
  function IsSettingsExists(AClass: TSpecificSettingsClass): Boolean;
  var
    i: integer;
  begin
    Result := False;
    for i := 0 to Count - 1 do
      if Items[i].InheritsFrom(AClass) then
      begin
        Result := True;
        Break;
      end;
  end;

var
  i: integer;
  ASettingsClass: TSpecificSettingsClass;
begin
  for i := 0 to TAbstractDataClassFactory.Current.Count - 1 do
    if TAbstractDataClassFactory.Current[i].InheritsFrom(TAbstractSpecificSettings) then
    begin
      ASettingsClass := TSpecificSettingsClass(TAbstractDataClassFactory.Current[i]);
      if not IsSettingsExists(ASettingsClass) then
        Add(ASettingsClass.Create(Self));
    end;
end;

function TSpecificSettingsList.SettingsByType<T>: T;
var
  i: integer;
begin
  Result := nil;
  for i := 0 to Count - 1 do
    if Items[i] is T then
    begin
      Result := T(Items[i]);
      Break;
    end;
end;

{ TSpecificSettingsContainer }

procedure TSpecificSettingsContainer.AfterLoad;
begin
  inherited;
  FSettingsList.CreateUnexistingSettings;
end;

procedure TSpecificSettingsContainer.BeforeLoad;
begin
  inherited;
  FSettingsList.Clear;
end;

constructor TSpecificSettingsContainer.Create(AOwner: TObject);
begin
  inherited;
  FSettingsList := TSpecificSettingsList.Create(Self);
  FSettingsList.CreateUnexistingSettings;
end;

class destructor TSpecificSettingsContainer.Destroy;
begin
  FInstance.Free;
  FInstance := nil;
end;

destructor TSpecificSettingsContainer.Destroy;
begin
  FreeAndNil(FSettingsList);
  inherited;
end;

class function TSpecificSettingsContainer.Instance: TSpecificSettingsContainer;
begin
  if not Assigned(FInstance) then
    FInstance := Self.Create(nil);
  Result := FInstance;
end;

procedure TSpecificSettingsContainer.intLoad(Node: IXMLNode);
begin
  inherited;
  FSettingsList.Load(Node);
  if Assigned(Node) then
    FLogDepthDays := StrToIntDef(GetNodeValueByName(Node.ChildNodes, 'LogDepthDays'), 20);
  if Assigned(Node) then
    FReloadDictionariesIntervalMin:=StrToIntDef(GetNodeValueByName(Node.ChildNodes, 'ReloadDictionariesIntervalMin'), 30);
end;

procedure TSpecificSettingsContainer.intSave(Node: IXMLNode);
begin
  inherited;
  FSettingsList.Save(Node);
  Node.AddChild('LogDepthDays').Text := IntToStr(FLogDepthDays);
  Node.AddChild('ReloadDictionariesIntervalMin').Text := IntToStr(FReloadDictionariesIntervalMin);
end;

function TSpecificSettingsContainer.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput;
  FieldNumber, WireType: integer): Boolean;
begin
  Result := inherited;
end;

procedure TSpecificSettingsContainer.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;

end;

function TSpecificSettingsContainer.SettingsByType<T>: T;
begin
  Result := FSettingsList.SettingsByType<T>;
end;

{ TAbstractSpecificSettings }

procedure TAbstractSpecificSettings.intLoad(Node: IXMLNode);
begin
  inherited;
  FTryIntervalMs := StrToIntDef(GetNodeValueByName(Node.ChildNodes, 'TryIntervalMs'), 1000);
  FMaxRetryCount := StrToIntDef(GetNodeValueByName(Node.ChildNodes, 'MaxRetryCount'), 3);
end;

procedure TAbstractSpecificSettings.intSave(Node: IXMLNode);
begin
  inherited;
  Node.AddChild('TryIntervalMs').Text := IntToStr(FTryIntervalMs);
  Node.AddChild('MaxRetryCount').Text := IntToStr(FMaxRetryCount);
end;

end.
