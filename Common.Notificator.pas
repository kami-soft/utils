unit Common.Notificator;

interface

uses
  System.SysUtils,
  System.Generics.Defaults,
  System.Generics.Collections,
  Common.Interfaces;

type
  TChangeNotificator = class(TInterfacedObject, IChangeNotificator)
  strict private
  type
    TImplementationKind = (ikUnknown, ikSelfImplemented, ikDelegatedImplemented);
  strict private
    FOwner: TObject;
    FNotifications: TList<TChangeNotificationEvent>;
    FLockCounter: integer;
    FImplementationKind: TImplementationKind;

    function GetImplementationKind: TImplementationKind;

    function EventComparator(const Left, Right: TChangeNotificationEvent): integer;
  protected
    procedure RegisterNotification(Notificator: TChangeNotificationEvent);
    procedure UnregisterNotification(Notificator: TChangeNotificationEvent);

    procedure DoNotify(Notification: TChangeNotificationStructure);
    procedure ReRaiseNotification(Notification: TChangeNotificationStructure);

    procedure Lock;
    procedure Unlock(const ADescription: string = '');

    procedure NotifyForSelfChanges(const ADescription: string = '');

    function GetOwner: TObject;
  public
    constructor Create(AOwner: TObject);
    destructor Destroy; override;
  end;

  TSubscriber = class(TInterfacedObject, ISubscriber)
  strict private
    FObservers: TList<IObserver>;
    FMREWS: TMultiReadExclusiveWriteSynchronizer;
  strict protected
    { ISubscriber }
    procedure Subscribe(Observer: IObserver); safecall;
    procedure Unsubscribe(Observer: IObserver); safecall;
    procedure Notify(Action: TChangeAction; Data: Pointer; Description: WideString); safecall;
  public
    constructor Create(AOwner: TObject);
    destructor Destroy; override;
  end;

  TObserverNotificationEvent = procedure(Action: TChangeAction; Data: Pointer; const Description: string) of object;

  TObserver = class(TInterfacedObject, IObserver)
  strict private
    FOnNotification: TObserverNotificationEvent;
    procedure Changed(Action: TChangeAction; Data: Pointer; Description: WideString); safecall;
  public
    property OnNotification: TObserverNotificationEvent read FOnNotification write FOnNotification;
  end;

implementation

{ TAbstractNotificator }

constructor TChangeNotificator.Create(AOwner: TObject);
begin
  inherited Create;
  FNotifications := TList<TChangeNotificationEvent>.Create
    (TComparer<TChangeNotificationEvent>.Construct(EventComparator));

  FOwner := AOwner;
end;

destructor TChangeNotificator.Destroy;
begin
  if FLockCounter <> 0 then
    raise EProgrammerNotFound.Create('Lock counterer must be zero');
  FreeAndNil(FNotifications);
  inherited;
end;

procedure TChangeNotificator.DoNotify(Notification: TChangeNotificationStructure);
var
  i: integer;
  Event: TChangeNotificationEvent;
begin
  if FLockCounter <> 0 then
    exit;
  for i := FNotifications.Count - 1 downto 0 do
  begin
    Event := FNotifications[i];
    if GetImplementationKind = ikDelegatedImplemented then
      Event(FOwner, Notification)
    else
      Event(Self, Notification);
  end;
end;

function TChangeNotificator.EventComparator(const Left, Right: TChangeNotificationEvent): integer;
begin
  if NativeUInt(TMethod(Left).Code) > NativeUInt(TMethod(Right).Code) then
    Result := 1
  else
    if NativeUInt(TMethod(Left).Code) < NativeUInt(TMethod(Right).Code) then
      Result := -1
    else
      if NativeUInt(TMethod(Left).Data) > NativeUInt(TMethod(Right).Data) then
        Result := 1
      else
        if NativeUInt(TMethod(Left).Data) < NativeUInt(TMethod(Right).Data) then
          Result := -1
        else
          Result := 0;
end;

function TChangeNotificator.GetOwner: TObject;
begin
  Result := FOwner;
end;

function TChangeNotificator.GetImplementationKind: TImplementationKind;
var
  intfOwner, intfSelf: IChangeNotificator;
begin
  if FImplementationKind <> ikUnknown then
    Result := FImplementationKind
  else
  begin
    Result := ikSelfImplemented;
    if Supports(GetOwner, IChangeNotificator, intfOwner) then
      if Supports(Self, IChangeNotificator, intfSelf) then
        if intfOwner = intfSelf then
          Result := ikDelegatedImplemented;

    FImplementationKind := Result;
  end;
end;

procedure TChangeNotificator.Lock;
begin
  Inc(FLockCounter);
end;

procedure TChangeNotificator.NotifyForSelfChanges(const ADescription: string = '');
begin
  ReRaiseNotification(TChangeNotificationStructure.Create(nil, caChange, ADescription));
end;

procedure TChangeNotificator.RegisterNotification(Notificator: TChangeNotificationEvent);
begin
  if not FNotifications.Contains(Notificator) then
  begin
    FNotifications.Insert(0, Notificator);
    GetImplementationKind;
  end;
end;

procedure TChangeNotificator.ReRaiseNotification(Notification: TChangeNotificationStructure);
begin
  if GetImplementationKind = ikDelegatedImplemented then
    Notification.NotifyObject := GetOwner
  else
    Notification.NotifyObject := Self;
  Notification.Action := caChange;

  DoNotify(Notification);
end;

procedure TChangeNotificator.Unlock(const ADescription: string = '');
begin
  if FLockCounter > 0 then
    Dec(FLockCounter)
  else
    raise ERangeError.Create('Lock / Unlock asymmetry');
  if FLockCounter = 0 then
    ReRaiseNotification(TChangeNotificationStructure.Create(nil, caChange, ADescription));
end;

procedure TChangeNotificator.UnregisterNotification(Notificator: TChangeNotificationEvent);
begin
  FNotifications.Remove(Notificator);
end;

{ TSubscriber }

constructor TSubscriber.Create(AOwner: TObject);
begin
  inherited Create;
  FMREWS := TMultiReadExclusiveWriteSynchronizer.Create;
  FObservers := TList<IObserver>.Create;
end;

destructor TSubscriber.Destroy;
begin
  Notify(caDelete, nil, 'ISubscriber');
  FMREWS.BeginWrite;
  try
    FreeAndNil(FObservers);
  finally
    FMREWS.EndWrite;
  end;
  FreeAndNil(FMREWS);
  inherited;
end;

procedure TSubscriber.Notify(Action: TChangeAction; Data: Pointer; Description: WideString);
var
  i: integer;
  o: IObserver;
begin
  FMREWS.BeginRead;
  try
    for i := FObservers.Count - 1 downto 0 do
    begin
      o := FObservers[i];
      o.Changed(Action, Data, Description);
    end;
  finally
    FMREWS.EndRead;
  end;
end;

procedure TSubscriber.Subscribe(Observer: IObserver);
begin
  FMREWS.BeginWrite;
  try
    if not FObservers.Contains(Observer) then
      FObservers.Add(Observer);
  finally
    FMREWS.EndWrite;
  end;
end;

procedure TSubscriber.Unsubscribe(Observer: IObserver);
begin
  FMREWS.BeginWrite;
  try
    FObservers.Remove(Observer);
  finally
    FMREWS.EndWrite;
  end;
end;

{ TObserver }

procedure TObserver.Changed(Action: TChangeAction; Data: Pointer; Description: WideString);
begin
  if Assigned(FOnNotification) then
    FOnNotification(Action, Data, Description);
end;

end.
