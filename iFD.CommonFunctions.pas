unit iFD.CommonFunctions;
{$IFDEF VER230}
{$DEFINE XE2}
{$DEFINE XE2Up}
{$ENDIF}
{$IFDEF VER280}
{$DEFINE XE7}
{$DEFINE XE7Up}
{$ENDIF}
{$IFDEF VER290}
{$DEFINE XE8}
{$DEFINE XE8Up}
{$ENDIF}
{$IFDEF VER300}
{$DEFINE DX}
{$DEFINE DXUp}
{$ENDIF}
{$IFDEF VER310}
{$DEFINE DX}
{$DEFINE DX1Up} //Berlin
{$ENDIF}
{$IFDEF VER320}
{$DEFINE DX}
{$DEFINE DX2Up} //Tokio
{$ENDIF}
{$IFDEF VER330}
{$DEFINE DX}
{$DEFINE DX2Up} //Tokio
{$ENDIF}

{$IFDEF DX2Up}
{$DEFINE DX1Up}
{$ENDIF}
{$IFDEF DX1Up}
{$DEFINE DXUp}
{$ENDIF}
{$IFDEF DXUp}
{$DEFINE XE8Up}
{$ENDIF}
{$IFDEF XE8Up}
{$DEFINE XE7Up}
{$ENDIF}
{$IFDEF XE7Up}
{$DEFINE XE2Up}
{$ENDIF}



interface

uses
  System.SysUtils,
  System.Classes;

const
  CONST_DENSITY: real = 0.79;
  CONST_LB_TO_LITRES: real = 4.546; // ���������� ������

  MeterInFeet = 0.3048;
  MeterInNM = 1852;
  MeterInKM = 1000;
  SecondInMinute = 60;
  MinuteInHour = 60;
  SecondInHour = SecondInMinute * MinuteInHour;

  PascalInMmHg = 133.322;
  PascalInBar = 100000;

  /// ������� ����������� �������� ������������� ��������� - ������� � ���������/�������� � �.�.
  /// TrTOT - ����� ����������� ������� � ��������� �����������
  /// TrLand - ����� ����������� ������� � ��������� ��������
function LightTimeCross(const ATOT, ATL, TrTOT, TrLand: TDateTime): TDateTime;
function TimeCrossDelta(const StartTime1, EndTime1, StartTime2, EndTime2: TDateTime): TDateTime;

function IsCoolTimeValid(const CoolTime: string): Boolean;
function CoolTimeToTime(const CoolTime: string): TDateTime;
function TimeToCoolTime(const dt: TDateTime): string;
function TimeToDateTime(const TransformedTime, ReferenceTime: TDateTime): TDateTime;

function GradMinToGrad(const AGrad: integer; AMin: Double): Double;
procedure GradToGradMin(const AGrad: Double; out Grad: integer; out Min: Double);

procedure Solaris(ADta: TDateTime; ALat, ALong: Double; out Dawn, Sunrise, Sunset, Dark: TDateTime; out Light, Sun: integer);

// ���� ���� � ������
function KGToLitres(const Kg: Double; const Density: Double): Double;
function KgToLB(const Kg: Double; const Density: Double): Double;
function LBToKg(const LB: Double; const Density: Double): Double;
function LitresToKG(const Litres: Double; const Density: Double): Double;
function LitresToLB(const Litres: Double): Double;
function LBToLitres(const LB: Double): Double;

// ����������
type
  TDistanceItem = (diMeters, diFeets, diMiles, diKM);

function ConvertDistance(const Value: Double; FromDist, ToDist: TDistanceItem): Double;

// ��������
type
  TTimeItem = (tiSecond, tiMinute, tiHour);

function ConvertSpeed(const Value: Double; FromDist: TDistanceItem; FromTime: TTimeItem; ToDist: TDistanceItem; ToTime: TTimeItem): Double;

// ��������
type
  TPressure = (pPa, pMmHg, pBar);
function ConvertPressure(const Value: Double; FromPressure, ToPressure: TPressure): Double;

var
  NormalizedFormatSettings: TFormatSettings;

procedure ForceQueue(proc: TThreadProcedure);

implementation

uses
  System.DateUtils,
  System.Math,
  System.Generics.Collections;

function XNormalizedFormatSettings: TFormatSettings;
begin
  Result := TFormatSettings.Create('');
  Result.DecimalSeparator := '.';
  Result.DateSeparator := '-';
  Result.TimeSeparator := ':';
  Result.ShortDateFormat := 'yyyy-mm-dd';
  Result.LongDateFormat := 'yyyy-mm-dd';
  Result.ShortTimeFormat := 'hh:nn:ss.zzz';
  Result.LongTimeFormat := 'hh:nn:ss.zzz';
end;

function LightTimeCross(const ATOT, ATL, TrTOT, TrLand: TDateTime): TDateTime;
var
  dTOT, dR: TDateTime;
begin
  Result := 0;
  if (ATOT = 0) or (ATL = 0) or (TrTOT = 0) or (TrLand = 0) then
    exit;

  if CompareDateTime(ATL, ATOT) < 0 then
    raise EConvertError.Create('ATL must be greater than ATOT');
  // ��������� �� ��������� ������� ������� � ��������
  // ����������� ����� ���� ������ � ��� ������, ���� �������� ������ �����/�� ������ �
  // �����������/�� ������� ���� � ������ ������.
  if ((CompareDateTime(ATOT, TrTOT) > 0) and (CompareDateTime(ATL, TrLand) < 0)) or //
    ((CompareDateTime(ATOT, TrTOT) < 0) and (CompareDateTime(ATL, TrLand) > 0)) then
    begin
      dTOT := ATL - ATOT;
      if CompareDateTime(TrTOT, TrLand) > 0 then
        begin
          dR := TrTOT - TrLand;

          Result := (dTOT * dR + dTOT * TrLand + dR * ATOT) / (dTOT + dR);
        end
      else
        begin
          dR := TrLand - TrTOT;

          Result := (dR * ATOT - dTOT * TrTOT) / (dR - dTOT)
        end;
    end;
end;

function TimeCrossDelta(const StartTime1, EndTime1, StartTime2, EndTime2: TDateTime): TDateTime;
var
  dtStart, dtEnd: TDateTime;
begin
  Result := 0;
  // ����������, ������������ �� ��� ������
  if (StartTime1 > EndTime1) or (StartTime2 > EndTime2) then
    exit;
  if (StartTime1 > EndTime2) or (StartTime2 > EndTime1) then
    exit;

  // �������� �����, ������� ��������������� ���������� �����������
  if StartTime1 > StartTime2 then // ������ ������� ������� ������ �������
    dtStart := StartTime1
  else
    dtStart := StartTime2;
  if EndTime2 > EndTime1 then // ����� ������� ������� ������ �������
    dtEnd := EndTime1
  else
    dtEnd := EndTime2;

  Result := dtEnd - dtStart;
end;

function LitresToLB(const Litres: Double): Double;
begin
  Result := Litres / CONST_LB_TO_LITRES
end;

function LBToLitres(const LB: Double): Double;
begin
  Result := LB * CONST_LB_TO_LITRES;
end;

function KGToLitres(const Kg: Double; const Density: Double): Double;
begin
  Result := Kg / Density;
end;

function KgToLB(const Kg: Double; const Density: Double): Double;
var
  Litres: Double;
begin
  Litres := KGToLitres(Kg, Density);
  Result := LitresToLB(Litres);
end;

function LBToKg(const LB: Double; const Density: Double): Double;
var
  Litres: Double;
begin
  Litres := LBToLitres(LB);
  Result := LitresToKG(Litres, Density);
end;

function LitresToKG(const Litres: Double; const Density: Double): Double;
begin
  Result := Litres * Density;
end;

function ConvertDistance(const Value: Double; FromDist, ToDist: TDistanceItem): Double;
begin
  // ����������� � �����
  case FromDist of
    diMeters:
      Result := Value;
    diFeets:
      Result := Value * MeterInFeet;
    diMiles:
      Result := Value * MeterInNM;
    diKM:
      Result := Value * MeterInKM;
    else
      Result := Value;
  end;

  // ����������� � �������� �������
  case ToDist of
    diMeters:
      ;
    diFeets:
      Result := Result / MeterInFeet;
    diMiles:
      Result := Result / MeterInNM;
    diKM:
      Result := Result / MeterInKM;
  end;
end;

function ConvertSpeed(const Value: Double; FromDist: TDistanceItem; FromTime: TTimeItem; ToDist: TDistanceItem; ToTime: TTimeItem): Double;
begin
  // ����������� � ����� � �������
  Result := ConvertDistance(Value, FromDist, diMeters);
  case FromTime of
    tiSecond:
      ;
    tiMinute:
      Result := Result / SecondInMinute;
    tiHour:
      Result := Result / SecondInHour;
  end;

  // ������ � ��� � Result ����� ����� � �������
  // ����������� � �������� �������
  Result := ConvertDistance(Result, diMeters, ToDist);

  case ToTime of
    tiSecond:
      ;
    tiMinute:
      Result := Result * SecondInMinute;
    tiHour:
      Result := Result * SecondInHour;
  end;
end;

function ConvertPressure(const Value: Double; FromPressure, ToPressure: TPressure): Double;
begin
  // ����������� � �������
  case FromPressure of
    pPa:
      Result := Value;
    pMmHg:
      Result := Value * PascalInMmHg;
    pBar:
      Result := Value * PascalInBar;
    else
      Result := Value;
  end;

  case ToPressure of
    pPa:
      ;
    pMmHg:
      Result := Result / PascalInMmHg;
    pBar:
      Result := Result / PascalInBar;
  end;
end;

function IsCoolTimeValid(const CoolTime: string): Boolean;
var
  tmpTime: TDateTime;
begin
  Result := False;
  if Length(Trim(CoolTime)) = 4 then
    begin
      tmpTime := CoolTimeToTime(CoolTime);
      if (tmpTime <> 0) or ((tmpTime = 0) and (Trim(CoolTime) = '0000')) then
        Result := True;
    end;
end;

function CoolTimeToTime(const CoolTime: string): TDateTime;
var
  s: string;
  sHour: string;
  sMin: string;

  Hour, Min: integer;
begin
  Result := 0;
  s := Trim(CoolTime);
  if Length(s) = 4 then
    begin
      {$IFDEF XE7Up} // ���� �� XE2
      sHour := s.Substring(0, 2);
      sMin := s.Substring(2, 2);
      {$ELSE}
      sHour := Copy(s, 1, 2);
      sMin := Copy(s, 3, 2);
      {$ENDIF}
      if TryStrToInt(sHour, Hour) and TryStrToInt(sMin, Min) then
        if (Hour >= 0) and (Hour < 24) and (Min >= 0) and (Min < 60) then
          begin
            Result := IncMinute(0, Min);
            Result := IncHour(Result, Hour);
          end;
    end;
end;

function TimeToCoolTime(const dt: TDateTime): string;
var
  Hour, Min: Word;
begin
  Hour := HoursBetween(0, dt);
  Min := MinutesBetween(IncHour(0, Hour), dt);
  Result := Format('%2.2d%2.2d', [Hour, Min]);
end;

function TimeToDateTime(const TransformedTime, ReferenceTime: TDateTime): TDateTime;
begin
  if MinutesBetween(TimeOf(TransformedTime), TimeOf(ReferenceTime)) > (12 * 60) then
    begin
      // ��������� �������� ����� 00:00, ���������� - � ����� �������
      if TimeOf(TransformedTime) > TimeOf(ReferenceTime) then
        // TransformedTime ������, ��� ReferenceTime
        Result := IncDay(TimeOf(TransformedTime) + DateOf(ReferenceTime), -1)
      else
        Result := IncDay(TimeOf(TransformedTime) + DateOf(ReferenceTime), 1);
    end
  else
    Result := TimeOf(TransformedTime) + DateOf(ReferenceTime);
end;

function GradMinToGrad(const AGrad: integer; AMin: Double): Double;
begin
  Result := AGrad + AMin / 60;
end;

procedure GradToGradMin(const AGrad: Double; out Grad: integer; out Min: Double);
begin
  Grad := Trunc(AGrad);
  Min := Frac(AGrad) * 60;
end;

procedure Solaris(ADta: TDateTime; ALat, ALong: Double; out Dawn, Sunrise, Sunset, Dark: TDateTime; out Light, Sun: integer);
var
  dd, mm, yy: Word;
  NTV, TV, NTZ, TZ, NTKT, TKT, NTNT, TNT: Double;

  NDEL, KVIS, NDAY, KCEL: integer;
  DAY, ANOM, E, DEL, DE, ANOMI, DSOL, SKL, X, Y, CT1, CT2, T1G, T2G, DG: Double;
  TVICH, TZICH, UV, TVSCH, TZSCH, TKTICH, TNTICH, TKTSCH, TNTSCH: Double;
  _ok: Boolean;
  i: integer;
begin
  ALat := ALat / 57.29578;
  ALong := ALong / 57.29578;

  DecodeDate(ADta, yy, mm, dd);

  NDEL := yy - 1980;
  KVIS := NDEL div 4;
  NDAY := NDEL * 365 + KVIS;
  KCEL := KVIS * 4;
  if KCEL = NDEL then
    begin
      if mm > 2 then
        Inc(NDAY);
    end
  else
    Inc(NDAY);

  case mm of
    1:
      ;
    2:
      NDAY := NDAY + 31;
    3:
      NDAY := NDAY + 59;
    4:
      NDAY := NDAY + 90;
    5:
      NDAY := NDAY + 120;
    6:
      NDAY := NDAY + 151;
    7:
      NDAY := NDAY + 181;
    8:
      NDAY := NDAY + 212;
    9:
      NDAY := NDAY + 242;
    10:
      NDAY := NDAY + 273;
    11:
      NDAY := NDAY + 304;
    12:
      NDAY := NDAY + 334;
  end;
  NDAY := NDAY + dd;

  DAY := NDAY - (ALong / 6.2831852) + 0.5;

  { ������� �������� }
  ANOM := 0.01720279 * DAY - 0.065674294;
  while (ANOM - 6.2831852) >= 0 do
    ANOM := ANOM - 6.2831852;

  E := ANOM;
  _ok := False;
  DEL := 0;
  i := 0;
  while True do
    begin
      if _ok then
        begin
          DE := DEL / (1 - 0.016718 * cos(E));
          E := E - DE;
        end;
      DEL := E - 0.016718 * sin(E) - ANOM;
      _ok := True;
      if ((ABS(DEL) - 1E-6 > 0) or (DEL < 9E-23)) then
        break;

      Inc(i);
      if i > 10000 then
        break;
    end;
  { ������� �������� }
  ANOMI := 2 * arctan(1.01686 * tan(E / 2));
  DSOL := ANOMI + 4.9322375;

  { ��������� ������, ��� }
  SKL := arcsin(0.39781868 * sin(DSOL));
  TV := 0;
  TZ := 0;
  TKT := 0;
  TNT := 0;
  X := sin(ALat) * sin(SKL);
  Y := cos(ALat) * cos(SKL);

  { �������� �������� ����� }
  CT1 := (-0.014834754 - X) / Y;
  CT2 := (-0.10452845 - X) / Y;
  DG := 0;
  UV := 0;

  if CT1 + 1 >= 0 then
    begin
      if CT1 - 1 <= 0 then
        begin
          T1G := arccos(CT1) * 57.29578;
          DG := ALong * 57.29578;
          { ����� ������/������� ���, ��� }
          TVICH := 12 - (T1G + DG) / 15;
          TZICH := 12 + (T1G - DG) / 15;
          { ��������� �������, ��� }
          UV := 0.12833333 * sin(DSOL + 4.5204026) + 0.165 * sin(2 * DSOL);
          { ����� ����/��� �������, ��� }
          TVSCH := TVICH - UV;
          TZSCH := TZICH - UV;

          if TVSCH < 0 then
            TVSCH := TVSCH + 24;
          if TVSCH > 24 then
            TVSCH := TVSCH - 24;
          if TZSCH < 0 then
            TZSCH := TZSCH + 24;
          if TZSCH > 24 then
            TZSCH := TZSCH - 24;
          NTV := Int(TVSCH);
          TV := (TVSCH - Int(NTV)) * 60;
          NTZ := Int(TZSCH);
          TZ := (TZSCH - Int(NTZ)) * 60;
        end
      else
        begin
          NTV := 70;
          NTZ := 90;
        end;
      if CT2 + 1 >= 0 then
        begin
          if CT2 - 1 <= 0 then
            begin
              T2G := arccos(CT2) * 57.29578;
              { ����� �����/��� �������, ��� }
              TKTICH := 12 - (T2G + DG) / 15;
              TNTICH := 12 + (T2G - DG) / 15;
              TKTSCH := TKTICH - UV;
              TNTSCH := TNTICH - UV;

              if TKTSCH < 0 then
                TKTSCH := TKTSCH + 24;
              if TKTSCH > 24 then
                TKTSCH := TKTSCH - 24;
              if TNTSCH < 0 then
                TNTSCH := TNTSCH + 24;
              if TNTSCH > 24 then
                TNTSCH := TNTSCH - 24;
              NTKT := Int(TKTSCH);
              TKT := (TKTSCH - Int(NTKT)) * 60;
              NTNT := Int(TNTSCH);
              TNT := (TNTSCH - Int(NTNT)) * 60;
            end
          else
            begin
              NTKT := 60;
              NTNT := 60;
            end
        end
      else
        begin
          NTKT := 60;
          NTNT := 60;
        end;
    end
  else
    begin;
      NTZ := 80;
      NTV := 90;
      NTKT := 90;
      NTNT := 90;
    end;
  if TV = 60 then
    TV := 0;
  if TZ = 60 then
    TZ := 0;
  if TKT = 60 then
    TKT := 0;
  if TNT = 60 then
    TNT := 0;

  Dawn := DateOf(ADta) + NTKT / 24 + TKT / 1440;

  Sunrise := DateOf(ADta) + NTV / 24 + TV / 1440;

  Sunset := DateOf(ADta) + NTZ / 24 + TZ / 1440;

  Dark := DateOf(ADta) + NTNT / 24 + TNT / 1440;

  if ALong > 0 then
    if (Dark - Dawn) <> 0 then
      begin
        if Dawn > Dark then
          Dawn := Dawn - 1;
        if Sunrise > Dark then
          Sunrise := Sunrise - 1;
        if Sunset > Dark then
          Sunset := Sunset - 1;
      end
    else
      if (Sunrise > Sunset) and (Sunrise <> Dawn) then
        Sunrise := Sunrise - 1;

  if Sunset > Dark then
    Sun := -1
  else
    if Sunrise = Dawn then
      Sun := 1
    else
      Sun := 0;
  if (Dawn = Dark) and (mm in [1, 2, 3, 10, 11, 12]) then
    Light := -1
  else
    if (Dawn = Dark) and (mm in [4 .. 9]) then
      Light := 1
    else
      Light := 0;
end;

{$IFNDEF DXUP}

type
  TThreadList<T> = class
  private
    FList: TList<T>;
    FLock: TObject;
    FDuplicates: TDuplicates;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Add(const Item: T);
    procedure Clear;
    function LockList: TList<T>;
    procedure Remove(const Item: T); inline;
    procedure RemoveItem(const Item: T);
    procedure UnlockList; inline;
    property Duplicates: TDuplicates read FDuplicates write FDuplicates;
  end;
  {$ENDIF}

type
  TThreadList = class(TThreadList<TThread>)
  public
    procedure OnThreadTerminated(Sender: TObject);
  end;

var
  FThreadList: TThreadList;

procedure ForceQueue(proc: TThreadProcedure);
{$IFNDEF DX2Up}
var
  Thread: TThread;
{$ENDIF}
begin
  {$IFNDEF DX2Up}
  if not Assigned(FThreadList) then
    FThreadList := TThreadList.Create;

  Thread := TThread.CreateAnonymousThread(
      procedure
    begin
      TThread.Queue(nil, proc);
    end);

  Thread.OnTerminate:=FThreadList.OnThreadTerminated;
  FThreadList.Add(Thread);
  Thread.Start;

  {$ELSE}
  TThread.ForceQueue(nil, proc);
  {$ENDIF}
end;

{$IFNDEF DXUP}
// XE2 �� �������� ����� TThreadList

{ TThreadList<T> }

procedure TThreadList<T>.Add(const Item: T);
begin
  LockList;
  try
    if (Duplicates = dupAccept) or (FList.IndexOf(Item) = -1) then
      FList.Add(Item)
    else
      if Duplicates = dupError then
        raise EListError.CreateFmt('Item duplicated', []);
  finally
    UnlockList;
  end;
end;

procedure TThreadList<T>.Clear;
begin
  LockList;
  try
    FList.Clear;
  finally
    UnlockList;
  end;
end;

constructor TThreadList<T>.Create;
begin
  inherited Create;
  FLock := TObject.Create;
  FList := TList<T>.Create;
  FDuplicates := dupIgnore;
end;

destructor TThreadList<T>.Destroy;
begin
  LockList; // Make sure nobody else is inside the list.
  try
    FList.Free;
    inherited Destroy;
  finally
    UnlockList;
    FLock.Free;
  end;
end;

function TThreadList<T>.LockList: TList<T>;
begin
  TMonitor.Enter(FLock);
  Result := FList;
end;

procedure TThreadList<T>.Remove(const Item: T);
begin
  RemoveItem(Item);
end;

procedure TThreadList<T>.RemoveItem(const Item: T);
begin
  LockList;
  try
    FList.Remove(Item);
  finally
    UnlockList;
  end;
end;

procedure TThreadList<T>.UnlockList;
begin
  TMonitor.exit(FLock);
end;
{$ENDIF}
{ TThreadList }

procedure TThreadList.OnThreadTerminated(Sender: TObject);
var
  List: TList<TThread>;
begin
  List := LockList;
  try
    List.Remove(TThread(Sender));
  finally
    UnlockList;
  end;
end;

initialization

NormalizedFormatSettings := XNormalizedFormatSettings;
FThreadList := nil;

finalization

FThreadList.Free;
FThreadList := nil;

end.
