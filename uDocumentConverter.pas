unit uDocumentConverter;
/// <author>kami</author>
/// <remarks>
///  ������ ��� �������������� RTF � PDF
///  Excel (xls/xlsx) � PDF
///  Word (doc/docx) � PDF
///  �������� ��������� ������� ��� �������� �������������.
///  �������������� Excel � Word � PDF ������� ������� MS Office!!!!!
/// </remarks>
///
{$DEFINE CPDFInExeFolder}

interface

uses
  System.SysUtils,
  System.Classes,
  System.Types,
  System.Generics.Collections;

type
  TPDFConverter = class(TObject)
  strict private
    class var FRTFDefaultFontName: string;
    class var FRTFDefaultFontSize: integer;
    class var FRTFDefaultMargins: TRect;
    class var FRTFHWND: THandle;

    class function CheckValidMargins: Boolean;

    class function ExecFileAndWait(const FileName, Params: string): Boolean;
    {$IFDEF UNION_PDF_Enable}
    class function ExecCPDF(const Params: string): Boolean;
    {$ENDIF}
  public
    class procedure Fin;
    /// <summary>
    /// ��� ������ �� ��������� ��� RTF-����������. �������������� �� ���������
    /// ���������, ������� �� ������ �������������� ��� ��� ������� ����������
    /// �������� ��������������� ������������ ��� plain-text ����������, �������
    /// ����� ��������� � PDF
    /// </summary>
    class property RTFDefaultFontName: string read FRTFDefaultFontName write FRTFDefaultFontName;
    /// <summary>
    /// ������ ������ �� ��������� ��� RTF-����������. �������������� �� ���������
    /// ���������, ������� �� ������ �������������� ��� ��� ������� ����������
    /// �������� ��������������� ������������ ��� plain-text ����������, �������
    /// ����� ��������� � PDF
    /// </summary>
    class property RTFDefaultFontSize: integer read FRTFDefaultFontSize write FRTFDefaultFontSize;
    /// <summary>
    /// ������� (����) ���������, ������������ ��� ����������� RTF � PDF. � �����������!
    /// �� ��������� ������� �� �������� RTF-��������, ������������ � ��������� ������� � �����
    /// ���� ��� ������� = 0 ��� ����� ������ ���� - �� �����������.
    /// </summary>
    class property RTFDefaultMargins: TRect read FRTFDefaultMargins write FRTFDefaultMargins;
    /// <summary>
    /// �������� ������� ��� �������������� RTF � PDF
    /// � ������������ PDFStream ��������� ������� (Position) ��������� � ����� ������!
    /// </summary>
    class procedure ConvertRTF(const rtfText: string; PDFStream: TStream); overload;
    class procedure ConvertRTF(const rtfText, PDFFileName: string); overload;
    class procedure ConvertRTFFile(const rtfFileName, PDFFileName: string);

    /// <summary>
    /// �������� ����� �������������� Excel � PDF.
    /// SheetNumForExport - ����� ���� ����� ������������� � PDF (������ ���� �������������� ��������,
    /// ��� ����������� Excel
    /// WrapDIRExcelPrintArea - ���� ��������� � True, �� ��������� ����� �������, ���
    /// �������������� ������� ��� DIR-������, ��������� �������� ��������.
    /// ����� ��������� ���������������� � ����������� ������ ������.
    /// ������� � PDF ��������� ������� ������!!! ���, ��� �� ���� �������������� �� �����!!!
    /// ����� ��������� ���������� EConvertError, ���� �� ������� ��������� Excel ��� �� �����
    ///  ��� ������ �������� ������
    /// </summary>
    class procedure ConvertExcel(const ExcelFileName, PDFFileName: string; SheetNumForExport: integer;
      WrapDIRExcelPrintArea: Boolean = False); overload;
    /// <summary>
    /// ��������� �������� �������������, � �������� ����� ��������� ��� ���������/����������
    /// xls ��� pdf � BLOB-���� � ���� ������.
    /// �� ���� ��������� �������� ��������������, ���
    /// ��������� ������� �������� ������ (���� �� ����) �� ����� ��������,
    /// � ��������� ������� ��������� ������ (���� �� ����) ��������������� � ����� ������
    ///  ��. �������� ���������� �������� ������� ConvertExcel
    /// </summary>
    class procedure ConvertExcel(ExcelStream: TStream; const PDFFileName: string; SheetNumForExport: integer;
      WrapDIRExcelPrintArea: Boolean = False); overload;
    class procedure ConvertExcel(const ExcelFileName: string; PDFStream: TStream; SheetNumForExport: integer;
      WrapDIRExcelPrintArea: Boolean = False); overload;
    class procedure ConvertExcel(ExcelStream, PDFStream: TStream; SheetNumForExport: integer;
      WrapDIRExcelPrintArea: Boolean = False); overload;

    /// <summary>
    /// �������� ����� �������������� ��������� Word � PDF
    /// ����� ��������� ���������� EConvertError, ���� �� ������� ��������� Word ��� �� �����
    ///  ��� ������ �������� ������
    /// </summary>
    class procedure ConvertWord(const WordFileName, PDFFileName: string); overload;
    class procedure ConvertWord(WordStream: TStream; const PDFFileName: string); overload;
    class procedure ConvertWord(const WordFileName: string; PDFStream: TStream); overload;
    class procedure ConvertWord(WordStream, PDFStream: TStream); overload;

    /// <summary>
    /// ����������� ������ ����� �� ����� �������������� �������� � PDF.
    ///  ����� ��������� ����������:
    ///    EConvertError, ���� �� ������� ��������� Excel ��� Word (��� �� �����
    ///      �� ������ �������� ������)
    ///    ������� ���������� ������ � �������� ��������, ���� �������� ����� ����������/�����������
    /// </summary>
    class function TryConvertAnyFileToPDF(const SourceFileName: string; const DestFileName: string): Boolean;

    {$IFDEF UNION_PDF_Enable}
    class function CheckPDFFile(const APDFFile: string): Boolean;
    /// <summary>
    /// ����������� ���������� PDF � ����.
    ///  ����� ��������� ���������� EResNotFound, ���� ����������� ������ pdfunion.RES �� ������ ��� ���������.
    ///  (������ ��� ����������� {$DEFINE CPDFInExeFolder})
    ///  ������� ���������� ������ � �������� ��������, ���� �������� ����� ����������/�����������
    /// </summary>
    class function UnionPDF(SourcePDFFiles: TStrings; const DestPDFFile: string): Boolean; overload;
    class function UnionPDF(const SourcePDFFile1, SourcePDFFile2: string; const DestPDFFile: string): Boolean; overload;
    class function UnionPDF(SourcePDFStreams: TObjectList<TStream>; DestPDF: TStream): Boolean; overload;
    class function UnionPDF(SourcePDF1, SourcePDF2, DestPDF: TStream): Boolean; overload;

    /// <summary>
    /// ����������� ������ ������ �� ����� �������������� �������� � PDF.
    ///  ����� ��������� ���������� EResNotFound, ���� ����������� ������
    /// pdfunion.RES �� ������ ��� ���������.
    ///  (������ ��� ����������� {$DEFINE CPDFInExeFolder})
    ///  ����� ��������� ����������:
    ///    EConvertError, ���� �� ������� ��������� Excel ��� Word
    ///    EResNotFound, ���� ����������� ������ pdfunion.RES �� ������ ��� ���������.
    ///    ������� ���������� ������ � �������� ��������, ���� �������� ����� ����������/�����������
    /// </summary>
    class function UnionAndCreatePDF(SourceDifferentFiles: TStrings; const DestPDFFile: string;
      StopIfAnySourceNotConverted: Boolean = True): Boolean;
    {$ENDIF}
  end;

implementation

uses
  System.Variants,
  Winapi.Windows,
  System.IOUtils,
  System.Win.ComObj,
  System.UITypes,
  Vcl.Graphics,
  SynGdiPlus,
  JvRichEdit,
  SynCommons,
  mORMotReport,
  uThreadSafeLogger;

{$IFDEF UNION_PDF_Enable}
{$IFNDEF CPDFInExeFolder}
{$R pdfunion.RES}
{$ENDIF}
{$ENDIF}

const
  wdExportFormatPDF = 17;
  wdExportFormatXPS = 18;

  wdExportOptimizeForOnScreen = 1;
  wdExportOptimizeForPrint = 0;

  wdExportAllDocument = 0;
  wdExportCurrentPage = 2;
  wdExportFromTo = 3;
  wdExportSelection = 1;

  wdExportDocumentContent = 0;
  wdExportDocumentWithMarkup = 7;

  wdExportCreateHeadingBookmarks = 1;
  wdExportCreateNoBookmarks = 0;
  wdExportCreateWordBookmarks = 2;

  wdDoNotSaveChanges = 0;
  wdPromptToSaveChange = -2;
  wdSaveChanges = -1;

const
  xlTypePDF = 0;
  xlQualityStandard = 0;

function EnquoteString(const Source: string): string;
begin
  Result := System.SysUtils.Trim(Source);
  if (Result <> '') then
  begin
    if Pos('"', Result) = 0 then
      Result := '"' + Result + '"';
  end;
end;

function EnquoteParams(Source: TStrings): string;
var
  s: string;
  i: integer;
begin
  Result := '';
  for i := 0 to Source.Count - 1 do
  begin
    s := EnquoteString(Source[i]);
    if (s <> '') then
      Result := Result + ' ' + s;
  end;
  Result := System.SysUtils.Trim(Result);
end;

{ TPDFConverter }

class procedure TPDFConverter.ConvertExcel(const ExcelFileName, PDFFileName: string; SheetNumForExport: integer;
  WrapDIRExcelPrintArea: Boolean);
  function TryCreateExcel: OleVariant;
  begin
    try
      Result := CreateOleObject('Excel.Application');
    except
      on e: Exception do
      begin
        Result := Unassigned;
      end;
    end;
  end;

var
  App, Book, Sheet: OleVariant;
  sPrintRange: string;
  sTmpPDFFileName: string;
begin
  App := TryCreateExcel;
  if VarIsEmptyOrNull(App) then
    raise EConvertError.Create('Can`t launch Excel app');
  try
    try
      Book := App.Workbooks.Open(FileName := ExcelFileName);
      try
        Sheet := Book.Worksheets[SheetNumForExport];
        if WrapDIRExcelPrintArea then
        begin
          Sheet.Range['A6:N3000'].AutoFilter(Field := 2, Criteria1 := '<>');
          sPrintRange := Sheet.Cells[6, 1].CurrentRegion.Address;
          sPrintRange := Copy(sPrintRange, 1, Pos(':', sPrintRange)) + '$N$' +
            Copy(sPrintRange, Pos(':', sPrintRange) + 4, Length(sPrintRange));
          Sheet.PageSetup.PrintArea := sPrintRange;
        end;

        sTmpPDFFileName := PDFFileName + '.pdf';

        Sheet.ExportAsFixedFormat(type := xlTypePDF, FileName := sTmpPDFFileName, Quality := xlQualityStandard,
          IncludeDocProperties := True, IgnorePrintAreas := False, OpenAfterPublish := False);

        TFile.Copy(sTmpPDFFileName, PDFFileName, True);
        System.SysUtils.DeleteFile(PChar(sTmpPDFFileName));
      finally
        Book.Close(SaveChanges := False);
        Book := Unassigned;
      end;
    except
      on e: Exception do
        raise EConvertError.Create('Error in Excel working');
    end;
  finally
    App.Quit;
    App := Unassigned;
  end;
end;

class procedure TPDFConverter.ConvertExcel(ExcelStream: TStream; const PDFFileName: string; SheetNumForExport: integer;
  WrapDIRExcelPrintArea: Boolean);
var
  ExcelFileName: string;
  FS: TFileStream;
begin
  ExcelFileName := TPath.GetTempFileName;
  try
    FS := TFileStream.Create(ExcelFileName, fmCreate);
    try
      FS.CopyFrom(ExcelStream, 0);
    finally
      FS.Free;
    end;

    ConvertExcel(ExcelFileName, PDFFileName, SheetNumForExport, WrapDIRExcelPrintArea);
  finally
    System.SysUtils.DeleteFile(ExcelFileName);
  end;
end;

class procedure TPDFConverter.ConvertExcel(const ExcelFileName: string; PDFStream: TStream; SheetNumForExport: integer;
  WrapDIRExcelPrintArea: Boolean);
var
  tmpFileName: string;
  PDFFileName: string;
  FS: TFileStream;
begin
  tmpFileName := TPath.GetTempFileName;
  try
    PDFFileName := ChangeFileExt(tmpFileName, '.pdf');
    try
      PDFStream.Size := 0;
      ConvertExcel(ExcelFileName, PDFFileName, SheetNumForExport, WrapDIRExcelPrintArea);

      FS := TFileStream.Create(PDFFileName, fmOpenRead or fmShareDenyNone);
      try
        PDFStream.CopyFrom(FS, 0);
      finally
        FS.Free;
      end;
    finally
      System.SysUtils.DeleteFile(PDFFileName);
    end;
  finally
    System.SysUtils.DeleteFile(tmpFileName);
  end;
end;

{$IFDEF UNION_PDF_Enable}

class function TPDFConverter.CheckPDFFile(const APDFFile: string): Boolean;
var
  Params: string;
begin
  Params := Format('-info %s', [EnquoteString(APDFFile)]);
  Result := ExecCPDF(Params);
end;
{$ENDIF}

class function TPDFConverter.CheckValidMargins: Boolean;
begin
  // ��� �� ����� ������� ������ ����
  Result := (FRTFDefaultMargins.Top >= 0) and (FRTFDefaultMargins.Left >= 0) and (FRTFDefaultMargins.Bottom >= 0) and
    (FRTFDefaultMargins.Right >= 0);
  // ��� ������� ���� ������� ������ ����.
  // �����, ����� ���������� ����������� ���������� �������, ��������� ������
  // ����������.
  if Result then
    Result := (FRTFDefaultMargins.Top + FRTFDefaultMargins.Left + FRTFDefaultMargins.Bottom +
        FRTFDefaultMargins.Right) > 0;
end;

class procedure TPDFConverter.ConvertExcel(ExcelStream, PDFStream: TStream; SheetNumForExport: integer;
  WrapDIRExcelPrintArea: Boolean);
var
  ExcelFileName: string;
  FS: TFileStream;
begin
  ExcelFileName := TPath.GetTempFileName;
  try
    FS := TFileStream.Create(ExcelFileName, fmCreate);
    try
      FS.CopyFrom(ExcelStream, 0);
    finally
      FS.Free;
    end;

    ConvertExcel(ExcelFileName, PDFStream, SheetNumForExport, WrapDIRExcelPrintArea);
  finally
    System.SysUtils.DeleteFile(ExcelFileName);
  end;
end;

function CreateWndForActiveX: THandle;
const
  WBWNDClassName = 'MESSAGE_ONLY_RICHEDITWND';
var
  WndClass: TWndClassEx;
  ClassRegistered: Boolean;
begin
  ClassRegistered := GetClassInfoEx(HInstance, WBWNDClassName, WndClass);
  if not ClassRegistered then
  begin
    FillChar(WndClass, SizeOf(TWndClassEx), 0);
    WndClass.cbSize := SizeOf(TWndClassEx);
    WndClass.HInstance := HInstance;
    WndClass.lpszClassName := WBWNDClassName;
    WndClass.lpfnWndProc := @DefWindowProc;
    RegisterClassEx(WndClass);
  end;
  Result := CreateWindowEx(0, WBWNDClassName, nil, 0, 0, 0, 0, 0, DWORD(HWND_MESSAGE), 0, 0, nil);
  if (Result = 0) then
    RaiseLastOSError;
end;

class procedure TPDFConverter.ConvertRTF(const rtfText: string; PDFStream: TStream);
var
  RichEdit: TJvRichEdit;
  Pages: TGDIPages;

  RTFStream: TStringStream;
begin
  if FRTFHWND = 0 then
    FRTFHWND := CreateWndForActiveX;
  RichEdit := TJvRichEdit.Create(nil);
  try
    RichEdit.ParentWindow := FRTFHWND;
    if RTFDefaultFontName <> '' then
      RichEdit.Font.Name := RTFDefaultFontName;
    if RTFDefaultFontSize <> 0 then
      RichEdit.Font.Size := RTFDefaultFontSize;
    RTFStream := TStringStream.Create(rtfText);
    try
      RTFStream.Seek(0, soBeginning);
      RichEdit.StreamFormat := sfDefault;
      RichEdit.Lines.LoadFromStream(RTFStream);
    finally
      RTFStream.Free;
    end;

    Pages := TGDIPages.Create(nil);
    try
      Pages.ExportPDFEmbeddedTTF := True;
      Pages.ExportPDFA1 := True;
      Pages.BeginDoc;
      if RTFDefaultFontName <> '' then
        Pages.Font.Name := RTFDefaultFontName;
      if RTFDefaultFontSize <> 0 then
        Pages.Font.Size := RTFDefaultFontSize;
      if CheckValidMargins then
        Pages.PageMargins := FRTFDefaultMargins;
      Pages.AppendRichEdit(RichEdit.Handle);
      Pages.EndDoc;
      Pages.ExportPDFStream(PDFStream);
    finally
      Pages.Free;
    end;
  finally
    RichEdit.Free;
  end;
end;

class procedure TPDFConverter.ConvertRTF(const rtfText, PDFFileName: string);
var
  FS: TFileStream;
begin
  FS := TFileStream.Create(PDFFileName, fmCreate);
  try
    ConvertRTF(rtfText, FS);
  finally
    FS.Free;
  end;
end;

class procedure TPDFConverter.ConvertRTFFile(const rtfFileName, PDFFileName: string);
var
  SL: TStringList;
begin
  SL := TStringList.Create;
  try
    SL.LoadFromFile(rtfFileName);
    ConvertRTF(SL.Text, PDFFileName);
  finally
    SL.Free;
  end;
end;

class procedure TPDFConverter.ConvertWord(WordStream, PDFStream: TStream);
var
  WordFileName: string;
  FS: TFileStream;
begin
  WordFileName := TPath.GetTempFileName;
  try
    FS := TFileStream.Create(WordFileName, fmCreate);
    try
      FS.CopyFrom(WordStream, 0);
    finally
      FS.Free;
    end;

    ConvertWord(WordFileName, PDFStream);
  finally
    System.SysUtils.DeleteFile(WordFileName);
  end;
end;

{$IFDEF UNION_PDF_Enable}

class function TPDFConverter.ExecCPDF(const Params: string): Boolean;
  procedure ExtractCpdf(const FileName: string);
  var
    res: TResourceStream;
  begin
    res := TResourceStream.Create(HInstance, 'COHERENT_PDFUNION_EXE', RT_RCDATA);
    try
      res.SaveToFile(FileName);
    finally
      res.Free;
    end;
  end;

var
  tmpExe: string;
begin
  //cmd: cpdf one.pdf two.pdf three.pdf -o merged.pdf
  {$IFDEF CPDFInExeFolder}
  tmpExe := IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0))) + 'cpdf.exe';
  {$ELSE}
  tmpExe := TPath.GetTempFileName;
  {$ENDIF}
  try
    {$IFNDEF CPDFInExeFolder}
    ExtractCpdf(tmpExe);
    {$ENDIF}
    Result := ExecFileAndWait(tmpExe, Params);
  finally
    {$IFNDEF CPDFInExeFolder}
    System.SysUtils.DeleteFile(tmpExe);
    {$ENDIF}
  end;
end;
{$ENDIF}

class function TPDFConverter.ExecFileAndWait(const FileName, Params: string): Boolean;
var
  si: TStartupInfo;
  pi: TProcessInformation;
  R: Cardinal;
begin
  FillChar(si, SizeOf(TStartupInfo), 0);
  FillChar(pi, SizeOf(TProcessInformation), 0);

  si.cb := SizeOf(TStartupInfo);
  Result := CreateProcess(PChar(FileName), PChar(EnquoteString(FileName) + ' ' + Params), nil, nil, False,
    CREATE_NEW_CONSOLE, nil, nil, si, pi);
  if Result then
  begin
    Result := WaitForSingleObject(pi.hProcess, 10000) = WAIT_OBJECT_0;
    if Result then
      Result := GetExitCodeProcess(pi.hProcess, R);
    if Result then
      Result := R = 0;
    CloseHandle(pi.hThread);
    CloseHandle(pi.hProcess);
  end;
end;

class procedure TPDFConverter.Fin;
begin
  DeallocateHWnd(FRTFHWND);
end;

class function TPDFConverter.TryConvertAnyFileToPDF(const SourceFileName, DestFileName: string): Boolean;
type
  TConvertedFileType = (cftUnknown, cftPDF, cftTXT, cftRTF, //
    cftBmp, cftDIB, cftJpg, cftJpeg, cftjpe, cftjfif, cftPng, cftTiff, cftTif, //
    cftWord, cftWordNew, cftExcel, cftExcelNew);

const
  FirstGraphic = cftBmp;
  LastGraphic = cftTif;

  function FileNameToFileType(const FileName: string): TConvertedFileType;
  const
    TypesExt: array [TConvertedFileType] of string = ('', '.pdf', '.txt', '.rtf', //
      '.bmp', '.dib', '.jpg', '.jpeg', '.jpe', '.jfif', '.png', '.tiff', '.tif', //
      '.doc', '.docx', '.xls', '.xlsx');
  var
    s: string;
    i: TConvertedFileType;
  begin
    Result := cftUnknown;
    s := TPath.GetExtension(FileName);
    for i := low(TConvertedFileType) to high(TConvertedFileType) do
      if SameText(TypesExt[i], s) then
      begin
        Result := i;
        Break;
      end;
  end;

var
  FileType: TConvertedFileType;
  bmp: TBitmap;
  Pages: TGDIPages;
  PageHeight: integer;
  PageWidth: integer;
  SourceRatio, TargetRatio: Double;
begin
  Result := False;
  FileType := FileNameToFileType(SourceFileName);
  case FileType of
    cftUnknown:
      Result := False;
    cftPDF:
      begin
        TFile.Copy(SourceFileName, DestFileName, True);
        Result := True;
      end;
    cftTXT, cftRTF:
      begin
        ConvertRTFFile(SourceFileName, DestFileName);
        Result := True;
      end;
    FirstGraphic .. LastGraphic:
      begin
        bmp := SynGdiPlus.LoadFrom(SourceFileName);
        try
          Pages := TGDIPages.Create(nil);
          try
            Pages.ExportPDFEmbeddedTTF := True;
            Pages.ExportPDFA1 := True;
            if bmp.Height > bmp.Width then
              Pages.Orientation := TPrinterOrientation.poPortrait
            else
              Pages.Orientation := TPrinterOrientation.poLandscape;
            Pages.BeginDoc;
            try
              if RTFDefaultFontName <> '' then
                Pages.Font.Name := RTFDefaultFontName;
              if RTFDefaultFontSize <> 0 then
                Pages.Font.Size := RTFDefaultFontSize;
              if CheckValidMargins then
                Pages.PageMargins := FRTFDefaultMargins;

              PageHeight := (Pages.PaperSize.cy - Pages.PageMargins.Top - Pages.PageMargins.Bottom) - 2;
              PageWidth := (Pages.PaperSize.cx - Pages.PageMargins.Left - Pages.PageMargins.Right) - 2;

              SourceRatio := bmp.Width / bmp.Height;
              TargetRatio := PageWidth / PageHeight;

              if SourceRatio > TargetRatio then
                Pages.DrawBMP(bmp, 0, PageWidth)
              else
                Pages.DrawBMP(bmp, 0, Round(PageHeight * SourceRatio));
            finally
              Pages.EndDoc;
            end;
            Pages.ExportPDF(DestFileName, False, False);
          finally
            Pages.Free;
          end;
        finally
          bmp.Free;
        end;
        Result := True;
      end;
    cftWord, cftWordNew:
      begin
        ConvertWord(SourceFileName, DestFileName);
        Result := True;
      end;
    cftExcel, cftExcelNew:
      begin
        ConvertExcel(SourceFileName, DestFileName, 1);
        Result := True;
      end;
  end;
end;

{$IFDEF UNION_PDF_Enable}

class function TPDFConverter.UnionPDF(const SourcePDFFile1, SourcePDFFile2, DestPDFFile: string): Boolean;
var
  SL: TStringList;
begin
  SL := TStringList.Create;
  try
    SL.Add(SourcePDFFile1);
    SL.Add(SourcePDFFile2);
    Result := UnionPDF(SL, DestPDFFile);
  finally
    SL.Free;
  end;
end;

class function TPDFConverter.UnionPDF(SourcePDFStreams: TObjectList<TStream>; DestPDF: TStream): Boolean;
  procedure SaveStreamToFile(Stream: TStream; const FileName: string);
  var
    tmpFile: TFileStream;
  begin
    tmpFile := TFileStream.Create(FileName, fmCreate);
    try
      tmpFile.CopyFrom(Stream, 0);
    finally
      tmpFile.Free;
    end;
  end;

  procedure LoadStreamFromFile(Stream: TStream; const FileName: string);
  var
    FS: TFileStream;
  begin
    FS := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
    try
      Stream.Size := 0;
      Stream.CopyFrom(FS, 0);
    finally
      FS.Free;
    end;
  end;

var
  SL: TStringList;
  sOutputPDF: string;
  i: integer;
begin
  try
    SL := TStringList.Create;
    try
      for i := 0 to SourcePDFStreams.Count - 1 do
        SL.Add(TPath.GetTempFileName);
      try
        sOutputPDF := TPath.GetTempFileName;
        try
          for i := 0 to SourcePDFStreams.Count - 1 do
            SaveStreamToFile(SourcePDFStreams[i], SL[i]);

          Result := UnionPDF(SL, sOutputPDF);
          if Result then
            LoadStreamFromFile(DestPDF, sOutputPDF);
        finally
          System.SysUtils.DeleteFile(sOutputPDF);
        end;
      finally
        for i := 0 to SourcePDFStreams.Count - 1 do
          System.SysUtils.DeleteFile(SL[i]);
      end;
    finally
      SL.Free;
    end;
  except
    on e: EResNotFound do
      raise;
    on e: Exception do
    begin
      Result := False;
      LogFileX.LogException(e);
    end;
  end;
end;

class function TPDFConverter.UnionPDF(SourcePDFFiles: TStrings; const DestPDFFile: string): Boolean;
var
  Params: string;
begin
  //cmd: cpdf one.pdf two.pdf three.pdf -o merged.pdf
  Params := Format('%s -o %s', [EnquoteParams(SourcePDFFiles), EnquoteString(DestPDFFile)]);
  Result := ExecCPDF(Params);
end;

class function TPDFConverter.UnionAndCreatePDF(SourceDifferentFiles: TStrings; const DestPDFFile: string;
  StopIfAnySourceNotConverted: Boolean): Boolean;
var
  SlPDFFiles: TStringList;
  s: string;
  i: integer;
begin
  Result := False;

  SlPDFFiles := TStringList.Create;
  try
    for i := 0 to SourceDifferentFiles.Count - 1 do
    begin
      s := TPath.GetTempFileName;
      try
        Result := TryConvertAnyFileToPDF(SourceDifferentFiles[i], s);
        if Result then
        begin
          SlPDFFiles.Add(s);
          s := '';
        end
        else
          if StopIfAnySourceNotConverted then
            Break;
      finally
        if s <> '' then
          System.SysUtils.DeleteFile(s);
      end;
    end;

    try
      if Result or (not StopIfAnySourceNotConverted) then
        Result := UnionPDF(SlPDFFiles, DestPDFFile);
    finally
      for i := 0 to SlPDFFiles.Count - 1 do
        System.SysUtils.DeleteFile(SlPDFFiles[i]);
    end;
  finally
    SlPDFFiles.Free;
  end;
end;

class function TPDFConverter.UnionPDF(SourcePDF1, SourcePDF2, DestPDF: TStream): Boolean;
var
  Sources: TObjectList<TStream>;
begin
  Sources := TObjectList<TStream>.Create(False);
  try
    Sources.Add(SourcePDF1);
    Sources.Add(SourcePDF2);
    Result := UnionPDF(Sources, DestPDF);
  finally
    Sources.Free;
  end;
end;

{$ENDIF}

class procedure TPDFConverter.ConvertWord(const WordFileName: string; PDFStream: TStream);
var
  tmpFileName: string;
  PDFFileName: string;
  FS: TFileStream;
begin
  tmpFileName := TPath.GetTempFileName;
  try
    PDFFileName := ChangeFileExt(TPath.GetTempFileName, '.pdf');
    try
      PDFStream.Size := 0;
      ConvertWord(WordFileName, PDFFileName);

      FS := TFileStream.Create(PDFFileName, fmOpenRead or fmShareDenyNone);
      try
        PDFStream.CopyFrom(FS, 0);
      finally
        FS.Free;
      end;
    finally
      System.SysUtils.DeleteFile(PDFFileName);
    end;
  finally
    System.SysUtils.DeleteFile(tmpFileName);
  end;
end;

class procedure TPDFConverter.ConvertWord(WordStream: TStream; const PDFFileName: string);
var
  WordFileName: string;
  FS: TFileStream;
begin
  WordFileName := TPath.GetTempFileName;
  try
    FS := TFileStream.Create(WordFileName, fmCreate);
    try
      FS.CopyFrom(WordStream, 0);
    finally
      FS.Free;
    end;

    ConvertWord(WordFileName, PDFFileName);
  finally
    System.SysUtils.DeleteFile(WordFileName);
  end;
end;

class procedure TPDFConverter.ConvertWord(const WordFileName, PDFFileName: string);
  function TryCreateWord: OleVariant;
  begin
    try
      Result := CreateOleObject('Word.Application');
    except
      on e: Exception do
      begin
        Result := Unassigned;
      end;
    end;
  end;

var
  App: OleVariant;
  Doc: OleVariant;
  sTmpPDFFileName: string;
begin
  App := TryCreateWord;
  try
    if VarIsEmptyOrNull(App) then
      raise EConvertError.Create('Can`t launch Word app');

    try
      Doc := App.Documents.Open(FileName := WordFileName);
      try
        sTmpPDFFileName := PDFFileName + '.pdf';

        Doc.ExportAsFixedFormat( //
          OutputFileName := sTmpPDFFileName, //
          ExportFormat := wdExportFormatPDF, //
          OpenAfterExport := False, //
          OptimizeFor := wdExportOptimizeForPrint, //
          Range := wdExportAllDocument, //
          Item := wdExportDocumentContent, //
          IncludeDocProps := True, //
          KeepIRM := True, //
          CreateBookmarks := wdExportCreateNoBookmarks, //
          DocStructureTags := True, //
          BitmapMissingFonts := True, //
          UseISO19005_1 := False);

        TFile.Copy(sTmpPDFFileName, PDFFileName, True);
        System.SysUtils.DeleteFile(sTmpPDFFileName);
      finally
        Doc.Close(SaveChanges := wdDoNotSaveChanges);
        Doc := Unassigned;
      end;
    except
      on e: Exception do
        raise EConvertError.Create('Error in Word working');
    end;
  finally
    App.Quit;
    App := Unassigned;
  end;
end;

initialization

Gdip.RegisterPictures;

finalization

TPDFConverter.Fin;

end.
