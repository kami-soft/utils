unit uAlphaBlendControl;
// if you use version below Tokyo - possibly
// you must to use custom manifest in application
interface

uses
  System.SysUtils,
  System.Classes,
  Winapi.Windows,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms;

type
  TWinControlHelper = class helper for TWinControl
  public
    procedure SetAlpha(AValue: Byte);
  end;

implementation


{ TPanelHelper }

procedure TWinControlHelper.SetAlpha(AValue: Byte);
const
  cUseAlpha: array [Boolean] of Integer = (0, LWA_ALPHA);
  cUseColorKey: array [Boolean] of Integer = (0, LWA_COLORKEY);
var
  AStyle: Integer;
  LastError: Cardinal;
begin
  if not(csDesigning in ComponentState) and (Assigned(SetLayeredWindowAttributes)) and HandleAllocated then
  begin
    if AValue < 255 then
    begin
      AStyle := GetWindowLong(Handle, GWL_EXSTYLE);
      SetLastError(0);
      if (AStyle and WS_EX_LAYERED) = 0 then
        if SetWindowLong(Handle, GWL_EXSTYLE, AStyle or WS_EX_LAYERED) = 0 then
        begin
          LastError := GetLastError;
          if LastError <> 0 then
            RaiseLastOSError;
        end;
      SetLastError(0);
      if not SetLayeredWindowAttributes(Handle, ColorToRGB(0), AValue, cUseAlpha[True] or cUseColorKey[False]) then
        RaiseLastOSError;
    end
    else
    begin
      AStyle := GetWindowLong(Handle, GWL_EXSTYLE);
      if (AStyle and WS_EX_LAYERED) <> 0 then
        if SetWindowLong(Handle, GWL_EXSTYLE, AStyle and (not WS_EX_LAYERED)) = 0 then
        begin
          LastError := GetLastError;
          if LastError <> 0 then
            RaiseLastOSError;
        end;
    end;
  end;
end;

end.
