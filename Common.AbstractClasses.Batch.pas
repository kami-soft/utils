unit Common.AbstractClasses.Batch;

interface

uses
  System.Classes,
  System.Generics.Collections,
  System.RTTI,
  Data.DB,
  Data.Win.ADODB,
  Common.AbstractClasses;

type
  TLoadConditions = record
    From: TDateTime;
    Till: TDateTime;
    COP: integer;
    Data: TValue;
  end;

  TBatchObject = class(TAbstractData)
  public
    class function GetReadSPName: string; virtual; abstract;
    // you can use either TBatchObject.FillParamsForReadProc,
    // TBatchList.FillParamsForReadProc or TLoader.SetAdditionalParams
    class procedure FillParamsForReadProc(Params: TParameters; const Conditions: TLoadConditions); virtual;

    class function GetDescription: string; virtual; abstract;
  end;

  TBatchClass = class of TBatchObject;

  TLoadAllower = reference to procedure(Sender: TObject; AClass: TBatchClass; var Allow: Boolean);
  TLoadFillParamsProc = reference to procedure(Sender: TObject; const Conditions: TLoadConditions; Params: TParameters);

  TBatchList<T: TBatchObject, constructor> = class(TAbstractDataList<T>)
  strict private
    FLoadConditions: TLoadConditions;
    FFillParamsProc: TLoadFillParamsProc;
    procedure ReadBatchItems(BatchClass: TBatchClass; SP: TADOStoredProc);
  strict protected
    // you can use either TBatchObject.FillParamsForReadProc,
    // TBatchList.FillParamsForReadProc or TLoader.SetAdditionalParams
    procedure FillParamsForReadProc(Params: TParameters); virtual;
    function CanLoad(ABatchClass: TBatchClass; LoadAllower: TLoadAllower): Boolean; virtual;
  public
    procedure ReadFromDB(Conn: TADOConnection; const Conditions: TLoadConditions; LoadAllower: TLoadAllower = nil; FillParams: TLoadFillParamsProc = nil);
  end;

  TLoader<T: TBatchObject, constructor> = class(TAbstractRefreshThread)
  strict private
    FConnStr: string;
    FLoadConditions: TLoadConditions;
    FList: TBatchList<T>;
    procedure OnintActionCallback(Sender: TObject; const Action: string);
  strict protected
    procedure Execute; override;
    procedure TerminatedSet; override;
    // you can use either TBatchObject.FillParamsForReadProc,
    // TBatchList.FillParamsForReadProc or TLoader.SetAdditionalParams
    procedure SetAdditionalParams(Sender: TObject; const Conditions: TLoadConditions; Params: TParameters); virtual;
    procedure LoadAllower(Sender: TObject; AClass: TBatchClass; var Allow: Boolean); virtual;
  public
    constructor Create; override;
    destructor Destroy; override;

    procedure Init(const ConnStr: string; const Conditions: TLoadConditions); virtual;

    procedure SetListNil;

    property List: TBatchList<T> read FList;
  end;

implementation

uses
  System.SysUtils,
  System.Math,
  Winapi.ActiveX;

{ TLazyList }

function TBatchList<T>.CanLoad(ABatchClass: TBatchClass; LoadAllower: TLoadAllower): Boolean;
begin
  Result := True;
  if Assigned(LoadAllower) then
    LoadAllower(Self, ABatchClass, Result);
end;

procedure TBatchList<T>.FillParamsForReadProc(Params: TParameters);
begin
  if Assigned(Params.FindParam('@From')) then
    Params.ParamByName('@From').Value := FLoadConditions.From;
  if Assigned(Params.FindParam('@Till')) then
    Params.ParamByName('@Till').Value := FLoadConditions.Till;
  if Assigned(Params.FindParam('@COP')) then
    Params.ParamByName('@COP').Value := FLoadConditions.COP;
end;

procedure TBatchList<T>.ReadFromDB(Conn: TADOConnection; const Conditions: TLoadConditions; LoadAllower: TLoadAllower = nil; FillParams: TLoadFillParamsProc = nil);
var
  SP: TADOStoredProc;
  BatchClass: TBatchClass;
  i: integer;
begin
  Clear;

  FLoadConditions := Conditions;
  FFillParamsProc := FillParams;

  SP := TADOStoredProc.Create(nil);
  try
    SP.Connection := Conn;
    SP.CursorType := ctStatic;
    SP.LockType := ltReadOnly;
    for i := 0 to TAbstractDataClassFactory.Current.Count - 1 do
    begin
      if Terminated then
        Break;
      if TAbstractDataClassFactory.Current[i].InheritsFrom(T) then
      begin
        BatchClass := TBatchClass(TAbstractDataClassFactory.Current[i]);
        if CanLoad(BatchClass, LoadAllower) then
          ReadBatchItems(BatchClass, SP);
      end;
    end;
  finally
    SP.Free;
  end;
end;

procedure TBatchList<T>.ReadBatchItems(BatchClass: TBatchClass; SP: TADOStoredProc);
var
  BatchObject: TBatchObject;
begin
  if not BatchClass.InheritsFrom(T) then
    raise EClassNotFound.Create('Class dependency mismatich');

  SP.ProcedureName := BatchClass.GetReadSPName;
  SP.Parameters.Refresh;

  FillParamsForReadProc(SP.Parameters);
  BatchClass.FillParamsForReadProc(SP.Parameters, FLoadConditions);
  if Assigned(FFillParamsProc) then
    FFillParamsProc(Self, FLoadConditions, SP.Parameters);

  intOnCurrentActionCallback(Self, '���������� ������ ' + BatchClass.GetDescription);

  SP.CommandTimeout := 60 * 5;
  SP.DisableControls;
  try
    SP.Open;
    try
      BeforeAppendFromDS;
      try
        Capacity := Max(Capacity, SP.RecordCount);
        while not SP.Eof do
        begin
          if Terminated then
            Break;
          BatchObject := BatchClass.Create(Self);
          try
            BatchObject.ReadFromDS(SP, SP.Connection);
            Add(BatchObject);
            BatchObject := nil;

            if (Count mod 100) = 0 then
              intOnCurrentActionCallback(Self, '������ ������ ' + BatchClass.GetDescription + ' ' + IntToStr(Count));
          finally
            BatchObject.Free;
          end;
          SP.Next;
        end;
      finally
        AfterAppendFromDS;
      end;
    finally
      SP.Close;
    end;
  finally
    SP.EnableControls;
  end;
end;

{ TLazyLoader<T> }

constructor TLoader<T>.Create;
begin
  inherited;
  FList := TBatchList<T>.Create(nil);
  FList.OnCurrentActionCallback := OnintActionCallback;
end;

destructor TLoader<T>.Destroy;
begin
  FreeAndNil(FList);
  inherited;
end;

procedure TLoader<T>.Execute;
var
  Conn: TADOConnection;
begin
  CoInitialize(nil);
  try
    Conn := TADOConnection.Create(nil);
    try
      Conn.ConnectionString := FConnStr;
      Conn.LoginPrompt := False;
      Conn.Mode := cmRead;
      try
        FList.ReadFromDB(Conn, FLoadConditions, LoadAllower, SetAdditionalParams);

        if FList.Count = 0 then
        begin
          ThreadSafeDoCallback('�� ��������� ���� ��� ������...');
          Sleep(5000);
        end;
      except
        on e: Exception do
        begin
          ReturnValue := -2;
          FExitDescription := '�� ������� ������� ������. ��������� ����������� � ��'#13#10 + e.ToString;
        end;
      end;
    finally
      Conn.Free;
    end;
  finally
    CoUninitialize;
  end;
end;

procedure TLoader<T>.Init(const ConnStr: string; const Conditions: TLoadConditions);
begin
  FConnStr := ConnStr;
  FLoadConditions := Conditions;
end;

procedure TLoader<T>.LoadAllower(Sender: TObject; AClass: TBatchClass; var Allow: Boolean);
begin

end;

procedure TLoader<T>.OnintActionCallback(Sender: TObject; const Action: string);
begin
  ThreadSafeDoCallback(Action);
end;

procedure TLoader<T>.SetAdditionalParams(Sender: TObject; const Conditions: TLoadConditions; Params: TParameters);
begin

end;

procedure TLoader<T>.SetListNil;
begin
  if Assigned(FList) then
    FList.OnCurrentActionCallback := nil;
  FList := nil;
end;

procedure TLoader<T>.TerminatedSet;
begin
  inherited;
  FList.TerminateReading;
end;

{ TBatchObject }

class procedure TBatchObject.FillParamsForReadProc(Params: TParameters; const Conditions: TLoadConditions);
begin

end;

end.
